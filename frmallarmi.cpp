#include "frmallarmi.h"
#include "ui_frmallarmi.h"

frmAllarmi::frmAllarmi(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmAllarmi)
{
    ui->setupUi(this);
    no_man = true;
    timer_flash = new QTimer(this);
    connect(timer_flash,SIGNAL(timeout()), this, SLOT(ToggleImage()));

    allarme = 0;
}

frmAllarmi::~frmAllarmi()
{
    timer_flash->stop();

    delete ui;
}

void frmAllarmi::on_buttonExit_3_clicked()
{
    if(okHome)
    {
        sabbiatrice->flag.home_mot2 = 1;
        okHome = false;
    }
    if(sabbiatrice && no_man)
    {
        sabbiatrice->Allarmi_old = -1;
    }
    close();
}
void frmAllarmi::SetManuale(int manuale)
{
    if(manuale == 1){
        ui->imgAllarme->setPixmap(QPixmap(":/png/47++.png"));
    }
    else if(manuale == 2){
        ui->imgAllarme->setPixmap(QPixmap(":/png/47++.png"));
    }
}
void frmAllarmi::SetImageMan(QString file,QString page)
{
    ui->imgAllarme->setPixmap(file);
    ui->lbHelp->setText("");
    no_man = false;
    ui->lbHelp->setText(page);

}
void frmAllarmi::SetImage()
{
    if( sabbiatrice->Warning ) allarme = sabbiatrice->Warning;
    else                       allarme = sabbiatrice->Allarmi;

    timer_flash->start(1000);

    viewImage();
}

void frmAllarmi::ToggleImage()
{
    (toggle) ? toggle =false: toggle=true;
    viewImage();
}

void frmAllarmi::on_imgAllarme_clicked()
{
    if(okHome)
    {
        sabbiatrice->flag.home_mot2 = 1;
        okHome = false;
    }
    sabbiatrice->Allarmi_old = -1;
    close();
}

void frmAllarmi::viewImage()
{
    switch ( allarme )
    {
    case 1:
        // Emergenza
        ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_69+.png"));
        ui->lbHelp->setText("9.0");
        okHome = true;
        break;
    case 2:
        // Ventilatore
        (toggle) ? ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_71+.png")) : ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_72+.png"));
        ui->lbHelp->setText("9.1");
        break;
    case 3:
        // VIBRATORE
        (toggle) ? ui->imgAllarme->setPixmap(QPixmap(":/png/ALm_83+.png")) : ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_84+.png"));
        ui->lbHelp->setText("9.6");
        break;
    case 5:
        // PORTELLO
        (toggle) ? ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_73+.png")) : ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_74+.png"));
        okHome = true;
        ui->lbHelp->setText("9.2");
        break;
    case 6:
        // VETRO
        (toggle) ? ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_76+.png")) : ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_77+.png"));
        ui->lbHelp->setText("9.3");
        // okHome = true;
        break;
    case 7:
        // MOT1 NASTRO
        (toggle) ? ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_79+.png")) : ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_80+.png"));
        ui->lbHelp->setText("9.4");
        okHome = true;
        break;
    case 8:
        // MOT2 PISTOLA
        (toggle) ? ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_102+.png")) : ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_103+.png"));
        ui->lbHelp->setText("9.5");
        okHome = true;
        break;
    case 10:
        // CONNESSIONE
        (toggle) ? ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_85+.png")) : ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_86+.png"));
        ui->lbHelp->setText("9.7");
        break;
    case 20:
        // MOT_HOME
        (toggle) ? ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_102+.png")) : ui->imgAllarme->setPixmap(QPixmap(":/png/Alm_103+.png"));
        ui->lbHelp->setText("0.0");
        break;
    default:
        //timer_flash->stop();
        break;
    }
}
