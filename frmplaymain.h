#ifndef FRMPLAYMAIN_H
#define FRMPLAYMAIN_H

#include <QDialog>
#include <programmi.h>
#include "frmallarmi.h"

namespace Ui {
class frmPlayMain;
}

class frmPlayMain : public QDialog
{
    Q_OBJECT

public:
    int result;
    prog_s prog;
    void Init();
    explicit frmPlayMain(QWidget *parent = 0);
    ~frmPlayMain();

private slots:
    void on_buttonExit_3_clicked();

    void on_btnStart_clicked();

    void on_btnMenuProgEdit_clicked();

    void on_btnStart_2_clicked();

private:
    Ui::frmPlayMain *ui;
};

#endif // FRMPLAYMAIN_H
