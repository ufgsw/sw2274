#include "formnewprogram.h"
#include "ui_formnewprogram.h"
#include "frmallarmi.h"
#include <QPainter>
#include "programmi.h"
#include <QDate>


FormNewProgram::FormNewProgram(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormNewProgram)
{
    ui->setupUi(this);

    area = 0;

    prog.naree = 0;
    prog.nome = "Program ";
    prog.RetToHome = 0;
    prog.secondi_impiegati = 0;
    prog.secondi_totali = 0;
    prog.campioni = 0;
    prog.altezza_vetro = 0;
    prog.larghezza_vetro = 0;
    prog.altezza_extra = 0;
    prog.larghezza_extra = 0;
    prog.esecuzioni = 0;
    prog.spessore_vetro = 0;
    prog.direzione = 0;


    for(int p=0; p<MAX_AREE ; p++)
    {
        prog.Area[p].y_iniziale = 0;
        prog.Area[p].y_finale = 0;
        prog.Area[p].x_iniziale = 0;
        prog.Area[p].x_finale = 0;
        prog.Area[p].step = 0;
        prog.Area[p].ripetizioni = 0;
        prog.Area[p].velocita_pistola = 0;
        prog.Area[p].velocita_nastro = 0;
        prog.Area[p].pressione_iniziale = 0;
        prog.Area[p].pressione_finale = 0;
        prog.Area[p].um = 0;
        prog.Area[p].pr = 0;
        prog.Area[p].pis1 = 0;
        prog.Area[p].pis2 = 0;

    }

//    ui->btnNomeProgramma->setText(prog.nome);

}


FormNewProgram::~FormNewProgram()
{
    delete ui;
}

void FormNewProgram::set_program_name(QString name)
{
    prog.nome = name;

    ui->labelNomeProg->setText( name );
}
void FormNewProgram::Init(QString um,int quotamax_y)
{
    UM = um;
    max_y = quotamax_y;
    if(UM == "mm")
    {
        fattore_unita   = 1.0;
    }
    else
    {
        fattore_unita   = FATTORE_MM_IN;
    }
    AggiornaDimensioni();

}

void FormNewProgram::set_prog(prog_s p)
{
    prog = p;
    AggiornaDimensioni();
    ui->labelNomeProg->setText(prog.nome);
}

void FormNewProgram::AggiornaDimensioni()
{
    QString text;
    if(UM == "mm")
    {
        text.sprintf("%6.0f", (float)prog.larghezza_vetro);
        ui->btnWidth->setText(text);
        text.sprintf("%6.0f", (float)prog.altezza_vetro);
        ui->btnHeigth->setText(text);
        text.sprintf("%6.0f", (float)prog.spessore_vetro);
        ui->btnThickness->setText(text);
    }
    else
    {
        text.sprintf("%4.2f", (float)prog.larghezza_vetro * fattore_unita);
        ui->btnWidth->setText(text);
        text.sprintf("%4.2f", (float)prog.altezza_vetro * fattore_unita);
        ui->btnHeigth->setText(text);
        text.sprintf("%4.2f", (float)prog.spessore_vetro * fattore_unita);
        ui->btnThickness->setText(text);
    }
}

void FormNewProgram::on_buttonExit_3_clicked()
{


    result = -1;
    close();

}


prog_s FormNewProgram::get_prog()
{
    return prog;
}


void FormNewProgram::on_btnHeigth_clicked()
{
    uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title( tr("Heigth") );
    uiTastieraN->set_unita_misura( "" );
    if(UM == "mm")
    {
        uiTastieraN->set_max( 65000);
        uiTastieraN->set_min( 10 );
        uiTastieraN->set_default( 100);
    }
    else
    {
        uiTastieraN->set_max( 2550 );
        uiTastieraN->set_min( 0 );
        uiTastieraN->set_default( 4 );
    }
    uiTastieraN->set_integer( false );

    uiTastieraN->exec();

    if(uiTastieraN->is_valid())
    {
        float val;
        val = uiTastieraN->get_val();
        prog.altezza_vetro =  (val / fattore_unita);
    }
    AggiornaDimensioni();
    delete uiTastieraN;   
}

void FormNewProgram::on_btnWidth_clicked()
{
    uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title( tr("Width") );
    uiTastieraN->set_unita_misura( UM );

    if(UM == "mm")
    {
        uiTastieraN->set_max( 100000 );
        uiTastieraN->set_min( 10 );
        uiTastieraN->set_default( 100);
    }
    else
    {
        uiTastieraN->set_max( 4000);
        uiTastieraN->set_min( 0 );
        uiTastieraN->set_default( 100);
    }
    uiTastieraN->set_integer( false );

    uiTastieraN->exec();
    if(uiTastieraN->is_valid())
    {
        float val;
            val = uiTastieraN->get_val();
        prog.larghezza_vetro = (val / fattore_unita);
    }

    AggiornaDimensioni();
    delete uiTastieraN;
}

void FormNewProgram::on_btnSave_clicked()
{
    if(prog.larghezza_vetro >  0 && prog.altezza_vetro > 0)
    {
        float peso;

        peso = ((float)(prog.larghezza_vetro * prog.altezza_vetro * prog.spessore_vetro) * 0.0000025) / ((float)prog.larghezza_vetro / 1000) ;
        if(peso > 75)
        {

//            ReportAllarmi alms;
//            s_Allarme alm;
//            alms.open();
//            alm.codice = 100;
//            alm.page = "9.8";

//            QString str;
//            str = QDate::currentDate().toString("dd.MM.yyyy");
//            str += " ";
//            str += QTime::currentTime().toString("hh:mm:ss");
//            alm.data_on   = str;

            frmAllarmi *frmallarmi = new frmAllarmi(this);
            frmallarmi->SetImageMan(":/png/AlmPeso.png","9.8");
            frmallarmi->exec();

//            str = QDate::currentDate().toString("dd.MM.yyyy");
//            str += " ";
//            str += QTime::currentTime().toString("hh:mm:ss");
//            alm.data_off   = str;
//            alms.add(alm);
//            alms.save("");

            delete frmallarmi;
        }
        result = 1;
    }
    else{
        result = -1;
    }
    close();
}



void FormNewProgram::on_btnThickness_clicked()
{
    uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title( tr("Thickness") );
    uiTastieraN->set_unita_misura( UM );
    if(UM == "mm"){
        uiTastieraN->set_max( 50 );
        uiTastieraN->set_default( 10 );
        uiTastieraN->set_integer( true );
    }
    else{
        uiTastieraN->set_max( 2 );
        uiTastieraN->set_default( 1 );
        uiTastieraN->set_integer( false );
    }
    uiTastieraN->set_min( 0 );

    uiTastieraN->exec();

    if(uiTastieraN->is_valid()){
        float val;
        val = uiTastieraN->get_val();


        prog.spessore_vetro = ((val / fattore_unita));
    }
    AggiornaDimensioni();


    delete uiTastieraN;

}
