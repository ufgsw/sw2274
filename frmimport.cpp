#include "frmimport.h"
#include "ui_frmimport.h"
#include "programmi.h"
#include <qdir.h>
#include "formmessagebox.h"
#include <QProcess>
#include <frmconfirm.h>

frmImport::frmImport(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmImport)
{
    ui->setupUi(this);

    QDir dir("/dev");
    QFileInfoList list = dir.entryInfoList(QStringList() << "sd*",QDir::System);
    for (int i = 0; i < list.size(); ++i)
    {
        QFileInfo fileInfo = list.at(i);
        device = fileInfo.fileName();
    }

    QString path;
    QString program;
    QStringList arguments;

    path = "/dev/" + device;

    QFile file(path);
    if( device != "" && file.exists())
    {
        QProcess* proc = new QProcess(this);
        program = "mount";
        arguments << path << "/media/storage";

        //mount = "mount /dev/";
        //mount += device;
        //mount += " /media/storage";

        proc->start( program , arguments);
        proc->waitForFinished(-1);

        QDir dir2("/media/storage");
        dir2.setCurrent("/media/storage");
        QFileInfoList list = dir2.entryInfoList(QStringList() << "*.json",QDir::Files);
        for (int i = 0; i < list.size(); ++i)
        {
            QFileInfo fileInfo = list.at(i);
            if(fileInfo.fileName() != "Jobs.json" &&
                    fileInfo.fileName() != "Maintenance.json" &&
                    fileInfo.fileName() != "Alarms.json" &&
                    fileInfo.fileName() != "Timers.json" &&
                    fileInfo.fileName() != "backup.json" &&
                    fileInfo.fileName() != "temp.json")
            {
                ui->cbListaFile->addItem(QString("%1").arg(fileInfo.fileName()));
            }
        }
    }
    else
    {
        QProcess* proc = new QProcess(this);
        program = "umount";
        arguments << path;

        proc->start( program , arguments );
        proc->waitForFinished(-1);
        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "INSERT USB KEY !");
        uiMessageBox->exec();

        delete uiMessageBox;
    }
}

frmImport::~frmImport()
{
    delete ui;
}

void frmImport::on_buttonExit_3_clicked()
{
    if(device != ""){
        QProcess* proc = new QProcess(this);
        QString mount;
        mount = "umount ";
        mount += device;
        proc->start(mount);
        proc->waitForFinished(-1);
    }
    close();
}

void frmImport::on_btnImage_2_clicked()
{
    Programmi prgs;
    QString filename;
    QString nfile;

    prgs.open();

    nfile = ui->cbListaFile->currentText();

    if( device == "" ) return;

    if(nfile == "restore.json")
    {
        frmConfirm *frmconfirm = new frmConfirm();
        frmconfirm->Init("CONFIRM RESTORE ?",false,0);
        frmconfirm->exec();
        if(frmconfirm->result == 1){
            filename = "/media/storage/";
            filename += nfile;
            prgs.import_restore(filename);

            prgs.save("");

            FormMessageBox *uiMessageBox = new FormMessageBox(this);
            uiMessageBox->setTitle( "RESTORE SUCCESSFUL !");
            uiMessageBox->exec();

            delete uiMessageBox;
        }
    }
    else
    {
        filename = "/media/storage/";
        filename += nfile;
        prgs.import_prog(filename);
        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "IMPORT SUCCESSFUL !");
        uiMessageBox->exec();

        delete uiMessageBox;
    }

    if(device != ""){
        QProcess* proc = new QProcess(this);
        QString mount;
        mount = "umount ";
        mount += device;
        proc->start(mount);
        proc->waitForFinished(-1);
    }
    close();


}
