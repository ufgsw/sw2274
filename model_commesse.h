#ifndef MODEL_COMMESSE_H
#define MODEL_COMMESSE_H





const int COLS_C= 6;
const int ROWS_C= 1000;

#include <QAbstractTableModel>

class model_commesse : public QAbstractTableModel
{
public:


    QString m_gridData[ROWS_C][COLS_C];  //holds text entered into QTableView


    model_commesse(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role)const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;



};

#endif // MODEL_COMMESSE_H
