#ifndef FORMELENCOSSID_H
#define FORMELENCOSSID_H

#include <QDialog>
#include <QStringList>

namespace Ui {
class FormElencoSSID;
}

class FormElencoSSID : public QDialog
{
    Q_OBJECT

public:
    explicit FormElencoSSID(QWidget *parent = 0);
    ~FormElencoSSID();

    bool is_valid() { return valid; }
    QString ssid() { return SSID; }

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_listSSID_currentTextChanged(const QString &currentText);

private:
    Ui::FormElencoSSID *ui;

    bool valid;

    QStringList list;
    QString SSID;
};

#endif // FORMELENCOSSID_H
