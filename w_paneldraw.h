#ifndef W_PANELDRAW_H
#define W_PANELDRAW_H


#include <QtGui>
#include <QWidget>
#include "programmi.h"

class ScribbleArea : public QWidget
{
    Q_OBJECT

public:

    prog_s  Prog;
    int     NArea;
    int16_t AreaSelected;
    ScribbleArea(QWidget *parent = nullptr);

    bool openImage(const QString &fileName);
    bool saveImage(const QString &fileName, const char *fileFormat);
    void setPenColor(const QColor &newColor);
    void setPenWidth(int newWidth);

    bool isModified() const { return modified; }
    QColor penColor() const { return myPenColor; }
    int penWidth() const { return myPenWidth; }

    void AddArea(s_Area  *area,ushort x1,ushort y1,ushort w,ushort h );
    void MoveArea(ushort area,const QPoint &endPoint);
    void CleanArea(ushort area);
    void CleanAll();
    void DeleteArea();
    void SelectArea(ushort area);
    void NormalizzaArea(int area);
    void DrawVetro();

public slots:
    void clearImage();

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

private:
    void drawLineTo(const QPoint &endPoint);
    void resizeImage(QImage *image, const QSize &newSize);
    int16_t FindAreaToClick(const QPoint &point);
    void DrawAreaVetro(short n_area,ushort xp,ushort yp,ushort wp,ushort hp,bool clean);

    bool    modified = false;
    bool    scribbling = false;
    int     myPenWidth = 1;
    QColor  myPenColor = Qt::blue;
    QImage  image;
    QPoint  lastPoint;
    QPoint  startPoint;
    QPoint  offsetPoint;


};

#endif // W_PANELDRAW_H
