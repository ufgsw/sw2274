#include "iot.h"
#include <QDebug>

#include <QNetworkDatagram>
#include <QFile>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#include <QSettings>

Iot::Iot(QObject *parent) : QObject(parent)
{
    socket    = new QUdpSocket(this);
    socket_tx = new QUdpSocket(this);

    socket->bind(  5001 , QUdpSocket::ShareAddress  );

    connect(socket, &QUdpSocket::readyRead,    this, &Iot::on_read_pending_datagram);
    connect(socket, &QUdpSocket::connected,    this, &Iot::on_socket_connect);
    connect(socket, &QUdpSocket::disconnected, this, &Iot::on_socket_disconnect);

    //configure_broker();
}

Iot::~Iot()
{
    //nodered->close();
}

void Iot::load_param()
{
    QFile ini( "/home/root/iot.ini");

    if( ini.exists() == false )
    {
        param.broker_address  = "10.10.0.77";
        param.topic           = "/home/sensors/";
        param.use_custom      = false;
        param.frequenza_invio = 5;
        param.client_id       = "ID1";
        param.token           = "";
        save_param();
    }
    else
    {
        QSettings settings( "/home/root/iot.ini", QSettings::IniFormat );
        param.broker_address  = settings.value( "broker_address").toString();
        param.topic           = settings.value("topic").toString();
        param.use_custom      = settings.value("use_custom").toBool();
        param.frequenza_invio = settings.value("frequenza_invio").toInt();
        param.client_id       = settings.value("clientid").toString();
        param.token           = settings.value("token").toString();
    }

}

void Iot::save_param()
{
    QSettings settings( "/home/root/iot.ini", QSettings::IniFormat );
    settings.setValue( "broker_address", param.broker_address );
    settings.setValue( "topic", param.topic);
    settings.setValue( "use_custom", param.use_custom );
    settings.setValue( "frequenza_invio", param.frequenza_invio );
    settings.setValue( "clientid", param.client_id );
    settings.setValue( "token", param.token );
    settings.sync();
}

void Iot::configure_broker(QString broker_address)
{
    QFile flow_default( ":/node-red/flow");
    //QFile flow_default( "/home/root/flows_nodered.json");
    BrokerAddress = broker_address;
    if( flow_default.exists() )
    {
        QJsonDocument jsonDoc;
        QJsonObject   jsonObj;
        QJsonArray    jsonArray;
        QJsonArray    jsonArrayNew;

        QJsonObject mqtt_in;
        QJsonObject mqtt_out;
        QJsonObject mqtt_broker;
        QJsonObject mqtt_broker_thingsboard;
        //QJsonObject credentials;

        flow_default.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = flow_default.readAll();
        flow_default.close();

        jsonDoc    = QJsonDocument::fromJson(saveData);
        jsonArray  = jsonDoc.array();
        jsonObj    = jsonDoc.object();

        // trovo l'mqtt broker da modificare
        foreach ( QJsonValue var, jsonArray)
        {
            QJsonObject obj = var.toObject();
            QString type = obj["type"].toString();
            if( type == "mqtt-broker" )
            {
                if( obj["name"].toString() == "UFG" )
                {
                    mqtt_broker = obj;
                    mqtt_broker["broker"] = broker_address;
                }
                else if( obj["name"].toString() == "Thingsboard" )
                {
                    mqtt_broker_thingsboard = obj;
                    //param.client_id = mqtt_broker_thingsboard["clientid"].toString();   // TODO: per ora è in sola lettura
                    //mqtt_broker_thingsboard["clientid"] = param.client_id;

                    //credentials = mqtt_broker_thingsboard["credentials"].toObject();
                    //credentials["user"] = param.token;
                }
            }
        }

        // trovo l'mqtt out da assegnare
        foreach ( QJsonValue var, jsonArray)
        {
            QJsonObject obj = var.toObject();
            QString    type = obj["type"].toString();

            if( type == "mqtt out" )
            {
                mqtt_out = obj;
                if( param.use_custom ) mqtt_out["broker"] = mqtt_broker["id"];
                else                   mqtt_out["broker"] = mqtt_broker_thingsboard["id"];
            }
            if( type == "mqtt in" )
            {
                mqtt_in = obj;
                if( param.use_custom ) mqtt_in["broker"] = mqtt_broker["id"];
                else
                {
                    mqtt_in["broker"] = mqtt_broker_thingsboard["id"];
                    mqtt_in["topic"]  = "v1/devices/me/attributes";
                }
            }
        }

        // ricompongo il json
        foreach ( QJsonValue var, jsonArray)
        {
            QJsonObject obj = var.toObject();
            QString type = obj["type"].toString();

            if( type == "mqtt-broker" )
            {
                if( obj["name"].toString() == "UFG" )
                {
                    jsonArrayNew.append( mqtt_broker );
                }
                else if( obj["name"].toString() == "Thingsboard")
                {
                    //credentials["user"]                    = param.token;
                    mqtt_broker_thingsboard["clientid"]    = param.client_id;
                    //mqtt_broker_thingsboard["credentials"] = credentials;
                    jsonArrayNew.append( mqtt_broker_thingsboard );
                }
                else
                {
                    jsonArrayNew.append( obj);
                }
            }
            else if( type == "mqtt out" )
            {
                jsonArrayNew.append( mqtt_out );
            }
            else if( type == "mqtt in" )
            {
                jsonArrayNew.append( mqtt_in );
            }
            else
            {
                jsonArrayNew.append( obj);
            }
        }

        // Salvo la nuova configurazione
        QJsonDocument newdoc;

        newdoc.setArray( jsonArrayNew );

        QFile nodered_flow( "/home/root/flows_nodered.json" );

        if( nodered_flow.open(QFile::WriteOnly | QFile::Text | QFile::Truncate  ) )
        {
            nodered_flow.write( newdoc.toJson() );
            nodered_flow.close();
            qDebug() << "Nodered configurato !";
        }
    }
}

void Iot::start_nodered()
{
    QStringList argument;

    argument << "/home/root/flows_nodered.json";

    broker_connected = false;

    nodered = new QProcess(this);
    nodered->start("node-red", argument);

    connect( nodered, &QProcess::readyRead, this, &Iot::on_nodered_readyread );
}

void Iot::restart_nodered()
{
    nodered->close();

    //configure_broker();
    start_nodered();
}

void Iot::write_qstring(QString str)
{
    socket_tx->connectToHost("127.0.0.1",5000);
    socket_tx->write( str.toLatin1() );
    socket_tx->disconnectFromHost();
}

void Iot::write_string(char *str)
{
//
//    socket->write( str );
    //
}

void Iot::write_buff(QByteArray arr)
{
    socket_tx->connectToHost("127.0.0.1",5000);
    socket_tx->write( arr.data() );
    socket_tx->disconnectFromHost();
}

void Iot::on_socket_connect()
{
    socket_connected  = true;
}

void Iot::on_socket_disconnect()
{
    socket_connected = false;
}

void Iot::on_read_pending_datagram()
{
    while (socket->hasPendingDatagrams())
    {
        QNetworkDatagram datagram = socket->receiveDatagram();

        //qDebug( datagram.data() );

        //digital_io->set_leds( true,false,false);
        emit dara_receive( datagram.data() );
    }
}

void Iot::on_nodered_readyread()
{
    QString msg = nodered->readAll();

    qDebug() << msg.toLatin1();

    if ( msg.contains( "Connected to broker") )
    {
        broker_connected = true;
        qDebug() << "Broker connesso !";
    }
}
