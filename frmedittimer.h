#ifndef FRMEDITTIMER_H
#define FRMEDITTIMER_H

#include <QDialog>
#include <QTimer>
#include <sabbiatrice.h>

namespace Ui {
class frmEditTimer;
}

class frmEditTimer : public QDialog
{
    Q_OBJECT

public:
    Sabbiatrice *sabbiatrice;
    int result;
    bool okReset;
    QString n_pistole;

    void Init(int code, int alarm, int tempo, uint limite);
    explicit frmEditTimer(QWidget *parent = 0);
    ~frmEditTimer();

private slots:
    void on_buttonExit_3_clicked();

    void reset_timer();

    void on_btnReset_pressed();

    void on_btnReset_released();

    void on_btnNext_clicked();


    void on_btnPrior_clicked();

    void on_btnReset_clicked();

private:
    uint Limite;
    QTimer *timer_btn;
    Ui::frmEditTimer *ui;
};

#endif // FRMEDITTIMER_H
