#ifndef IOT_H
#define IOT_H

#include <QObject>
#include <QTcpSocket>
#include <QUdpSocket>


#include <QProcess>

enum type_broker
{
    broker_custom,
    broker_thingsboard
};


class Iot : public QObject
{
    Q_OBJECT
public:
    explicit Iot(QObject *parent = nullptr);
    ~Iot();

    void load_param();
    void save_param();

    void configure_broker(QString broker_address);
    void start_nodered();
    void restart_nodered();

    void write_qstring(QString str);
    void write_string( char* str);
    void write_buff(QByteArray arr);

    struct
    {
        bool    use_custom;
        QString topic;
        QString broker_address;
        QString client_id;
        QString token;
        int     frequenza_invio;
    }param;

    bool broker_connected;

private:
    QProcess* nodered;

    QUdpSocket* socket;
    QUdpSocket* socket_tx;

    QString BrokerAddress;

    bool socket_connected;

signals:

    void dara_receive(QByteArray rx);

public slots:

    void on_socket_connect();
    void on_socket_disconnect();
    void on_read_pending_datagram();
    void on_nodered_readyread();

};

#endif // IOT_H
