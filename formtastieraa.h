#ifndef FORMTASTIERAA_H
#define FORMTASTIERAA_H

#include <QDialog>

namespace Ui {
class FormTastieraA;
}

enum type_keyboard
{
    type_maiuscolo,
    type_minuscolo,
    type_symbol1,
    type_symbol2
};

class FormTastieraA : public QDialog
{
    Q_OBJECT

public:
    explicit FormTastieraA(QWidget *parent = 0);
    ~FormTastieraA();

    void set_maxlen(int val) { maxlen = val; }
    void set_text(QString str);
    void set_title(QString title);
    QString get_text();
    bool is_valid();

private:

    void update_keys();

private slots:
    void on_buttonFN_clicked();

    void on_button123_clicked();

    void on_buttonOK_clicked();

    void on_buttonAnnulla_clicked();

    void on_buttonQ_clicked();

    void on_buttonW_clicked();

    void on_buttonE_clicked();

    void on_buttonR_clicked();

    void on_buttonT_clicked();

    void on_buttonY_clicked();

    void on_buttonU_clicked();

    void on_buttonI_clicked();

    void on_buttonO_clicked();

    void on_buttonP_clicked();

    void on_buttonA_clicked();

    void on_buttonS_clicked();

    void on_buttonD_clicked();

    void on_buttonF_clicked();

    void on_buttonG_clicked();

    void on_buttonH_clicked();

    void on_buttonJ_clicked();

    void on_buttonK_clicked();

    void on_buttonL_clicked();

    void on_buttonZ_clicked();

    void on_buttonX_clicked();

    void on_buttonC_clicked();

    void on_buttonV_clicked();

    void on_buttonB_clicked();

    void on_buttonN_clicked();

    void on_buttonM_clicked();

    void on_buttonSpace_clicked();

    void on_buttonCAN_clicked();

private:
    Ui::FormTastieraA *ui;

    type_keyboard tastiera_attuale;
    type_keyboard tastiera_old;

    int maxlen;
    bool valid;

};

#endif // FORMTASTIERAA_H
