#include "formsetupethernet.h"
#include "ui_formsetupethernet.h"

#include <QProcess>
#include <QMessageBox>

#include <QFile>

FormSetupEthernet::FormSetupEthernet(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormSetupEthernet)
{
    ui->setupUi(this);

    m_process = new QProcess(this);
    connect( m_process, &QProcess::readyReadStandardOutput , this, &FormSetupEthernet::ready_read_ping );

    cambio_ssid = false;

    ui->checkDHCP->setChecked( false );

    QFile file("/etc/network/interfaces");
    if( file.exists() )
    {
        file.open( QFile::ReadOnly );

        QByteArray   dati = file.readAll();
        QStringList righe = QString(dati).split("\n",QString::SkipEmptyParts);

        for( int t=0; t < righe.count();t++)
        {
            QString riga = righe.at(t);

            if( riga.contains( "iface eth0 inet dhcp") )
            {
                ui->checkCABLE->setChecked( true );
                //eth0_dhcp = true;
                ui->checkDHCP->setChecked( true );

                ui->buttonIP->setEnabled( false );
                ui->buttonNM->setEnabled( false );
                ui->buttonNT->setEnabled( false );
                ui->buttonGW->setEnabled( false );
            }
            if( riga.contains("iface eth0 inet static"))
            {
                ui->checkCABLE->setChecked( true );

                QString ip = righe.at( t+1);
                QString nm = righe.at( t+2);
                QString nt = righe.at( t+3);
                QString gw = righe.at( t+4);

                QStringList ip_list = ip.split( " " ,QString::SkipEmptyParts );
                QStringList nm_list = nm.split( " " ,QString::SkipEmptyParts );
                QStringList nt_list = nt.split( " " ,QString::SkipEmptyParts );
                QStringList gw_list = gw.split( " ", QString::SkipEmptyParts );

                ui->buttonIP->setText( ip_list.last());
                ui->buttonNM->setText( nm_list.last());
                ui->buttonNT->setText( nt_list.last());
                ui->buttonGW->setText( gw_list.last());
            }
            //
            if( riga.contains( "iface wlan0 inet dhcp") )
            {
                //wifi_dhcp = true;
                ui->checkWIFI->setChecked(true);

                ui->checkDHCP->setChecked( true );

                ui->buttonIP->setEnabled( false );
                ui->buttonNM->setEnabled( false );
                ui->buttonNT->setEnabled( false );
                ui->buttonGW->setEnabled( false );
            }
            if( riga.contains("iface wlan0 inet static"))
            {
                ui->checkWIFI->setChecked(true);

                QString ip = righe.at( t+1);
                QString nm = righe.at( t+2);
                QString nt = righe.at( t+3);
                QString gw = righe.at( t+4);

                QStringList ip_list = ip.split( " " ,QString::SkipEmptyParts );
                QStringList nm_list = nm.split( " " ,QString::SkipEmptyParts );
                QStringList nt_list = nt.split( " " ,QString::SkipEmptyParts );
                QStringList gw_list = gw.split( " ", QString::SkipEmptyParts );

                ui->buttonIP->setText( ip_list.last());
                ui->buttonNM->setText( nm_list.last());
                ui->buttonNT->setText( nt_list.last());
                ui->buttonGW->setText( gw_list.last());
            }
        }
    }

    QFile file_wpa( "/etc/wpa_supplicant.conf");
    if( file_wpa.exists() )
    {
        file_wpa.open( QFile::ReadOnly );

        QByteArray   dati = file_wpa.readAll();
        QStringList righe = QString(dati).split("\n",QString::SkipEmptyParts);

        foreach ( QString riga, righe)
        {
            if( riga.contains("ssid") )
            {
                QStringList lista = riga.split("\"",QString::SkipEmptyParts);
                if( lista.count() > 1 ) ui->buttonSSID->setText( lista.at(1));
            }
            if( riga.contains("#psk") )
            {
                QStringList lista = riga.split("\"",QString::SkipEmptyParts);
                if( lista.count() > 1 ) ui->buttonPassword->setText( lista.at(1));
            }
        }
    }

    ui->buttonSSID->setVisible(     ui->checkWIFI->isChecked() );
    ui->buttonPassword->setVisible( ui->checkWIFI->isChecked() );

    ui->labelSSID->setVisible(     ui->checkWIFI->isChecked() );
    ui->labelPassword->setVisible( ui->checkWIFI->isChecked() );

}

FormSetupEthernet::~FormSetupEthernet()
{
    delete ui;
}

void FormSetupEthernet::ready_read_ping()
{
    QByteArray arr =  m_process->readAllStandardOutput();
    ui->txtCommand->append( QString(arr));
}

void FormSetupEthernet::on_checkBox_toggled(bool checked)
{
    ui->buttonIP->setEnabled( !checked );
    ui->buttonGW->setEnabled( !checked );
    ui->buttonNM->setEnabled( !checked );
    ui->buttonNT->setEnabled( !checked );
}

void FormSetupEthernet::on_buttonIfconfig_clicked()
{

}

void FormSetupEthernet::on_buttonPing_clicked()
{
    ui->txtCommand->setText( "Ping...");
    QString ip = ui->buttonIpPing->text();


    QStringList arguments;
      arguments << "-c" << "4" << ip.toLatin1();

    m_process->start("ping",arguments);
}


void FormSetupEthernet::on_buttonExit_clicked()
{
    close();
}


void FormSetupEthernet::on_buttonSave_clicked()
{
    QString interface;

    interface  = "# /etc/network/interfaces -- configuration file for ifup(8), ifdown(8)\n";
    interface += "# microgea network settings\n";
    interface += "# The loopback interface\n";

    interface += "auto lo\n";
    interface += "iface lo inet loopback\n";

    if( ui->checkCABLE->isChecked() == true )
    {
        interface += "# Eth0 interfaces\n";
        interface += "auto eth0\n";

        if( ui->checkDHCP->isChecked() )
        {
            interface += "iface eth0 inet dhcp\n";
        }
        else
        {
            interface += "iface eth0 inet static\n";
            interface += "\t address " + ui->buttonIP->text() + "\n";
            interface += "\t netmask " + ui->buttonNM->text() + "\n";
            interface += "\t network " + ui->buttonNT->text() + "\n";
            interface += "\t gateway " + ui->buttonGW->text() + "\n";
            //interface += "\t dns-search google.com\n";
            //interface += "\t dns-nameservers 8.8.8.8\n";
        }
    }
    if( ui->checkWIFI->isChecked() == true )
    {
        interface += "# Wireless interfaces\n";
        interface += "auto wlan0\n";
        if( ui->checkDHCP->isChecked() )
        {
            interface += "iface wlan0 inet dhcp\n";
        }
        else
        {
            interface += "iface wlan0 inet static\n";
            interface += "\t address " + ui->buttonIP->text() + "\n";
            interface += "\t netmask " + ui->buttonNM->text() + "\n";
            interface += "\t network " + ui->buttonNT->text() + "\n";
            interface += "\t gateway " + ui->buttonGW->text() + "\n";
            //interface += "\t dns-search google.com\n";
            //interface += "\t dns-nameservers 8.8.8.8\n";
        }
        interface += "wireless_mode managed\n";
        interface += "wireless_essid any\n";
        interface += "wpa-driver wext\n";
        interface += "wpa-conf /etc/wpa_supplicant.conf\n";

    }



    QFile file("/etc/network/interfaces");

    file.open( QFile::WriteOnly );

    file.write( interface.toLatin1() );

    file.close();

    //
    if( ui->checkWIFI->isChecked() == true && cambio_ssid == true && (ui->buttonPassword->text().length() >= 8))
    {
        // wpa_passphrase SSID password > /etc/wpa_supplicant.conf
        QFile fileset("/home/root/setwifi.sh");

        QProcess *procssid = new QProcess(this);
        QStringList arg;

        arg << ui->buttonSSID->text() << ui->buttonPassword->text() << ">" << "/etc/wpa_supplicant.conf";

        QString ssid = "wpa_passphrase \"" +  ui->buttonSSID->text() + "\" " + ui->buttonPassword->text() + " > /etc/wpa_supplicant.conf\n";

        fileset.open( QFile::WriteOnly );
        fileset.write( ssid.toLatin1() );
        fileset.close();

        procssid->start( "sh", QStringList() << "/home/root/setwifi.sh" );

        //procssid->start( QString("wpa_passphrase") , arg );
        //procssid->start( "wpa_passphrase \"UFG DIPENDENTI\" 12345678 > /etc/wpa_supplicant.conf ");

        procssid->waitForFinished( 5000 );

        qDebug( procssid->readAll() );

        // wpa_supplicant -iwlan0 -Dnl80211 -c/etc/wpa_supplicant.conf -B

        QProcess *wpa = new QProcess(this);
        arg.clear();
        arg << "-iwlan0" << "-Dnl80211" << "-c" << "/etc/wpa_supplicant.conf" << "-B";
        wpa->start( "wpa_supplicant", arg);
        wpa->waitForFinished( 5000 );

        qDebug( "Setting wpa ok");
    }


    // SYNC
    QProcess* sync = new QProcess(this);
    sync->start("sync");
    sync->waitForFinished( 5000 );

    uiMessageBox = new FormMessageBox(this);
    uiMessageBox->setTitle( "SAVE SUCCESSFUL !");
    uiMessageBox->exec();

    delete uiMessageBox;

}

void FormSetupEthernet::on_buttonIP_clicked()
{
    uiTastieraIP = new FormTastieraIP(this);
    uiTastieraIP->exec();

    if( uiTastieraIP->is_valid() )
    {
        ui->buttonIP->setText( uiTastieraIP->get_val() );
    }

    delete uiTastieraIP;
}

void FormSetupEthernet::on_buttonNM_clicked()
{
    uiTastieraIP = new FormTastieraIP(this);
    uiTastieraIP->exec();

    if( uiTastieraIP->is_valid() )
    {
        ui->buttonNM->setText( uiTastieraIP->get_val() );
    }

    delete uiTastieraIP;
}

void FormSetupEthernet::on_buttonNT_clicked()
{
    uiTastieraIP = new FormTastieraIP(this);
    uiTastieraIP->exec();

    if( uiTastieraIP->is_valid() )
    {
        ui->buttonNT->setText( uiTastieraIP->get_val() );
    }

    delete uiTastieraIP;
}

void FormSetupEthernet::on_buttonGW_clicked()
{
    uiTastieraIP = new FormTastieraIP(this);
    uiTastieraIP->exec();

    if( uiTastieraIP->is_valid() )
    {
        ui->buttonGW->setText( uiTastieraIP->get_val() );
    }

    delete uiTastieraIP;
}

void FormSetupEthernet::on_checkDHCP_toggled(bool checked)
{
    ui->buttonIP->setEnabled( !checked );
    ui->buttonNM->setEnabled( !checked );
    ui->buttonNT->setEnabled( !checked );
    ui->buttonGW->setEnabled( !checked );

}

void FormSetupEthernet::on_buttonIpPing_clicked()
{
    uiTastieraIP = new FormTastieraIP(this);
    uiTastieraIP->exec();

    if( uiTastieraIP->is_valid() )
    {
        ui->buttonIpPing->setText( uiTastieraIP->get_val() );
    }

    delete uiTastieraIP;
}

void FormSetupEthernet::on_buttonRestart_clicked()
{
    ui->txtCommand->setText("Restart..");

    QStringList arguments;
      arguments << "restart";

    m_process->start("/etc/init.d/networking",arguments);

}

void FormSetupEthernet::on_buttonIfConfig_clicked()
{
    ui->txtCommand->setText( "");
    m_process->start("ifconfig");
}

void FormSetupEthernet::on_buttonPassword_clicked()
{
    uiTastieraA = new FormTastieraA(this);
    uiTastieraA->set_title( tr("Password"));
    uiTastieraA->exec();

    //if( uiTastieraA->result() == 1)
    //{
        if( uiTastieraA->get_text().length() >= 8 )
        {
            cambio_ssid = true;
            ui->buttonPassword->setText( uiTastieraA->get_text() );
        }
        else
            ui->buttonPassword->setText( tr("Type >= 8 character"));
    //}

    delete uiTastieraA;
}

void FormSetupEthernet::on_buttonSSID_clicked()
{
    uiElencoSSID = new FormElencoSSID(this);
    uiElencoSSID->setGeometry(100,20,600,440);

    uiElencoSSID->exec();

    if( uiElencoSSID->is_valid() )
    {
        ui->buttonSSID->setText( uiElencoSSID->ssid() );
        cambio_ssid = true;
    }

    delete uiElencoSSID;
}

void FormSetupEthernet::on_checkWIFI_clicked()
{
    ui->buttonSSID->setVisible( true );
    ui->buttonPassword->setVisible( true );

    ui->labelSSID->setVisible( true );
    ui->labelPassword->setVisible( true );
}

void FormSetupEthernet::on_checkCABLE_clicked()
{
    ui->buttonSSID->setVisible( false );
    ui->buttonPassword->setVisible( false );

    ui->labelSSID->setVisible( false );
    ui->labelPassword->setVisible( false );
}
