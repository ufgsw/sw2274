#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "w_paneldraw.h"
#include "frmmanualenastro.h"
#include "frmsplash.h"
#include "frminfo.h"
#include "frmcommesse.h"
#include "formorologio.h"
#include "release.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->labelErrCode->setVisible( false );

    commesse = Commesse::getInstance();
    commesse->open();

    alms = ReportAllarmi::getInstance();
    alms->open();

    progs = Programmi::getInstance();
    try
    {
        progs->open();
    }
    catch(...)
    {

    }

    configs = Config::getInstance();
    if(configs->open() == false)
    {
        configs->modello = progs->modello;
        configs->As4_0   = progs->As4_0;
        configs->Tipo    = progs->Tipo;
        configs->sn      = progs->sn;
        configs->Year    = progs->Year;
        configs->Manufacturer   = progs->Manufacturer;
        configs->Automatic_Guns = progs->Automatic_Guns;
        configs->UM     = progs->UM;
        configs->Volt   = progs->Volt;
        configs->Hz     = progs->Hz;
        configs->A      = progs->A;
        configs->kW     = progs->kW;
        configs->Bar    = progs->Bar;
        configs->Weigh  = progs->Weigh;
        configs->DoHoming = progs->DoHoming;
        configs->save();
        configs->open();
    }
    else
    {
        progs->modello = configs->modello;
        progs->As4_0   = configs->As4_0;
        progs->Tipo    = configs->Tipo;
        progs->sn      = configs->sn;
        progs->Year    = configs->Year;
        progs->Manufacturer   = configs->Manufacturer;
        progs->Automatic_Guns = configs->Automatic_Guns;
        progs->UM       = configs->UM;
        progs->Volt     = configs->Volt;
        progs->Hz       = configs->Hz;
        progs->A        = configs->A;
        progs->kW       = configs->kW;
        progs->Bar      = configs->Bar;
        progs->Weigh    = configs->Weigh;
        progs->DoHoming = configs->DoHoming;
    }


    orologio    = new Orologio();
    sabbiatrice = new Sabbiatrice();
    sabbiatrice->Init_Hardware();

    // Selezione modello
    if(progs->modello == "Mistral 120 ev+" || progs->modello == "Mistral 180 ev+" || progs->modello == "Mistral 260 ev+" || progs->modello == "Mistral 230 v")
    {
        ui->btnSetupTempi->setVisible(true);
        sabbiatrice->Tipo    = MISTRAL;
        progs->Tipo          = MISTRAL;
    }
    else if(progs->modello == "Zephir 120+" || progs->modello == "Zephir 180+" || progs->modello == "Zephir 260+")
    {
        ui->btnSetupTempi->setVisible(false);
        sabbiatrice->Tipo    = ZEPHIR;
        progs->Tipo          = ZEPHIR;
    }

    sabbiatrice->Modello       = progs->modello;
    sabbiatrice->serial_number = progs->sn;

    esegui_sabbiatura = new EseguiSabbiatura();

    // udpSocket = new QUdpSocket(this);
    // udpSocket->bind(5000, QUdpSocket::ShareAddress);
    // connect(udpSocket, &QUdpSocket::readyRead,this, &MainWindow::processPendingDatagrams);

    timer_alarm = new QTimer(this);
    connect(timer_alarm,SIGNAL(timeout()), this, SLOT(Win_Allarmi()));
    timer_alarm->start(500);

    sabbiatrice->Allarmi     = 0;
    sabbiatrice->Allarmi_old = 0;
    frmallarmi = new frmAllarmi(this);
    frmallarmi->sabbiatrice = sabbiatrice;

    frmmanualenastro =  new frmManualeNastro();

    sabbiatrice->start();
    esegui_sabbiatura->sabbiatrice = sabbiatrice;
    esegui_sabbiatura->start();

    last_idx = 0;

    frmSplash *frmsplash = new  frmSplash();
    frmsplash->Init(progs->modello,progs->sn);
    frmsplash->exec();

    sabbiatrice->win_start = 1;

    SelectedComm = "";

    frmplaymain = new frmPlayMain();
    frmplayexec = new frmPlayExec();
    frmplaytimer = new frmPlayTimer();

    frmplayexec->esegui_sabbiatura = esegui_sabbiatura;

    //else loop_pezza = false;
}

MainWindow::~MainWindow()
{
    delete ui;
}
QString MainWindow::ShowDateTime()
{
    QString str;

    str = QDate::currentDate().toString("dd.MM.yyyy");
    str += " ";
    str += QTime::currentTime().toString("hh:mm:ss");

    ui->btnOrologio->setText(str);

    return str;
}
void MainWindow::processPendingDatagrams()
{
    QByteArray datagram;
    //! [2]
    while (udpSocket->hasPendingDatagrams()) {
        datagram.resize(int(udpSocket->pendingDatagramSize()));
        udpSocket->readDatagram(datagram.data(), datagram.size());
        //ui->statusLabel->setText(tr("\"%1\"").arg(datagram.constData()));
        //NuovaArea(false);
    }
    //! [2]
}
QString MainWindow::GetPage(int allarme)
{
    /*
#define ALM_EMERGENZA   1
#define ALM_VENTILATORE 2
#define ALM_VIBRATORE   3
#define ALM_PORTELLO    5
#define ALM_VETRO       6
#define ALM_MOT_1       7
#define ALM_MOT_2       8
#define ALM_HOME        20
*/

    QString page;

    switch(allarme){
    case ALM_EMERGENZA:
        page = "9.0";
        break;
    case ALM_VENTILATORE:
        page = "9.1";
        break;
    case ALM_VIBRATORE:
        page = "9.6";
        break;
    case ALM_PORTELLO:
        page = "9.2";
        break;
    case ALM_VETRO:
        page = "9.3";
        break;
    case ALM_MOT_1:
        page = "9.4";
        break;
    case ALM_MOT_2:
        page = "9.5";
        break;
    case ALM_HOME:
        page = "0.0";
        break;
    case 10:
        page = "9.7";
        break;
    case 100:
        page = "9.8";
        break;

    }

    return page;
}

void MainWindow::Win_Allarmi(void)
{
    static bool okWarning_old=false;
    static char manuale_old=0;



    bool okWarning=false;

    ShowDateTime();

    if( sabbiatrice->warningTempiLavorazione.type.service == 1 ) ui->labelErrCode->setVisible( true  );
    else                                                         ui->labelErrCode->setVisible( false );

    if(sabbiatrice->warningTempiLavorazione.type.rotazione_ugello1 == 1
            || sabbiatrice->warningTempiLavorazione.type.rotazione_ugello2 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello1 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello2 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello_iniettore1 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello_iniettore2 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_blocchetto_porta_ugello1 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_blocchetto_porta_ugello2 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_corpo_pistola1 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_corpo_pistola2 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_tubo_pescaggio1 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_tubo_pescaggio2 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_puleggia_motore_pistola == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_puleggia_motore_nastro == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_cuscinetti == 1
            || sabbiatrice->warningTempiLavorazione.type.rotazione_corazza == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_corazza == 1
            || sabbiatrice->warningTempiLavorazione.type.pulizia_manichette == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_manichette == 1){

        okWarning = true;
    }
    if(okWarning != okWarning_old)
    {
        if(okWarning)
        {
            ui->btnSetupTempi->setIcon(QIcon(":/png/34+.png"));
        }
        else
        {
            ui->btnSetupTempi->setIcon(QIcon(":/png/30+.png"));
        }
    }
    okWarning_old = okWarning;

    //sabbiatrice->manuale = 0; //+++
    if(sabbiatrice->manuale != manuale_old)
    {
        if(sabbiatrice->manuale > 0)
        {
            frmmanualenastro->Init(sabbiatrice->manuale);
            frmmanualenastro->show();
        }
        else
        {
            frmmanualenastro->hide();
        }
        manuale_old = sabbiatrice->manuale;
    }

    if(sabbiatrice->Allarmi != sabbiatrice->Allarmi_old)
    {
        s_Allarme alm;
        bool okalarm = false;
        if(sabbiatrice->Allarmi > 0 && sabbiatrice->Allarmi <= 20)
        {
            alms->open();
            // alm.codice    = sabbiatrice->Allarmi;
            alm.code      = GetPage(sabbiatrice->Allarmi);
            alm.data_on   = ShowDateTime();
            okalarm = true;
        }
        switch (sabbiatrice->Allarmi){
            // EMERGENZA
        case ALM_EMERGENZA:
            // PORTELLO
        case ALM_PORTELLO:
            // MOT2
        case ALM_MOT_2:
        case ALM_HOME:
            // VETRO
        case ALM_VETRO:
        case 10:
            sabbiatrice->vis_allarme = true;
            frmallarmi->SetImage();
            frmallarmi->exec();
            sabbiatrice->vis_allarme = false;

            sabbiatrice->flag.send_reset_alarm_mot1 = 1;
            QThread::msleep(200);
            sabbiatrice->flag.send_reset_alarm_mot2 = 1;
            QThread::msleep(200);
            sabbiatrice->flag.send_home_mot2		= 1;
            QThread::msleep(200);

            //sabbiatrice->Home_Pistola();
            sabbiatrice->Allarmi_old = 0;

            break;
            // VENTILATORE
        case ALM_VENTILATORE:
            // VIBRATORE
        case ALM_VIBRATORE:
            // MOT_1
        case ALM_MOT_1:
            sabbiatrice->vis_allarme = true;
            frmallarmi->SetImage();
            frmallarmi->exec();
            sabbiatrice->vis_allarme = false;
            sabbiatrice->Allarmi_old = 0;
            break;
        default:
            sabbiatrice->Allarmi_old = sabbiatrice->Allarmi;
            break;
        }

        if(okalarm)
        {
            alm.data_off   = ShowDateTime();
            alms->add(alm);
            alms->save("");
            //if(sabbiatrice->Par.as4_0 ){
            sabbiatrice->updateiot_alarm();
            //}
        }
    }

    if(sabbiatrice->Warning != sabbiatrice->Warning_old)
    {
        s_Allarme alm;
        bool okalarm = false;

        switch (sabbiatrice->Warning)
        {
            // VETRO
        case ALM_VETRO:
            sabbiatrice->vis_allarme = true;
            frmallarmi->SetImage();
            frmallarmi->exec();
            sabbiatrice->vis_allarme = false;

            sabbiatrice->flag.send_reset_alarm_mot1 = 1;
            QThread::msleep(200);
            sabbiatrice->flag.send_reset_alarm_mot2 = 1;
            QThread::msleep(200);
            sabbiatrice->flag.send_home_mot2		= 1;
            QThread::msleep(200);

            sabbiatrice->Warning_old = 0;
            break;
        default:
            break;
        }
    }
}


void MainWindow::on_btnStart_clicked()
{
    int res;

    SelectedComm = "";
    commesse->setCurrent( SelectedComm );

    if( sabbiatrice->parametri->param.as4_0 > 0 )
    {
        frmCommesse * frmcommesse = new frmCommesse();
        frmcommesse->Init();
        frmcommesse->exec();

        if(frmcommesse->result == 1)
        {
            QStringList list;
            QString     cerca;
            int idx = -1;

            list = progs->elenco_nomi();

            for (int i = 0; i < list.size(); ++i)
            {
                if(list[i] == frmcommesse->SelectedProg)
                {
                    idx = i;
                    break;
                }
            }
            if(idx >= 0)
            {
                SelectedProg = frmcommesse->SelectedProg;
                progs->setCurrent(frmcommesse->SelectedProg);
                SelectedComm = frmcommesse->SelectedComm;
                commesse->setCurrent(SelectedComm);
                frmcommesse->Message("");
                EseguiProgrammaSabbiatura();
            }
            else
            {
                frmcommesse->Message("Program missing");
                frmcommesse->exec();
            }
        }
        else
        {
            if(sabbiatrice->Tipo == MISTRAL)
            {
                frmselectprog = new formselectprog(this);
                frmselectprog->progs = progs;
                frmselectprog->last_idx = last_idx;
                frmselectprog->Init(false,progs->UM);
                frmselectprog->exec();
                res = frmselectprog->result;
                last_idx = frmselectprog->last_idx;
                delete frmselectprog;
            }
            else
            {
                last_idx = 0;
                res = 1;
                progs->setCurrent("PROG");
            }

            if(res > 0)
            {
                EseguiProgrammaSabbiatura();
            }

        }
        delete frmcommesse;
    }
    else
    {
        if(sabbiatrice->Tipo == MISTRAL)
        {
            frmselectprog = new formselectprog(this);
            frmselectprog->progs = progs;
            frmselectprog->last_idx = last_idx;
            frmselectprog->Init(false,progs->UM);
            frmselectprog->exec();
            res = frmselectprog->result;
            last_idx = frmselectprog->last_idx;
            delete frmselectprog;
        }
        else
        {
            last_idx = 0;
            res = 1;
            progs->setCurrent("PROG");
        }

        if(res > 0)
        {
            EseguiProgrammaSabbiatura();
        }
    }
}

void MainWindow::EseguiProgrammaSabbiatura()
{
    sabbiatrice->Prog = progs->current();
    esegui_sabbiatura->DoHoming = progs->DoHoming;

    frmplaymain->prog = esegui_sabbiatura->sabbiatrice->Prog;//  progs->current();
    frmplaymain->Init();
    frmplaymain->exec();

    if( frmplaymain->result == 1 )
    {
        frmplayexec->SetNomeProgramma(sabbiatrice->Prog.nome,progs->Tipo);
        frmplayexec->exec();
    }
}

//void MainWindow::EseguiProgramma(bool loop)
//{
//    bool esegui = false;

//    return;


//    frmplaymain->prog = progs->current();
//    frmplaymain->Init();

//    if(loop == false)
//    {
//        frmplaymain->exec();
//    }
//    else
//    {
//        frmplaymain->result = 1;
//    }

//    if(frmplaymain->result == 1)
//    {
//        sabbiatrice->Prog = progs->current();
//        sabbiatrice->AzzeraCronometro(); //>cronometro = 0;
//        //esegui_sabbiatura->sabbiatrice = sabbiatrice;
//        esegui_sabbiatura->Stop_manuale = 0;
//        esegui_sabbiatura->DoHoming = progs->DoHoming;

//        if( esegui_sabbiatura->isRunning() == false )
//            esegui_sabbiatura->start();

//        // esegui_sabbiatura->Start_sabbiatura = 1;
//        frmplayexec->SetNomeProgramma(sabbiatrice->Prog.nome,progs->Tipo);
//        frmplayexec->exec();

//        if(frmplayexec->result == 3)
//        {
//            //frmPlayTimer *frmplaytimer = new frmPlayTimer();
//            frmplaytimer->Init(sabbiatrice->cronometro);
//            if(loop == false)
//            {
//                frmplaytimer->exec();
//            }
//            else
//            {
//                frmplaytimer->result = 1;
//            }
//            if(frmplaytimer->result == 1)
//            {
//                esegui = true;
//            }
//        }
//        sabbiatrice->Prog = esegui_sabbiatura->sabbiatrice->Prog;
//        progs->replace(esegui_sabbiatura->sabbiatrice->Prog);
//        progs->save("");
////        if( SelectedComm != "" )
////        {
////            s_Commessa comm;
////            comm               = comms->get_byname(SelectedComm);
////            comm.qta_fatti     = comm.qta_fatti + 1;
////            comm.tempo_totale += sabbiatrice->Prog.secondi_impiegati;
////            comm.tempo_singolo = sabbiatrice->Prog.secondi_impiegati;
////            comms->replace(comm);
////            comms->save();

////            sabbiatrice->updateiot();
////        }

//        //delete frmplayexec;

//    }
//    else if(frmplaymain->result == 2)
//    {
//        //frmplaytimer = new frmPlayTimer();
//        sabbiatrice->Prog = progs->current();
//        frmplaytimer->Init(sabbiatrice->Prog.secondi_impiegati);
//        frmplaytimer->exec();
//        if(frmplaytimer->result > 0){

//        }
//        esegui = false;
//        //delete frmplaytimer;

//    }
//    //delete frmplaymain;

//    if(esegui)
//    {
//        EseguiProgramma(loop_pezza);
//    }

//}

void MainWindow::on_btnComandiManuali_clicked()
{
    frmManuale = new formmanuale();
    frmManuale->sabbiatrice = sabbiatrice;
    frmManuale->Init(progs->Automatic_Guns,progs->modello);
    sabbiatrice->SetInManuale(true);
    frmManuale->exec();
    sabbiatrice->SetInManuale(false);

    delete frmManuale;

}

void MainWindow::on_btnMenuProgEdit_clicked()
{
    int res;
    if(sabbiatrice->Tipo == MISTRAL)
    {
        frmselectprog = new formselectprog(this);
        frmselectprog->progs = progs;
        frmselectprog->last_idx = last_idx;
        frmselectprog->Init(true,progs->UM);
        frmselectprog->exec();
        res = frmselectprog->result;
        if(res > 0)
        {
            last_idx = frmselectprog->last_idx;

            frmeditprog_1 = new frmEditProg_1();
            frmeditprog_1->prog = progs->current();
            progs->export_prog("/home/root/temp.json", frmeditprog_1->prog );

            frmeditprog_1->Init();
            frmeditprog_1->exec();

            res = frmeditprog_1->result;
            if(res > 0)
            {
                progs->replace(frmeditprog_1->prog);
                progs->save("");
                progs->current() = frmeditprog_1->prog;            
            }
            else
            {
                progs->import_prog("/home/root/temp.json");
            }
            delete frmeditprog_1;
        }
    }
    else
    {
        res = 1;
        last_idx = 0;
        progs->setCurrent("PROG");
    }

    if(res > 0)
    {
        frmBuildProgram  = new frmbuildprogram();
        frmBuildProgram->prog = progs->current();
        frmBuildProgram->prg = progs;
        frmBuildProgram->Init(progs->UM,sabbiatrice->parametri->param.quotamax_y);
        frmBuildProgram->exec();
        if(frmBuildProgram->result > 0)
        {
            if(sabbiatrice->Tipo == ZEPHIR)
            {
                frmBuildProgram->prog.secondi_impiegati = 0;
                frmBuildProgram->prog.esecuzioni = 0;
                frmBuildProgram->prog.nome = "PROG";
            }
            progs->replace(frmBuildProgram->prog);
            progs->save("");
        }
        else{
            progs->import_prog("/home/root/temp.json");

        }

        delete frmBuildProgram;

    }


    if(sabbiatrice->Tipo == MISTRAL){
        delete frmselectprog;
    }

}

void MainWindow::on_btnSetupTempi_clicked()
{
    frmMenuSetup = new frmMainSetup();
    frmMenuSetup->sabbiatrice = sabbiatrice;
    frmMenuSetup->Init(1,progs->Automatic_Guns);
    frmMenuSetup->exec();

    delete frmMenuSetup;
    QString ore;
}


void MainWindow::on_btnSetup40_clicked()
{
    frmMenu4_0 *frmmenusetup4_0 = new frmMenu4_0();
    frmmenusetup4_0->exec();
    delete frmmenusetup4_0;

    progs->open();


    /*

    FormTastieraN *uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title("Password");
    uiTastieraN->set_max( 9999 );
    uiTastieraN->set_min( 0 );
    uiTastieraN->set_default( 0 );
    uiTastieraN->set_integer( true );

    uiTastieraN->exec();

    if(uiTastieraN->get_val() == 1010){

        frmufgtest = new frmUfgTest();
        frmufgtest->sabbiatrice = sabbiatrice;
        frmufgtest->progs = progs;
        frmufgtest->Init();
        sabbiatrice->SetInManuale(true);
        frmufgtest->exec();
        progs = frmufgtest->progs;
        progs->save("");
        sabbiatrice->parametri->param = sabbiatrice->Par;
        sabbiatrice->parametri->save();
        delete frmufgtest;
    }


    sabbiatrice->SetInManuale(false);

    delete uiTastieraN;
*/
}

void MainWindow::on_buttonExit_5_pressed()
{

}

void MainWindow::on_btnTempi_clicked()
{    
    frmmaintimer = new frmMainTimer();
    frmmaintimer->sabbiatrice = sabbiatrice;
    frmmaintimer->Init(sabbiatrice->tempiLavoro.lavorazione_totale,sabbiatrice->tempiLavoro.lavorazione_parziale,sabbiatrice->tempiLavoro.accensione);
    frmmaintimer->exec();

//    if(frmmaintimer->okReset)
//    {
//        sabbiatrice->tempiLavoro.lavorazione_parziale = 0;
//        sabbiatrice->SalvaTempi();
//    }
    delete frmmaintimer;
}

void MainWindow::on_buttonExit_6_clicked()
{

    frmInfo *frminfo = new  frmInfo();
    //frminfo->config = progs;
    frminfo->Init(false);
    frminfo->exec();

    if( frminfo->reset_service == true )
    {
        sabbiatrice->tempiLavoro.service = 0;
        sabbiatrice->warningTempiLavorazione.type.service = 0;
        sabbiatrice->SalvaTempi();
    }

    if( frminfo->reset_timers == true )
    {
        sabbiatrice->reset_all_timers();
        alms->clear();

        Maintenance* mm = Maintenance::GetInstance();
        mm->clear();
    }
    
    if(frminfo->okEdit)
    {
        frmufgtest = new frmUfgTest();
        frmufgtest->sabbiatrice = sabbiatrice;
        frmufgtest->progs = progs;
        frmufgtest->Init();
        sabbiatrice->SetInManuale(true);
        frmufgtest->exec();
        //progs = frmufgtest->progs;
        //progs->save("");

        configs->modello = progs->modello;
//        configs->As4_0   = progs->As4_0;
//        configs->Tipo    = progs->Tipo;
//        configs->sn      = progs->sn;
//        configs->Year    = progs->Year;
//        configs->Manufacturer   = progs->Manufacturer;
//        configs->Automatic_Guns = progs->Automatic_Guns;
//        configs->UM       = progs->UM;
//        configs->Volt     = progs->Volt;
//        configs->Hz       = progs->Hz;
//        configs->A        = progs->A;
//        configs->kW       = progs->kW;
//        configs->Bar      = progs->Bar;
//        configs->Weigh    = progs->Weigh;
//        configs->DoHoming = progs->DoHoming;

        configs->save();

        progs->save("");

        sabbiatrice->parametri->save();


        delete frmufgtest;

        // Selezione modello
        if(progs->modello == "Mistral 120 ev+" || progs->modello == "Mistral 180 ev+" || progs->modello == "Mistral 260 ev+" || progs->modello == "Mistral 230 v")
        {
            ui->btnSetupTempi->setVisible(true);
            sabbiatrice->Tipo    = MISTRAL;
            progs->Tipo          = MISTRAL;
        }
        else if(progs->modello == "Zephir 120+" || progs->modello == "Zephir 180+" || progs->modello == "Zephir 260+")
        {
            ui->btnSetupTempi->setVisible(false);
            sabbiatrice->Tipo    = ZEPHIR;
            progs->Tipo          = ZEPHIR;
        }

        sabbiatrice->Modello       = configs->modello;
        sabbiatrice->serial_number = configs->sn;
    }

    sabbiatrice->SetInManuale(false);

    delete frminfo;
}

void MainWindow::on_buttonExit_5_clicked()
{
    //sabbiatrice->updateiot_param();
}

void MainWindow::on_btnOrologio_clicked()
{
    FormOrologio *frmorologio = new  FormOrologio();
    frmorologio->exec();

}


