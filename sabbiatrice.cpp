#include "sabbiatrice.h"
#include <QTimer>
#include <QProcess>
#include <math.h>
#include <QThread>
#include <qdatetime.h>
#include <QJsonDocument>
#include <QJsonObject>

#include <QDate>
#include <QTime>

#include "release.h"

#define SIZE_PAR   0x0200
#define SIZE_TEMPI 0x0100
#define SIZE_PROG  5024

#define FR_INI_PAR       0x0000  // Adress di inizio salvataggio Parametri
#define FR_INI_TEMPI     0x0200
#define FR_INI_PROG		 0x0300

#define FR_INI_NIMG      0xffe0	 // Numero di immagini importate
#define FR_INI_TEST      0xfff0
#define FR_DATO_TEST     0xaa55aa55

#define CAN_DEVICE  0
// Uso la stessa mail box per inviare messaggi
#define MB_TX_AZ_PEZZA      14

#define ID_IO_PEZZA 	   0x100
#define ID_CMD_PEZZA	   0x200

#define FLEXCAN_STANDARD   0x00

Sabbiatrice::Sabbiatrice()
{
    mRun = false;    
    use_topic = false;

    warningTempiLavorazione.val                      = 0;
    tempiLavoro.accensione                           = 0;
    tempiLavoro.lavorazione_parziale                 = 0;
    tempiLavoro.lavorazione_totale                   = 0;
    //tempiLavoro.release                              = 0;

    tempiLavoro.rotazione_ugello1                     = 0;
    tempiLavoro.rotazione_ugello2                     = 0;
    tempiLavoro.sostituzione_ugello1                  = 0;
    tempiLavoro.sostituzione_ugello2                  = 0;
    tempiLavoro.sostituzione_ugello_iniettore1        = 0;
    tempiLavoro.sostituzione_ugello_iniettore2        = 0;
    tempiLavoro.sostituzione_blocchetto_porta_ugello1 = 0;
    tempiLavoro.sostituzione_blocchetto_porta_ugello2 = 0;
    tempiLavoro.sostituzione_corpo_pistola1           = 0;
    tempiLavoro.sostituzione_corpo_pistola2           = 0;
    tempiLavoro.sostituzione_tubo_pescaggio1          = 0;
    tempiLavoro.sostituzione_tubo_pescaggio2          = 0;
    tempiLavoro.sostituzione_puleggia_motore_pistola  = 0;
    tempiLavoro.sostituzione_puleggia_motore_nastro   = 0;
    tempiLavoro.sostituzione_cuscinetti               = 0;
    tempiLavoro.rotazione_corazza                     = 0;
    tempiLavoro.sostituzione_corazza                  = 0;
    tempiLavoro.pulizia_manichette                    = 0;
    tempiLavoro.sostituzione_manichette               = 0;

    vis_allarme = false;
    win_start  = 0;
    manuale    = 0;

    //commesse = Commesse::getInstance();

    dteStart = new QDateTime(QDateTime::currentDateTime());

    canbus = new CanBusThread();
    connect(canbus, &CanBusThread::data_receive ,this, &Sabbiatrice::processCanBus);

    iot = new Iot();
    connect(iot, &Iot::dara_receive ,this, &Sabbiatrice::iot_dara_receive);

    QTimer* timer1s = new QTimer(this);
    connect( timer1s, &QTimer::timeout, this, &Sabbiatrice::on_timer_1s );
    timer1s->start( 1000 );

    QTimer* timer100ms = new QTimer(this);
    connect( timer100ms, &QTimer::timeout, this, &Sabbiatrice::on_timer_100ms );
    timer100ms->start( 100 );

    mot1_in_allarme = false;
    mot_pistola_in_allarme = false;

    Warning        = 0;
    Warning_old    = 0;
    Allarmi        = 0;
    Allarmi_old    = 0;
    Output         = 0;
    Input          = 0;
    Pressione_set  = 0;
    Comandi_mot    = 0;
    Quota_mot1     = 0;
    Quota_mot2     = 0;
    Quota_mot1_set = 0;
    Quota_mot2_set = 0;
    Velo_mot1_set  = 0;
    Velo_mot2_set  = 0;
    Status_mot_nastro    = 0;
    Status_mot_pistola    = 0;

    flag.send_reset_alarm_mot1 = 1;
    flag.send_reset_alarm_mot2 = 1;
}

void Sabbiatrice::processCanBus(int id, unsigned char len, unsigned char b0,unsigned char b1,unsigned char b2,unsigned char b3,unsigned char b4,unsigned char b5,unsigned char b6,unsigned char b7)
{
    uint16_t ui16;
    int32_t i32;
    Timer_rx_azionamento = 10000;
    
    switch(id){
        case RX_ID1:        //   0x180 Ricevo dall'azionamento gli ingressi
            ui16  = (uint16_t)b0 << 8;
            ui16 |= (uint16_t)b1;
            Input = ui16;
            Status_mot_nastro = b2;
            i32  = (int32_t)b3 << 24;
            i32 |= (int32_t)b4 << 16;
            i32 |= (int32_t)b5 << 8;
            i32 |= (int32_t)b6;
            Quota_mot1        = i32;
            parametri->param.tipo_macchina = ( Input >> 10 ) & 0x03;
        break;
        case RX_ID2:        //   0x181 Ricevo dall'azionamento info varie
            switch( b0 ) {
                 case CMD_HOME_MOT1:
                     Risposta_azionamento = b0;
                 break;
                 case CMD_HOME_MOT2:
                     Risposta_azionamento = b0;
                 break;
                 case CMD_MOVE_QUOTA_MOT1:
                     Risposta_azionamento = b0;
                 break;
                 case CMD_MOVE_QUOTA_MOT2:
                     Risposta_azionamento = b0;
                 break;
            }
        case RX_ID3:        //   0x182 Ricevo dall'azionamento lo status 2
            ui16  = (uint16_t)b0 << 8;
            ui16 |= (uint16_t)b1;
            //Input = ui16;

            Status_mot_pistola = b2;

            i32  = (uint32_t)b3 << 24;
            i32 |= (uint32_t)b4 << 16;
            i32 |= (uint32_t)b5 << 8;
            i32 |= (uint32_t)b6;

            Quota_mot2 = i32;

        break;
        case RX_ID4:
        case RX_ID5:
        case RX_ID6:
        case RX_ID7:
        case RX_ID8:
        case RX_ID9:
        case RX_ID10:
            qDebug("Altro");
        break;

    }

    if( Status_mot_nastro & Stato_motore ) mot1_in_allarme = true;
    if( Status_mot_pistola & Stato_motore ) mot_pistola_in_allarme = true;


//    static uint16_t stato_old;

//    Input       = canbus->Input;
//    Status_mot1 = canbus->Status_mot1;
//    Quota_mot1  = canbus->Quota_mot1;
//    Status_mot2 = canbus->Status_mot2;
//    Quota_mot2  = canbus->Quota_mot2;
//    Timer_rx_azionamento = canbus->Timer_rx_azionamento;
//    Risposta_azionamento = canbus->Risposta_azionamento;
//    Par.tipo_macchina    = canbus->TipoMacchina;

//    if( stato_old != Status_mot2)
//    {
//        QString msg = QString("Status motore 2 : %1").arg(Status_mot2);
//        qDebug( msg.toLatin1());
//    }
//    stato_old = Status_mot2;
}

void Sabbiatrice::on_timer_100ms()
{
    if ( flag.esegui_filtro == 1 )
    {
        if(Timer_pausa)
        {
            --Timer_pausa;
            MANICH1_OFF();
            MANICH2_OFF();
            MANICH3_OFF();
            Timer_lavoro = parametri->param.timer_lavoro;
        }
        else
        {
            if ( Timer_lavoro )
            {
                --Timer_lavoro;
                QString text;
                text.sprintf("Manic %d", lastmanic);
                qDebug(text.toLatin1());
                switch ( lastmanic )
                {
                case 0: MANICH1_ON(); break;
                case 1: MANICH2_ON(); break;
                case 2: MANICH3_ON(); break;
                }
                if ( !Timer_lavoro )
                {
                    ++lastmanic;
                    if(lastmanic > 2) lastmanic = 0;
                    Timer_pausa = parametri->param.timer_pausa * 10;
                }
            }
        }
    }
}


void Sabbiatrice::on_timer_1s()
{
    static int time_test_salva = 0;

    if( win_start == 0 ) return;

    if( ++time_test_salva == 10 )
    {

    }

    if( ( Input & Ventilatore_manuale) != 0 || flexe == GO )
    {
        timeoff_filtro = parametri->param.timeout_filtro;
        {
            ++tempiLavoro.accensione;

            if( Output & Motore_ventilazione )
            {
                ++tempiLavoro.pulizia_manichette;
                ++tempiLavoro.sostituzione_manichette;
            }

            if( flexe == GO && flag.in_pausa == 0)
            {
                ++cronometro; // = dteStart->secsTo( QDateTime::currentDateTime() );

                ++tempiLavoro.service;
                ++tempiLavoro.lavorazione_parziale;
                ++tempiLavoro.lavorazione_totale;
                // Macchina in funzione

                // Pistola 1 in funzione
                if( Output & Elettrovalvola_pistola_1 )
                {
                    ++tempiLavoro.rotazione_ugello1;
                    ++tempiLavoro.sostituzione_ugello1;
                    ++tempiLavoro.sostituzione_ugello_iniettore1;
                    ++tempiLavoro.sostituzione_blocchetto_porta_ugello1;
                    ++tempiLavoro.sostituzione_corpo_pistola1;
                    ++tempiLavoro.sostituzione_tubo_pescaggio1;
                }
                // Pistola 2 in funzione
                if( Output & Elettrovalvola_pistola_2 )
                {
                    ++tempiLavoro.rotazione_ugello2;
                    ++tempiLavoro.sostituzione_ugello2;
                    ++tempiLavoro.sostituzione_ugello_iniettore2;
                    ++tempiLavoro.sostituzione_blocchetto_porta_ugello2;
                    ++tempiLavoro.sostituzione_corpo_pistola2;
                    ++tempiLavoro.sostituzione_tubo_pescaggio2;
                }
                ++tempiLavoro.sostituzione_puleggia_motore_pistola;
                ++tempiLavoro.sostituzione_puleggia_motore_nastro;
                ++tempiLavoro.sostituzione_cuscinetti;
                ++tempiLavoro.rotazione_corazza;
                ++tempiLavoro.sostituzione_corazza;
            }
        }

        if ( Timer_minuti ) --Timer_minuti;
        else
        {
            Timer_minuti = 60;

            Verifica_TempiLavoro();

            //flag.save_par_fr = 1;
            //flag.save_tempi  = 1;
        }

//        if ( flag.esegui_filtro == 1 )
//        {
//            if(Timer_pausa)
//            {
//                --Timer_pausa;
//                MANICH1_OFF();
//                MANICH2_OFF();
//                MANICH3_OFF();
//                Timer_lavoro = parametri->param.timer_lavoro;
//            }
//            else
//            {
//                if ( Timer_lavoro )
//                {
//                    --Timer_lavoro;
//                    QString text;
//                    text.sprintf("Manic %d", lastmanic);
//                    qDebug(text.toLatin1());
//                    switch ( lastmanic )
//                    {
//                    case 0: MANICH1_ON(); break;
//                    case 1: MANICH2_ON(); break;
//                    case 2: MANICH3_ON(); break;
//                    }
//                    if ( !Timer_lavoro )
//                    {
//                        ++lastmanic;
//                        if(lastmanic > 2) lastmanic = 0;
//                        Timer_pausa = parametri->param.timer_pausa;
//                    }
//                }
//            }
//        }
    }
    else if ( flag.in_manuale == 0 )
    {
        MANICH1_OFF();
        MANICH2_OFF();
        MANICH3_OFF();
    }

    /* -----------------19/06/2002 17.51-----------------
    * Timer ventola,vibratore,filtro
    * --------------------------------------------------*/
    if( flexe != GO && flag.in_manuale == 0)
    {
        if ( ( Input & Ventilatore_manuale) != 0 ) // && flag.win_start )
        {
            flag.esegui_filtro = 1;  /* 13.01.2006 */
            VENTOLA_ON();
            VIBRAT_ON();
            timeoff_filtro = 1;
        }
        else
        {
            if(timeoff_filtro  ) --timeoff_filtro;
            else
            {
                VENTOLA_OFF();
                PULNAST_OFF();
                VIBRAT_OFF();
            }
        }
    }
}

void Sabbiatrice::reset_all_timers()
{
    warningTempiLavorazione.val = 0;

    tempiLavoro.accensione                           = 0;
    tempiLavoro.lavorazione_parziale                 = 0;
    tempiLavoro.lavorazione_totale                   = 0;
    //tempiLavoro.release                              = 0;

    tempiLavoro.rotazione_ugello1                     = 0;
    tempiLavoro.rotazione_ugello2                     = 0;
    tempiLavoro.sostituzione_ugello1                  = 0;
    tempiLavoro.sostituzione_ugello2                  = 0;
    tempiLavoro.sostituzione_ugello_iniettore1        = 0;
    tempiLavoro.sostituzione_ugello_iniettore2        = 0;
    tempiLavoro.sostituzione_blocchetto_porta_ugello1 = 0;
    tempiLavoro.sostituzione_blocchetto_porta_ugello2 = 0;
    tempiLavoro.sostituzione_corpo_pistola1           = 0;
    tempiLavoro.sostituzione_corpo_pistola2           = 0;
    tempiLavoro.sostituzione_tubo_pescaggio1          = 0;
    tempiLavoro.sostituzione_tubo_pescaggio2          = 0;
    tempiLavoro.sostituzione_puleggia_motore_pistola  = 0;
    tempiLavoro.sostituzione_puleggia_motore_nastro   = 0;
    tempiLavoro.sostituzione_cuscinetti               = 0;
    tempiLavoro.rotazione_corazza                     = 0;
    tempiLavoro.sostituzione_corazza                  = 0;
    tempiLavoro.pulizia_manichette                    = 0;
    tempiLavoro.sostituzione_manichette               = 0;


    SalvaTempi();
}

void Sabbiatrice::run()
{

    Init_Var();
    QThread::msleep(500);
    Init_Hardware();

    Output = 0;
    Pressione_set = 0;
    GetRealase();

    Tx_buff[0] = 0;
    Tx_buff[1] = 0;
    Tx_buff[2] = 0;
    Tx_buff[3] = 0;
    Tx_buff[4] = 0;
    Tx_buff[5] = 0;
    Tx_buff[6] = 0;
    Tx_buff[7] = 0;

    FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_IO_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    QThread::msleep(100);

    Timer_funzionamento = 3600000;
    parametri->param.time_on_lamp    = 0;

    mRun     = true;
    dteStart = new QDateTime(QDateTime::currentDateTime());

    while(mRun)
    {
        QThread::usleep(980);
        Main_task(0);
    }

}

void Sabbiatrice::Init_Hardware(void)
{    
    canbus->start();
}

void Sabbiatrice::GetRealase(void)
{
    Tx_buff[0] = CMD_GET_RELEASE;
    Tx_buff[1] = 0;
    Tx_buff[2] = 0;
    Tx_buff[3] = 0;
    Tx_buff[4] = 0;
    Tx_buff[5] = 0;
    Tx_buff[6] = 0;
    Tx_buff[7] = 0;

    FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    QThread::msleep(100);

}
void Sabbiatrice::Init_Var(void)
{
    flexe 		= HALTED;
    Output 		= 0;

    veloy     		=  100;
    velox     		=  100;

    parametri = Parametri::getInstance();
    //parametri->param = Par;
    try
    {
        parametri->open();
    }
    catch(...){
        parametri->tempi.accensione              = 0;
        parametri->tempi.lavorazione_parziale    = 0;
        parametri->tempi.lavorazione_totale      = 0;
        parametri->tempi.pulizia_manichette      = 0;
        parametri->tempi.rotazione_corazza       = 0;
        parametri->tempi.rotazione_ugello1       = 0;
        parametri->tempi.rotazione_ugello2       = 0;
        parametri->tempi.sostituzione_blocchetto_porta_ugello1 = 0;
        parametri->tempi.sostituzione_blocchetto_porta_ugello2 = 0;
        parametri->tempi.sostituzione_corazza         = 0;
        parametri->tempi.sostituzione_corpo_pistola1  = 0;
        parametri->tempi.sostituzione_corpo_pistola2  = 0;
        parametri->tempi.sostituzione_cuscinetti      = 0;
        parametri->tempi.sostituzione_manichette      = 0;
        parametri->tempi.sostituzione_puleggia_motore_nastro   = 0;
        parametri->tempi.sostituzione_puleggia_motore_pistola  = 0;
        parametri->tempi.sostituzione_tubo_pescaggio1 = 0;
        parametri->tempi.sostituzione_tubo_pescaggio2 = 0;
        parametri->tempi.sostituzione_ugello1         = 0;
        parametri->tempi.sostituzione_ugello2         = 0;
        parametri->tempi.sostituzione_ugello_iniettore1 = 0;
        parametri->tempi.sostituzione_ugello_iniettore2 = 0;
        parametri->tempi.service = 0;
        parametri->param.timer_pausa = 0;
        parametri->param.timer_lavoro = 0;
    }

    ftempi = Tempi::getInstance();
    //parametri->param = Par;
    if(ftempi->open() == false)
    {
        ftempi->tempi.accensione                    = parametri->tempi.accensione;
        ftempi->tempi.lavorazione_parziale       = parametri->tempi.lavorazione_parziale;
        ftempi->tempi.lavorazione_totale      = parametri->tempi.lavorazione_totale;
        ftempi->tempi.pulizia_manichette      = parametri->tempi.pulizia_manichette;
        ftempi->tempi.rotazione_corazza       = parametri->tempi.rotazione_corazza;
        ftempi->tempi.rotazione_ugello1       = parametri->tempi.rotazione_ugello1;
        ftempi->tempi.rotazione_ugello2       = parametri->tempi.rotazione_ugello2;
        ftempi->tempi.sostituzione_blocchetto_porta_ugello1 = parametri->tempi.sostituzione_blocchetto_porta_ugello1;
        ftempi->tempi.sostituzione_blocchetto_porta_ugello2 = parametri->tempi.sostituzione_blocchetto_porta_ugello2;
        ftempi->tempi.sostituzione_corazza         = parametri->tempi.sostituzione_corazza;
        ftempi->tempi.sostituzione_corpo_pistola1  = parametri->tempi.sostituzione_corpo_pistola1;
        ftempi->tempi.sostituzione_corpo_pistola2  = parametri->tempi.sostituzione_corpo_pistola2;
        ftempi->tempi.sostituzione_cuscinetti      = parametri->tempi.sostituzione_cuscinetti;
        ftempi->tempi.sostituzione_manichette      = parametri->tempi.sostituzione_manichette;
        ftempi->tempi.sostituzione_puleggia_motore_nastro   = parametri->tempi.sostituzione_puleggia_motore_nastro;
        ftempi->tempi.sostituzione_puleggia_motore_pistola  = parametri->tempi.sostituzione_puleggia_motore_pistola;
        ftempi->tempi.sostituzione_tubo_pescaggio1 = parametri->tempi.sostituzione_tubo_pescaggio1;
        ftempi->tempi.sostituzione_tubo_pescaggio2 = parametri->tempi.sostituzione_tubo_pescaggio2;
        ftempi->tempi.sostituzione_ugello1         = parametri->tempi.sostituzione_ugello1;
        ftempi->tempi.sostituzione_ugello2         = parametri->tempi.sostituzione_ugello2;
        ftempi->tempi.sostituzione_ugello_iniettore1 = parametri->tempi.sostituzione_ugello_iniettore1;
        ftempi->tempi.sostituzione_ugello_iniettore2 = parametri->tempi.sostituzione_ugello_iniettore2;
        ftempi->tempi.service = parametri->tempi.service;
        //ftempi->param.timer_pausa = parametri->param.timer_pausa;
        //ftempi->param.timer_lavoro = parametri->param.timer_lavoro;

        ftempi->save();
        ftempi->open();

    }

    parametri->tempi.accensione              = ftempi->tempi.accensione;
    parametri->tempi.lavorazione_parziale    = ftempi->tempi.lavorazione_parziale;
    parametri->tempi.lavorazione_totale      = ftempi->tempi.lavorazione_totale;
    parametri->tempi.pulizia_manichette      = ftempi->tempi.pulizia_manichette;
    parametri->tempi.rotazione_corazza       = ftempi->tempi.rotazione_corazza;
    parametri->tempi.rotazione_ugello1       = ftempi->tempi.rotazione_ugello1;
    parametri->tempi.rotazione_ugello2       = ftempi->tempi.rotazione_ugello2;
    parametri->tempi.sostituzione_blocchetto_porta_ugello1 = ftempi->tempi.sostituzione_blocchetto_porta_ugello1;
    parametri->tempi.sostituzione_blocchetto_porta_ugello2 = ftempi->tempi.sostituzione_blocchetto_porta_ugello2;
    parametri->tempi.sostituzione_corazza         = ftempi->tempi.sostituzione_corazza;
    parametri->tempi.sostituzione_corpo_pistola1  = ftempi->tempi.sostituzione_corpo_pistola1;
    parametri->tempi.sostituzione_corpo_pistola2  = ftempi->tempi.sostituzione_corpo_pistola2;
    parametri->tempi.sostituzione_cuscinetti      = ftempi->tempi.sostituzione_cuscinetti;
    parametri->tempi.sostituzione_manichette      = ftempi->tempi.sostituzione_manichette;
    parametri->tempi.sostituzione_puleggia_motore_nastro   = ftempi->tempi.sostituzione_puleggia_motore_nastro;
    parametri->tempi.sostituzione_puleggia_motore_pistola  = ftempi->tempi.sostituzione_puleggia_motore_pistola;
    parametri->tempi.sostituzione_tubo_pescaggio1 = ftempi->tempi.sostituzione_tubo_pescaggio1;
    parametri->tempi.sostituzione_tubo_pescaggio2 = ftempi->tempi.sostituzione_tubo_pescaggio2;
    parametri->tempi.sostituzione_ugello1         = ftempi->tempi.sostituzione_ugello1;
    parametri->tempi.sostituzione_ugello2         = ftempi->tempi.sostituzione_ugello2;
    parametri->tempi.sostituzione_ugello_iniettore1 = ftempi->tempi.sostituzione_ugello_iniettore1;
    parametri->tempi.sostituzione_ugello_iniettore2 = ftempi->tempi.sostituzione_ugello_iniettore2;
    parametri->tempi.service = ftempi->tempi.service;

    //parametri->param.timer_pausa = ftempi->param.timer_pausa;
    //parametri->param.timer_lavoro = ftempi->param.timer_lavoro;

    Timer_pausa 	= parametri->param.timer_pausa;
    Timer_lavoro	= parametri->param.timer_lavoro;

    //Par = parametri->param;
    tempiLavoro = parametri->tempi;

    Num_prog_exe = parametri->param.programma_in_uso;

    //if( Par.unita_misura == UM_MM )  sUnita_misura = "mm";
    //else							 sUnita_misura = "in";

    Timer_send 				= 100;
    Timer_rx_azionamento    = 5000;

    flag.pausa_abilitata = 1;
    flag.in_pausa 		 = 0;
    flag.in_manuale      = 0;
    flag.home_mot2  	 = 1;
    flag.win_start		 = 0;
    flag.esegui_filtro   = 0;

    lastmanic = lastmanic < 0 ? 0 : lastmanic;
    lastmanic = lastmanic > 2 ? 2 : lastmanic;

    Timer_secondi = 0;
    Timer_minuti  = 0;

    parametri->param.tipo_macchina = 0;
    flag.save_par_fr  = 1;

    flag.seng_move_quota_mot1 = 0;
    flag.seng_move_quota_mot2 = 0;
    flag.send_velo_mot1 = 0;
    flag.send_velo_mot2 = 0;
    flag.send_quota_mot1 = 0;
    flag.send_quota_mot2 = 0;

    timeoff_filtro = 1;
}

void Sabbiatrice::AzzeraCronometro()
{
    QTime adessoT = QTime::currentTime();
    QDate adessoD = QDate::currentDate();

    if( dteStart != nullptr )
    {
        dteStart->setTime(adessoT);
        dteStart->setDate(adessoD);
        cronometro = 0;
    }
}

void Sabbiatrice::updateiot()
{        
    QFile file( "/home/root/Jobs.json");

    QJsonDocument jsonDoc;
    QJsonObject   jsonObj;

    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc = QJsonDocument::fromJson(saveData);
        jsonObj = jsonDoc.object();
        jsonObj["model"] = Modello;
        jsonDoc.setObject( jsonObj );

        QString topic = QString("/pezza/%1/read_jobs$").arg( serial_number );
        QString msg = topic + jsonDoc.toJson();

        iot->write_buff( msg.toLatin1() );
    }

}
void Sabbiatrice::updateiot_alarm()
{
    QFile file( "/home/root/Alarms.json");

    QJsonDocument jsonDoc;
    QJsonObject   jsonObj;

    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc = QJsonDocument::fromJson(saveData);
        jsonObj = jsonDoc.object();
        jsonObj["model"] = Modello;
        jsonDoc.setObject( jsonObj );

        QString topic = QString("/pezza/%1/alarms$").arg( serial_number );
        QString msg = topic + jsonDoc.toJson();

        iot->write_buff( msg.toLatin1() );
    }
}
void Sabbiatrice::updateiot_param()
{
    //*** parametri->save_timer("");

    QFile file( "/home/root/Timers.json");

    QJsonDocument jsonDoc;
    QJsonObject   jsonObj;

    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc = QJsonDocument::fromJson(saveData);
        jsonObj = jsonDoc.object();
        jsonObj["model"] = Modello;
        jsonDoc.setObject( jsonObj );

        QString topic = QString("/pezza/%1/timers$").arg( serial_number );
        QString msg = topic + jsonDoc.toJson();

        iot->write_buff( msg.toLatin1() );
    }
}

void Sabbiatrice::updateiot_status()
{
    QJsonDocument jsonDoc;
    QJsonObject   jsonObj;

    if( flexe == GO && flag.in_pausa == 0) jsonObj["status"]  = "run";
    else                                   jsonObj["status"]  = "stop";

    jsonObj["prog"]    = Prog.nome;
    jsonObj["release"] = RELEASE;
    jsonObj["model"]   = Modello;

    jsonDoc.setObject( jsonObj );

    QString topic = QString("/pezza/%1/status$").arg( serial_number );
    QString msg = topic + jsonDoc.toJson();

    iot->write_buff( msg.toLatin1() );
}

void Sabbiatrice::updateiot_maintenance()
{
    if( Tipo == MISTRAL )
    {
        //***parametri->save_timer("");

        QFile file( "/home/root/Maintenance.json");

        QJsonDocument jsonDoc;
        QJsonObject   jsonObj;

        if( file.exists() == true )
        {
            file.open(QFile::ReadOnly | QFile::Text );
            QByteArray saveData = file.readAll();
            file.close();

            jsonDoc = QJsonDocument::fromJson(saveData);
            jsonObj = jsonDoc.object();
            jsonObj["model"] = Modello;
            jsonDoc.setObject( jsonObj );

            QString topic = QString("/pezza/%1/maintenance$").arg( serial_number );
            QString msg   = topic + jsonDoc.toJson();

            iot->write_buff( msg.toLatin1() );
        }
    }
}

void Sabbiatrice::iot_dara_receive(QByteArray rx)
{
    QString stringrx = QString(rx);
    QString topic;
    QString payload;

    if( rx.contains( "$") )
    {
        QStringList list = stringrx.split("$");
        topic   = list.at(0);
        payload = list.at(1);

        if( topic.contains( serial_number ) )
        {
            // il messaggio è mio
            if( topic.contains("write_jobs") )
            {
                QJsonDocument jsonDoc;

                QJsonObject   obj;
                jsonDoc = QJsonDocument::fromJson(payload.toLatin1());

                QFile file( "/home/root/Jobs.json");

                file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
                file.write( payload.toLatin1());
                file.close();

                QProcess *proc = new QProcess(this);
                proc->start("sync");
                proc->waitForFinished(10000);
                delete proc;
            }
            if( topic.contains("command"))
            {
                if( payload.contains("Alarms"))
                {
                    // reset del log allarmi
                    ReportAllarmi* allarmi = ReportAllarmi::getInstance();
                    allarmi->clear();
                }
                if( payload.contains("Service"))
                {
                    // reset dell' errore di  service
                    tempiLavoro.service = 0;
                    warningTempiLavorazione.type.service = 0;
                    SalvaTempi();
                }
                if( payload.contains("Status"))
                {
                    // Richiesta dello stato macchina
                    updateiot_status();
                }
            }
        }
    }
}

void Sabbiatrice::Main_task(uint32_t initial_data)
{
    Esegui_Comandi_Manuali();
    //ATTESA START MANUALE

    //if( Timeout ) 				--Timeout;
    if( Timer_rx_azionamento )  --Timer_rx_azionamento;
    //if( Timer_test_out )		--Timer_test_out;
    //if( Timer_test_pressione )  --Timer_test_pressione;
    if( Timer_send ) 			--Timer_send;

    if( flexe != GO ) --Timer_funzionamento;
    else                Timer_funzionamento = 3600000;

    if( Timer_funzionamento == 0 )
    {
        Timer_funzionamento = 3600000;
        flag.disable_mot    = 1;
    }

    Verifica_Allarmi();
    Verifica_TempiLavoro(); //+++

    if( flag.send_home_mot2 )
    {
        flag.send_home_mot2   = 0;
        flag.mot_disabilitato = 0;
        Tx_buff[0] = CMD_HOME_MOT2;
        Tx_buff[1] = 0;
        Tx_buff[2] = 0;
        Tx_buff[3] = 0;
        Tx_buff[4] = 0;
        Tx_buff[5] = 0;
        Tx_buff[6] = 0;
        Tx_buff[7] = 0;
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    }
    else if( flag.send_reset_alarm_mot1 )
    {
        flag.send_reset_alarm_mot1 = 0;
        flag.mot_disabilitato      = 0;
        Tx_buff[0] = CMD_RESET_ALARM_MOT1;
        Tx_buff[1] = 0;
        Tx_buff[2] = 0;
        Tx_buff[3] = 0;
        Tx_buff[4] = 0;
        Tx_buff[5] = 0;
        Tx_buff[6] = 0;
        Tx_buff[7] = 0;
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
        QThread::usleep(50000);
        mot1_in_allarme = false;
        Allarmi = 0;
        Allarmi_old = 0;
    }
    else if( flag.send_reset_alarm_mot2 )
    {
        flag.send_reset_alarm_mot2 = 0;
        Tx_buff[0] = CMD_RESET_ALARM_MOT2;
        Tx_buff[1] = 0;
        Tx_buff[2] = 0;
        Tx_buff[3] = 0;
        Tx_buff[4] = 0;
        Tx_buff[5] = 0;
        Tx_buff[6] = 0;
        Tx_buff[7] = 0;
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
        QThread::usleep(50000);
        mot_pistola_in_allarme = false;
        Allarmi = 0;
        Allarmi_old = 0;
    }
    else if ( flag.seng_move_quota_mot1 ) {
        flag.seng_move_quota_mot1 = 0;
        // L' azionamento risponde con fatto al raggiungimento di Quota_fatto_mot1_set
        Tx_buff[0] = CMD_MOVE_QUOTA_MOT1;
        Tx_buff[1] = (char)(Quota_mot1_set >> 24);
        Tx_buff[2] = (char)(Quota_mot1_set >> 16);
        Tx_buff[3] = (char)(Quota_mot1_set >>  8);
        Tx_buff[4] = (char)(Quota_mot1_set & 0x00ff);
        Tx_buff[5] = (char)(Velo_mot1_set >> 8);
        Tx_buff[6] = (char)(Velo_mot1_set & 0x00ff);
        Tx_buff[7] = (char)(Quota_fatto_mot1_set);
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    }
    else if( flag.seng_move_quota_mot2 ) {
        flag.seng_move_quota_mot2 = 0;
        // L' azionamento risponde con fatto al raggiungimento di Quota_fatto_mot2_set
        Tx_buff[0] = CMD_MOVE_QUOTA_MOT2;
        Tx_buff[1] = (char)(Quota_mot2_set >> 24);
        Tx_buff[2] = (char)(Quota_mot2_set >> 16);
        Tx_buff[3] = (char)(Quota_mot2_set >>  8);
        Tx_buff[4] = (char)(Quota_mot2_set & 0x00ff);
        Tx_buff[5] = (char)(Velo_mot2_set >> 8);
        Tx_buff[6] = (char)(Velo_mot2_set & 0x00ff);
        Tx_buff[7] = (char)(Quota_fatto_mot2_set);
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    }
    else if( flag.send_quota_mot1) {
        flag.send_quota_mot1 = 0;
        Tx_buff[0] = CMD_SET_QUOTA_MOT1;
        Tx_buff[1] = (char)(Quota_mot1_set >> 24);
        Tx_buff[2] = (char)(Quota_mot1_set >> 16);
        Tx_buff[3] = (char)(Quota_mot1_set >>  8);
        Tx_buff[4] = (char)(Quota_mot1_set & 0x00ff);
        Tx_buff[5] = 0;
        Tx_buff[6] = 0;
        Tx_buff[7] = 0;
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    }
    else if( flag.send_quota_mot2) {
        flag.send_quota_mot2 = 0;
        Tx_buff[0] = CMD_SET_QUOTA_MOT2;
        Tx_buff[1] = (char)(Quota_mot2_set >> 24);
        Tx_buff[2] = (char)(Quota_mot2_set >> 16);
        Tx_buff[3] = (char)(Quota_mot2_set >>  8);
        Tx_buff[4] = (char)(Quota_mot2_set & 0x00ff);
        Tx_buff[5] = 0;
        Tx_buff[6] = 0;
        Tx_buff[7] = 0;
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    }
    else if( flag.send_velo_mot1 ) {
        flag.send_velo_mot1 = 0;
        // Per fermare il motore si manda la velocita a 0
        // con Byte 3 a 1 ilmotore resta abilitato, a 0 lo disabilita
        Tx_buff[0] = CMD_MOVE_VELO_MOT1;
        Tx_buff[1] = (char)(Velo_mot1_set >> 8);
        Tx_buff[2] = (char)(Velo_mot1_set & 0x00ff);
        Tx_buff[3] = 0;
        Tx_buff[4] = 0;
        Tx_buff[5] = 0;
        Tx_buff[6] = 0;
        Tx_buff[7] = 0;
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    }
    else if( flag.send_velo_mot2 ) {
        flag.send_velo_mot2 = 0;
        // Per fermare il motore si manda la velocita a 0
        // con Byte 3 a 1 ilmotore resta abilitato, a 0 lo disabilita
        Tx_buff[0] = CMD_MOVE_VELO_MOT2;
        Tx_buff[1] = (char)(Velo_mot2_set >> 8);
        Tx_buff[2] = (char)(Velo_mot2_set & 0x00ff);
        Tx_buff[3] = 0;
        Tx_buff[4] = 0;
        Tx_buff[5] = 0;
        Tx_buff[6] = 0;
        Tx_buff[7] = 0;
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    }
    else if( Timer_send == 0)
    {
        Timer_send = 50;
        Tx_buff[0] = (uchar)(Output >> 8);
        Tx_buff[1] = (uchar)(Output & 0x00ff);
        Tx_buff[2] = Pressione_set;
        Tx_buff[3] = 0;
        Tx_buff[4] = 0;
        Tx_buff[5] = 0;
        Tx_buff[6] = 0;
        Tx_buff[7] = 0;
        /*
        QString text;
        text.sprintf("Input %d", Output);
        qDebug(text.toLatin1());
        */
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_IO_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    }
    else if( flag.disable_mot )
    {
        flag.disable_mot 		 = 0;
        flag.mot_disabilitato = 1;
        Tx_buff[0] = CMD_DISABLE_ALL_MOT;
        Tx_buff[1] = 0;
        Tx_buff[2] = 0;
        Tx_buff[3] = 0;
        Tx_buff[4] = 0;
        Tx_buff[5] = 0;
        Tx_buff[6] = 0;
        Tx_buff[7] = 0;
        FLEXCAN_Tx_message( CAN_DEVICE, MB_TX_AZ_PEZZA , ID_CMD_PEZZA , FLEXCAN_STANDARD, 8, Tx_buff);
    }

}
void Sabbiatrice::Esegui_Comandi_Manuali()
{
    static int manold = -1;
    static bool xmoving = false;
    static bool ymoving = false;

    if(flexe == GO){ //flag.in_manuale == 0)
        return;
    }

    if((Input & Pulsante_nastro_avanti) != 0 && vis_allarme == false)
    {
        manuale = 1;
    }
    else if((Input & Pulsante_nastro_indietro) != 0 && vis_allarme == false)
    {
        manuale = 2;
    }
    else if(xmoving)
    {
        manuale = 0;
    }
    if(( Input & FC_Vetro) == 0)
    {
        manuale = 0;
    }



    //QThread::msleep(2);

    /* -----------------28/06/2002 12.39-----------------
     * Verifica cambio stato manuale
     * --------------------------------------------------*/
    if ( manuale != manold )
    {
        manold = manuale;
        switch ( manuale )
        {
        case 0:
            // TUTTO FERMO
            PULNAST_OFF();
            Stop_Mot_Vel( MOT_X ,0 );
            Stop_Mot_Vel( MOT_Y ,0 );
            xmoving = false;
            ymoving = false;

            break;
        case  1:
            // NASTRO AVANTI
            if ( xmoving )
            {
                Stop_Mot_Vel( MOT_X ,0 );
                QThread::msleep(20);
            }
            Move_Mot_Vel( MOT_X, (short)(VELMAX_X*velox/100) );
            xmoving = true;
            if ( ( Input & Ventilatore_manuale)!= 0 ) PULNAST_ON();
            break;
        case  2:
            // NASTRO INDIETRO
            if ( xmoving )
            {
                Stop_Mot_Vel( MOT_X ,0 );
                QThread::msleep(20);
            }
            Move_Mot_Vel( MOT_X, -(short)(VELMAX_X*velox/100) );
            xmoving = true;
            if ( ( Input & Ventilatore_manuale)!= 0  ) PULNAST_ON();
            break;
        case  3:
            // PISTOLA SU
            manold = manuale;
            if ( ymoving )
            {
                Stop_Mot_Vel( MOT_Y , 0 );
                QThread::msleep(20);
            }
            Move_Mot_Vel( MOT_Y, (short)(VELMAX_Y*velox/100) );
            ymoving = true;
            break;
        case  4:
            // PISTOLA GIU
            manold = manuale;
            if ( ymoving ) {
                Stop_Mot_Vel( MOT_Y , 0 );
                QThread::msleep(20);
            }
            Move_Mot_Vel( MOT_Y, -(short)(VELMAX_Y*velox/100) );
            ymoving = true;
            break;
        }
    }

}

void Sabbiatrice::Esegui_Sabbiatura(u_int32_t initial_data)
{

}

void Sabbiatrice::Set_Mot_Zero(int mot)
{
    if( mot == MOT_X) {
        Quota_mot1_set = 0;
        flag.send_quota_mot1 = 1;
    }
    else {
        Quota_mot2_set = 0;
        flag.send_quota_mot2 = 1;
    }
}

void Sabbiatrice::Move_Mot_Pos( int nmot, int32_t quota , char quota_ala, int16_t vel)
{
    if( nmot == MOT_X)
    {
        if( abs( Quota_mot1 - quota ) < quota_ala )  return;

        if( quota == 0 ) quota = 1;	// andare a zero ci sono problemi, boh

        Quota_mot1_set = quota;

        if(      Quota_mot1_set > Quota_mot1 ) Quota_fatto_mot1_set = quota_ala;
        else if( Quota_mot1_set < Quota_mot1 ) Quota_fatto_mot1_set = -quota_ala;

        Velo_mot1_set  = (int16_t)vel;
        flag.seng_move_quota_mot1 = 1;
        QThread::msleep(50);

        while( !( Status_mot_nastro & Quota_raggiunta))
        {
            if( flag.stop_esegui ) break;
            if( Allarmi )          break;
            QThread::msleep(5);

            // se non raggiunge la quota l'azionamento, verifico io
            if( abs( Quota_mot1 - quota ) < quota_ala ) break;

        }
    }
    else
    {
        if( quota_ala < RANGE_QUOTA_FATTO_Y ) quota_ala = 2;
        if( abs( Quota_mot2 - quota ) < quota_ala )  return;

        Quota_mot2_set = quota;

        if(      Quota_mot2_set > Quota_mot2 ) Quota_fatto_mot2_set = quota_ala;
        else if( Quota_mot2_set < Quota_mot2 ) Quota_fatto_mot2_set = -quota_ala;

        Velo_mot2_set  = (int16_t)vel;
        flag.seng_move_quota_mot2 = 1;
        QThread::msleep(200);
        int conta = 0;
        qDebug("Muovo motore 2");

        while(!(Status_mot_pistola & Quota_raggiunta))
        {
            if( flag.stop_esegui ) break;
            if( Allarmi )          break;
            QThread::msleep(10);
            //QString text;
            //text.sprintf("Quota_mot2 :  %d", canbus->Quota_mot2);
            //qDebug(text.toLatin1());
            // sale
            /*
            if(Quota_fatto_mot2_set > 0){
                if(canbus->Quota_mot2 > Quota_mot2_set){
                    qDebug("Sopra");
                    break;
                 }

            }
            if(Quota_fatto_mot2_set < 0){
                if(canbus->Quota_mot2 < Quota_mot2_set){
                    qDebug("Sotto");
                    break;
                 }

            }
            */
            conta++;
            if(conta > 1000 && abs(Quota_mot2 - quota) < (quota / 10) )
            {
                qDebug("Break");
                break;
            }
        }
        QThread::msleep(10);
        //qDebug("Arrivato");

        //QString text;
        //text.sprintf("Fatto Quota_mot2 :  %d", canbus->Quota_mot2);
        //qDebug(text.toLatin1());
    }

}

void Sabbiatrice::Move_Mot_Vel( int nmot, int16_t vel)
{
    if( nmot == MOT_X) {
        Velo_mot1_set = vel;
        flag.send_velo_mot1 = 1;
    }
    else {
        Velo_mot2_set = vel;
        flag.send_velo_mot2 = 1;
    }
}

void Sabbiatrice::Stop_Mot_Vel( int nmot, int en_ds )
{
    if( nmot == MOT_X)
    {
        Velo_mot1_set = 0;
        flag.send_velo_mot1 = 1;
    }
    else
    {
        Velo_mot2_set = 0;
        flag.send_velo_mot2 = 1;
    }
}
void Sabbiatrice::Move_AsseY_To_Zero(void)
{
    Move_Mot_Pos( MOT_Y , parametri->param.ofsety , RANGE_QUOTA_FATTO_Y , VELMAX_Y);
}
void Sabbiatrice::Set_Pressione( float bar)
{
    //	if( Par.unita_misura == UM_MM) {
    //		bar = bar * 10;
    //	}
    //	else {
    //		bar = bar / 1.45f;
    //	}
    bar = bar * 10;
    Pressione_set = (uchar)bar;
}
void Sabbiatrice::Home_Pistola(void)
{
    flag.send_home_mot2 = 1;
    while(Status_mot_pistola & BIT_2)
    {
        // Aspetto che parte l'home
        if( Allarmi ) break;
        QThread::msleep(100);
    }
    while(!(Status_mot_pistola & BIT_2) )
    { //!MOT2_HOME_OK ) {
        // Aspetto che sia finito l'home
        if( Allarmi ) break;
        QThread::msleep(100);
    }
    QThread::msleep(1000);
    Set_Mot_Zero(MOT_Y);
    flag.home_mot2 = 0;
    QThread::msleep(1000);

}

int Sabbiatrice::Verifica_Allarmi(void)
{
    static int Allarme_old = -1;

    if( win_start == 0 ) return 0;

    if      ( ( Input & Emergenza ) == 0  )                           Allarmi = 1;
    else if ( ( Input & FC_Portello) == 0 && flag.in_pausa == 0 	) Allarmi = 5;    
    else if ( ( Input & Termico_ventilatore) 	== 0    			) Allarmi = 2;
    else if ( ( Input & Termico_vibratore) 	    == 0    			) Allarmi = 3;
    else if(  ( Status_mot_nastro & Stato_motore)                   ) Allarmi = 7;
    else if( mot_pistola_in_allarme )                                 Allarmi = 8;
    else                                                              Allarmi = 0;

    if ( ( Input & FC_Vetro) == 0 ) Warning = 6;
    else                            Warning = 0;

    if(  Timer_rx_azionamento == 0)
    {
        Allarmi = 10;
    }

//    if(Allarmi != Allarme_old)
//    {
//        QString text;
//        text.sprintf("Allarmi %d", Allarmi);
//        qDebug(text.toLatin1());
//    }

    //if( Warning ) flag.in_pausa = 1;

    Allarme_old = Allarmi;
    if( (( Input & FC_Portello) == 0) && flag.mot_disabilitato == 0 && flag.in_pausa == 0  ) flag.disable_mot = 1;

    return 0;
}

int Sabbiatrice::Verifica_TempiLavoro()
{
    static uint32_t old = 0 ;

    uFlagTempiLavorazione tmp;
    tmp.val = 0;

    if( tempiLavoro.rotazione_ugello1                     >   54000 ) tmp.type.rotazione_ugello1 = 1; // 15 ore                         // code 8.1
    if( tempiLavoro.rotazione_ugello2                     >   54000 ) tmp.type.rotazione_ugello2 = 1; // 15 ore                         // code 8.1.1
    if( tempiLavoro.sostituzione_ugello1                  >  216000 ) tmp.type.sostituzione_ugello1 = 1; // 80 ore                      // code 8.2
    if( tempiLavoro.sostituzione_ugello2                  >  216000 ) tmp.type.sostituzione_ugello2 = 1; // 80 ore                      // code 8.2.1
    if( tempiLavoro.sostituzione_ugello_iniettore1        > 1800000 ) tmp.type.sostituzione_ugello_iniettore1 = 1; // 500 ore           // code 8.3
    if( tempiLavoro.sostituzione_ugello_iniettore2        > 1800000 ) tmp.type.sostituzione_ugello_iniettore2 = 1; // 500 ore           // code 8.3.1
    if( tempiLavoro.sostituzione_blocchetto_porta_ugello1 > 1440000 ) tmp.type.sostituzione_blocchetto_porta_ugello1 = 1; // 400 ore    // code 8.4
    if( tempiLavoro.sostituzione_blocchetto_porta_ugello2 > 1440000 ) tmp.type.sostituzione_blocchetto_porta_ugello2 = 1; // 400 ore    // code 8.4.1
    if( tempiLavoro.sostituzione_corpo_pistola1           > 1800000 ) tmp.type.sostituzione_corpo_pistola1 = 1; // 500 ore              // code 8.5
    if( tempiLavoro.sostituzione_corpo_pistola2           > 1800000 ) tmp.type.sostituzione_corpo_pistola2 = 1; // 500 ore              // code 8.5.1
    if( tempiLavoro.sostituzione_tubo_pescaggio1          > 1260000 ) tmp.type.sostituzione_tubo_pescaggio1 = 1; // 350 ore             // code 8.6
    if( tempiLavoro.sostituzione_tubo_pescaggio2          > 1260000 ) tmp.type.sostituzione_tubo_pescaggio2 = 1; // 350 ore             // code 8.6.1
    if( tempiLavoro.sostituzione_puleggia_motore_pistola  > 2520000 ) tmp.type.sostituzione_puleggia_motore_pistola = 1; // 700 ore     // code 8.7
    if( tempiLavoro.sostituzione_puleggia_motore_nastro   > 2520000 ) tmp.type.sostituzione_puleggia_motore_nastro = 1; // 700 ore      // code 8.8
    if( tempiLavoro.sostituzione_cuscinetti               > 1800000 ) tmp.type.sostituzione_cuscinetti = 1; // 500 ore                  // code 8.9
    if( tempiLavoro.rotazione_corazza                     > 1350000 ) tmp.type.rotazione_corazza = 1; // 375 ore                        // code 8.10
    if( tempiLavoro.sostituzione_corazza                  > 5400000 ) tmp.type.sostituzione_corazza = 1; // 1500 ore                    // code 8.10.1
    if( tempiLavoro.pulizia_manichette                    > 1080000 ) tmp.type.pulizia_manichette = 1; // 300 ore                       // code 8.12
    if( tempiLavoro.sostituzione_manichette               >12600000 ) tmp.type.sostituzione_manichette = 1; // 3500 ore                 // code 8.12.1
    if( tempiLavoro.service                               > 3600000 ) tmp.type.service = 1; // 1000 ore


   // tmp.type.rotazione_ugello1 = 1;

    warningTempiLavorazione.val = tmp.val;

    if( warningTempiLavorazione.val != old )
    {
        Maintenance* manutenzioni = Maintenance::GetInstance();

        if( warningTempiLavorazione.type.rotazione_ugello1  ) manutenzioni->add( "8.1" );
        if( warningTempiLavorazione.type.rotazione_ugello2  ) manutenzioni->add( "8.1.1" );
        if( warningTempiLavorazione.type.sostituzione_ugello1 ) manutenzioni->add( "8.2" );
        if( warningTempiLavorazione.type.sostituzione_ugello2 ) manutenzioni->add( "8.2.1" );
        if( warningTempiLavorazione.type.sostituzione_ugello_iniettore1 ) manutenzioni->add( "8.3" );
        if( warningTempiLavorazione.type.sostituzione_ugello_iniettore2 ) manutenzioni->add( "8.3.1" );
        if( warningTempiLavorazione.type.sostituzione_blocchetto_porta_ugello1 ) manutenzioni->add( "8.4");
        if( warningTempiLavorazione.type.sostituzione_blocchetto_porta_ugello2 ) manutenzioni->add( "8.4.1");
        if( warningTempiLavorazione.type.sostituzione_corpo_pistola1 ) manutenzioni->add("8.5");
        if( warningTempiLavorazione.type.sostituzione_corpo_pistola2 ) manutenzioni->add("8.5.1");
        if( warningTempiLavorazione.type.sostituzione_tubo_pescaggio1 ) manutenzioni->add("8.6");
        if( warningTempiLavorazione.type.sostituzione_tubo_pescaggio2 ) manutenzioni->add("8.6.1");
        if( warningTempiLavorazione.type.sostituzione_puleggia_motore_pistola ) manutenzioni->add("8.7");
        if( warningTempiLavorazione.type.sostituzione_puleggia_motore_nastro ) manutenzioni->add("8.8");
        if( warningTempiLavorazione.type.sostituzione_cuscinetti ) manutenzioni->add("8.9");
        if( warningTempiLavorazione.type.rotazione_corazza ) manutenzioni->add("8.10");
        if( warningTempiLavorazione.type.sostituzione_corazza ) manutenzioni->add("8.10.1");
        if( warningTempiLavorazione.type.pulizia_manichette ) manutenzioni->add("8.12");
        if( warningTempiLavorazione.type.sostituzione_manichette ) manutenzioni->add("8.12.1");

        old = warningTempiLavorazione.val;

        updateiot_maintenance();
    }

    return 1;
}

void Sabbiatrice::SalvaTempi(void)
{
    //parametri->param = parametri->param;
    parametri->tempi = tempiLavoro ;
    ftempi->tempi.accensione                    = parametri->tempi.accensione;
    ftempi->tempi.lavorazione_parziale          = parametri->tempi.lavorazione_parziale;
    ftempi->tempi.lavorazione_totale            = parametri->tempi.lavorazione_totale;
    ftempi->tempi.pulizia_manichette            = parametri->tempi.pulizia_manichette;
    ftempi->tempi.rotazione_corazza             = parametri->tempi.rotazione_corazza;
    ftempi->tempi.rotazione_ugello1             = parametri->tempi.rotazione_ugello1;
    ftempi->tempi.rotazione_ugello2             = parametri->tempi.rotazione_ugello2;
    ftempi->tempi.sostituzione_blocchetto_porta_ugello1 = parametri->tempi.sostituzione_blocchetto_porta_ugello1;
    ftempi->tempi.sostituzione_blocchetto_porta_ugello2 = parametri->tempi.sostituzione_blocchetto_porta_ugello2;
    ftempi->tempi.sostituzione_corazza         = parametri->tempi.sostituzione_corazza;
    ftempi->tempi.sostituzione_corpo_pistola1  = parametri->tempi.sostituzione_corpo_pistola1;
    ftempi->tempi.sostituzione_corpo_pistola2  = parametri->tempi.sostituzione_corpo_pistola2;
    ftempi->tempi.sostituzione_cuscinetti      = parametri->tempi.sostituzione_cuscinetti;
    ftempi->tempi.sostituzione_manichette      = parametri->tempi.sostituzione_manichette;
    ftempi->tempi.sostituzione_puleggia_motore_nastro   = parametri->tempi.sostituzione_puleggia_motore_nastro;
    ftempi->tempi.sostituzione_puleggia_motore_pistola  = parametri->tempi.sostituzione_puleggia_motore_pistola;
    ftempi->tempi.sostituzione_tubo_pescaggio1 = parametri->tempi.sostituzione_tubo_pescaggio1;
    ftempi->tempi.sostituzione_tubo_pescaggio2 = parametri->tempi.sostituzione_tubo_pescaggio2;
    ftempi->tempi.sostituzione_ugello1         = parametri->tempi.sostituzione_ugello1;
    ftempi->tempi.sostituzione_ugello2         = parametri->tempi.sostituzione_ugello2;
    ftempi->tempi.sostituzione_ugello_iniettore1 = parametri->tempi.sostituzione_ugello_iniettore1;
    ftempi->tempi.sostituzione_ugello_iniettore2 = parametri->tempi.sostituzione_ugello_iniettore2;
    ftempi->tempi.service = parametri->tempi.service;
    ftempi->save();
}

void Sabbiatrice::FLEXCAN_Tx_message(int device, int mb_tx_av_altezza , int id_cmd_pezza , int flexcan_standard, int len, uchar *buff)
{
    can_frame tx;

    tx.can_dlc = len;
    tx.can_id  = id_cmd_pezza;
    tx.data[0] = buff[0];
    tx.data[1] = buff[1];
    tx.data[2] = buff[2];
    tx.data[3] = buff[3];
    tx.data[4] = buff[4];
    tx.data[5] = buff[5];
    tx.data[6] = buff[6];
    tx.data[7] = buff[7];

    canbus->write_frame(tx);
}

void Sabbiatrice::SetInManuale(bool man)
{
    flag.in_manuale = man;
}
void Sabbiatrice::SetManuale(u_int16_t cmd)
{
    manuale = cmd;
}
void Sabbiatrice::Ventola(bool on)
{
    (on) ? VENTOLA_ON() : VENTOLA_OFF();
}
void Sabbiatrice::Vibratore(bool on)
{
    (on) ? VIBRAT_ON() : VIBRAT_OFF();
}
void Sabbiatrice::Manichetta(int num,bool on)
{
    switch(num){
    case 1:
        (on) ? MANICH1_ON() : MANICH1_OFF();
        break;
    case 2:
        (on) ? MANICH2_ON() : MANICH2_OFF();
        break;
    case 3:
        (on) ? MANICH3_ON() : MANICH3_OFF();
        break;
    }
}
void Sabbiatrice::Pistola(int num,bool on)
{
    switch(num){
    case 1:
        (on) ? PISTOLA1_ON() : PISTOLA1_OFF();
        break;
    case 2:
        (on) ? PISTOLA2_ON() : PISTOLA2_OFF();
        break;
    case 3:
        (on) ? PISTOLA2_ON() : PISTOLA2_OFF();
        break;
    }
}


