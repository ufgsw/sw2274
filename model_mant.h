#ifndef MODEL_MANT_H
#define MODEL_MANT_H


const int COLS_M= 3;
const int ROWS_M= 30;

#include <QAbstractTableModel>

class model_mant : public QAbstractTableModel
{
public:


    QString m_gridData[ROWS_M][COLS_M];  //holds text entered into QTableView


    model_mant(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role)const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

};

#endif // MODEL_MANT_H
