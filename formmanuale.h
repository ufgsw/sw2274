#ifndef FORMMANUALE_H
#define FORMMANUALE_H

#include <QDialog>
#include "sabbiatrice.h"

namespace Ui {
class formmanuale;
}

class formmanuale : public QDialog
{
    Q_OBJECT

public:
    Sabbiatrice *sabbiatrice;
    explicit formmanuale(QWidget *parent = 0);
    void Init(QString n_pistole,QString tipo);
    ~formmanuale();

private slots:
    void on_buttonExit_clicked();

    void on_btnPistolaDw_clicked();

    void on_btnPistolaUp_clicked();

    void on_btnNastroInd_clicked();

    void on_btnNastroAv_clicked();

    void on_btnNastroAv_pressed();

    void on_btnNastroAv_released();

    void on_btnNastroInd_pressed();

    void on_btnNastroInd_released();


    void on_btn1_clicked();

    void on_buttonExit_3_clicked();

    void on_btn1_pressed();

    void on_btn1_released();

    void on_btn2_pressed();

    void on_btn2_released();

    void on_btn3_pressed();

    void on_btn3_released();

    void on_btn4_pressed();

    void on_btn4_released();

    void on_btn5_pressed();

    void on_btn5_released();

    void on_btn6_pressed();

    void on_btn6_released();

    void on_btn7_pressed();

    void on_btn7_released();



private:

    bool        pressed[8];

    float GetNumber(QString title,u_int16_t min,u_int16_t max,bool asinteger);
    Ui::formmanuale *ui;
};

#endif // FORMMANUALE_H
