#ifndef FRMPLAYEXEC_H
#define FRMPLAYEXEC_H

#include <QDialog>
#include <QTimer>
#include <eseguisabbiatura.h>
#include "programmi.h"
#include "frmplaymain.h"
#include "frmplaytimer.h"

namespace Ui {
class frmPlayExec;
}

class frmPlayExec : public QDialog
{
    Q_OBJECT

public:
    int result;

    EseguiSabbiatura *esegui_sabbiatura;
    void SetNomeProgramma(QString nome, short tipo);
    explicit frmPlayExec(QWidget *parent = 0);
    ~frmPlayExec();

private slots:
    void processTimer();

    void on_btnFine_clicked();

    void on_btnStart_clicked();

    void on_btnPausa_clicked();

private:
    bool avvio;
    bool fine;
    Ui::frmPlayExec *ui;

    QTimer  *timer;

    frmPlayMain*  uiPlayMain;
    frmPlayTimer* uiPlayTimer;

    Programmi* progs;
    Commesse*  commesse;

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);
    void showEvent(QShowEvent *event);
};

#endif // FRMPLAYEXEC_H
