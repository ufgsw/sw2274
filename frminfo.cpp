#include "frminfo.h"
#include "ui_frminfo.h"
#include "formtastieraa.h"
#include "formtastietan.h"
#include "release.h"

frmInfo::frmInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmInfo)
{
    ui->setupUi(this);
    ui->tbProg->setModel(&myModelInfo);
    ui->tbProg->setColumnWidth(0,390);
    ui->tbProg->setColumnWidth(1,390);
    for(int i=0;i < ROWS_I;i++){
        ui->tbProg->setRowHeight(i,6);
    }

    config = Config::getInstance();

    reset_service = false;
    reset_timers  = false;
/*
    myModel.m_gridData[0][0] = "1";
    myModel.m_gridData[0][1] = "Programma 1";


    myModel.m_gridData[1][0] = "2";
    myModel.m_gridData[1][1] = "Programma 2";
*/

}

frmInfo::~frmInfo()
{
    delete ui;
}

void frmInfo::Init(bool edit)
{
    for( int r=0;r < ROWS_I;r++ )
    {
        for( int c=0;c < COLS_I;c++ )
        {
            myModelInfo.m_gridData[r][c] =  "";
        }
    }

    myModelInfo.m_gridData[0][0] =  "Model";
    myModelInfo.m_gridData[1][0] =  "S/N";
    myModelInfo.m_gridData[2][0] =  "Year";
    myModelInfo.m_gridData[3][0] =  "Manufacturer";
    myModelInfo.m_gridData[4][0] =  "Automatic guns";
    myModelInfo.m_gridData[5][0] =  "UM";
    myModelInfo.m_gridData[6][0] =  "SW Rel.";
    myModelInfo.m_gridData[7][0] =  "Voltage";
    myModelInfo.m_gridData[8][0] =  "Hz";
    myModelInfo.m_gridData[9][0] =  "Amperage";
    myModelInfo.m_gridData[10][0] =  "kW";
    myModelInfo.m_gridData[11][0] =  "Bar";
    myModelInfo.m_gridData[12][0] =  "kg";
    //myModelInfo.m_gridData[13][0] =  "Always home";

    myModelInfo.m_gridData[0][1] =  config->modello;
    myModelInfo.m_gridData[1][1] =  config->sn;
    myModelInfo.m_gridData[2][1] =  config->Year;
    myModelInfo.m_gridData[3][1] =  config->Manufacturer;
    myModelInfo.m_gridData[4][1] =  config->Automatic_Guns;
    myModelInfo.m_gridData[5][1] =  config->UM;
    myModelInfo.m_gridData[6][1] =  release_fw;
    myModelInfo.m_gridData[7][1] =  config->Volt;
    myModelInfo.m_gridData[8][1] =  config->Hz;
    myModelInfo.m_gridData[9][1] =  config->A;
    myModelInfo.m_gridData[10][1] =  config->kW;
    myModelInfo.m_gridData[11][1] =  config->Bar;
    myModelInfo.m_gridData[12][1] =  config->Weigh;
    //myModelInfo.m_gridData[13][1] =  progs->DoHoming;



    okEdit = edit;

    if(!edit){
    }
    else{
    }

}


void frmInfo::on_buttonExit_3_clicked()
{

    config->modello  = myModelInfo.m_gridData[0][1];
    config->sn       = myModelInfo.m_gridData[1][1];
    config->Year = myModelInfo.m_gridData[2][1];
    config->Manufacturer = myModelInfo.m_gridData[3][1];
    config->Automatic_Guns = myModelInfo.m_gridData[4][1];
    config->UM = myModelInfo.m_gridData[5][1];
    //progs->release = myModelInfo.m_gridData[6][1];
    config->Volt = myModelInfo.m_gridData[7][1];
    config->Hz = myModelInfo.m_gridData[8][1];
    config->A  = myModelInfo.m_gridData[9][1];
    config->kW = myModelInfo.m_gridData[10][1];
    config->Bar = myModelInfo.m_gridData[11][1];
    config->Weigh = myModelInfo.m_gridData[12][1];
    //progs->DoHoming = myModelInfo.m_gridData[13][1];




    config->save();
    close();
}


void frmInfo::on_buttonExit_5_clicked()
{
    close();
}

void frmInfo::on_buttonExit_8_clicked()
{
    FormTastieraA *uiTastieraA = new FormTastieraA(this);

    uiTastieraA->set_title("Password");
    uiTastieraA->set_text("");
    uiTastieraA->exec();

    QString pass = uiTastieraA->get_text();
    if(pass == "P47858655f?" || pass == "1247" )
    {
        QString danger  = "QTableView {color: rgb(232, 120, 41) }";
        ui->tbProg->setStyleSheet(danger);
        okEdit = true;
    }
    else if( pass == "f68857075P?")
    {
        reset_service = true;
    }
    delete uiTastieraA;

}

void frmInfo::on_tbProg_clicked(const QModelIndex &index)
{
    int row = index.row();

    if(okEdit)
    {
        if( row == 1 || row == 2 ||  row == 4 || row == 5 || row >= 7)
        {
            FormTastieraA *uiTastieraA = new FormTastieraA(this);
            uiTastieraA->set_title(myModelInfo.m_gridData[index.row()][0]);
            uiTastieraA->set_text(myModelInfo.m_gridData[index.row()][1]);
            uiTastieraA->exec();

            if( uiTastieraA->is_valid() )
            {
                myModelInfo.m_gridData[index.row()][1] = uiTastieraA->get_text();

                if( row == 1 )
                {
                    // Modificato il SN
                    // Prendo l'occasione per azzerare tutti i tempi
                    reset_timers = true;
                }
            }
            delete uiTastieraA;
        }
    }
}

void frmInfo::on_buttonUpgrade_clicked()
{
    uiUpgrade = new FormUpgrade(this);
    uiUpgrade->exec();
}
