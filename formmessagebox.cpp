#include "formmessagebox.h"
#include "ui_formmessagebox.h"

FormMessageBox::FormMessageBox(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormMessageBox)
{
    ui->setupUi(this);
}

FormMessageBox::~FormMessageBox()
{
    delete ui;
}

void FormMessageBox::setTitle(QString txt)
{
    ui->label->setText( txt );
}

void FormMessageBox::on_pushButton_clicked()
{
    close();
}
