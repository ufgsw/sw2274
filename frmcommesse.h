#ifndef FRMCOMMESSE_H
#define FRMCOMMESSE_H

#include <QDialog>
#include "model_commesse.h"

namespace Ui {
class frmCommesse;
}

class frmCommesse : public QDialog
{
    Q_OBJECT

public:
    int result;
    QString SelectedProg;
    QString SelectedComm;
    explicit frmCommesse(QWidget *parent = 0);
    ~frmCommesse();
    void Init();
    void Message(QString msg);
private slots:
    void on_buttonExit_clicked();

    void on_buttonExit_3_clicked();

    void on_tbProg_clicked(const QModelIndex &index);

    void on_tbProg_entered(const QModelIndex &index);

private:

    model_commesse myModel;
    Ui::frmCommesse *ui;
};

#endif // FRMCOMMESSE_H
