#ifndef FRMCONFIRM_H
#define FRMCONFIRM_H

#include <QDialog>

namespace Ui {
class frmConfirm;
}

class frmConfirm : public QDialog
{
    Q_OBJECT

public:
    int result;

    void Init(QString msg,bool icona,int tipo);
    void InitTitle(QString help,QString file);
    explicit frmConfirm(QWidget *parent = 0);
    ~frmConfirm();

private slots:
    void on_btnOk_clicked();

    void on_btnCancel_clicked();

private:
    Ui::frmConfirm *ui;
};

#endif // FRMCONFIRM_H
