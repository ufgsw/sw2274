#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "programmi.h"
#include <QUdpSocket>
#include <QTimer>
#include "sabbiatrice.h"
#include "eseguisabbiatura.h"

#include "formselectprog.h"
#include "frmmainsetup.h"
#include "formmanuale.h"
#include "frmeditprog_1.h"
#include "frmedittimer.h"
#include "frmbuildprogram.h"
#include "frmplaymain.h"
#include "frmplayexec.h"
#include "frmplaytimer.h"
#include "frmufgtest.h"
#include "frmallarmi.h"
#include "formsetupethernet.h"
#include "frmmaintimer.h"
#include "frmmanualenastro.h"
#include "frmmenu4_0.h"
#include "orologio.h"
#include "maintenance.h"

class ScribbleArea;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void Win_Allarmi(void);


    void processPendingDatagrams();

    void on_btnStart_clicked();

    void on_btnComandiManuali_clicked();

    void on_btnMenuProgEdit_clicked();

    void on_btnSetupTempi_clicked();

    void on_btnSetup40_clicked();

    void on_buttonExit_5_pressed();

    void on_btnTempi_clicked();
    
    void on_buttonExit_6_clicked();

    void on_buttonExit_5_clicked();

    void on_btnOrologio_clicked();

private:

    QTimer *timer_alarm;
    //QTimer *timer_clock;
    int             last_idx;

    Orologio        *orologio;

    Commesse        *commesse;
    Programmi       *progs;
    ReportAllarmi   *alms;
    Config          *configs;

    ScribbleArea         *scribbleArea;
    Sabbiatrice          *sabbiatrice;
    EseguiSabbiatura     *esegui_sabbiatura;

    formselectprog  *frmselectprog;
    frmEditProg_1   *frmeditprog_1;
    frmMainSetup    *frmMenuSetup;
    formmanuale     *frmManuale;
    frmEditTimer    *frmedittimer;
    frmbuildprogram *frmBuildProgram;
    frmPlayMain     *frmplaymain;
    frmPlayExec    *frmplayexec;
    frmPlayTimer    *frmplaytimer;
    frmUfgTest     *frmufgtest;
    frmAllarmi     *frmallarmi;
    FormSetupEthernet *frmSetupEthernet;
    frmMainTimer   *frmmaintimer;
    frmManualeNastro    *frmmanualenastro;



    QString         SelectedComm;
    QString         SelectedProg;

    QUdpSocket *udpSocket = nullptr;

    void  EseguiProgrammaSabbiatura();
    //void  EseguiProgramma(bool loop);


    QString  ShowDateTime();
    QString GetPage(int allarme);
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
