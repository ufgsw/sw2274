#include "formorologio.h"
#include "ui_formorologio.h"

#include <QTime>
#include <QDate>

FormOrologio::FormOrologio(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormOrologio)
{
    ui->setupUi(this);

    connect( ui->orologio, &Orologio::update_datetime, this, &FormOrologio::update_time );
}

FormOrologio::~FormOrologio()
{
    delete ui;
}

void FormOrologio::on_buttonOre_clicked()
{
    uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title( tr("HH"));
    uiTastieraN->set_max( 23 );
    uiTastieraN->set_min( 0 );
    uiTastieraN->set_default( 1 );

    uiTastieraN->exec();

    QTime time  = QTime::currentTime();

    if( uiTastieraN->is_valid())
    {
        int ora = uiTastieraN->get_val();

        time.setHMS( ora, time.minute(), time.second() );

        ui->orologio->set_time( time );
    }

}

void FormOrologio::on_buttonMinuti_clicked()
{
    uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title( tr("MI"));
    uiTastieraN->set_max( 59 );
    uiTastieraN->set_min( 0 );
    uiTastieraN->set_default( 1 );

    uiTastieraN->exec();

    QTime time  = QTime::currentTime();

    if( uiTastieraN->is_valid())
    {
        int min = uiTastieraN->get_val();

        time.setHMS( time.hour(), min , time.second() );

        ui->orologio->set_time( time );
    }
}

void FormOrologio::update_time(QDate d, QTime t)
{
    QString dt = QString( "%1/%2/%3    %4:%5.%6" )
            .arg( d.year() ,4, 10, QChar('0') )
            .arg( d.month() ,2, 10, QChar('0') )
            .arg( d.day() ,2, 10, QChar('0') )
            .arg( t.hour() ,2, 10, QChar('0') )
            .arg( t.minute() ,2,10, QChar('0') )
            .arg( t.second() ,2,10,QChar('0') );

    ui->label->setText( dt);
}

void FormOrologio::on_buttonSalva_clicked()
{
    close();
}

void FormOrologio::on_buttonGiorno_clicked()
{
    uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title( tr("DD"));
    uiTastieraN->set_max( 31 );
    uiTastieraN->set_min( 1 );
    uiTastieraN->set_default( 1 );

    uiTastieraN->exec();

    QDate data = QDate::currentDate();

    if( uiTastieraN->is_valid())
    {
        int day = uiTastieraN->get_val();

        data.setDate( data.year(), data.month(), day );

        ui->orologio->set_date( data );
    }
}

void FormOrologio::on_buttonMese_clicked()
{
    uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title( tr("MM"));
    uiTastieraN->set_max( 12 );
    uiTastieraN->set_min( 1 );
    uiTastieraN->set_default( 1 );

    uiTastieraN->exec();

    QDate data = QDate::currentDate();

    if( uiTastieraN->is_valid())
    {
        int mese = uiTastieraN->get_val();

        data.setDate( data.year(), mese , data.day() );

        ui->orologio->set_date( data );
    }
}

void FormOrologio::on_buttonAnno_clicked()
{
    uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title( tr("YYYY"));
    uiTastieraN->set_max( 2099 );
    uiTastieraN->set_min( 2021 );
    uiTastieraN->set_default( 2021 );

    uiTastieraN->exec();

    QDate data = QDate::currentDate();

    if( uiTastieraN->is_valid())
    {
        int anno = uiTastieraN->get_val();

        data.setDate( anno , data.month() , data.day() );

        ui->orologio->set_date( data );
    }
}
