#ifndef FORMMESSAGEBOX_H
#define FORMMESSAGEBOX_H

#include <QDialog>

namespace Ui {
class FormMessageBox;
}

class FormMessageBox : public QDialog
{
    Q_OBJECT

public:
    explicit FormMessageBox(QWidget *parent = 0);
    ~FormMessageBox();

    void setTitle(QString txt);

private slots:
    void on_pushButton_clicked();

private:
    Ui::FormMessageBox *ui;
};

#endif // FORMMESSAGEBOX_H
