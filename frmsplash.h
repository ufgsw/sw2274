#ifndef FRMSPLASH_H
#define FRMSPLASH_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class frmSplash;
}

class frmSplash : public QDialog
{
    Q_OBJECT

public:
    void Init(QString modello,QString matricola);
    explicit frmSplash(QWidget *parent = 0);
    ~frmSplash();
private slots:
    void reset_timer();

    void on_imgCentrale_clicked();

private:
    QTimer *timer_btn;
    Ui::frmSplash *ui;
};

#endif // FRMSPLASH_H
