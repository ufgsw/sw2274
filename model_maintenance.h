#ifndef MODEL_MAINTENANCE_H
#define MODEL_MAINTENANCE_H

#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include "maintenance.h"

const int COLS_C= 4;
const int ROWS_C= 1000;

class model_maintenance : public QAbstractTableModel
{
public:
    model_maintenance(QObject *parent = nullptr);

    //QString m_gridData[ROWS_C][COLS_C];  //holds text entered into QTableView

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role)const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void loadData();

private :
    Maintenance* mData;

};

#endif // MODEL_MAINTENANCE_H
