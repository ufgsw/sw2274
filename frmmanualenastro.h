#ifndef FRMMANUALENASTRO_H
#define FRMMANUALENASTRO_H

#include <QDialog>

namespace Ui {
class frmManualeNastro;
}

class frmManualeNastro : public QDialog
{
    Q_OBJECT

public:
    void Init(int manuale);
    explicit frmManualeNastro(QWidget *parent = 0);
    ~frmManualeNastro();

private:
    Ui::frmManualeNastro *ui;
};

#endif // FRMMANUALENASTRO_H
