#include "frmnewname.h"
#include "ui_frmnewname.h"
#include "formtastieraa.h"

frmNewName::frmNewName(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmNewName)
{
    ui->setupUi(this);
}

frmNewName::~frmNewName()
{
    delete ui;
}
void frmNewName::Init(QString nome)
{
    ui->btnNomeProgramma->setText(nome);

    FormTastieraA *uiTastieraA = new FormTastieraA(this);

    //uiTastieraA->set_title( tr("Name") );
    uiTastieraA->set_text(ui->btnNomeProgramma->text());
    uiTastieraA->exec();

    if( uiTastieraA->is_valid() )
    {
        nome = uiTastieraA->get_text();
        if(nome != "")
        {
            ui->btnNomeProgramma->setText(nome);
            result = 1;
        }
        else
        {
            result = 0;
        }
    }
    else
    {
        result = 0;
    }
    delete uiTastieraA;
}

void frmNewName::on_buttonExit_clicked()
{
    nome = ui->btnNomeProgramma->text();
    result = 1;
    close();
}


void frmNewName::on_buttonExit_3_clicked()
{
    result = 0;
    close();

}

void frmNewName::on_buttonExit_8_clicked()
{
    FormTastieraA *uiTastieraA = new FormTastieraA(this);

    //uiTastieraA->set_title( tr("Name") );
    uiTastieraA->set_text(ui->btnNomeProgramma->text());
    uiTastieraA->exec();

    nome = uiTastieraA->get_text();
    if(nome != ""){
        ui->btnNomeProgramma->setText(nome);
    }
    delete uiTastieraA;

}
