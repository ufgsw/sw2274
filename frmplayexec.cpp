#include "frmplayexec.h"
#include "ui_frmplayexec.h"
#include "def.h"
#include "release.h"

//#define loop_pezza

frmPlayExec::frmPlayExec(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmPlayExec)
{
    ui->setupUi(this);
    avvio = false;
    fine = false;

    timer = new QTimer(this);
    timer->setInterval(500);
    connect(timer, &QTimer::timeout, this, &frmPlayExec::processTimer);

    uiPlayMain  = new frmPlayMain(this);
    uiPlayTimer = new frmPlayTimer(this);

    progs    = Programmi::getInstance();
    commesse = Commesse::getInstance();

    esegui_sabbiatura = nullptr;
}

frmPlayExec::~frmPlayExec()
{
    delete ui;
}

void frmPlayExec::closeEvent(QCloseEvent *event)
{
    timer->stop();
}

void frmPlayExec::showEvent(QShowEvent *event)
{
    QString safe = "QProgressBar {background-color: rgb(255, 255, 255) }";

    timer->start();
    avvio = false;
    fine  = false;


    ui->btnPausa->setEnabled(true);
    ui->btnStart->setEnabled(false);
    ui->btnFine->setEnabled(false);
    ui->pbAvanzamento->setStyleSheet(safe);
    ui->pbAvanzamentoArea->setStyleSheet(safe);
    ui->lbHelp->setText("1.2");

    if( esegui_sabbiatura != nullptr)
    {
        esegui_sabbiatura->sabbiatrice->flag.in_pausa = 0;
        esegui_sabbiatura->sabbiatrice->flag.stop_esegui = 0;
        esegui_sabbiatura->Fine = false;
    }
    ui->pbAvanzamento->setValue(0);
    ui->pbAvanzamentoArea->setValue(0);
}


void frmPlayExec::processTimer()
{
    uint h,m,s;
    int val = 0;

    if( esegui_sabbiatura == nullptr ) return;

    if( esegui_sabbiatura->sabbiatrice->Warning )
    {
        this->on_btnPausa_clicked();
    }


    timer->stop();
    if( esegui_sabbiatura->sabbiatrice->flexe == GO )
    {
        h = (uint)(  esegui_sabbiatura->sabbiatrice->cronometro / 3600);
        m = (uint)(( esegui_sabbiatura->sabbiatrice->cronometro - ( h * 3600) ) / 60);
        s = (uint)(( esegui_sabbiatura->sabbiatrice->cronometro - ( h * 3600) - (m * 60)));

        ui->lbTempo->setText(QString("h : %1:%2:%3").arg(h,2,10,QChar('0')).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));
    }
    else
    {
        ui->lbTempo->setText( "h: 00:00:00" );//   QString("h : %1:%2:%3").arg(h,2,10,QChar('0')).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));
    }

    if(esegui_sabbiatura->X_tot_da_fare)  val = (esegui_sabbiatura->X_tot_fatto * 100 ) / esegui_sabbiatura->X_tot_da_fare ;
    else            val = 0;
    if( val < 0   ) val = 0;
    if( val > 100 ) val = 100;

    ui->pbAvanzamento->setValue(val);

    if(esegui_sabbiatura->X_area_da_fare) val = (esegui_sabbiatura->X_area_fatto * 100 ) / esegui_sabbiatura->X_area_da_fare;
    else               val = 0;

    if( val < 0   ) val = 0;
    if( val > 100 ) val = 100;


    ui->pbAvanzamentoArea->setValue(val);
    if(esegui_sabbiatura->Started)
    {
        ui->lbNumAree->setText(QString("%1/%2").arg(esegui_sabbiatura->currarea + 1).arg(esegui_sabbiatura->sabbiatrice->Prog.naree));
    }
    else
    {
        ui->lbNumAree->setText(QString("%1/%2").arg(1).arg(esegui_sabbiatura->sabbiatrice->Prog.naree));
    }
    if(val >= 100){
        ui->btnPausa->setEnabled(false);
        ui->btnStart->setEnabled(true);
        ui->btnFine->setEnabled(true);
    }

    if(avvio == false)
    {
        avvio = true;
        on_btnStart_clicked();
    }

    if(esegui_sabbiatura->Fine)
    {
        esegui_sabbiatura->Fine = false;

        if( esegui_sabbiatura->sabbiatrice->flag.sabbiatura_completata )
        {
            esegui_sabbiatura->sabbiatrice->flag.sabbiatura_completata = 0;

            // Salvataggio dei tempi
            esegui_sabbiatura->sabbiatrice->SalvaTempi();
            esegui_sabbiatura->sabbiatrice->updateiot_param();
            // Salvataggio delle commesse
            if( commesse->current_name() != "" )
            {
                s_Commessa comm;
                comm = commesse->current();
                if(esegui_sabbiatura->sabbiatrice->flag.stop_esegui == 0)
                {
                    comm.qta_fatti     = comm.qta_fatti + 1;
                    comm.tempo_singolo = esegui_sabbiatura->sabbiatrice->cronometro; //esegui_sabbiatura->sabbiatrice->Prog.secondi_impiegati;
                }
                comm.tempo_totale += esegui_sabbiatura->sabbiatrice->cronometro; //esegui_sabbiatura->sabbiatrice->Prog.secondi_impiegati;

                commesse->replace( comm );
                commesse->save();
                esegui_sabbiatura->sabbiatrice->updateiot();
            }
            esegui_sabbiatura->sabbiatrice->Prog.campioni++;
            esegui_sabbiatura->sabbiatrice->Prog.esecuzioni++;
            esegui_sabbiatura->sabbiatrice->Prog.secondi_impiegati = esegui_sabbiatura->sabbiatrice->cronometro;
            esegui_sabbiatura->sabbiatrice->Prog.secondi_totali    = esegui_sabbiatura->sabbiatrice->Prog.secondi_totali + esegui_sabbiatura->sabbiatrice->cronometro;
            progs->replace(esegui_sabbiatura->sabbiatrice->Prog);
            progs->save("");
        }
#ifdef loop_pezza
        on_btnStart_clicked();
#else
        if( esegui_sabbiatura->sabbiatrice->Allarmi != 0 )
        {
            close();
        }
        else
        {
            uiPlayTimer->Init(esegui_sabbiatura->sabbiatrice->Prog.secondi_impiegati);
            uiPlayTimer->exec();
            if(uiPlayTimer->result == 1 )
            {
                uiPlayMain->prog = esegui_sabbiatura->sabbiatrice->Prog;//  progs->current();
                uiPlayMain->Init();
                uiPlayMain->exec();
                if( uiPlayMain->result == 1)
                {
                    on_btnStart_clicked();
                }
                else
                {
                    close();
                }
            }
            else
            {
                close();
            }
        }
#endif
    }
    timer->start();
}
void frmPlayExec::SetNomeProgramma(QString nome,short tipo)
{
    ui->lbNomeProgramma->setText(nome);

    if(tipo != MISTRAL)
    {
        ui->btnAddArea->setVisible(false);
        ui->pbAvanzamentoArea->setVisible(false);
        ui->lbNumAree->setVisible(false);
    }

}
void frmPlayExec::on_btnFine_clicked()
{
    esegui_sabbiatura->sabbiatrice->flag.stop_esegui = 1;
    esegui_sabbiatura->sabbiatrice->flag.in_pausa    = 0;
    result = -1;
    close();

}

void frmPlayExec::on_btnStart_clicked()
{
     QString safe = "QProgressBar {background-color: rgb(255, 255, 255) }";

     if(esegui_sabbiatura->sabbiatrice->flag.in_pausa == 0 || esegui_sabbiatura->Start_sabbiatura == 0)
     {
        esegui_sabbiatura->sabbiatrice->AzzeraCronometro();
        esegui_sabbiatura->Stop_manuale = 0;

        QThread::usleep(500);
        esegui_sabbiatura->Start_sabbiatura = 1;
        while(!esegui_sabbiatura->Started)
        {
            esegui_sabbiatura->Start_sabbiatura = 1;
            QThread::usleep(50);
        }

        ui->btnPausa->setEnabled(true);
        ui->btnStart->setEnabled(false);
        ui->btnFine->setEnabled(false);
        ui->lbHelp->setText("1.2");
    }
    else
     {
        esegui_sabbiatura->sabbiatrice->flag.in_pausa = 0;

        ui->btnPausa->setEnabled(true);
        ui->btnStart->setEnabled(false);
        ui->btnFine->setEnabled(false);
        ui->lbHelp->setText("1.2");
    }
     ui->pbAvanzamento->setStyleSheet(safe);
     ui->pbAvanzamentoArea->setStyleSheet(safe);

}

void frmPlayExec::on_btnPausa_clicked()
{
    //QString danger  = "QProgressBar::chunk {background: rgb(232, 120, 41) }";
    QString danger  = "QProgressBar {background-color: rgb(232, 120, 41) }";
    if(esegui_sabbiatura->sabbiatrice->flag.in_pausa == 0)
    {
        esegui_sabbiatura->sabbiatrice->flag.in_pausa = 1;
        ui->btnPausa->setEnabled(false);
        ui->btnStart->setEnabled(true);
        ui->btnFine->setEnabled(true);
        ui->pbAvanzamento->setStyleSheet(danger);
        ui->pbAvanzamentoArea->setStyleSheet(danger);
        ui->lbHelp->setText("1.2.1");
    }


}


