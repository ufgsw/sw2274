#include "frmlogmanutenzione.h"
#include "ui_frmlogmanutenzione.h"
#include <qprocess.h>
#include "formmessagebox.h"
#include <qdir.h>

#include <QSortFilterProxyModel>

frmLogManutenzione::frmLogManutenzione(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmLogManutenzione)
{

    ui->setupUi(this);

    //myModel.loadData();

//    QSortFilterProxyModel sortmodel = QSortFilterProxyModel();
//    sortmodel.setSourceModel( &myModel );
//    sortmodel.setSortRole(1);

    ui->tbProg->setModel(&myModel);
    //ui->tbProg->setSortingEnabled( true);


    ui->tbProg->setColumnWidth(0,100);
    ui->tbProg->setColumnWidth(1,220);
    ui->tbProg->setColumnWidth(2,120);
    ui->tbProg->setColumnWidth(3,220);

}

QString frmLogManutenzione::SecToOraString(int cronometro)
{
    uint h,m,s;
    QString str;
    h = (uint)(  cronometro / 3600);
    m = (uint)(( cronometro - ( h * 3600) ) / 60);
    s = (uint)(( cronometro - ( h * 3600) - (m * 60)));

    str = QString("h : %1:%2:%3").arg(h,2,10,QChar('0')).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0'));

    return str;

}

frmLogManutenzione::~frmLogManutenzione()
{
    delete ui;
}

void frmLogManutenzione::on_buttonExit_3_clicked()
{
    close();
}

void frmLogManutenzione::on_btnExport_clicked()
{
    QString path;
    QString program;
    QStringList arguments;

    QString device;
    QDir dir("/dev");
   // dir.setCurrent("/dev");
    QFileInfoList list = dir.entryInfoList(QStringList() << "sd*",QDir::System);
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        device = fileInfo.fileName();
    }

    path = "/dev/" + device;

    QFile file(path);

    if(file.exists() && device != "")
    {
        QProcess* proc = new QProcess(this);

        program = "mount";
        arguments << path << "/media/storage";

        proc->start(program,arguments);
        proc->waitForFinished(-1);

        Maintenance* manutenzioni = Maintenance::GetInstance();
        manutenzioni->save_as( "/media/storage/Maintenance.json" );

        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "EXPORT SUCCESSFUL ! !");
        uiMessageBox->exec();

        program = "umount";
        arguments.clear();
        arguments << path;

        proc->start( program, arguments);
        proc->waitForFinished(-1);

        delete uiMessageBox;
    }
    else
    {
        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "INSERT USB KEY !");
        uiMessageBox->exec();

        delete uiMessageBox;



    }


}
