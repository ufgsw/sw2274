#ifndef FORMMENU_H
#define FORMMENU_H

#include <QDialog>
#include "frmmainsetup.h"


namespace Ui {
class formMenu;
}

class formMenu : public QDialog
{
    Q_OBJECT

public:
    explicit formMenu(QWidget *parent = 0);
    ~formMenu();

private slots:
    void on_btnMenuSetup_clicked();

private:
    frmMainSetup * frmSetup;

    Ui::formMenu *ui;
};

#endif // FORMMENU_H
