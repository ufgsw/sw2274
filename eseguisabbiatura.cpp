#include "eseguisabbiatura.h"
#include "def.h"
#include "release.h"
#include <math.h>


EseguiSabbiatura::EseguiSabbiatura()
{
    mRun = false;
    Started = false;
    Fine = false;
    fatt_unita = 1;
}
void EseguiSabbiatura::run()
{

    mRun = true;
    Started = false;
    while(mRun)
    {
        QThread::msleep(1000);
        Esegui_Sabbiatura(0);
        mRun = 0;
    }
}
void EseguiSabbiatura::Esegui_Sabbiatura(u_int32_t initial_data)
{
    //TPGM     pgm;
    int      narea;
    u_int16_t   velmaxy,velmaxx;
    int      step,xini,xfin,yini,yfin,yy,nripe,nrip,deltax;
    int      ryini,ryfin,rdeltay,ryy,rquotay;
    int      npassi,comodo;
    float    pini,pfin,press;
    float    deltap = 0;
    char     corsa;
    char     quotaalay;

    float    swap;
    int      swapx;
    int      dx1,dx2;
    short    dirarea;
    int      XtotPrec;

    QString text;

    Fine = false;

    // MAIN LOOP
    for ( ;; )
    {
        QThread::msleep(1);

        sabbiatrice->PISTOLA1_OFF();
        sabbiatrice->PISTOLA2_OFF();
        sabbiatrice->PULNAST_OFF();
        sabbiatrice->VIBRAT_OFF();
        sabbiatrice->Set_Pressione(0);

        X_tot_fatto    = 0;
        X_area_fatto   = 0;

        if( sabbiatrice->flag.home_mot2 && sabbiatrice->Allarmi != 8 )
        {
            sabbiatrice->Home_Pistola();
            qDebug("Home Pistola");
        }

        sabbiatrice->Set_Mot_Zero(MOT_X);
        //qDebug("Zero X");

        // ATTESA START
        sabbiatrice->flag.stop_esegui = 0;
        sabbiatrice->flag.in_pausa    = 0;

        Start_sabbiatura = 0;
        while(Start_sabbiatura == 0)
        {
            if( sabbiatrice->flag.home_mot2 )
            {
                sabbiatrice->Stop_Mot_Vel( MOT_X, 0);
                sabbiatrice->Home_Pistola();
            }
            QThread::msleep(5);

        }

        qDebug("Start");
        Started = true;
        Fine = false;
        sabbiatrice->AzzeraCronometro(); //sabbiatrice->cronometro = 0;

        sabbiatrice->Set_Mot_Zero(MOT_X);

        QThread::msleep(500);


        currarea 	   	 = 0;
        totaree  	   	 = 0;
        quotax   	   	 = 0;
        Y_tot_da_fare    = YtotDaFare();
        X_tot_da_fare  	 = XtotDaFare();
        X_tot_fatto    	 = 0;
        X_area_da_fare 	 = 0;
        X_area_fatto   	 = 0;
        sabbiatrice->flexe          	   = GO;
        sabbiatrice->flag.esegui_filtro = 1;

        /* -----------------24/06/2002 16.46-----------------
        * Uscite
        * --------------------------------------------------*/
        //if ( !sabbiatrice->Par.esclusioni[VENTOLA_FILTRO] )
        {
            sabbiatrice->VENTOLA_ON();
            sabbiatrice->PULNAST_ON();
            QThread::msleep(100);


        }
        //if ( !sabbiatrice->Par.esclusioni[VIBRATORE] )
        {
            sabbiatrice->VIBRAT_ON();
            QThread::msleep(100);

        }

        /* -----------------19/06/2002 14.08-----------------
        * Velocita' e posizioni dei movimenti
        * -------------------------------------------------- */
        velmaxx = (short)(VELMAX_X *  sabbiatrice->parametri->param.velox / 100);

        /* -----------------17/06/2002 18.30-----------------
        * Muove asse x - Entra vetro e azzera quota
        * -------------------------------------------------- */

        //CalcolaTempoLavoroPrevisto();
        sabbiatrice->Move_Mot_Pos( MOT_X, (int32_t)(sabbiatrice->parametri->param.ofsetx+3) , RANGE_QUOTA_FATTO_X , velmaxx );

        quotax    = 0;
        abspassiX = 0;
        sabbiatrice->Set_Mot_Zero( MOT_X );
        QThread::msleep(20);


        if ( sabbiatrice->flag.stop_esegui || sabbiatrice->Allarmi ) goto FineEsegui;

        /* ----------------- 17/06/2002 18.34 -----------------
        * Pistola a quota di home
        * --------------------------------------------------*/
        sabbiatrice->Move_Mot_Pos( MOT_Y,  sabbiatrice->parametri->param.ofsety , RANGE_QUOTA_FATTO_Y , VELMAX_Y );

        quotay  = sabbiatrice->parametri->param.ofsety;
        rquotay = sabbiatrice->parametri->param.ofsety;

        if ( sabbiatrice->flag.stop_esegui || sabbiatrice->Allarmi ) goto FineEsegui;

        /* -----------------24/06/2002 17.08-----------------
        * Ciclo sulle aree
        * --------------------------------------------------*/
        XtotPrec = 0;
        sabbiatrice->flag.pausa_abilitata = 1;
        naree = sabbiatrice->Prog.naree;
        for ( narea=0 ; narea < sabbiatrice->Prog.naree; ++narea )
        {
            currarea     = narea;
            /* -----------------24/06/2002 18.17-----------------
            * Preleva dati area da sabbiare
            * --------------------------------------------------*/
            xini    = sabbiatrice->Prog.Area[narea].x_iniziale;
            xfin    = sabbiatrice->Prog.Area[narea].x_finale;
            step    = sabbiatrice->Prog.Area[narea].step;

            deltax  = xfin - xini;

            dirarea = 1;

            /* -----------------01/09/2003 18.14-----------------
           * Se tipo complex Sceglie la x di inizio dell'area piu' vicina alla quota corrente
           * --------------------------------------------------*/
            //if ( sabbiatrice->Par.tipo_macchina == MISTRAL )
            //{
            dx1 = abs(quotax - xini);
            dx2 = abs(quotax - xfin);
            if ( dx2 < dx1 )
            {
                dirarea = -1;
                swapx = xini;
                xini  = xfin;
                xfin  = swapx;
            }
            //   qDebug("COMPLEX");
            //}

            xareamissing   = abs(xfin - xini);
            X_area_da_fare = xareamissing;
            X_area_fatto   = 0;

            /* -----------------01/09/2003 18.20-----------------
           * Calcola passo variazione della pressione pistole
           * --------------------------------------------------*/
            pini    = sabbiatrice->Prog.Area[narea].pressione_iniziale;
            pfin    = sabbiatrice->Prog.Area[narea].pressione_finale;

            nripe   = sabbiatrice->Prog.Area[narea].ripetizioni;

            if ( step )
            {
                npassi  = (int) (deltax / step);
                if ( npassi ) {
                    deltap  = (pfin - pini) / npassi;
                }
                else {
                    deltap = 0.0;
                }
            }

            /* -----------------01/09/2003 18.26-----------------
           * Cambia segno al passo x e al passo della pressione
           * se sabbiatura all'indietro
           * --------------------------------------------------*/
            if ( dirarea == -1 )
            {
                step   = -step;
                deltap = -deltap;
                swap   = pini;
                pini   = pfin;
                pfin   = swap;
            }

            velmaxy = (short)(VELMAX_Y * sabbiatrice->Prog.Area[narea].velocita_pistola / 100);

            if ( sabbiatrice->flag.stop_esegui || sabbiatrice->Allarmi ) goto FineEsegui;

            yini   = sabbiatrice->Prog.Area[narea].y_iniziale + sabbiatrice->parametri->param.ofsety;
            yfin   = sabbiatrice->Prog.Area[narea].y_finale   + sabbiatrice->parametri->param.ofsety;

            /* -----------------26/08/2002 13.07-----------------
           * corse corrette di 'riduzy' mm per tenere conto dell'area del getto della pistola
           * --------------------------------------------------*/

            ryini   = yini;
            ryfin   = yfin;
            rdeltay =  ryfin - ryini;
            if ( rdeltay <= 5 )
            {
                step      = 0;
                nripe     = 1;
                ryini     = (ryini+ryfin)/2;
                ryfin     = ryini;
                rdeltay   = 0;
                quotaalay = 0;
                timery    = 0;
            }

            if ( rdeltay > sabbiatrice->parametri->param.anticipoy ) quotaalay = (char)sabbiatrice->parametri->param.anticipoy;
            else 	quotaalay = 1; // Bruno, prima 0))

            /* -----------------24/06/2002 17.33-----------------
           * Sceglie la y iniziale della pistola
           *    se y > di yfin parte da yfin
           *    se y < di yini parte da yini
           *    se y in mezzo parte dal punto piu' vicino
           * --------------------------------------------------*/
            if ( rquotay > ryfin )
            {
                comodo = ryini;
                ryini  = ryfin;
                ryfin  = comodo;
            }
            else if ( rquotay > ryini )
            {
                comodo = abs(rquotay-ryini);
                if ( abs(rquotay-ryfin) < comodo)
                {
                    comodo = ryini;
                    ryini  = ryfin;
                    ryfin  = comodo;
                }
            }

            QThread::msleep(200);


            /* -----------------24/06/2002 17.08-----------------
           * Porta asse x a inizio area    minimo 4 mm
           * --------------------------------------------------*/
            if ( quotax != xini )
            {
                sabbiatrice->Move_Mot_Pos( MOT_X, xini , RANGE_QUOTA_FATTO_X ,velmaxx );
                qDebug("Sposta a Inizio Area");
                quotax = xini;
            }
            else
            {
                qDebug("Sono a Inizio Area");
                xini = quotax;
                sabbiatrice->flag.mot_x_done = 1;
            }

            //if( sabbiatrice->Warning ) sabbiatrice->flag.in_pausa = true;
            if ( sabbiatrice->flag.stop_esegui || sabbiatrice->Allarmi ) goto FineEsegui;

            //_time_delay(10);// proc_sleep(10);
            //WaitMotPos( MOT_X ,30000);

            /* -----------------24/06/2002 17.10-----------------
           * Porta asse y a inizio area (ridotta)    minimo 15
           * --------------------------------------------------*/
            if ( rquotay != ryini )
            {
                sabbiatrice->Move_Mot_Pos( MOT_Y, ryini , RANGE_QUOTA_FATTO_Y ,VELMAX_Y );
                rquotay = ryini;
                quotay  = yini;
            }
            else
            {
                rquotay = ryini;
                quotay  = yini;
                sabbiatrice->flag.mot_y_done = 1;
            }
            //--------------
            if ( sabbiatrice->flag.stop_esegui || sabbiatrice->Allarmi ) goto FineEsegui;

            /* -----------------28/06/2002 10.48-----------------
           * Aspetta che i 2 assi siano arrivati
           * --------------------------------------------------*/
            QThread::msleep(10);


            if ( sabbiatrice->flag.stop_esegui || sabbiatrice->Allarmi ) goto FineEsegui;

            while ( sabbiatrice->flag.in_pausa )
            {
                QThread::msleep(10);

                if ( sabbiatrice->flag.stop_esegui || sabbiatrice->Allarmi ) goto FineEsegui;
            }

            /* -----------------24/06/2002 17.17-----------------
           * Accensione pistola
           * --------------------------------------------------*/
            if ( sabbiatrice->Prog.Area[narea].pis1 + sabbiatrice->Prog.Area[narea].pis2 )
            {
                sabbiatrice->Set_Pressione( pini );
                if ( sabbiatrice->Prog.Area[narea].pis1 ) sabbiatrice->PISTOLA1_ON();
                if ( sabbiatrice->Prog.Area[narea].pis2 ) sabbiatrice->PISTOLA2_ON();
            }

            /* -----------------03/07/2002 11.03-----------------
           * Se movimento continuo start asse x
           * --------------------------------------------------*/
            if (!step)
            {
                if( dirarea > 0 ){
                    sabbiatrice->Move_Mot_Vel( MOT_X,  velmaxx);
                }
                else{
                    sabbiatrice->Move_Mot_Vel( MOT_X, -velmaxx);
                }
            }
            //sabbiatrice->partito = GO;

            // Sabbiatura area
            corsa = 0;
            if(nripe <= 0){
                nripe = 1;
            }
            while ( !FineArea(dirarea,xfin) )
            {
                for ( nrip = 0 ; nrip < nripe; ++nrip )
                {

                    if ( step )
                    {
                        sabbiatrice->Set_Pressione(pini);
                        QThread::msleep(5);

                    }
                    else
                    {
                        press = pini+(pfin-pini)*(quotax-xini)/(xfin-xini);
                        sabbiatrice->Set_Pressione(press);
                        QThread::msleep(5);
                    }
                    /* -----------------03/07/2002 11.46-----------------
                 * Corsa pistola
                 * --------------------------------------------------*/
                    if ( rdeltay )
                    {
                        yy  = corsa == 0 ? yfin  : yini;
                        ryy = corsa == 0 ? ryfin : ryini;
                        corsa = ~corsa;

                        sabbiatrice->Move_Mot_Pos( MOT_Y, ryy , quotaalay,velmaxy );

                        // -------  ver. 6.8  --------
                        if( sabbiatrice->flag.in_pausa )
                        {
                            sabbiatrice->PISTOLA1_OFF();
                            sabbiatrice->PISTOLA2_OFF();
                            while ( sabbiatrice->flag.in_pausa )
                            {
                                sabbiatrice->Move_Mot_Pos( MOT_Y, ryy , quotaalay,velmaxy );
                                QThread::msleep(50);

                                if ( sabbiatrice->flag.stop_esegui || sabbiatrice->Allarmi ) goto FineEsegui;
                            }
                            if ( sabbiatrice->Prog.Area[narea].pis1 ) sabbiatrice->PISTOLA1_ON();
                            if ( sabbiatrice->Prog.Area[narea].pis2 ) sabbiatrice->PISTOLA2_ON();
                        }
                        // ---------------------------
                        if ( sabbiatrice->flag.stop_esegui || sabbiatrice->Allarmi ) goto FineEsegui;
                        quotay  = yy;
                        rquotay = ryy;
                    }
                    else
                    {
                        quotay  = yini;
                        rquotay = ryini;
                        if ( sabbiatrice->flag.mot_y_done && !timery)
                        {
                            sabbiatrice->Move_Mot_Pos( MOT_Y, ryini , quotaalay,velmaxy );
                            timery = 100;
                        }
                    }
                    /* -----------------03/07/2002 11.46-----------------
                 * Avanzamento vetro (solo se a passi, altrimenti
                 * la nuova quotax arriva da CAN)
                 * --------------------------------------------------*/
                    if ( step ) {
                        if (nrip == nripe-1)
                        {
                            sabbiatrice->Move_Mot_Pos( MOT_X, (quotax+step) , RANGE_QUOTA_FATTO_X ,velmaxx );

                            quotax       += step;
                            X_area_fatto += abs(step);
                            X_tot_fatto  += abs(step);
                            pini         += deltap;
                        }
                    }
                    else
                    {
                        // ???????????????????????????????????????
                        //quotax = (abspassiX / AsseX.cPosToTick);
                        quotax         = sabbiatrice->Quota_mot1;
                        xareamissing   = abs(xfin - quotax);
                        comodo         = abs(xini-xfin)-xareamissing;
                        X_area_fatto   = comodo > 0 ? comodo : 0;
                        X_tot_fatto    = XtotPrec + X_area_fatto;
                    }

                    if ( sabbiatrice->flag.stop_esegui || sabbiatrice->Allarmi ) goto FineEsegui;
                }
            }
            if (!step)
            {
                sabbiatrice->Stop_Mot_Vel( MOT_X, 0);
                //qDebug("Move_X:  Continuo");
            }
            /* -----------------28/06/2002 12.33-----------------
           * Finita area - Spegne le pistola
           * --------------------------------------------------*/
            sabbiatrice->PISTOLA1_OFF();
            sabbiatrice->PISTOLA2_OFF();
            sabbiatrice->Pressione_set = 0;
            XtotPrec += abs(xini-xfin);

            if ( sabbiatrice->flag.stop_esegui ) break;
        }

        //       sabbiatrice->Prog.secondi_impiegati = sabbiatrice->cronometro;
        //       sabbiatrice->Prog.secondi_totali    = sabbiatrice->Prog.secondi_totali + sabbiatrice->cronometro;
        //       sabbiatrice->Prog.campioni          = sabbiatrice->Prog.campioni + 1;
        //       sabbiatrice->flag.save_prog_in_uso = 1;

FineEsegui:
        X_tot_fatto    = X_tot_da_fare;
        X_area_fatto   = X_area_da_fare;


        /* -----------------12/07/2002 12.29-----------------
        * Off Uscite
        * --------------------------------------------------*/
        qDebug("Fine Esegui");

        sabbiatrice->flag.pausa_abilitata 	= 0;
        sabbiatrice->flag.esegui_filtro      = 0;
        sabbiatrice->PISTOLA1_OFF();
        sabbiatrice->PISTOLA2_OFF();
        sabbiatrice->Pressione_set = 0;
        sabbiatrice->PULNAST_OFF();
        sabbiatrice->VIBRAT_OFF();
        sabbiatrice->PISTOLA1_OFF();
        sabbiatrice->PISTOLA2_OFF();

        /* -----------------17/06/2002 18.34-----------------
        * Pistola a quota di home
        * --------------------------------------------------*/
        if( sabbiatrice->Allarmi != ALM_MOT_1 )
            sabbiatrice->Stop_Mot_Vel( MOT_X, 0);

        QThread::msleep(50);


        if ( sabbiatrice->Allarmi != ALM_EMERGENZA && sabbiatrice->Allarmi != ALM_PORTELLO && sabbiatrice->Allarmi != ALM_MOT_2 )
        {
            // no emergenza
            // no allarme portello
            // no allarme mot y

            sabbiatrice->Stop_Mot_Vel( MOT_Y, 0);
            QThread::msleep(200);

            sabbiatrice->Move_AsseY_To_Zero();

            // home release 105
            if( RELEASE_HOME_PISTOLA_SEMPRE )
            {
                sabbiatrice->Home_Pistola();
                qDebug("Home Pistola");
            }
        }

        /*-----------------09/10/04 14:26-------------------
        *      Modifica ritorno a casa del vetro
        * --------------------------------------------------*/
        if( sabbiatrice->Prog.RetToHome == 1 && sabbiatrice->flag.stop_esegui == 0  && sabbiatrice->Allarmi == 0)
        {  // 12.05.2006

            sabbiatrice->Move_Mot_Vel( MOT_X, -velmaxx);

            while ( sabbiatrice->Quota_mot1 > - sabbiatrice->parametri->param.ofsetx )
            {
                QThread::msleep(2);

                if( sabbiatrice->Allarmi ) break;
            }
            sabbiatrice->Stop_Mot_Vel( MOT_X, 0);
        }

        /* -----------------28/06/2002 13.00-----------------
        * Finita sabbiatura Riavvia Task Comandi Manuali
        * --------------------------------------------------*/
        if( sabbiatrice->flag.stop_esegui == 0 && sabbiatrice->Allarmi == 0 )
        {
            sabbiatrice->flag.sabbiatura_completata = 1;
        }

        sabbiatrice->flexe = HALTED;
        Started = false;

        Fine = true;

        sabbiatrice->partito = HALTED;
        QThread::msleep(100);
    }
}

short EseguiSabbiatura::FineArea(short dirarea, int xfin )
{
    short fine = 1;

    if ( dirarea == 1 )
    {
        fine = quotax < xfin ? 0 : 1;
    }
    else
    {
        fine = quotax > xfin ? 0 : 1;
    }

    return fine;
}
int EseguiSabbiatura::XtotDaFare()
{
    int  xtot;//,narea,xini,xfin;
    int  narea;
    xtot = 0;
    for ( narea=0; narea < sabbiatrice->Prog.naree; ++narea )
    {
        xtot += abs( sabbiatrice->Prog.Area[narea].x_finale - sabbiatrice->Prog.Area[narea].x_iniziale );
    }
    return xtot;
}
int EseguiSabbiatura::YtotDaFare()
{
    int  ytot;//,narea,xini,xfin;
    int  narea;
    ytot = 0;
    for ( narea=0; narea < sabbiatrice->Prog.naree; ++narea )
    {
        if(sabbiatrice->Prog.Area[narea].stato == ATTIVA){
            ytot += abs( sabbiatrice->Prog.Area[narea].y_finale - sabbiatrice->Prog.Area[narea].y_iniziale );
        }
    }
    return ytot;
}




