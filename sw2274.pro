#-------------------------------------------------
#
# Project created by QtCreator 2021-01-13T03:09:44
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sw2274
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    formtastieraa.cpp \
    formtastietan.cpp \
    programmi.cpp \
    formnewprogram.cpp \
    w_paneldraw.cpp \
    formeditarea.cpp \
    ethreceiver.cpp \
    sabbiatrice.cpp \
    canbusthread.cpp \
    formmanuale.cpp \
    formselectprog.cpp \
    frmbuildprogram.cpp \
    frmmainsetup.cpp \
    frmeditprog_1.cpp \
    frmedittimer.cpp \
    model.cpp \
    savemsg.cpp \
    frmplaymain.cpp \
    frmplayexec.cpp \
    frmplaytimer.cpp \
    frmmaintimer.cpp \
    frmconfirm.cpp \
    frmufgtest.cpp \
    eseguisabbiatura.cpp \
    frmallarmi.cpp \
    formsetupethernet.cpp \
    formtastieraip.cpp \
    frmmanualenastro.cpp \
    frmnewname.cpp \
    frmsplash.cpp \
    frminfo.cpp \
    modelinfo.cpp \
    iot.cpp \
    frmcommesse.cpp \
    model_commesse.cpp \
    formorologio.cpp \
    orologio.cpp \
    formelencossid.cpp \
    formmessagebox.cpp \
    frmmenu4_0.cpp \
    frmlogmanutenzione.cpp \
    model_mant.cpp \
    frmlogallarmi.cpp \
    model_alarm.cpp \
    frmimport.cpp \
    maintenance.cpp \
    model_maintenance.cpp \
    formupgrade.cpp \
    downloadmanager.cpp \
    parametrimacchina.cpp

HEADERS += \
        mainwindow.h \
    formtastieraa.h \
    formtastietan.h \
    globs.h \
    programmi.h \
    formnewprogram.h \
    w_paneldraw.h \
    formeditarea.h \
    ethreceiver.h \
    sabbiatrice.h \
    def.h \
    canbusthread.h \
    formmanuale.h \
    formselectprog.h \
    frmbuildprogram.h \
    frmmainsetup.h \
    frmeditprog_1.h \
    frmedittimer.h \
    model.h \
    savemsg.h \
    frmplaymain.h \
    frmplayexec.h \
    frmplaytimer.h \
    frmmaintimer.h \
    frmconfirm.h \
    frmufgtest.h \
    eseguisabbiatura.h \
    frmallarmi.h \
    formsetupethernet.h \
    formtastieraip.h \
    frmmanualenastro.h \
    frmnewname.h \
    frmsplash.h \
    frminfo.h \
    modelinfo.h \
    iot.h \
    frmcommesse.h \
    model_commesse.h \
    formorologio.h \
    orologio.h \
    formelencossid.h \
    formmessagebox.h \
    frmmenu4_0.h \
    frmlogmanutenzione.h \
    model_mant.h \
    frmlogallarmi.h \
    model_alarm.h \
    frmimport.h \
    maintenance.h \
    model_maintenance.h \
    formupgrade.h \
    release.h \
    downloadmanager.h \
    parametrimacchina.h

FORMS += \
        mainwindow.ui \
    formtastieraa.ui \
    formtastietan.ui \
    formnewprogram.ui \
    formeditarea.ui \
    formmanuale.ui \
    formselectprog.ui \
    frmbuildprogram.ui \
    frmmainsetup.ui \
    frmeditprog_1.ui \
    frmedittimer.ui \
    savemsg.ui \
    frmplaymain.ui \
    frmplayexec.ui \
    frmplaytimer.ui \
    frmmaintimer.ui \
    frmconfirm.ui \
    frmufgtest.ui \
    frmallarmi.ui \
    formsetupethernet.ui \
    formtastieraip.ui \
    frmmanualenastro.ui \
    frmnewname.ui \
    frmsplash.ui \
    frminfo.ui \
    frmcommesse.ui \
    formorologio.ui \
    formelencossid.ui \
    formmessagebox.ui \
    frmmenu4_0.ui \
    frmlogmanutenzione.ui \
    frmlogallarmi.ui \
    frmimport.ui \
    formupgrade.ui

#
# libcrypto
#
LIBS += -L lcrypto

target.path ="/home/root"

INSTALLS+= target

RESOURCES += \
    resource.qrc

DISTFILES +=
