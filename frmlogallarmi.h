#ifndef FRMLOGALLARMI_H
#define FRMLOGALLARMI_H

#include <QDialog>
#include "model_alarm.h"

namespace Ui {
class frmLogAllarmi;
}

class frmLogAllarmi : public QDialog
{
    Q_OBJECT

public:
    explicit frmLogAllarmi(QWidget *parent = 0);
    ~frmLogAllarmi();

private slots:
    void on_buttonExit_3_clicked();

    void on_btnExport_clicked();

private:
    ReportAllarmi alms;

    model_alarm  myModel;
    Ui::frmLogAllarmi *ui;
};

#endif // FRMLOGALLARMI_H
