#include "model.h"
#include <QFont>
#include <QBrush>
MyModel::MyModel(QObject *parent)
    : QAbstractTableModel(parent)
{
    progs = Programmi::getInstance();
    nrows = 1;
}

int MyModel::rowCount(const QModelIndex & /*parent*/) const
{
    return progs->count();
}

int MyModel::columnCount(const QModelIndex & /*parent*/) const
{
    return COLS;
}
QVariant MyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        switch (section)
        {
        case 0:
            return QString("NAME");
        case 1:
            return QString("AREA");
        case 2:
            return QString("EXEC");
        case 3:
            return QString("LAST");
        case 4:
            return QString("TOTAL");
        }
    }
    return QVariant();
}
QVariant MyModel::data(const QModelIndex &index, int role) const
{    
    prog_s prog;
    int row = index.row();
    int col = index.column();
    // generate a log message when this method gets called
    //qDebug(QString("row %1, col%2, role %3").arg(row).arg(col).arg(role));

    switch (role) {
    case Qt::DisplayRole:

        //
        prog = progs->get_byindex( row );

//            myModel.m_gridData[n_prog][0] =  prog.nome;
//            myModel.m_gridData[n_prog][1] =  QString( "%1").arg(prog.naree);
//            myModel.m_gridData[n_prog][2] =  QString( "%1").arg(prog.esecuzioni);
//            //ore = QString( "%1:%1").arg((prog.secondi_impiegati / 3600),(prog.secondi_impiegati / 60);
//            myModel.m_gridData[n_prog][3] =  SecondToTime(prog.secondi_impiegati);
//            myModel.m_gridData[n_prog][4] =  SecondToTime(prog.secondi_totali);

        if( col == 0 ) return prog.nome;
        if( col == 1 ) return QString( "%1").arg(prog.naree);
        if( col == 2 ) return QString( "%1").arg(prog.esecuzioni);
        if( col == 3 )
        {
            int sec = prog.secondi_impiegati;
            int seconds = sec % 60;
            int minutes = (sec / 60) % 60;
            int hours = (sec / 60 / 60);

            QString timeString = QString("%1:%2:%3")
                .arg(hours, 2, 10, QChar('0'))
                .arg(minutes, 2, 10, QChar('0'))
                .arg(seconds, 2, 10, QChar('0'));

            return timeString;
        }
        if( col == 4 )
        {
            int sec = prog.secondi_totali;
            int seconds = sec % 60;
            int minutes = (sec / 60) % 60;
            int hours = (sec / 60 / 60);

            QString timeString = QString("%1:%2:%3")
                .arg(hours, 2, 10, QChar('0'))
                .arg(minutes, 2, 10, QChar('0'))
                .arg(seconds, 2, 10, QChar('0'));

            return timeString;
        }

//        if(m_gridData[row][col] != NULL)
//            return m_gridData[row][col];
    case Qt::FontRole:

        if (row == -1) { //change font only for cell(0,0)
            QFont boldFont;
            boldFont.setBold(true);
            return boldFont;
        }
        break;
    case Qt::BackgroundRole:
        //if (row == 0)  //change background
        //    return QBrush(Qt::red);
        break;
    case Qt::TextAlignmentRole:
       // if (row == 1 && col == 1) //change text alignment only for cell(1,1)
       //     return int(Qt::AlignRight | Qt::AlignVCenter);
        break;
    case Qt::CheckStateRole:
        //if (row == 1 && col == 0) //add a checkbox to cell(1,0)
        //    return Qt::Checked;
        break;
    }
    return QVariant();
}

QString MyModel::SecondToTime(int sec)
{
    int seconds = sec % 60;
    int minutes = (sec / 60) % 60;
    int hours = (sec / 60 / 60);

    QString timeString = QString("%1:%2:%3")
        .arg(hours, 2, 10, QChar('0'))
        .arg(minutes, 2, 10, QChar('0'))
        .arg(seconds, 2, 10, QChar('0'));

    return timeString;
}

bool MyModel::insertRows(int row, int count, const QModelIndex &parent)
{
}

bool MyModel::insertProgs(prog_s prog)
{
}
