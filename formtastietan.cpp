#include "formtastietan.h"
#include "ui_formtastietan.h"

#include <QDebug>

FormTastieraN::FormTastieraN(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormTastieraN)
{
    ui-> setupUi(this);

    max =  999999;
    min = -999999;
    def = 0;

    primo = true;
    isInteger = false;
    maxdec    = 2;

    ui->labelVal->setText( "0" );
    ui->buttonDef->setVisible(false);

}

FormTastieraN::~FormTastieraN()
{
    delete ui;
}

void FormTastieraN::set_title(QString title)
{
    ui->labelTitle->setText( title );
}

void FormTastieraN::set_unita_misura(QString um)
{
    ui->labelUM->setText( um);
}

void FormTastieraN::set_max(float val)
{
    max = val;
}

void FormTastieraN::set_min(float val)
{
    min = val;
}

void FormTastieraN::set_default(float val)
{
    def = val;
}

void FormTastieraN::set_maxdec(int val)
{
    maxdec = val;
}

void FormTastieraN::set_integer(bool val)
{
    isInteger = val;
    if( val == true ) ui->buttonPunto->setEnabled( false );
    else              ui->buttonPunto->setEnabled( true );
}

float FormTastieraN::get_val()
{
    return ui->labelVal->text().toFloat();
}

QString FormTastieraN::get_string()
{
    return ui->labelVal->text();
}

void FormTastieraN::add_num(QString num)
{
    QString next;
    if( primo == true && num == "0" ) return;
    if( primo == true && num == "." )
    {
        ui->labelVal->setText( "0.");
        primo = false;
        return;
    }
    if( primo == true )
    {
        ui->labelVal->setText( "" );
        primo = false;
    }

    QStringList list = ui->labelVal->text().split(".");
    if( list.count() == 2 )
    {
        if( list.at(1).length() < maxdec ) next = ui->labelVal->text() + num;
        else                               next = ui->labelVal->text();
    }
    else
        next  = ui->labelVal->text() + num;

    qDebug( next.toLatin1() );

    ui->labelVal->setText( next );

    verifica_limiti();
}

bool FormTastieraN::verifica_limiti()
{
    float val = ui->labelVal->text().toFloat();

    if( val > max || val < min )
    {
        ui->labelVal->setStyleSheet( "Color : red");
        return false;
    }
    else
    {
        ui->labelVal->setStyleSheet( "Color : black");
        return true;
    }

}
void FormTastieraN::on_buttonOK_clicked()
{
    if( verifica_limiti() == true ){
        valid = true;
        close();
    }
}

void FormTastieraN::on_button0_clicked()
{
    if( ui->labelVal->text() != "0" )  add_num( "0" );
}

void FormTastieraN::on_button1_clicked()
{
    add_num( "1" );
}

void FormTastieraN::on_button2_clicked()
{
    add_num( "2" );
}

void FormTastieraN::on_button3_clicked()
{
    add_num( "3" );
}

void FormTastieraN::on_button4_clicked()
{
    add_num( "4" );
}

void FormTastieraN::on_button5_clicked()
{
    add_num ( "5" );
}

void FormTastieraN::on_button6_clicked()
{
    add_num( "6" );
}

void FormTastieraN::on_button7_clicked()
{
    add_num( "7" );
}

void FormTastieraN::on_button8_clicked()
{
    add_num( "8" );
}

void FormTastieraN::on_button9_clicked()
{
    add_num( "9");
}

void FormTastieraN::on_buttonPunto_clicked()
{
    if( ui->labelVal->text().contains( "." ) == false ) add_num( "." );
}

void FormTastieraN::on_buttonCanc_clicked()
{
    QString next = ui->labelVal->text();

    if( next.size() > 1 )
        next.remove( next.size() -1, 1 );
    else
        next = "0";

    ui->labelVal->setText( next );
    verifica_limiti();
}

bool FormTastieraN::is_valid()
{
   return valid;
}
void FormTastieraN::on_buttonAnnulla_clicked()
{
    ui->labelVal->setText( "" );
    valid = false;
    close();
}

void FormTastieraN::on_buttonMax_clicked()
{
    ui->labelVal->setText( QString("%1").arg(max)  );
    if( max != 0 ) primo = false;
    else           primo = true;
    ui->labelVal->setStyleSheet( "Color : black");
}

void FormTastieraN::on_buttonMin_clicked()
{
    ui->labelVal->setText( QString("%1").arg(min)  );
    if( min != 0 ) primo = false;
    else           primo = true;
    ui->labelVal->setStyleSheet( "Color : black");
}

void FormTastieraN::on_buttonDef_clicked()
{
    ui->labelVal->setText( QString("%1").arg(def)  );
    if( min != 0 ) primo = false;
    else           primo = true;
    ui->labelVal->setStyleSheet( "Color : black");
}

void FormTastieraN::on_buttonPM_clicked()
{
    QString next = ui->labelVal->text();

    if( next.contains( "-") )
    {
        next.remove(0,1);
    }
    else
    {
        next.insert(0,"-");
    }
    ui->labelVal->setText( next );
}
