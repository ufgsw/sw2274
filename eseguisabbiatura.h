#ifndef ESEGUISABBIATURA_H
#define ESEGUISABBIATURA_H
#include <QThread>
#include "sabbiatrice.h"


class EseguiSabbiatura: public QThread
{
public:
    Sabbiatrice     *sabbiatrice;

    bool    Started;
    bool    Fine;

    int     currarea;
    int     totaree;
    int     naree;

    int     Y_tot_da_fare;
    int     X_tot_da_fare;
    int     X_area_da_fare;
    int 	X_area_fatto;
    int 	X_tot_fatto;

    int     Stop_manuale;
    int     Start_sabbiatura;

    int     fatt_unita;

    QString DoHoming;


    EseguiSabbiatura();
    void Esegui_Sabbiatura(u_int32_t initial_data);
private:

    bool    mRun;



    int     quotax;
    int     quotay;
    int 	abspassiX;

    int      xareamissing;
    int32_t  timery;


    int     xtotdafare;
    int     xareadafare;

    short   FineArea(short dirarea, int xfin );
    int     XtotDaFare();
    int     YtotDaFare();



protected:
    void run() override;

};

#endif // ESEGUISABBIATURA_H
