#include "orologio.h"

#include <QDebug>
#include <QPainter>
#include <QTimer>
#include <QProcess>

Orologio::Orologio(QWidget *parent) : QWidget(parent)
{
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Orologio::update );
    timer->start(1000);

    new_time = QTime::currentTime();
    new_date = QDate::currentDate();
}

void Orologio::set_time(QTime time)
{
    new_time = time;
    salva();
    repaint();
}

void Orologio::set_date(QDate date)
{
    new_date = date;
    salva();
    repaint();
}

QString Orologio::get_date_time_string()
{
    QString str;
    //QDate new_date = QTime::currentTime();
    //QTime new_time = QDate::currentDate();

    str = QDate::currentDate().toString("dd.MM.yyyy");
    str += " ";
    str += QTime::currentTime().toString("hh:mm:ss");


    return str;

}

void Orologio::paintEvent(QPaintEvent *event)
{
    static const QPoint hourHand[3] =
    {
        QPoint(5, 6),
        QPoint(-5, 6),
        QPoint(0, -40)
    };
    static const QPoint minuteHand[3] =
    {
        QPoint(5, 6),
        QPoint(-5, 6),
        QPoint(0, -70)
    };
    static const QPoint secondHand[3] =
    {
        QPoint(3, 4),
        QPoint(-3, 4),
        QPoint(0, -90)
    };

    QColor hourColor(127, 0, 127);
    QColor minuteColor(0, 127, 127, 191);
    QColor secondColor(250, 20, 20);

    int side = qMin(width(), height());
    QTime time = QTime::currentTime();
    QDate date = QDate::currentDate();

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.translate(width() / 2, height() / 2);
    painter.scale(side / 200.0, side / 200.0);

    painter.setPen(Qt::NoPen);
    painter.setBrush(hourColor);

    painter.save();
    painter.rotate(30.0 * ((time.hour() + time.minute() / 60.0)));
    painter.drawConvexPolygon(hourHand, 3);
    painter.restore();

    painter.setPen(hourColor);

    for (int i = 0; i < 12; ++i)
    {
        painter.drawLine(88, 0, 96, 0);
        painter.rotate(30.0);
    }

    painter.setPen(Qt::NoPen);
    painter.setBrush(minuteColor);

    painter.save();
    painter.rotate(6.0 * (time.minute() + time.second() / 60.0));
    painter.drawConvexPolygon(minuteHand, 3);
    painter.restore();

    painter.setPen(minuteColor);

    for (int j = 0; j < 60; ++j)
    {
        if ((j % 5) != 0)
            painter.drawLine(92, 0, 96, 0);
        painter.rotate(6.0);
    }

    painter.setPen(Qt::NoPen);
    painter.setBrush(secondColor);

    painter.save();
    painter.rotate(6.0 * time.second() );
    painter.drawConvexPolygon(secondHand, 3);
    painter.restore();

    painter.setPen(Qt::blue);
    painter.setBrush(Qt::NoBrush);
    painter.save();
    painter.drawEllipse( QPoint( 0,0),98,98);
    painter.restore();

    emit update_datetime(date,time);
}

void Orologio::salva()
{
    // date +%Y%m%d -s "20081128"
    // date +%T -s "10:13:13"

    QString cmdData = "date";
    QString cmdSave = "hwclock";

    QStringList argumentData;
    QStringList argumentSave;

    QString sTime = QString("%1:%2:%3")
            .arg( new_time.hour() )
            .arg( new_time.minute())
            .arg( new_time.second() );

    QString sDate = QString("%1%2%3")
            .arg(new_date.year(),4,10,QChar('0'))
            .arg(new_date.month(),2,10,QChar('0'))
            .arg(new_date.day(),2,10,QChar('0'));

    // Salvo la data
    argumentData.clear();
    argumentData << "+%Y%m%d" << "-s" << sDate;

    QProcess *myProces = new QProcess(this);
    myProces->start( cmdData , argumentData );
    myProces->waitForFinished( 10000 );
    qDebug( myProces->readAll() );

    delete myProces;

    // Salvo l'orario
    argumentData.clear();
    argumentData << "+%T" << "-s" << sTime;

    myProces = new QProcess(this);
    myProces->start( cmdData, argumentData );
    myProces->waitForFinished( 10000 );
    qDebug( myProces->readAll() );

    delete myProces;

    // Salvo nell'orologio di sistema ( Hardware )  hwclock --systohc
    argumentSave << "--systohc";

    myProces = new QProcess(this);
    myProces->start( cmdSave , argumentSave );
    myProces->waitForFinished( 10000 );
    qDebug( myProces->readAll() );

    delete myProces;

    // Modifico il file timestamp
    QFile timestamp;
    timestamp.setFileName("/etc/timestamp");
    //if( timestamp.exists() )
    {
        // Orario esempio : 2022 09 15 09 26 25
        timestamp.open( QIODevice::WriteOnly );

        char s[14];
        sprintf( s, "%04d%02d%02d%02d%02d%02d", new_date.year(), new_date.month(), new_date.day(), new_time.hour(), new_time.minute(), new_time.second());

        timestamp.write( QByteArray(s), 14);

        timestamp.close();
    }

    // sync
    myProces = new QProcess(this);
    myProces->start("sync");
    myProces->waitForFinished();
    delete myProces;
}

void Orologio::update()
{
    repaint();
}
