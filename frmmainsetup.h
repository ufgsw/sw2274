#ifndef FRMMAINSETUP_H
#define FRMMAINSETUP_H

#include <QDialog>
#include "frmedittimer.h"
#include <sabbiatrice.h>
#include "maintenance.h"

namespace Ui {
class frmMainSetup;
}

class frmMainSetup : public QDialog
{
    Q_OBJECT

public:
    u_int16_t result;
    Sabbiatrice *sabbiatrice;
    QString n_pistole;


    void Init(int alarm,QString n_pistole);
    explicit frmMainSetup(QWidget *parent = 0);
    ~frmMainSetup();

private slots:

    void on_buttonExit_3_clicked();

    void on_btn1_clicked();
    
    void on_btn2_clicked();

    void on_btn3_clicked();

    void on_btn4_clicked();

    void on_btn5_clicked();

    void on_btn6_clicked();

    void on_btn7_clicked();

    void on_btn8_clicked();

    void on_btn9_clicked();

    void on_btn10_clicked();

private:
    
    frmEditTimer    *frmedittimer;

    Ui::frmMainSetup *ui;
};

#endif // FRMMAINSETUP_H
