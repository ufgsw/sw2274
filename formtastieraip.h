#ifndef FORMTASTIERAIP_H
#define FORMTASTIERAIP_H

#include <QDialog>

namespace Ui {
class FormTastieraIP;
}

class FormTastieraIP : public QDialog
{
    Q_OBJECT

public:
    explicit FormTastieraIP(QWidget *parent = 0);
    ~FormTastieraIP();

    QString get_val();
    bool    is_valid();

private slots:
    void on_button1_clicked();

    void on_button2_clicked();

    void on_button3_clicked();

    void on_button4_clicked();

    void on_button5_clicked();

    void on_button6_clicked();

    void on_button7_clicked();

    void on_button8_clicked();

    void on_button9_clicked();

    void on_buttonCanc_clicked();

    void on_button0_clicked();

    void on_buttonPunto_clicked();

    void on_buttonOK_clicked();

    void on_buttonAnnulla_clicked();

private:
    Ui::FormTastieraIP *ui;

    bool valid;
    bool primo;
    int  punti;
};

#endif // FORMTASTIERAIP_H
