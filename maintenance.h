#ifndef MAINTENANCE_H
#define MAINTENANCE_H

#include <QObject>
#include <QDateTime>
#include <QList>

typedef struct
{
    QString   alert;
    QString   done;
    QString   reset;
    int       lifespan;
    bool      end;
}maintenance_s;

class Maintenance : public QObject
{
    Q_OBJECT
public:
    explicit Maintenance(QObject *parent = nullptr);

    static Maintenance* GetInstance()
    {
        static Maintenance* instance = 0;
        if( instance == 0 )
        {
            instance = new Maintenance();
            instance->load();
        }
        return instance;
    }

    void load();
    void save();
    void save_as(QString path);
    void add( QString code);
    void reset( QString code, int lifespan);
    void clear();

    bool is_pending( QString code);

    QList<maintenance_s> manutenzioni;

private:

signals:

public slots:
};

#endif // MAINTENANCE_H
