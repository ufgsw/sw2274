#ifndef FRMNEWNAME_H
#define FRMNEWNAME_H

#include <QDialog>

namespace Ui {
class frmNewName;
}

class frmNewName : public QDialog
{
    Q_OBJECT

public:
    int result;
    QString nome;

    void Init(QString nome);
    explicit frmNewName(QWidget *parent = 0);
    ~frmNewName();

private slots:
    void on_buttonExit_clicked();

    void on_buttonExit_3_clicked();

    void on_buttonExit_8_clicked();

private:
    Ui::frmNewName *ui;
};

#endif // FRMNEWNAME_H
