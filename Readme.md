# MISTRAL IOT

La macchina è in grado di comunicare con un Broker esterno tramite protocollo **MQTT**



#### Gestione delle **commesse**

Il topic deve essere registrato con **/pezza/serial_number/jobs** mentre il payload è una serie di informazioni in formato **JSON**

I tempi ( last e total ) sono in secondi

```json
{
    "model" : "mistral xx",
    "Jobs": [
        {
            "name":"Commessa 3",
            "program":"Vetro 1",
            "to_do":15,
            "done":0,
            "last" : 3600,
            "total" : 18000
        },
        {
            "name":"Commessa 4",
            "program":"Vetro 2",
            "to_do":10,
            "done":0,
            "last" : 3600, 
            "total" : 18000
        }
    ]
}

```



#### Ricezione degli **allarmi**

Il topic deve essere registrato con **/pezza/serial_number/alarms** mentre il payload è una serie di informazioni in formato **JSON**

```json
{
  "model" : "mistral xx",
  "Alarms": [
    {
      "code": 2,
      "date_on":  "Sat Dec 2 2021 4:21:22",
      "date_off": "Sat Dec 2 2021 5:21:22",
    },
    {
      "code": 3,
      "date_on":  "Sat Dec 2 2021 7:21:22",
      "date_off": "Sat Dec 2 2021 8:21:22",
    },
    {
      "code": 4,
      "date_on":  "Sat Dec 2 2021 10:21:22",
      "date_off": "Sat Dec 2 2021 11:21:22",
    },
    {
      "code": 6,
      "date_on":  "Sat Dec 2 2021 14:21:22",
      "date_off": "Sat Dec 2 2021 15:21:22",
    }
  ]
}
```



#### Ricezione dei contatori ( in secondi )

Il topic deve essere registrato con **/pezza/serial_number/timers** mentre il payload è una serie di informazioni in formato **JSON**

```json
{
  "model" : "mistral xx",
  "Timer":[
      {
        "timer_fan":180017,
        "timer_partial":44,
        "timer_autogun":10553,
        "clean_filter":8969,

        "turn_hmguard":5489,
        "turn_nozzle1":602,
        "turn_nozzle2":2541,
        "replace_holder1":3620,
        "replace_holder2":2541,
        "replace_hmguard":5489,
        "replace_gunbody1":3620,
        "replace_gunbody2":2541,
        "replace_bearings":5489,
        "replace_filter":8969,
        "replace_conveyorpulley":3828,
        "replace_gunpulley":5489,
        "replace_suctionpipe1":3620,
        "replace_suctionpipe2":3809,
        "replace_nozzle1":602,
        "replace_nozzle2":2541,
        "replace_airnozzle1":3620,
        "replace_airnozzle2":2541
      }
  ]
}
```



#### Ricezione degli avvisi di manutenzione

Il topic deve essere registrato con **/pezza/serial_number/maintenance** mentre il payload è una serie di informazioni in formato **JSON**

```json
{
    "model" : "mistral xx",
    "Maintenance": [
	    {
    		"alert" : "8.0",
    		"done" : "data scadenza",
    		"reset" : "data di reset alert",
    		"lifespan" : "minuti"
    	}
    ]                                                                           
}  
```



#### Ricezione dello stato macchina

Il topic deve essere registrato con **/pezza/serial_number/status** mentre il payload è una serie di informazioni in formato **JSON**

```json
{
    "model" : "mistral xx",    
    "status": "run o stop",
    "prog" : "nome programma corrente",
    "release" : xx
}
```



---



#### Reset del log allarmi

Il topic deve essere registrato con **/pezza/serial_number/command** mentre il payload è una serie di informazioni in formato **JSON**

```json
{
  "Alarms": 0,
}
```

#### Reset dell' errore service

Il topic deve essere registrato con **/pezza/serial_number/command** mentre il payload è una serie di informazioni in formato **JSON**

```json
{
  "Service": 0,
}
```

#### Richiesta dello stato macchina

Il topic deve essere registrato con **/pezza/serial_number/command** mentre il payload è una serie di informazioni in formato **JSON**

```json
{
  "Status": 0,
}
```







