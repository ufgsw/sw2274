#include "frmplaytimer.h"
#include "ui_frmplaytimer.h"

frmPlayTimer::frmPlayTimer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmPlayTimer)
{
    ui->setupUi(this);
}

frmPlayTimer::~frmPlayTimer()
{
    delete ui;
}
void frmPlayTimer::Init(int cronometro)
{
    uint h,m,s;
    h = (uint)(  cronometro / 3600);
    m = (uint)(( cronometro - ( h * 3600) ) / 60);
    s = (uint)(( cronometro - ( h * 3600) - (m * 60)));

    ui->lbTempo->setText(QString("h : %1:%2:%3").arg(h,2,10,QChar('0')).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));

}

void frmPlayTimer::on_buttonExit_3_clicked()
{
    result = -1;
    close();
}

void frmPlayTimer::on_btnStart_clicked()
{
    result = 1;
    close();
}
