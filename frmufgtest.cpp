#include "frmufgtest.h"
#include "ui_frmufgtest.h"
#include "formtastietan.h"
#include "formtastieraa.h"
#include "iot.h"
#include "formsetupethernet.h"

frmUfgTest::frmUfgTest(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmUfgTest)
{
    ui->setupUi(this);

    pressed = false;

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &frmUfgTest::processTimer);
    timer->start(200);
}

frmUfgTest::~frmUfgTest()
{
    delete ui;
}

void frmUfgTest::processTimer()
{
    timer->stop();

}
void frmUfgTest::Init()
{
    ui->btnOffsetY->setText( QString("%1").arg(sabbiatrice->parametri->param.ofsety));
    ui->btnOffsetX->setText( QString("%1").arg(sabbiatrice->parametri->param.ofsetx));
    ui->btnFiltroPausa->setText( QString("%1").arg(sabbiatrice->parametri->param.timer_pausa));
    ui->btnFiltroLavoro->setText( QString("%1").arg( (float)sabbiatrice->parametri->param.timer_lavoro / 10));
    ui->btnVentilatore->setText( QString("%1").arg(sabbiatrice->parametri->param.timeout_filtro));
    ui->btnVelAsseX->setText( QString("%1").arg(sabbiatrice->parametri->param.velox));
    ui->btnQuotaMaxY->setText( QString("%1").arg(sabbiatrice->parametri->param.quotamax_y));
    ui->btnAnticipoStep->setText( QString("%1").arg(sabbiatrice->parametri->param.anticipoy));
    ui->cb40->setChecked(sabbiatrice->parametri->param.as4_0);

    //ui->btnUDPPort->setText( QString("%1").arg(sabbiatrice->Par.UDPPort));
    //ui->btnIPServer->setText(sabbiatrice->Par.IPServer);


    ui->cbModello->setCurrentText(QString(progs->modello));

}

float frmUfgTest::GetNumber(QString title,float min,float max,bool asinteger)
{
    float num;

    FormTastieraN *uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title( tr(title.toLatin1()) );
    uiTastieraN->set_max( max );
    uiTastieraN->set_min( min );
    uiTastieraN->set_default( 1 );
    uiTastieraN->set_integer( asinteger );

    uiTastieraN->exec();
    if(uiTastieraN->is_valid()){
        num = uiTastieraN->get_val();
    }
    else{
        num = -1;
    }

    delete uiTastieraN;

    return num;

}

void frmUfgTest::on_btnAvantiX_pressed()
{
    if(!pressed)
    {
        pressed = true;
        sabbiatrice->Move_Mot_Vel(MOT_X,1000);
    }
}

void frmUfgTest::on_btnAvantiX_released()
{
    if(pressed)
    {
        sabbiatrice->Stop_Mot_Vel(MOT_X,0);
        pressed = false;
    }
}

void frmUfgTest::on_btnAvantiY_pressed()
{
    if(!pressed)
    {
        pressed = true;
        sabbiatrice->Move_Mot_Vel(MOT_Y,1000);
    }
}

void frmUfgTest::on_btnAvantiY_released()
{
    if(pressed)
    {
        sabbiatrice->Stop_Mot_Vel(MOT_Y,0);
        pressed = false;
    }
}

void frmUfgTest::on_buttonExit_3_clicked()
{
    sabbiatrice->parametri->param.as4_0 = ui->cb40->checkState();
    close();
}




void frmUfgTest::on_btnOffsetY_clicked()
{
    float num =  GetNumber("",0,100,true);
    if(num >= 0.){
        sabbiatrice->parametri->param.ofsety = num;
    }
    ui->btnOffsetY->setText( QString("%1").arg(sabbiatrice->parametri->param.ofsety));
}

void frmUfgTest::on_btnOffsetX_clicked()
{
    float num =  GetNumber("",0,100,true);
    if(num >= 0.){
        sabbiatrice->parametri->param.ofsetx = num;
    }
    ui->btnOffsetX->setText( QString("%1").arg(sabbiatrice->parametri->param.ofsetx));

}

void frmUfgTest::on_btnFiltroPausa_clicked()
{
    float num =  GetNumber("",0,60,true);
    if(num >= 0.){
        sabbiatrice->parametri->param.timer_pausa = num;
    }
    ui->btnFiltroPausa->setText( QString("%1").arg(sabbiatrice->parametri->param.timer_pausa));

}

void frmUfgTest::on_btnFiltroLavoro_clicked()
{
    float num =  GetNumber("",0.3,30.0,false);
    if(num >= 0.){
        sabbiatrice->parametri->param.timer_lavoro = num * 10;
    }
    ui->btnFiltroLavoro->setText( QString("%1").arg(  (float)sabbiatrice->parametri->param.timer_lavoro / 10 ));

}

void frmUfgTest::on_btnVentilatore_clicked()
{
    float num =  GetNumber("",0,600,true);
    if(num >= 0.){
        sabbiatrice->parametri->param.timeout_filtro = num;
    }
    ui->btnVentilatore->setText( QString("%1").arg(sabbiatrice->parametri->param.timeout_filtro));

}

void frmUfgTest::on_btnVelAsseX_clicked()
{
    float num =  GetNumber("",50,100,true);
    if(num >= 0.){
        sabbiatrice->parametri->param.velox = num;
    }
    ui->btnVelAsseX->setText( QString("%1").arg(sabbiatrice->parametri->param.velox));
}


void frmUfgTest::on_cbModello_currentIndexChanged(int index)
{
    if(index == 0)      progs->modello = "Mistral 120 ev+";
    else if(index == 1) progs->modello = "Mistral 180 ev+";
    else if(index == 2) progs->modello = "Mistral 260 ev+";
    else if(index == 3) progs->modello = "Mistral 230 v";
    else if(index == 4) progs->modello = "Zephir 120+";
    else if(index == 5) progs->modello = "Zephir 180+";
    else if(index == 6) progs->modello = "Zephir 260+";


    switch(index){
        case 0:
            sabbiatrice->parametri->param.anticipoy = 1;
            sabbiatrice->parametri->param.quotamax_y = 1200;
            sabbiatrice->parametri->param.ofsety = 15;
            sabbiatrice->parametri->param.ofsetx = 400;
            sabbiatrice->parametri->param.timer_pausa = 60;
            sabbiatrice->parametri->param.timer_lavoro = 2;
            sabbiatrice->parametri->param.timeout_filtro = 25;
            sabbiatrice->parametri->param.velox = 100;
        break;
        case 1:
            sabbiatrice->parametri->param.anticipoy = 1;
            sabbiatrice->parametri->param.quotamax_y = 1800;
            sabbiatrice->parametri->param.ofsety = 15;
            sabbiatrice->parametri->param.ofsetx = 400;
            sabbiatrice->parametri->param.timer_pausa = 60;
            sabbiatrice->parametri->param.timer_lavoro = 2;
            sabbiatrice->parametri->param.timeout_filtro = 25;
            sabbiatrice->parametri->param.velox = 100;
        break;
        case 2:
            sabbiatrice->parametri->param.anticipoy = 1;
            sabbiatrice->parametri->param.quotamax_y = 2600;
            sabbiatrice->parametri->param.ofsety = 20;
            sabbiatrice->parametri->param.ofsetx = 400;
            sabbiatrice->parametri->param.timer_pausa = 60;
            sabbiatrice->parametri->param.timer_lavoro = 2;
            sabbiatrice->parametri->param.timeout_filtro = 25;
            sabbiatrice->parametri->param.velox = 100;
        break;
        case 3:
            sabbiatrice->parametri->param.anticipoy = 1;
            sabbiatrice->parametri->param.quotamax_y = 2300;
            sabbiatrice->parametri->param.ofsety = 20;
            sabbiatrice->parametri->param.ofsetx = 400;
            sabbiatrice->parametri->param.timer_pausa = 60;
            sabbiatrice->parametri->param.timer_lavoro = 2;
            sabbiatrice->parametri->param.timeout_filtro = 25;
            sabbiatrice->parametri->param.velox = 100;
        break;
        case 4:
            sabbiatrice->parametri->param.anticipoy = 1;
            sabbiatrice->parametri->param.quotamax_y = 1200;
            sabbiatrice->parametri->param.ofsety = 15;
            sabbiatrice->parametri->param.ofsetx = 400;
            sabbiatrice->parametri->param.timer_pausa = 60;
            sabbiatrice->parametri->param.timer_lavoro = 2;
            sabbiatrice->parametri->param.timeout_filtro = 10;
            sabbiatrice->parametri->param.velox = 100;
        break;
        case 5:
            sabbiatrice->parametri->param.anticipoy = 1;
            sabbiatrice->parametri->param.quotamax_y = 1800;
            sabbiatrice->parametri->param.ofsety = 15;
            sabbiatrice->parametri->param.ofsetx = 400;
            sabbiatrice->parametri->param.timer_pausa = 60;
            sabbiatrice->parametri->param.timer_lavoro = 2;
            sabbiatrice->parametri->param.timeout_filtro = 25;
            sabbiatrice->parametri->param.velox = 100;
        break;
        case 6:
            sabbiatrice->parametri->param.anticipoy = 1;
            sabbiatrice->parametri->param.quotamax_y = 2600;
            sabbiatrice->parametri->param.ofsety = 20;
            sabbiatrice->parametri->param.ofsetx = 400;
            sabbiatrice->parametri->param.timer_pausa = 60;
            sabbiatrice->parametri->param.timer_lavoro = 2;
            sabbiatrice->parametri->param.timeout_filtro = 25;
            sabbiatrice->parametri->param.velox = 100;
        break;

    }

    ui->btnOffsetY->setText( QString("%1").arg(sabbiatrice->parametri->param.ofsety));
    ui->btnOffsetX->setText( QString("%1").arg(sabbiatrice->parametri->param.ofsetx));
    ui->btnFiltroPausa->setText( QString("%1").arg(sabbiatrice->parametri->param.timer_pausa));
    ui->btnFiltroLavoro->setText( QString("%1").arg(sabbiatrice->parametri->param.timer_lavoro));
    ui->btnVentilatore->setText( QString("%1").arg(sabbiatrice->parametri->param.timeout_filtro));
    ui->btnVelAsseX->setText( QString("%1").arg(sabbiatrice->parametri->param.velox));
    ui->btnQuotaMaxY->setText( QString("%1").arg(sabbiatrice->parametri->param.quotamax_y));
    ui->btnAnticipoStep->setText( QString("%1").arg(sabbiatrice->parametri->param.anticipoy));

    //ui->btnUDPPort->setText( QString("%1").arg(sabbiatrice->Par.UDPPort));
    //ui->btnIPServer->setText(sabbiatrice->Par.IPServer);

    qDebug(progs->modello.toLatin1());


}

void frmUfgTest::on_btnModello_clicked()
{

}

void frmUfgTest::on_btnIPServer_clicked()
{
//    FormTastieraA *uiTastieraA = new FormTastieraA(this);

//    uiTastieraA->set_text(sabbiatrice->Par.IPServer);
//    uiTastieraA->exec();
//    if(uiTastieraA->is_valid()){
//        QString IPServer;
//        IPServer = uiTastieraA->get_text();
//        strcpy(sabbiatrice->Par.IPServer,IPServer.toLatin1());
//        ui->btnIPServer->setText(IPServer);
//        Iot *iot = new Iot();
//        iot->configure_broker(IPServer);

//        delete iot;
//    }

//    delete uiTastieraA;



}

void frmUfgTest::on_btnUDPPort_clicked()
{


//    FormTastieraN *uiTastieraN = new FormTastieraN(this);

//    uiTastieraN->set_title("UDP");
//    uiTastieraN->set_max( 20000 );
//    uiTastieraN->set_min( 1024 );
//    uiTastieraN->set_default( 5000 );
//    uiTastieraN->set_integer( true );

//    uiTastieraN->exec();
//    float num =   uiTastieraN->get_val();
//    if(num >= 0.){
//        sabbiatrice->Par.UDPPort = num;
//    }

//    ui->btnUDPPort->setText( QString("%1").arg(sabbiatrice->Par.UDPPort));

//    delete uiTastieraN;


}

void frmUfgTest::on_btnQuotaMaxY_clicked()
{

}

void frmUfgTest::on_btnIPLocal_2_clicked()
{

    FormSetupEthernet *frmSetupEth = new FormSetupEthernet(this);

    frmSetupEth->exec();

    delete frmSetupEth;

/*



    FormTastieraA *uiTastieraA = new FormTastieraA(this);

    uiTastieraA->set_text(sabbiatrice->Par.IPLocal);
    uiTastieraA->exec();
    QString IPLocal;
    IPLocal = uiTastieraA->get_text();
    strcpy(sabbiatrice->Par.IPLocal,IPLocal.toLatin1());
    ui->btnIPLocal->setText(IPLocal);

    delete uiTastieraA;

*/

}
