#include "frmbuildprogram.h"
#include "ui_frmbuildprogram.h"
#include "w_paneldraw.h"
#include "formtastietan.h"
#include "savemsg.h"
#include "frmconfirm.h"
#include <formnewprogram.h>
#include "formmessagebox.h"


frmbuildprogram::frmbuildprogram(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmbuildprogram)
{
    ui->setupUi(this);
    scribbleArea = NULL;
    PixelVetroHeigth = 400;
    PixelVetroWidth  = 620;

     scribbleArea = new ScribbleArea(this);
    //+++scribbleArea->Prog = progs->current();
    //centralWidget()->setLayout(ui->verticalLayout);
    //setCentralWidget(scribbleArea);



    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &frmbuildprogram::processTimer);
    timer_refresh = new QTimer(this);
    connect(timer_refresh, &QTimer::timeout, this, &frmbuildprogram::processTimerRefresh);

}

frmbuildprogram::~frmbuildprogram()
{
    delete ui;
}


void frmbuildprogram::on_btnAddArea_clicked()
{
    NuovaArea(true);


}
void frmbuildprogram::Init(QString um,int quota_max_y)
{
    scribbleArea->Prog = prog;
    max_y = quota_max_y;
    UM = um;

    ui->lbUM->setText(UM);
    int w = 630; //scribbleArea->width();
    int h = 340; //scribbleArea->width();
    float rapp_y,rapp_x;

    rapp_x = (float)w / (float)scribbleArea->Prog.larghezza_vetro;
    rapp_y = (float)h / (float)scribbleArea->Prog.altezza_vetro;

    if(rapp_x > rapp_y)
    {
        scribbleArea->Prog.pixel_per_mm = rapp_y;
    }
    else
    {
        scribbleArea->Prog.pixel_per_mm = rapp_x;
    }

    PixelVetroHeigth = scribbleArea->Prog.altezza_vetro * scribbleArea->Prog.pixel_per_mm;
    PixelVetroWidth  = scribbleArea->Prog.larghezza_vetro * scribbleArea->Prog.pixel_per_mm;

    AggiornaDimensioni();

    ui->layVetro->setGeometry(QRect(0,0,PixelVetroWidth,PixelVetroWidth));
    ui->layVetro->addWidget(scribbleArea, Qt::AlignCenter); // center alignment

    if(prg->Tipo != MISTRAL)
    {
        ui->btnNextArea->setVisible(false);
        ui->btnPriorArea->setVisible(false);
        ui->lbNumArea->setVisible(false);
    }

    timer->start(1000);
}

void frmbuildprogram::processTimer()
{
    timer->stop();

    scribbleArea->CleanAll();
    //ui->layVetro->setGeometry(QRect(0,0,PixelVetroWidth,PixelVetroWidth));
    for( int n=0; n < scribbleArea->Prog.naree  ; n++)
    {
        scribbleArea->MoveArea(n,QPoint(0,0));

    }
    if(scribbleArea->Prog.naree > 0){
        scribbleArea->SelectArea(0);
    }
    //+++ scribbleArea->DrawVetro();

    timer_refresh->start(1000);

}
void frmbuildprogram::processTimerRefresh()
{
    ui->lbNumArea->setText(QString("%1").arg(scribbleArea->AreaSelected+1));
}
void frmbuildprogram::NuovaArea(bool editable)
{
    if(scribbleArea == NULL)
            return;

    if(prg->Tipo != MISTRAL){
        if(scribbleArea->Prog.naree >= 1){
            return;
        }
    }

    uiEditArea = new formEditArea(this);
    uiEditArea->Tipo = prg->Tipo;
    s_Area area;
    area.x_iniziale         = 0;
    area.x_finale           = scribbleArea->Prog.larghezza_vetro;
    area.y_iniziale         = 0;
    area.y_finale           = scribbleArea->Prog.altezza_vetro;
    area.width              = area.x_finale - area.x_iniziale;
    area.height             = area.y_finale - area.y_iniziale;
    area.pressione_iniziale = 0;
    area.pressione_finale   = 0;
    area.step               = 10;
    area.ripetizioni        = 1;
    area.velocita_nastro    = 100;
    area.velocita_pistola   = 100;
    area.pis1               = 0;
    area.pis2               = 0;

    if(prg->Tipo != MISTRAL)
    {
        prog.secondi_impiegati = 0;
        prog.esecuzioni = 0;
    }

    int16_t narea;
    narea = scribbleArea->Prog.naree;

    if(editable)
    {
        uiEditArea->Init(&area, scribbleArea->Prog. pixel_per_mm, 0, scribbleArea->Prog.altezza_vetro , prg->Automatic_Guns,prg->UM);
        uiEditArea->exec();
    }
    scribbleArea->AddArea(&area,area.x_iniziale,
                            area.y_iniziale,
                            (area.x_finale - area.x_iniziale),
                            (area.y_finale - area.y_iniziale));


    prog.Area[prog.naree] = area;
    scribbleArea->Prog.Area[prog.naree] = area;
    prog.naree++;
    scribbleArea->Prog.naree++;

    prg->replace(scribbleArea->Prog);
    prg->save("");

    timer->start(500);

    delete uiEditArea;
}


void frmbuildprogram::on_btnEditArea_clicked()
{
    if(scribbleArea == NULL || scribbleArea->AreaSelected < 0)
            return;

    uiEditArea = new formEditArea(this);
    uiEditArea->Tipo = prg->Tipo;
    int16_t narea;
    narea = scribbleArea->AreaSelected;
    //qDebug(QString( "Area %1").arg( narea ).toLatin1());

    if(narea >= 0)
    {
        scribbleArea->CleanArea(narea);
        uiEditArea->Init(&(scribbleArea->Prog.Area[narea]),scribbleArea->Prog.pixel_per_mm,0,max_y,prg->Automatic_Guns,prg->UM);
        uiEditArea->exec();
        if(prg->Tipo != MISTRAL)
        {
            prog.secondi_impiegati = 0;
            prog.esecuzioni = 0;
        }
        prg->replace(scribbleArea->Prog);
        prg->save("");

        QPoint offset;
        offset.setX(0);
        offset.setY(0);
        scribbleArea->MoveArea(narea,offset);
        scribbleArea->NormalizzaArea(narea);
    }

    delete uiEditArea;

}

void frmbuildprogram::on_btnDeleteArea_clicked()
{
    on_btnEditProgram_2_clicked();
}

void frmbuildprogram::on_btnEditProgram_2_clicked()
{
    frmConfirm *frmconfirm = new frmConfirm();
    frmconfirm->Init("?",true,0);
    frmconfirm->InitTitle("2.9",":/png/04+.png");
    frmconfirm->exec();
    if(frmconfirm->result == 1)
    {
        scribbleArea->DeleteArea();
        prg->replace(scribbleArea->Prog);
        prog = scribbleArea->Prog;
        prg->save("");

        timer->start(500);
    }
}

void frmbuildprogram::RefreshHeigth()
{

    PixelVetroHeigth = scribbleArea->Prog.altezza_vetro * scribbleArea->Prog.pixel_per_mm;
    PixelVetroWidth  = scribbleArea->Prog.larghezza_vetro * scribbleArea->Prog.pixel_per_mm;


    int w = 630; //scribbleArea->width();
    int h = 340; //scribbleArea->width();
    float rapp_y,rapp_x;

    rapp_x = (float)w / (float)scribbleArea->Prog.larghezza_vetro;
    rapp_y = (float)h / (float)scribbleArea->Prog.altezza_vetro;

    if(rapp_x > rapp_y)
    {
        scribbleArea->Prog.pixel_per_mm = rapp_y;
    }
    else
    {
        scribbleArea->Prog.pixel_per_mm = rapp_x;
    }
    PixelVetroHeigth = scribbleArea->Prog.altezza_vetro * scribbleArea->Prog.pixel_per_mm;
    PixelVetroWidth  = scribbleArea->Prog.larghezza_vetro * scribbleArea->Prog.pixel_per_mm;

    AggiornaDimensioni();

    ui->layVetro->setGeometry(QRect(0,0,PixelVetroWidth,PixelVetroWidth));
    ui->layVetro->addWidget(scribbleArea, Qt::AlignCenter); // center alignment
    timer->start(500);
}

void frmbuildprogram::AggiornaDimensioni()
{

    QString text;
    if(UM == "mm"){
        text.sprintf("%6.0f", (float)scribbleArea->Prog.larghezza_vetro);
         ui->btnWidth->setText(text);
        text.sprintf("%6.0f", (float)scribbleArea->Prog.altezza_vetro);
        ui->btnHeigth->setText(text);
        text.sprintf("%6.0f", (float)scribbleArea->Prog.spessore_vetro);
        ui->btnThickness->setText(text);
    }
    else{
        float fattore_unita = FATTORE_MM_IN;
        text.sprintf("%6.2f", (float)scribbleArea->Prog.larghezza_vetro * fattore_unita);
        ui->btnWidth->setText(text);
        text.sprintf("%6.2f", (float)scribbleArea->Prog.altezza_vetro * fattore_unita);
        ui->btnHeigth->setText(text);
        text.sprintf("%6.2f", (float)scribbleArea->Prog.spessore_vetro * fattore_unita);
        ui->btnThickness->setText(text);

    }
}


void frmbuildprogram::on_buttonExit_3_clicked()
{
   /*
    if(prg->Tipo == MISTRAL){
        if(changed){
            prog = scribbleArea->Prog;
            result = 1;
            changed = false;
        }
        else{
            result = -1;
        }
    }
    else{
        result = -1;
    }
    */

    frmConfirm *frmconfirm = new frmConfirm();
    frmconfirm->Init("?",true,0);
    frmconfirm->InitTitle("2.10",":/png/04+.png");
    frmconfirm->exec();

    if(frmconfirm->result == 1)
    {
        result = -1;
        close();
     }
}

void frmbuildprogram::RefreshWidth()
{


    ui->btnWidth->setText( QString("%1").arg(scribbleArea->Prog.larghezza_vetro));


    int w = 630; //scribbleArea->width();
    int h = 340; //scribbleArea->width();
    float rapp_y,rapp_x;

    rapp_x = (float)w / (float)scribbleArea->Prog.larghezza_vetro;
    rapp_y = (float)h / (float)scribbleArea->Prog.altezza_vetro;

    if(rapp_x > rapp_y){
        scribbleArea->Prog.pixel_per_mm = rapp_y;
    }
    else{
        scribbleArea->Prog.pixel_per_mm = rapp_x;

    }
    PixelVetroHeigth = scribbleArea->Prog.altezza_vetro * scribbleArea->Prog.pixel_per_mm;
    PixelVetroWidth  = scribbleArea->Prog.larghezza_vetro * scribbleArea->Prog.pixel_per_mm;

    AggiornaDimensioni();
    /*
    QString text;
    if(UM == "mm"){
        text.sprintf("%6.0f ", (float)prog.larghezza_vetro);
        text += UM;
        ui->btnWidth->setText(text);
        text.sprintf("%6.0f ", (float)prog.altezza_vetro);
        text += UM;
        ui->btnHeigth->setText(text);
        text.sprintf("%6.0f ", (float)prog.spessore_vetro);
        text += UM;
        ui->btnThickness->setText(text);
    }
    else{
        float fattore_unita = FATTORE_MM_IN;
        text.sprintf("%6.2f ", (float)prog.larghezza_vetro * fattore_unita);
        text += UM;
        ui->btnWidth->setText(text);
        text.sprintf("%6.2f ", (float)prog.altezza_vetro * fattore_unita);
        text += UM;
        ui->btnHeigth->setText(text);
        text.sprintf("%6.2f ", (float)prog.spessore_vetro * fattore_unita);
        text += UM;
        ui->btnThickness->setText(text);

    }
    */

    ui->layVetro->setGeometry(QRect(0,0,PixelVetroWidth,PixelVetroWidth));
    ui->layVetro->addWidget(scribbleArea, Qt::AlignCenter); // center alignment
    timer->start(500);

}

void frmbuildprogram::on_btnSave_clicked()
{
    SaveMsg *uiSave = new SaveMsg(this);
    uiSave->Init(scribbleArea->Prog.nome);
    uiSave->exec();
    prog = scribbleArea->Prog;
    result = 1;
    changed = false;
    close();

}


void frmbuildprogram::on_btnEditProgram_3_clicked()
{
    on_btnEditArea_clicked();
}

void frmbuildprogram::on_buttonExit_8_clicked()
{
    FormNewProgram  *frm = new  FormNewProgram();
    frm->Init(UM,max_y);
    frm->set_prog(scribbleArea->Prog);
    frm->exec();
    scribbleArea->Prog = frm->prog;
    prog = frm->prog;
    AggiornaDimensioni();

    scribbleArea->Prog.altezza_vetro = prog.altezza_vetro;
    scribbleArea->Prog.larghezza_vetro = prog.larghezza_vetro;
    scribbleArea->Prog.spessore_vetro = prog.spessore_vetro;
    RefreshHeigth();
    RefreshWidth();


}


void frmbuildprogram::on_btnNextArea_clicked()
{
    if(scribbleArea->AreaSelected < scribbleArea->Prog.naree-1){
        scribbleArea->MoveArea(scribbleArea->AreaSelected,QPoint(0,0));
        scribbleArea->AreaSelected++;
        scribbleArea->SelectArea(scribbleArea->AreaSelected);
        ui->lbNumArea->setText( QString("%1").arg(scribbleArea->AreaSelected+1));
    }
}

void frmbuildprogram::on_btnPriorArea_clicked()
{
    if(scribbleArea->AreaSelected > 0){
        scribbleArea->MoveArea(scribbleArea->AreaSelected,QPoint(0,0));
        scribbleArea->AreaSelected--;
        scribbleArea->SelectArea(scribbleArea->AreaSelected);
        ui->lbNumArea->setText( QString("%1").arg(scribbleArea->AreaSelected+1));
    }

}

void frmbuildprogram::on_btnThickness_clicked()
{

}

void frmbuildprogram::on_btnExport_clicked()
{
    QString device;

    QString filename;

    filename = "/media/storage/";
    filename += scribbleArea->Prog.nome;
    filename += ".json";

    Programmi prgs;
    prgs.open();

    QDir dir("/dev");
    QFileInfoList list = dir.entryInfoList(QStringList() << "sd*",QDir::System);

    for (int i = 0; i < list.size(); ++i)
    {
        QFileInfo fileInfo = list.at(i);
        device = fileInfo.fileName();
    }

    QString     path;
    QString     program;
    QStringList arguments;

    path = "/dev/" + device;

    QFile file( path);

    if( device != "" && file.exists() )
    {
        QProcess* proc = new QProcess(this);
        program = "mount";
        arguments << path << "/media/storage";

        proc->start(program,arguments);
        proc->waitForFinished(-1);

        prgs.export_prog(filename,scribbleArea->Prog);

        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "EXPORT SUCCESSFUL !");
        uiMessageBox->exec();

        program = "umount";
        arguments.clear();
        arguments << path;

        proc->start( program , arguments );
        proc->waitForFinished(-1);

        delete uiMessageBox;
    }
    else
    {
        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "INSERT USB KEY !");
        uiMessageBox->exec();

        delete uiMessageBox;

    }

}

void frmbuildprogram::on_btnWidth_clicked()
{

}
