#include "canbusthread.h"

void CanBusThread::init()
{
    int iMode = 1; // no blocking
    mInit = true;

    /* open socket */
    if ((socket_d = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        //perror("socket");
        qDebug( "Errore 1 creazione socket ");
        mInit = false;
    }
    else
    {
        strcpy(ifr.ifr_name, "can0");
        ioctl(socket_d, SIOCGIFINDEX, &ifr);
        ioctl(socket_d, FIONBIO, &iMode);

        addr.can_family  = PF_CAN;
        addr.can_ifindex = ifr.ifr_ifindex;

        rfilter[0].can_id   = 0x001;
        rfilter[0].can_mask = 0xfff;
        rfilter[1].can_id   = 0x003;
        rfilter[1].can_mask = 0xfff;
        rfilter[2].can_id   = 0x181;
        rfilter[2].can_mask = 0xfff;
//        setsockopt( socket_d , SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));
//        setsockopt( socket_d , SOL_CAN_RAW, CAN_RAW_FILTER,0 , 0);

        /*
        ifr.ifr_ifru.ifru_ivalue = 1000000/8;
        ret = ioctl(socket_d, SIOCSCANBAUDRATE, &ifr);
        if (ret)
        {
            qDebug( "Errore 2 creazione socket ");
            mInit = false;
        }
        else
*/
        {
            if (bind(socket_d, (struct sockaddr *)&addr, sizeof(addr)) < 0)
            {
                //perror("bind");
                qDebug( "Errore bind creazione socket ");
                mInit = false;
            }
        }
    }
    mRun = true;

    qDebug( "Init thread can ok");
}

void CanBusThread::run()
{
    int lenrx;
    int conta=0;

    init();

    while(mRun)
    {
        QThread::msleep(5);
        lenrx = read(socket_d , &rframe, sizeof(rframe));
        if( lenrx == sizeof(rframe) )
        {
            emit data_receive( rframe.can_id , rframe.can_dlc, rframe.data[0],rframe.data[1],rframe.data[2],rframe.data[3],rframe.data[4],rframe.data[5],rframe.data[6],rframe.data[7] );

            // analizzaCanBus( rframe.can_id , rframe.can_dlc, rframe.data[0],rframe.data[1],rframe.data[2],rframe.data[3],rframe.data[4],rframe.data[5],rframe.data[6],rframe.data[7] );

            //conta++;
            //QString text;
            //text.sprintf("%d", conta);
            //qDebug(text.toLatin1());
            //emit frame_receive( rframe );
        }
    }
}

void CanBusThread::write_frame(can_frame tx)
{
    int nbytes;

    if( mInit == false ) return;

    if ((nbytes = write(socket_d, &tx, sizeof(tx))) != sizeof(tx))
    {
        //qDebug("Write error");
        return;
    }
    //qDebug( "Write ok");

}

//void CanBusThread::analizzaCanBus(int id, unsigned char len, unsigned char b0,unsigned char b1,unsigned char b2,unsigned char b3,unsigned char b4,unsigned char b5,unsigned char b6,unsigned char b7)
//{

//}



