#ifndef FORMNEWPROGRAM_H
#define FORMNEWPROGRAM_H

#include <QDialog>
#include <programmi.h>
#include <formtastietan.h>
#include <formtastieraa.h>

namespace Ui {
class FormNewProgram;
}

class FormNewProgram : public QDialog
{
    Q_OBJECT

public:
    explicit FormNewProgram(QWidget *parent = 0);
    ~FormNewProgram();
    prog_s prog;

    int result;

    void   set_program_name(QString name);
    void   set_prog(prog_s p);
    prog_s get_prog();
    void   Init(QString um,int quotamax_y);


private slots:

    void on_buttonExit_3_clicked();

    void on_btnHeigth_clicked();

    void on_btnWidth_clicked();

    void on_btnSave_clicked();

    void on_btnThickness_clicked();

private:
    Ui::FormNewProgram *ui;

    FormTastieraN* uiTastieraN;
    FormTastieraA* uiTastieraA;

    void AggiornaDimensioni();
    QString UM;
    float fattore_unita;
    int     max_y;

    u_int16_t area;

};

#endif // FORMNEWPROGRAM_H
