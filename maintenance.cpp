#include "maintenance.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QProcess>

Maintenance::Maintenance(QObject *parent) : QObject(parent)
{

}

void Maintenance::load()
{
    QString path = "/home/root/Maintenance.json";

    QFile file(path);

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jArray;

    if( file.exists() )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc    = QJsonDocument::fromJson(saveData);
        jInfo      = jsonDoc.object();
        jArray     = jInfo["Maintenance"].toArray();

        foreach( QJsonValue value , jArray )
        {
            QJsonObject obj = value.toObject();
            maintenance_s manutenzione;

            manutenzione.alert    = obj["alert"].toString();
            manutenzione.done     = obj["done"].toString();
            manutenzione.reset    = obj["reset"].toString();
            manutenzione.lifespan = obj["lifespan"].toInt();

            manutenzioni.append( manutenzione );
        }
    }
}

void Maintenance::save()
{
    if( manutenzioni.count() == 0 ) return;

    QString path = "/home/root/Maintenance.json";

    save_as( path );
}

void Maintenance::save_as(QString path)
{
    QFile file(path);

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jArray;

    foreach (maintenance_s manutenzione , manutenzioni)
    {
        QJsonObject obj;

        obj["alert"]    = manutenzione.alert;
        obj["done"]     = manutenzione.done;
        obj["reset"]    = manutenzione.reset;
        obj["lifespan"] = manutenzione.lifespan;

        jArray.append( obj );
    }

    jInfo["Maintenance"] = jArray;

    jsonDoc.setObject(jInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.close();

    QProcess *proc = new QProcess(this);
    proc->start("sync");
    proc->waitForFinished(10000);
    delete proc;
}

void Maintenance::add(QString code)
{
    if( is_pending(code) == false )
    {
        maintenance_s nuova_manutenzione;
        nuova_manutenzione.alert    = code;
        nuova_manutenzione.done     = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm");
        nuova_manutenzione.lifespan = 0;
        nuova_manutenzione.reset    = "";

        manutenzioni.append( nuova_manutenzione );
        save();
    }
}

void Maintenance::reset(QString code, int lifespan)
{
    if( is_pending(code) == false )
    {
        maintenance_s nuova_manutenzione;
        nuova_manutenzione.alert    = code;
        nuova_manutenzione.done     = "";
        nuova_manutenzione.lifespan = lifespan / 60;
        nuova_manutenzione.reset    = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm");

        manutenzioni.append( nuova_manutenzione );
    }
    else
    {
        for( int p = 0; p < manutenzioni.count() ; p++)
        {
            maintenance_s* modify = (maintenance_s*)&manutenzioni.at(p);

            if( modify->alert == code && modify->reset == "" )
            {
                modify->reset = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm");
                modify->lifespan = lifespan / 60;
            }
        }
    }
    save();
}

void Maintenance::clear()
{
    manutenzioni.clear();
    QString path = "/home/root/Maintenance.json";
    save_as( path );
}

bool Maintenance::is_pending(QString code)
{
    bool ret = false;
    foreach (maintenance_s manutenzione, manutenzioni)
    {
        if( manutenzione.alert == code )
        {
            if( manutenzione.reset == "" ) ret = true;
        }
    }
    return ret;
}
