#ifndef SAVEMSG_H
#define SAVEMSG_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class SaveMsg;
}

class SaveMsg : public QDialog
{
    Q_OBJECT

public:
    explicit SaveMsg(QWidget *parent = 0);
    ~SaveMsg();
    void Init(QString nome);
private slots:
    void processTimer();

private:
    QTimer *timer;
    Ui::SaveMsg *ui;
};

#endif // SAVEMSG_H
