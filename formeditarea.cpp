#include "formeditarea.h"
#include "ui_formeditarea.h"
#include "def.h"

#include "parametrimacchina.h"

#define MAX_WIDTH 100000

formEditArea::formEditArea(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::formEditArea)
{
    ui->setupUi(this);
}

formEditArea::~formEditArea()
{
    delete ui;
}

void formEditArea::Init(s_Area  *area,float pixel_coef,int num,int maxh,QString n_pistole,QString um)
{

    Parametri *par = Parametri::getInstance();

    pixel_per_mm = pixel_coef;
    UM = um;
    if(um == "mm")
    {
        fattore_unita   = 1.0;
        fattore_unita_p = 1.0;
    }
    else
    {
        fattore_unita   = FATTORE_MM_IN;
        fattore_unita_p = FATTORE_BAR_PSI;
    }

    pArea      = area;
    max_height = par->param.quotamax_y;

    AggiornaValori();

    if(pArea->pis1 == 1)
    {
        ui->btnPistola1->setIcon(QIcon(":/png/58-A.png"));
    }
    else
    {
        ui->btnPistola1->setIcon(QIcon(":/png/58-B.png"));
    }

    if(pArea->pis2 == 1)
    {
        ui->btnPistola2->setIcon(QIcon(":/png/58-A.png"));
        ui->btnPistola3->setIcon(QIcon(":/png/58-A.png"));
    }
    else
    {
        ui->btnPistola2->setIcon(QIcon(":/png/58-B.png"));
        ui->btnPistola3->setIcon(QIcon(":/png/58-B.png"));
    }

    if(Tipo != MISTRAL)
    {
        ui->btnBarFin->setVisible(false);
        ui->btnBarIni->setVisible(false);
        ui->icoBarFin->setVisible(false);
        ui->icoBarIni->setVisible(false);
    }
    if(n_pistole == "1")
    {
        ui->btnPistola2->setVisible(false);
        ui->btnPistola3->setVisible(false);
    }
    else if(n_pistole == "2")
    {
        ui->btnPistola2->setVisible(true);
        ui->btnPistola3->setVisible(false);
    }
    else if(n_pistole == "3")
    {
        ui->btnPistola2->setVisible(true);
        ui->btnPistola3->setVisible(true);
    }

}
void formEditArea::AggiornaValori()
{
    QString text;
    QString up;
    if(UM == "mm")
    {
        up = "bar";

        text.sprintf("%6.0f ", (float)pArea->x_iniziale);
        text += UM;
        ui->btnXIni->setText(text);
        text.sprintf("%6.0f ", (float)pArea->x_finale);
        text += UM;
        ui->btnXFin->setText(text);
        text.sprintf("%6.0f ", (float)pArea->y_iniziale);
        text += UM;
        ui->btnYIni->setText(text);
        text.sprintf("%6.0f ", (float)pArea->y_finale);
        text += UM;
        ui->btnYFin->setText(text);
        text.sprintf("%6.0f ", (float)pArea->step * fattore_unita);
        text += UM;
        ui->btnStepNastro->setText(text);
    }
    else
    {
        up = "psi";

        text.sprintf("%6.2f ", (float)pArea->x_iniziale * fattore_unita);
        text += UM;
        ui->btnXIni->setText(text);
        text.sprintf("%6.2f ", (float)pArea->x_finale * fattore_unita);
        text += UM;
        ui->btnXFin->setText(text);
        text.sprintf("%6.2f ", (float)pArea->y_iniziale * fattore_unita);
        text += UM;
        ui->btnYIni->setText(text);
        text.sprintf("%6.2f ", (float)pArea->y_finale * fattore_unita);
        text += UM;
        ui->btnYFin->setText(text);
        text.sprintf("%6.2f ", (float)pArea->step * fattore_unita);
        text += UM;
        ui->btnStepNastro->setText(text);
    }

    text.sprintf("%6.2f ", pArea->pressione_iniziale * fattore_unita_p);
    text += up;
    ui->btnBarIni->setText(text);
    text.sprintf("%6.2f ", pArea->pressione_finale * fattore_unita_p);
    text += up;
    ui->btnBarFin->setText(text);

    ui->btnVelPistola->setText( QString("%1 %").arg(pArea->velocita_pistola));
    ui->btnRipetizione->setText( QString("%1").arg(pArea->ripetizioni));
}

void formEditArea::on_buttonExit_clicked()
{
    close();
}

void formEditArea::Normalizza(){

    pArea->p_pixel.setX(pArea->x_iniziale * pixel_per_mm);
    pArea->p_pixel.setY(pArea->y_iniziale * pixel_per_mm);
    pArea->width              = pArea->x_finale - pArea->x_iniziale;
    pArea->height             = pArea->y_finale - pArea->y_iniziale;
    pArea->dim_pixel.setX(pArea->width  * pixel_per_mm);
    pArea->dim_pixel.setY(pArea->height * pixel_per_mm);

}

void formEditArea::on_btnXIni_clicked()
{

    float val;
    if(UM == "mm")
    {
        val = GetNumber("X1",pArea->x_iniziale * fattore_unita,0,(pArea->x_finale * fattore_unita)-1,true);
    }
    else
    {
        val = GetNumber("X1",pArea->x_iniziale * fattore_unita,0,(pArea->x_finale * fattore_unita)-1,false);
    }


    pArea->x_iniziale = ((val / fattore_unita));
    AggiornaValori();
    Normalizza();

}
void formEditArea::on_btnYIni_clicked()
{

    float val;
    if(UM == "mm")
    {
        val = GetNumber("Y1",pArea->y_iniziale * fattore_unita,0,(pArea->y_finale * fattore_unita)-1,true);
    }
    else
    {
        val = GetNumber("Y1",pArea->y_iniziale * fattore_unita,0,(pArea->y_finale * fattore_unita)-1,false);
    }

    pArea->y_iniziale =  (val / fattore_unita);

    AggiornaValori();
    Normalizza();

}
void formEditArea::on_btnXFin_clicked()
{
    float val;

    if(UM == "mm")
    {
        val = GetNumber("X2",pArea->x_finale * fattore_unita,(pArea->x_iniziale * fattore_unita)+1,MAX_WIDTH * fattore_unita ,true);
    }
    else
    {
        val = GetNumber("X2",pArea->x_finale * fattore_unita,(pArea->x_iniziale * fattore_unita)+1,MAX_WIDTH * fattore_unita ,false);
    }

    pArea->x_finale = (val / fattore_unita);
    AggiornaValori();
    Normalizza();
}


void formEditArea::on_btnYFin_clicked()
{
    float val;

    if(UM == "mm")
    {
        val = GetNumber("Y2",pArea->y_finale * fattore_unita,(pArea->y_iniziale * fattore_unita)+1, max_height * fattore_unita ,true);
    }
    else
    {
        val = GetNumber("Y2",pArea->y_finale * fattore_unita,(pArea->y_iniziale * fattore_unita)+1, max_height * fattore_unita ,false);
    }
    pArea->y_finale = (val / fattore_unita);
    AggiornaValori();
    Normalizza();

}

float formEditArea::GetNumber(QString title,float old,float min,float max,bool asinteger)
{
    float num;

    uiTastieraN = new FormTastieraN(this);

//    if(UM != "mm")
//    {
//        min = min * FATTORE_MM_IN;
//        max = max * FATTORE_MM_IN;
//    }

    uiTastieraN->set_title( tr(title.toLatin1()) );
    uiTastieraN->set_max( max );
    uiTastieraN->set_min( min );
    uiTastieraN->set_default( 1 );
    uiTastieraN->set_integer( asinteger );

    uiTastieraN->exec();
    if(uiTastieraN->is_valid())
    {
        num = uiTastieraN->get_val();
    }
    else
    {
        num = old;
    }

    delete uiTastieraN;

    return num;
}

void formEditArea::on_btnBarIni_clicked()
{
    pArea->pressione_iniziale = GetNumber("" , pArea->pressione_iniziale * fattore_unita_p , 0, 8 * fattore_unita_p, false);
    QString text;

    if(UM == "mm")
    {
        text.sprintf("%6.2f bar", pArea->pressione_iniziale);
    }
    else
    {
        text.sprintf("%6.2f psi", pArea->pressione_iniziale);
    }

    ui->btnBarIni->setText(text);
    pArea->pressione_iniziale = pArea->pressione_iniziale / fattore_unita_p;

}

void formEditArea::on_btnBarFin_clicked()
{
    pArea->pressione_finale = GetNumber("",pArea->pressione_finale * fattore_unita_p,0,8 * fattore_unita_p,false);
    QString text;

    if(UM == "mm")
    {
        text.sprintf("%6.2f bar", pArea->pressione_finale);
    }
    else
    {
        text.sprintf("%6.2f psi", pArea->pressione_finale);
    }
    ui->btnBarFin->setText(text);
    pArea->pressione_finale = pArea->pressione_finale / fattore_unita_p;

}

void formEditArea::on_btnRipetizione_clicked()
{
    pArea->ripetizioni = GetNumber("",pArea->ripetizioni,1,99,true);
    ui->btnRipetizione->setText( QString("%1").arg(pArea->ripetizioni));

}

void formEditArea::on_btnVelPiostola_clicked()
{

}

void formEditArea::on_btnPistola1_clicked()
{
    if(pArea->pis1 == 0)
    {
        ui->btnPistola1->setIcon(QIcon(":/png/58-A.png"));
        pArea->pis1 = 1;
    }
    else
    {
        ui->btnPistola1->setIcon(QIcon(":/png/58-B.png"));
        pArea->pis1 = 0;
    }
}

void formEditArea::on_btnPistola2_clicked()
{
    if(pArea->pis2 == 0)
    {
        ui->btnPistola2->setIcon(QIcon(":/png/58-A.png"));
        ui->btnPistola3->setIcon(QIcon(":/png/58-A.png"));
        pArea->pis2 = 1;
    }
    else
    {
        ui->btnPistola2->setIcon(QIcon(":/png/58-B.png"));
        ui->btnPistola3->setIcon(QIcon(":/png/58-B.png"));
        pArea->pis2 = 0;
    }
}

void formEditArea::on_btnPistola3_clicked()
{
    on_btnPistola2_clicked();
}


void formEditArea::on_btnVelPistola_clicked()
{
    pArea->velocita_pistola = GetNumber("",pArea->velocita_pistola,50,100,false);
    ui->btnVelPistola->setText( QString("%1 %").arg(pArea->velocita_pistola));
}

void formEditArea::on_btnStepNastro_clicked()
{
    float val;
    val = GetNumber("",pArea->step * fattore_unita,0,9999,false);
//    QString text;

//    if(UM == "mm"){
//        text.sprintf("%6.0f ", (int)val);
//    }
//    else{
//        text.sprintf("%6.2f ", (float)val);
//    }
    //text += UM;
    pArea->step = val / fattore_unita;
    //ui->btnStepNastro->setText(text);

    AggiornaValori();
}
