#include "frmmanualenastro.h"
#include "ui_frmmanualenastro.h"

frmManualeNastro::frmManualeNastro(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmManualeNastro)
{
    ui->setupUi(this);
}

frmManualeNastro::~frmManualeNastro()
{
    delete ui;
}
void frmManualeNastro::Init(int manuale)
{
    if(manuale == 2){
        ui->lbHelp->setText("0.1");
        ui->imgFreccia1->setIcon(QIcon(":/png/20+.png"));
        ui->imgFreccia2->setIcon(QIcon(":/png/20+.png"));
    }
    else if(manuale == 1){
        ui->lbHelp->setText("0.2");
        ui->imgFreccia1->setIcon(QIcon(":/png/21+.png"));
        ui->imgFreccia2->setIcon(QIcon(":/png/21+.png"));
    }

}
