#ifndef FORMTASTIERAN_H
#define FORMTASTIERAN_H

#include <QDialog>

namespace Ui {
class FormTastieraN;
}

class FormTastieraN : public QDialog
{
    Q_OBJECT

public:
    explicit FormTastieraN(QWidget *parent = 0);
    ~FormTastieraN();

    bool valid;

    void set_title(QString title );
    void set_unita_misura(QString um);

    void set_max(float val);
    void set_min(float val);
    void set_default(float val );
    void set_maxdec(int val);
    void set_integer( bool val);
    bool is_valid();

    float   get_val();
    QString get_string();

private:
    float max;
    float min;
    float def;
    float val;
    int   maxdec;

    bool isInteger;
    bool primo;

    void add_num(QString num);
    bool verifica_limiti();

private slots:
    void on_buttonOK_clicked();

    void on_button9_clicked();

    void on_button0_clicked();

    void on_button1_clicked();

    void on_button2_clicked();

    void on_button3_clicked();

    void on_button4_clicked();

    void on_button5_clicked();

    void on_button6_clicked();

    void on_button7_clicked();

    void on_button8_clicked();

    void on_buttonPunto_clicked();

    void on_buttonCanc_clicked();

    void on_buttonAnnulla_clicked();

    void on_buttonMax_clicked();

    void on_buttonMin_clicked();

    void on_buttonDef_clicked();

    void on_buttonPM_clicked();

private:
    Ui::FormTastieraN *ui;
};

#endif // FORMTASTIETAN_H
