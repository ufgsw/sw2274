#ifndef FORMSELECTPROG_H
#define FORMSELECTPROG_H

#include <QDialog>
#include "model.h"
#include "programmi.h"



namespace Ui {
class formselectprog;
}

class formselectprog : public QDialog
{
    Q_OBJECT

public:
    Programmi* progs;

    void Init(bool edit,QString um);

    int     result;
    int     last_idx;
    int     n_prog;
    explicit formselectprog(QWidget *parent = 0);
    ~formselectprog();

private slots:
    void on_buttonExit_clicked();

    void on_buttonExit_3_clicked();

    void on_btnNext_clicked();

    void on_btnPrior_clicked();

    void on_btnAggiungi_clicked();

    void on_btnRemove_clicked();

    void on_btnCerca_clicked();

    void on_tbProg_clicked(const QModelIndex &index);

    void on_btnCopia_clicked();

    void on_btnExport_clicked();

    void on_tbProg_activated(const QModelIndex &index);

    void on_tbProg_entered(const QModelIndex &index);

private:
    QString UM;

    QString last_nome;
    QString SecondToTime(int sec);
    MyModel myModel;
    Ui::formselectprog *ui;
};

#endif // FORMSELECTPROG_H
