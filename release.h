#ifndef RELEASE_H
#define RELEASE_H

/*
 *  le versioni dispari eseguono l'home pistola sempre
 *  le versioni pari    eseguono l'home alla fine di ogni programma SE IMPOSTATO
 *
 */

#define RELEASE 116
#define release_fw "1.1.6"
#define release_hw "1.0.0"

#define RELEASE_HOME_PISTOLA_SEMPRE ((RELEASE % 2) == 1)

#endif // RELEASE_H
