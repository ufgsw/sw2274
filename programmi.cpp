#include "programmi.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QProcess>
#include <def.h>

Config::Config(QObject *parent) : QObject(parent)
{
    filename = "/home/root/ConfigPezza.json";
    Manufacturer = "Fratelli Pezza";
}
void Config::save()
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;


    jInfo["modello"]          = modello;
    jInfo["release"]          = "4.0";
    jInfo["firmware"]         = "4.0";

    jInfo["Model"]            = modello;
    jInfo["S/N"]              = sn;
    jInfo["Year"]             = Year;
    jInfo["Manufacturer"]     = Manufacturer;
    jInfo["Automatic Guns"]   = Automatic_Guns;
    jInfo["UM"]               = UM;
    jInfo["Volt"]             = Volt;
    jInfo["Hz"]               = Hz;
    jInfo["A"]                = A;
    jInfo["kW"]               = kW;
    jInfo["Bar"]              = Bar;
    jInfo["Weigh"]            = Weigh;
    jInfo["DoHoming"]         = DoHoming;

    jsonDoc.setObject(jInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.waitForBytesWritten(-1);
    file.close();

    QProcess *proc = new QProcess(this);
    proc->start("sync");
    proc->waitForFinished(10000);
    delete proc;

}
bool Config::open()
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;



    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc    = QJsonDocument::fromJson(saveData);
        jInfo      = jsonDoc.object();
        modello    = jInfo["modello"].toString();
        //release    = jInfo["release"].toString();
        modello    = jInfo["Model"].toString();
        sn         = jInfo["S/N"].toString();
        Year       = jInfo["Year"].toString();
        //Manufacturer    = jInfo["Manufacturer"].toString();
        Automatic_Guns  = jInfo["Automatic Guns"].toString();
        UM              = jInfo["UM"].toString();
        Volt            = jInfo["Volt"].toString();
        Hz              = jInfo["Hz"].toString();
        A               = jInfo["A"].toString();
        kW              = jInfo["kW"].toString();
        Bar             = jInfo["Bar"].toString();
        Weigh           = jInfo["Weigh"].toString();
        DoHoming        = jInfo["DoHoming"].toString();

        As4_0           = 1;

        if(UM == "")
        {
            UM = "mm";
        }
        if(Automatic_Guns == "")
        {
            Automatic_Guns ="1";
        }

        qDebug( "file programma caricato " );
        return true;
    }
    else
    {
        qDebug( "file programma non trovato " );
        return false;
    }

}

/*******************************************************************************
 *
 *
 */

Programmi::Programmi(QObject *parent) : QObject(parent)
{
    filename = "/home/root/ProgrammaPezza.json";
    Manufacturer = "Fratelli Pezza";
}

void Programmi::save(QString to_remove)
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;

    programmi.remove("");

    for( prog_s prog : programmi.values() )
    {
        QJsonObject objProg;

        if(prog.nome != to_remove)
        {
            objProg["nome"]                 = prog.nome;
            objProg["naree"]                = prog.naree;
            objProg["RetToHome"]            = prog.RetToHome;
            objProg["direzione"]            = prog.direzione;
            objProg["secondi_impiegati"]    = prog.secondi_impiegati;
            objProg["secondi_totali"]       = prog.secondi_totali;
            objProg["esecuzioni"]           = prog.esecuzioni;
            objProg["campioni"]             = prog.campioni;
            objProg["pixel_per_mm"]         = prog.pixel_per_mm;
            objProg["larghezza_vetro"]      = prog.larghezza_vetro;
            objProg["altezza_vetro"]        = prog.altezza_vetro;
            objProg["larghezza_extra"]      = prog.larghezza_extra;
            objProg["altezza_extra"]        = prog.altezza_extra;
            objProg["spessore_vetro"]       = prog.spessore_vetro;

            QJsonArray    jArea;
            if(prog.naree > MAX_AREE){
                prog.naree = MAX_AREE;
            }

            for( int r=0; r < prog.naree ; r++)
            {
                QJsonObject objAree;

//                qDebug((QString( "Xi : %1").arg(prog.Area[1].x_iniziale)).toLatin1());
//                qDebug((QString( "Yi : %1").arg(prog.Area[1].y_iniziale)).toLatin1());
//                qDebug((QString( "Xf : %1").arg(prog.Area[1].x_finale)).toLatin1());
//                qDebug((QString( "Yf : %1").arg(prog.Area[1].y_finale)).toLatin1());

                prog.Area[r].velocita_nastro = 100;
                if(prog.Area[r].velocita_pistola < 50)
                {
                    prog.Area[r].velocita_pistola = 50;
                }
                objAree["y_iniziale"] = prog.Area[r].y_iniziale;
                objAree["y_finale"] = prog.Area[r].y_finale;
                objAree["x_iniziale"] = prog.Area[r].x_iniziale;
                objAree["x_finale"] = prog.Area[r].x_finale;
                objAree["step"] = prog.Area[r].step;
                objAree["ripetizioni"] = prog.Area[r].ripetizioni;
                objAree["velocita_pistola"] = prog.Area[r].velocita_pistola;
                objAree["velocita_nastro"] = prog.Area[r].velocita_nastro;
                objAree["pressione_iniziale"] = prog.Area[r].pressione_iniziale;
                objAree["pressione_finale"] = prog.Area[r].pressione_finale;
                objAree["um"] = prog.Area[r].um;
                objAree["pr"] = prog.Area[r].pr;
                objAree["pis1"] = prog.Area[r].pis1;
                objAree["pis2"] = prog.Area[r].pis2;
                objAree["dim_pixel_x"] = prog.Area[r].dim_pixel.x();
                objAree["dim_pixel_y"] = prog.Area[r].dim_pixel.y();
                objAree["p_pixel_x"] = prog.Area[r].p_pixel.x();
                objAree["p_pixel_y"] = prog.Area[r].p_pixel.y();
                objAree["stato"] = prog.Area[r].stato;

                jArea.append( objAree );
            }
            objProg["aree in prog"]    = jArea;
            jProgs.append( objProg );
        }
    }

    jInfo["modello"]          = modello;
    jInfo["release"]          = "4.0";
    jInfo["firmware"]         = "4.0";

    jInfo["Model"]            = modello;
    jInfo["S/N"]              = sn;
    jInfo["Year"]             = Year;
    jInfo["Manufacturer"]     = Manufacturer;
    jInfo["Automatic Guns"]   = Automatic_Guns;
    jInfo["UM"]               = UM;
    jInfo["Volt"]             = Volt;
    jInfo["Hz"]               = Hz;
    jInfo["A"]                = A;
    jInfo["kW"]               = kW;
    jInfo["Bar"]              = Bar;
    jInfo["Weigh"]            = Weigh;
    jInfo["DoHoming"]         = DoHoming;

    jInfo["Programmi"]         = jProgs;

    jsonDoc.setObject(jInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.waitForBytesWritten(-1);
    file.close();

    QProcess *proc = new QProcess(this);
    proc->start("sync");
    proc->waitForStarted();
    proc->waitForFinished();
    delete proc;

    qDebug("Programmi salvati !");

}
void Programmi::import_prog(QString filename)
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;
    QString sw;

    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc    = QJsonDocument::fromJson(saveData);
        jInfo      = jsonDoc.object();
        jProgs     = jInfo["Programmi"].toArray();

    }
    else{
        return;
    }

    foreach( QJsonValue prog , jProgs )
    {
        QJsonObject obj = prog.toObject();
        prog_s newprog;

        newprog.nome = obj["nome"].toString();
        newprog.naree = obj["naree"].toInt();
        newprog.RetToHome = obj["RetToHome"].toInt();
        newprog.direzione = obj["direzione"].toInt();
        newprog.secondi_impiegati = obj["secondi_impiegati"].toInt();
        newprog.secondi_totali = obj["secondi_totali"].toInt();
        newprog.esecuzioni = obj["esecuzioni"].toInt();
        newprog.campioni = obj["campioni"].toInt();
        newprog.pixel_per_mm = obj["pixel_per_mm"].toDouble();

        newprog.larghezza_vetro = obj["larghezza_vetro"].toDouble();
        newprog.altezza_vetro = obj["altezza_vetro"].toDouble();
        newprog.larghezza_extra = obj["larghezza_extra"].toDouble();
        newprog.altezza_extra   = obj["altezza_extra"].toDouble();
        newprog.spessore_vetro  = obj["spessore_vetro"].toDouble();

        //qDebug( newprog.nome.toLatin1());
        //qDebug( obj["naree"].toString().toLatin1());

        QJsonArray a_aree;
        a_aree = obj["aree in prog"].toArray();
        int p = 0;
        foreach (QJsonValue area, a_aree)
        {
            QJsonObject objarea = area.toObject();
            newprog.Area[p].y_iniziale = objarea["y_iniziale"].toDouble();
            newprog.Area[p].y_finale = objarea["y_finale"].toDouble();
            newprog.Area[p].x_iniziale = objarea["x_iniziale"].toDouble();
            newprog.Area[p].x_finale = objarea["x_finale"].toDouble();
            newprog.Area[p].step = objarea["step"].toDouble();
            newprog.Area[p].ripetizioni = objarea["ripetizioni"].toInt();
            newprog.Area[p].velocita_pistola = objarea["velocita_pistola"].toInt();
            newprog.Area[p].velocita_nastro = objarea["velocita_nastro"].toInt();
            newprog.Area[p].pressione_iniziale = objarea["pressione_iniziale"].toDouble();
            newprog.Area[p].pressione_finale = objarea["pressione_finale"].toDouble();
            newprog.Area[p].um = objarea["um"].toInt();
            newprog.Area[p].pr = objarea["pr"].toInt();
            newprog.Area[p].pis1 = objarea["pis1"].toInt();
            newprog.Area[p].pis2 = objarea["pis2"].toInt();

            newprog.Area[p].dim_pixel.setX(objarea["dim_pixel_x"].toInt());
            newprog.Area[p].dim_pixel.setY(objarea["dim_pixel_y"].toInt());
            newprog.Area[p].p_pixel.setX(objarea["p_pixel_x"].toInt());
            newprog.Area[p].p_pixel.setY(objarea["p_pixel_y"].toInt());

            newprog.Area[p].stato = objarea["stato"].toInt();

//            qDebug((QString( "Xi : %1").arg(newprog.Area[p].x_iniziale)).toLatin1());
//            qDebug((QString( "Yi : %1").arg(newprog.Area[p].y_iniziale)).toLatin1());
//            qDebug((QString( "Xf : %1").arg(newprog.Area[p].x_finale)).toLatin1());
//            qDebug((QString( "Yf : %1").arg(newprog.Area[p].y_finale)).toLatin1());

            if( p < MAX_AREE ) ++p;
        }
        while( p < MAX_AREE )
        {
            newprog.Area[p].y_iniziale = 0;
            newprog.Area[p].y_finale = 0;
            newprog.Area[p].x_iniziale = 0;
            newprog.Area[p].x_finale = 0;
            newprog.Area[p].step = 0;
            newprog.Area[p].ripetizioni = 0;
            newprog.Area[p].velocita_pistola   = 0;
            newprog.Area[p].velocita_nastro    = 0;
            newprog.Area[p].pressione_iniziale = 0;
            newprog.Area[p].pressione_finale   = 0;
            newprog.Area[p].um = 0;
            newprog.Area[p].pr = 0;
            newprog.Area[p].pis1  = 0;
            newprog.Area[p].pis2  = 0;
            newprog.Area[p].stato = 0;

            ++p;
        }

        if(exist(newprog.nome))
        {
            replace(newprog);
        }
        else
        {
            add(newprog);
        }
        save("");
    }
}
void Programmi::export_all_prog()
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;

    programmi.remove("");

    for( prog_s prog : programmi.values() )
    {
        QJsonObject objProg;

        objProg["nome"]                 = prog.nome;
        objProg["naree"]                = prog.naree;
        objProg["RetToHome"]            = prog.RetToHome;
        objProg["direzione"]            = prog.direzione;
        objProg["secondi_impiegati"]    = prog.secondi_impiegati;
        objProg["secondi_totali"]       = prog.secondi_totali;
        objProg["esecuzioni"]           = prog.esecuzioni;
        objProg["campioni"]             = prog.campioni;
        objProg["pixel_per_mm"]         = prog.pixel_per_mm;
        objProg["larghezza_vetro"]      = prog.larghezza_vetro;
        objProg["altezza_vetro"]        = prog.altezza_vetro;
        objProg["larghezza_extra"]      = prog.larghezza_extra;
        objProg["altezza_extra"]        = prog.altezza_extra;
        objProg["spessore_vetro"]       = prog.spessore_vetro;

        QJsonArray    jArea;
        if(prog.naree > MAX_AREE){
            prog.naree = MAX_AREE;
        }

        for( int r=0; r < prog.naree ; r++)
        {
            QJsonObject objAree;

//            qDebug((QString( "Xi : %1").arg(prog.Area[1].x_iniziale)).toLatin1());
//            qDebug((QString( "Yi : %1").arg(prog.Area[1].y_iniziale)).toLatin1());
//            qDebug((QString( "Xf : %1").arg(prog.Area[1].x_finale)).toLatin1());
//            qDebug((QString( "Yf : %1").arg(prog.Area[1].y_finale)).toLatin1());

            prog.Area[r].velocita_nastro = 100;
            if(prog.Area[r].velocita_pistola < 50){
                prog.Area[r].velocita_pistola = 50;
            }
            objAree["y_iniziale"]  = prog.Area[r].y_iniziale;
            objAree["y_finale"]    = prog.Area[r].y_finale;
            objAree["x_iniziale"]  = prog.Area[r].x_iniziale;
            objAree["x_finale"]    = prog.Area[r].x_finale;
            objAree["step"]        = prog.Area[r].step;
            objAree["ripetizioni"] = prog.Area[r].ripetizioni;
            objAree["velocita_pistola"]   = prog.Area[r].velocita_pistola;
            objAree["velocita_nastro"]    = prog.Area[r].velocita_nastro;
            objAree["pressione_iniziale"] = prog.Area[r].pressione_iniziale;
            objAree["pressione_finale"]   = prog.Area[r].pressione_finale;
            objAree["um"]   = prog.Area[r].um;
            objAree["pr"]   = prog.Area[r].pr;
            objAree["pis1"] = prog.Area[r].pis1;
            objAree["pis2"] = prog.Area[r].pis2;
            objAree["dim_pixel_x"] = prog.Area[r].dim_pixel.x();
            objAree["dim_pixel_y"] = prog.Area[r].dim_pixel.y();
            objAree["p_pixel_x"]   = prog.Area[r].p_pixel.x();
            objAree["p_pixel_y"]   = prog.Area[r].p_pixel.y();
            objAree["stato"]       = prog.Area[r].stato;

            jArea.append( objAree );
        }
        objProg["aree in prog"]    = jArea;
        jProgs.append( objProg );
    }

    jInfo["Programmi"]         = jProgs;

    jsonDoc.setObject(jInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.close();

    QProcess *proc = new QProcess(this);
    proc->start("sync");
    proc->waitForFinished(10000);
    delete proc;

}
void Programmi::export_prog(QString filename, prog_s prog)
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;

    programmi.remove("");

    QJsonObject objProg;

    objProg["nome"]                 = prog.nome;
    objProg["naree"]                = prog.naree;
    objProg["RetToHome"]            = prog.RetToHome;
    objProg["direzione"]            = prog.direzione;
    objProg["secondi_impiegati"]    = prog.secondi_impiegati;
    objProg["secondi_totali"]       = prog.secondi_totali;
    objProg["esecuzioni"]           = prog.esecuzioni;
    objProg["campioni"]             = prog.campioni;
    objProg["pixel_per_mm"]         = prog.pixel_per_mm;
    objProg["larghezza_vetro"]      = prog.larghezza_vetro;
    objProg["altezza_vetro"]        = prog.altezza_vetro;
    objProg["larghezza_extra"]      = prog.larghezza_extra;
    objProg["altezza_extra"]        = prog.altezza_extra;
    objProg["spessore_vetro"]       = prog.spessore_vetro;

    QJsonArray    jArea;
    if(prog.naree > MAX_AREE)
    {
        prog.naree = MAX_AREE;
    }

    for( int r=0; r < prog.naree ; r++)
    {
        QJsonObject objAree;

        prog.Area[r].velocita_nastro = 100;
        if(prog.Area[r].velocita_pistola < 50){
            prog.Area[r].velocita_pistola = 50;
        }
        objAree["y_iniziale"] = prog.Area[r].y_iniziale;
        objAree["y_finale"] = prog.Area[r].y_finale;
        objAree["x_iniziale"] = prog.Area[r].x_iniziale;
        objAree["x_finale"] = prog.Area[r].x_finale;
        objAree["step"] = prog.Area[r].step;
        objAree["ripetizioni"] = prog.Area[r].ripetizioni;
        objAree["velocita_pistola"] = prog.Area[r].velocita_pistola;
        objAree["velocita_nastro"] = prog.Area[r].velocita_nastro;
        objAree["pressione_iniziale"] = prog.Area[r].pressione_iniziale;
        objAree["pressione_finale"] = prog.Area[r].pressione_finale;
        objAree["um"] = prog.Area[r].um;
        objAree["pr"] = prog.Area[r].pr;
        objAree["pis1"] = prog.Area[r].pis1;
        objAree["pis2"] = prog.Area[r].pis2;
        objAree["dim_pixel_x"] = prog.Area[r].dim_pixel.x();
        objAree["dim_pixel_y"] = prog.Area[r].dim_pixel.y();
        objAree["p_pixel_x"] = prog.Area[r].p_pixel.x();
        objAree["p_pixel_y"] = prog.Area[r].p_pixel.y();
        objAree["stato"] = prog.Area[r].stato;

        jArea.append( objAree );
    }
    objProg["aree in prog"]    = jArea;
    jProgs.append( objProg );

    jInfo["Programmi"]         = jProgs;

    jsonDoc.setObject(jInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.close();

    QProcess *proc = new QProcess(this);
    proc->start("sync");
    proc->waitForFinished(10000);
    delete proc;
}
bool Programmi::exist(QString nome)
{
    bool result = false;
    for( prog_s prog : programmi.values() )
    {
        if(prog.nome == nome){
            result = true;
            break;
        }
    }
    return result;
}
void Programmi::open()
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;
//    QString sw;

    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc         = QJsonDocument::fromJson(saveData);
        jInfo           = jsonDoc.object();
        jProgs          = jInfo["Programmi"].toArray();

        modello         = jInfo["modello"].toString();
        modello         = jInfo["Model"].toString();
        sn              = jInfo["S/N"].toString();
        Year            = jInfo["Year"].toString();
        Automatic_Guns  = jInfo["Automatic Guns"].toString();
        UM              = jInfo["UM"].toString();
        Volt            = jInfo["Volt"].toString();
        Hz              = jInfo["Hz"].toString();
        A               = jInfo["A"].toString();
        kW              = jInfo["kW"].toString();
        Bar             = jInfo["Bar"].toString();
        Weigh           = jInfo["Weigh"].toString();
        DoHoming        = jInfo["DoHoming"].toString();

        As4_0           = 1;

        if(UM == "")
        {
            UM = "mm";
        }
        if(Automatic_Guns == "")
        {
            Automatic_Guns ="1";
        }
        qDebug( "file programma caricato " );
        programmi.clear();
    }
    else
    {
        qDebug( "file programma non trovato " );
        prog_s prog;

        prog.nome = "Default";
        prog.naree = 0;
        prog.RetToHome = 1;
        prog.direzione = 0;
        prog.secondi_impiegati = 0;
        prog.secondi_totali = 0;
        prog.esecuzioni = 0;
        prog.campioni = 0;
        prog.pixel_per_mm = 10;
        prog.larghezza_vetro = 100;
        prog.altezza_vetro = 100;
        prog.larghezza_extra = 0;
        prog.altezza_extra = 0;
        prog.spessore_vetro = 3;
        add(prog);
        save("");
    }
    foreach( QJsonValue prog , jProgs )
    {
        QJsonObject obj = prog.toObject();
        prog_s newprog;

        newprog.nome = obj["nome"].toString();
        newprog.naree = obj["naree"].toInt();
        newprog.RetToHome = obj["RetToHome"].toInt();
        newprog.direzione = obj["direzione"].toInt();
        newprog.secondi_impiegati = obj["secondi_impiegati"].toInt();
        newprog.secondi_totali = obj["secondi_totali"].toInt();
        newprog.esecuzioni = obj["esecuzioni"].toInt();
        newprog.campioni = obj["campioni"].toInt();
        newprog.pixel_per_mm = obj["pixel_per_mm"].toDouble();
        newprog.larghezza_vetro = obj["larghezza_vetro"].toDouble();
        newprog.altezza_vetro = obj["altezza_vetro"].toDouble();
        newprog.larghezza_extra = obj["larghezza_extra"].toDouble();
        newprog.altezza_extra = obj["altezza_extra"].toDouble();
        newprog.spessore_vetro = obj["spessore_vetro"].toDouble();

        qDebug( newprog.nome.toLatin1());
        qDebug( obj["naree"].toString().toLatin1());

        QJsonArray a_aree;
        a_aree = obj["aree in prog"].toArray();
        int p = 0;
        foreach (QJsonValue area, a_aree)
        {
            QJsonObject objarea = area.toObject();
            newprog.Area[p].y_iniziale = objarea["y_iniziale"].toDouble();
            newprog.Area[p].y_finale = objarea["y_finale"].toDouble();
            newprog.Area[p].x_iniziale = objarea["x_iniziale"].toDouble();
            newprog.Area[p].x_finale = objarea["x_finale"].toDouble();
            newprog.Area[p].step = objarea["step"].toDouble();
            newprog.Area[p].ripetizioni = objarea["ripetizioni"].toInt();
            newprog.Area[p].velocita_pistola = objarea["velocita_pistola"].toInt();
            newprog.Area[p].velocita_nastro = objarea["velocita_nastro"].toInt();
            newprog.Area[p].pressione_iniziale = objarea["pressione_iniziale"].toDouble();
            newprog.Area[p].pressione_finale = objarea["pressione_finale"].toDouble();
            newprog.Area[p].um = objarea["um"].toInt();
            newprog.Area[p].pr = objarea["pr"].toInt();
            newprog.Area[p].pis1 = objarea["pis1"].toInt();
            newprog.Area[p].pis2 = objarea["pis2"].toInt();

            newprog.Area[p].dim_pixel.setX(objarea["dim_pixel_x"].toInt());
            newprog.Area[p].dim_pixel.setY(objarea["dim_pixel_y"].toInt());
            newprog.Area[p].p_pixel.setX(objarea["p_pixel_x"].toInt());
            newprog.Area[p].p_pixel.setY(objarea["p_pixel_y"].toInt());

            newprog.Area[p].stato = objarea["stato"].toInt();

//            qDebug((QString( "Xi : %1").arg(newprog.Area[p].x_iniziale)).toLatin1());
//            qDebug((QString( "Yi : %1").arg(newprog.Area[p].y_iniziale)).toLatin1());
//            qDebug((QString( "Xf : %1").arg(newprog.Area[p].x_finale)).toLatin1());
//            qDebug((QString( "Yf : %1").arg(newprog.Area[p].y_finale)).toLatin1());

            if( p < MAX_AREE ) ++p;
        }
        while( p < MAX_AREE )
        {
            newprog.Area[p].y_iniziale = 0;
            newprog.Area[p].y_finale   = 0;
            newprog.Area[p].x_iniziale = 0;
            newprog.Area[p].x_finale   = 0;
            newprog.Area[p].step       = 0;
            newprog.Area[p].ripetizioni        = 0;
            newprog.Area[p].velocita_pistola   = 0;
            newprog.Area[p].velocita_nastro    = 0;
            newprog.Area[p].pressione_iniziale = 0;
            newprog.Area[p].pressione_finale   = 0;
            newprog.Area[p].um    = 0;
            newprog.Area[p].pr    = 0;
            newprog.Area[p].pis1  = 0;
            newprog.Area[p].pis2  = 0;
            newprog.Area[p].stato = 0;

            ++p;
        }
        programmi[newprog.nome] = newprog;
    }



}
void Programmi::import_restore(QString filename)
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;
    QString sw;

    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc    = QJsonDocument::fromJson(saveData);
        jInfo      = jsonDoc.object();
        jProgs     = jInfo["Programmi"].toArray();

        programmi.clear();
    }
    else{
        return;
    }

    foreach( QJsonValue prog , jProgs )
    {
        QJsonObject obj = prog.toObject();
        prog_s newprog;

        newprog.nome = obj["nome"].toString();
        newprog.naree = obj["naree"].toInt();
        newprog.RetToHome = obj["RetToHome"].toInt();
        newprog.direzione = obj["direzione"].toInt();
        newprog.secondi_impiegati = obj["secondi_impiegati"].toInt();
        newprog.secondi_totali = obj["secondi_totali"].toInt();
        newprog.esecuzioni = obj["esecuzioni"].toInt();
        newprog.campioni = obj["campioni"].toInt();
        newprog.pixel_per_mm = obj["pixel_per_mm"].toDouble();
        newprog.larghezza_vetro = obj["larghezza_vetro"].toDouble();
        newprog.altezza_vetro = obj["altezza_vetro"].toDouble();
        newprog.larghezza_extra = obj["larghezza_extra"].toDouble();
        newprog.altezza_extra = obj["altezza_extra"].toDouble();
        newprog.spessore_vetro = obj["spessore_vetro"].toDouble();

        qDebug( newprog.nome.toLatin1());
        qDebug( obj["naree"].toString().toLatin1());

        QJsonArray a_aree;
        a_aree = obj["aree in prog"].toArray();
        int p = 0;
        foreach (QJsonValue area, a_aree)
        {
            QJsonObject objarea = area.toObject();
            newprog.Area[p].y_iniziale = objarea["y_iniziale"].toDouble();
            newprog.Area[p].y_finale = objarea["y_finale"].toDouble();
            newprog.Area[p].x_iniziale = objarea["x_iniziale"].toDouble();
            newprog.Area[p].x_finale = objarea["x_finale"].toDouble();
            newprog.Area[p].step = objarea["step"].toDouble();
            newprog.Area[p].ripetizioni = objarea["ripetizioni"].toInt();
            newprog.Area[p].velocita_pistola = objarea["velocita_pistola"].toInt();
            newprog.Area[p].velocita_nastro = objarea["velocita_nastro"].toInt();
            newprog.Area[p].pressione_iniziale = objarea["pressione_iniziale"].toDouble();
            newprog.Area[p].pressione_finale = objarea["pressione_finale"].toDouble();
            newprog.Area[p].um = objarea["um"].toInt();
            newprog.Area[p].pr = objarea["pr"].toInt();
            newprog.Area[p].pis1 = objarea["pis1"].toInt();
            newprog.Area[p].pis2 = objarea["pis2"].toInt();

            newprog.Area[p].dim_pixel.setX(objarea["dim_pixel_x"].toInt());
            newprog.Area[p].dim_pixel.setY(objarea["dim_pixel_y"].toInt());
            newprog.Area[p].p_pixel.setX(objarea["p_pixel_x"].toInt());
            newprog.Area[p].p_pixel.setY(objarea["p_pixel_y"].toInt());

            newprog.Area[p].stato = objarea["stato"].toInt();

//            qDebug((QString( "Xi : %1").arg(newprog.Area[p].x_iniziale)).toLatin1());
//            qDebug((QString( "Yi : %1").arg(newprog.Area[p].y_iniziale)).toLatin1());
//            qDebug((QString( "Xf : %1").arg(newprog.Area[p].x_finale)).toLatin1());
//            qDebug((QString( "Yf : %1").arg(newprog.Area[p].y_finale)).toLatin1());

            if( p < MAX_AREE ) ++p;
        }
        while( p < MAX_AREE )
        {
            newprog.Area[p].y_iniziale = 0;
            newprog.Area[p].y_finale = 0;
            newprog.Area[p].x_iniziale = 0;
            newprog.Area[p].x_finale = 0;
            newprog.Area[p].step = 0;
            newprog.Area[p].ripetizioni = 0;
            newprog.Area[p].velocita_pistola = 0;
            newprog.Area[p].velocita_nastro = 0;
            newprog.Area[p].pressione_iniziale = 0;
            newprog.Area[p].pressione_finale = 0;
            newprog.Area[p].um = 0;
            newprog.Area[p].pr = 0;
            newprog.Area[p].pis1 = 0;
            newprog.Area[p].pis2 = 0;
            newprog.Area[p].stato = 0;

            ++p;
        }
        programmi[newprog.nome] = newprog;
    }
}
bool Programmi::add(prog_s prog)
{
    bool trovato = false;

    if( prog.nome == "" ) return false;

    for( prog_s p : programmi )
    {
        if( prog.nome == p.nome ) trovato = true;
    }
    if( trovato == true ) return false;

    programmi[prog.nome] = prog;

    //programmi.append( prog );
    return true;
}
void Programmi::remove(prog_s prog)
{
    save(prog.nome);
    open();
}
void Programmi::replace(prog_s prog)
{
    programmi[prog.nome] = prog;
}
void Programmi::rename(prog_s prog,QString newname)
{
    QString oldprog_name = prog.nome;

    programmi.remove( oldprog_name );

    prog.nome = newname;

    programmi[prog.nome]      = prog;

    save(prog.nome);
}
void Programmi::setCurrent(QString name)
{
    current_name = name;
}
QStringList Programmi::elenco_nomi()
{
    programmi.remove("");

    QStringList list;

    list = programmi.keys();

    return list;
}
prog_s Programmi::get_byname(QString name)
{
    return programmi[name];
}
prog_s Programmi::get_byindex( int index)
{
    prog_s prog;
    QStringList list = programmi.keys();

    if( index < list.count())
    {
        prog = programmi[list.at(index)];
        prog.nome = list.at(index);

    }
    return prog;
}
prog_s Programmi::current()
{
    return programmi[current_name];
}
int Programmi::count()
{
    return programmi.count();
}

/*******************************************************************************
 *
 *
 */

Tempi::Tempi(QObject *parent) : QObject(parent)
{
    filename = "/home/root/TimerPezza.json";
}
void Tempi::save()
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   objTempi;
    QJsonObject   objInfo;

    objTempi["accensione"]                              = tempi.accensione;
    objTempi["lavorazione_parziale"]                    = tempi.lavorazione_parziale;
    objTempi["lavorazione_totale"]                      = tempi.lavorazione_totale;
    objTempi["pulizia_manichette"]                      = tempi.pulizia_manichette;
    objTempi["rotazione_corazza"]                       = tempi.rotazione_corazza;
    objTempi["rotazione_ugello1"]                       = tempi.rotazione_ugello1;
    objTempi["rotazione_ugello2"]                       = tempi.rotazione_ugello2;
    objTempi["sostituzione_blocchetto_porta_ugello1"]   = tempi.sostituzione_blocchetto_porta_ugello1;
    objTempi["sostituzione_blocchetto_porta_ugello2"]   = tempi.sostituzione_blocchetto_porta_ugello2;
    objTempi["sostituzione_corazza"]                    = tempi.sostituzione_corazza;
    objTempi["sostituzione_corpo_pistola1"]             = tempi.sostituzione_corpo_pistola1;
    objTempi["sostituzione_corpo_pistola2"]             = tempi.sostituzione_corpo_pistola2;
    objTempi["sostituzione_cuscinetti"]                 = tempi.sostituzione_cuscinetti;
    objTempi["sostituzione_manichette"]                 = tempi.sostituzione_manichette;
    objTempi["sostituzione_puleggia_motore_nastro"]     = tempi.sostituzione_puleggia_motore_nastro;
    objTempi["sostituzione_puleggia_motore_pistola"]    = tempi.sostituzione_puleggia_motore_pistola;
    objTempi["sostituzione_tubo_pescaggio1"]            = tempi.sostituzione_tubo_pescaggio1;
    objTempi["sostituzione_tubo_pescaggio2"]            = tempi.sostituzione_tubo_pescaggio2;
    objTempi["sostituzione_ugello1"]                    = tempi.sostituzione_ugello1;
    objTempi["sostituzione_ugello2"]                    = tempi.sostituzione_ugello2;
    objTempi["sostituzione_ugello_iniettore1"]          = tempi.sostituzione_ugello_iniettore1;
    objTempi["sostituzione_ugello_iniettore2"]          = tempi.sostituzione_ugello_iniettore2;
    objTempi["service"]                                 = tempi.service;

    objInfo["Tempi"] = objTempi;

    jsonDoc.setObject(objInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.close();

//    QProcess *proc = new QProcess(this);
//    proc->start("sync");
//    proc->waitForFinished(-1);
//    delete proc;

    this->save_timer("/home/root/Timers.json");
}

void Tempi::save_timer(QString nomefile)
{
    if(nomefile == "")
    {
        nomefile = "/home/root/Timers.json";
    }

    QFile file(nomefile);

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jParam;
    QJsonArray    jTempi;

    QJsonObject   objProg;
    QJsonObject   objTempi;
    //programmi.remove("");

    objTempi["timer_fan"]               = tempi.accensione;
    objTempi["timer_partial"]           = tempi.lavorazione_parziale;
    objTempi["timer_autogun"]           = tempi.lavorazione_totale;
    objTempi["clean_filter"]            = tempi.pulizia_manichette;
    objTempi["turn_hmguard"]            = tempi.rotazione_corazza;
    objTempi["turn_nozzle1"]            = tempi.rotazione_ugello1;
    objTempi["turn_nozzle2"]            = tempi.rotazione_ugello2;
    objTempi["replace_holder1"]         = tempi.sostituzione_blocchetto_porta_ugello1;
    objTempi["replace_holder2"]         = tempi.sostituzione_blocchetto_porta_ugello2;
    objTempi["replace_hmguard"]         = tempi.sostituzione_corazza;
    objTempi["replace_gunbody1"]        = tempi.sostituzione_corpo_pistola1;
    objTempi["replace_gunbody2"]        = tempi.sostituzione_corpo_pistola2;
    objTempi["replace_bearings"]        = tempi.sostituzione_cuscinetti;
    objTempi["replace_filter"]          = tempi.sostituzione_manichette;
    objTempi["replace_conveyorpulley"]  = tempi.sostituzione_puleggia_motore_nastro;
    objTempi["replace_gunpulley"]       = tempi.sostituzione_puleggia_motore_pistola;
    objTempi["replace_sectionpipe1"]    = tempi.sostituzione_tubo_pescaggio1;
    objTempi["replace_sectionpipe2"]    = tempi.sostituzione_tubo_pescaggio2;
    objTempi["replace_nozzle1"]         = tempi.sostituzione_ugello1;
    objTempi["replace_nozzle2"]         = tempi.sostituzione_ugello2;
    objTempi["replace_airnozzle1"]      = tempi.sostituzione_ugello_iniettore1;
    objTempi["replace_airnozzle2"]      = tempi.sostituzione_ugello_iniettore2;

    jTempi.append( objTempi );

    jInfo["Timer"]     = jTempi;

    jsonDoc.setObject(jInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.close();

//    QProcess *proc = new QProcess(this);
//    proc->start("sync");
//    proc->waitForFinished(-1);
//    delete proc;
}
bool Tempi::open()
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonObject   jTempi;


    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc    = QJsonDocument::fromJson(saveData);
        jInfo      = jsonDoc.object();

        if( jInfo.contains("Tempi"))
        {
            jTempi = jInfo["Tempi"].toObject();

            tempi.accensione             = jTempi["accensione"].toInt();
            tempi.lavorazione_parziale   = jTempi["lavorazione_parziale"].toInt();
            tempi.lavorazione_totale     = jTempi["lavorazione_totale"].toInt();
            tempi.pulizia_manichette     = jTempi["pulizia_manichette"].toInt();
            tempi.rotazione_corazza      = jTempi["rotazione_corazza"].toInt();
            tempi.rotazione_ugello1      = jTempi["rotazione_ugello1"].toInt();
            tempi.rotazione_ugello2      = jTempi["rotazione_ugello2"].toInt();
            tempi.sostituzione_blocchetto_porta_ugello1      = jTempi["sostituzione_blocchetto_porta_ugello1"].toInt();
            tempi.sostituzione_blocchetto_porta_ugello2      = jTempi["sostituzione_blocchetto_porta_ugello2"].toInt();
            tempi.sostituzione_corazza             = jTempi["sostituzione_corazza"].toInt();
            tempi.sostituzione_corpo_pistola1      = jTempi["sostituzione_corpo_pistola1"].toInt();
            tempi.sostituzione_corpo_pistola2      = jTempi["sostituzione_corpo_pistola2"].toInt();
            tempi.sostituzione_cuscinetti          = jTempi["sostituzione_cuscinetti"].toInt();
            tempi.sostituzione_manichette          = jTempi["sostituzione_manichette"].toInt();
            tempi.sostituzione_puleggia_motore_nastro      = jTempi["sostituzione_puleggia_motore_nastro"].toInt();
            tempi.sostituzione_puleggia_motore_pistola     = jTempi["sostituzione_puleggia_motore_pistola"].toInt();
            tempi.sostituzione_tubo_pescaggio1      = jTempi["sostituzione_tubo_pescaggio1"].toInt();
            tempi.sostituzione_tubo_pescaggio2      = jTempi["sostituzione_tubo_pescaggio2"].toInt();
            tempi.sostituzione_ugello1              = jTempi["sostituzione_ugello1"].toInt();
            tempi.sostituzione_ugello2              = jTempi["sostituzione_ugello2"].toInt();
            tempi.sostituzione_ugello_iniettore1    = jTempi["sostituzione_ugello_iniettore1"].toInt();
            tempi.sostituzione_ugello_iniettore2    = jTempi["sostituzione_ugello_iniettore2"].toInt();
            tempi.service                           = jTempi["service"].toInt();
            qDebug( "file tempi caricato " );
        }
        else
        {
            qDebug( "AZZ file tempi corrotto !!" );
            tempi.accensione              = 0;
            tempi.lavorazione_parziale    = 0;
            tempi.lavorazione_totale      = 0;
            tempi.pulizia_manichette      = 0;
            tempi.rotazione_corazza       = 0;
            tempi.rotazione_ugello1       = 0;
            tempi.rotazione_ugello2       = 0;
            tempi.sostituzione_blocchetto_porta_ugello1 = 0;
            tempi.sostituzione_blocchetto_porta_ugello2 = 0;
            tempi.sostituzione_corazza         = 0;
            tempi.sostituzione_corpo_pistola1  = 0;
            tempi.sostituzione_corpo_pistola2  = 0;
            tempi.sostituzione_cuscinetti      = 0;
            tempi.sostituzione_manichette      = 0;
            tempi.sostituzione_puleggia_motore_nastro   = 0;
            tempi.sostituzione_puleggia_motore_pistola  = 0;
            tempi.sostituzione_tubo_pescaggio1 = 0;
            tempi.sostituzione_tubo_pescaggio2 = 0;
            tempi.sostituzione_ugello1         = 0;
            tempi.sostituzione_ugello2         = 0;
            tempi.sostituzione_ugello_iniettore1 = 0;
            tempi.sostituzione_ugello_iniettore2 = 0;
            tempi.service = 0;

            save();
        }


        return true;
    }
    else{
        return false;
    }

}

//*******************************************************************************

Parametri::Parametri(QObject *parent) : QObject(parent)
{
    filename = "/home/root/ParametriPezza.json";
}

void Parametri::save()
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jParam;
    QJsonArray    jTempi;

    QJsonObject   objProg;
    QJsonObject   objTempi;
    //programmi.remove("");

    objProg["Release_ufg"]                  = param.Release_ufg;        // Se Cambiata la Release ufg aggiorna i dati in memoria ( PAR )
    objProg["Release_prg"]                  = param.Release_prg;		// Se cambiata resetto i programmi

    objProg["time_on_lamp"]                 = param.time_on_lamp; 		// dopo questo tempo la luminosita si abbassa
    objProg["lingua"]                       = param.lingua;
    objProg["super_pass"]                   = param.super_pass;
    objProg["mid_pass"]                     = param.mid_pass;
    objProg["low_pass"]                     = param.low_pass;
    objProg["tipo_macchina"]                = param.tipo_macchina;
    objProg["time_on_soffio"]               = param.time_on_soffio;
    objProg["velox"]                        = param.velox;
    objProg["anticipoy"]                    = param.anticipoy;
    objProg["opt1"]                         = param.opt1;
    objProg["opt2"]                         = param.opt2;
    objProg["quotamax_y"]                   = param.quotamax_y;
    objProg["ofsety"]                       = param.ofsety - 5;
    objProg["ofsetx"]                       = param.ofsetx;
    objProg["timer_pausa"]                  = param.timer_pausa;
    objProg["timer_lavoro"]                 = param.timer_lavoro;
    objProg["timeout_filtro"]               = param.timeout_filtro;
    // char     esclusioni[12];
    objProg["unita_misura"]                 = param.unita_misura;
    objProg["programma_in_uso"]             = param.programma_in_uso;

    objProg["matricola"]                    = param.matricola;
    objProg["ipserver"]                     = param.IPServer;
    objProg["iplocal"]                      = param.IPLocal;
    objProg["udpport"]                      = param.UDPPort;
    objProg["as4_0"]                        = param.as4_0;

    jParam.append( objProg );

    objTempi["accensione"]                              = tempi.accensione;
    objTempi["lavorazione_parziale"]                    = tempi.lavorazione_parziale;
    objTempi["lavorazione_totale"]                      = tempi.lavorazione_totale;
    objTempi["pulizia_manichette"]                      = tempi.pulizia_manichette;
    objTempi["rotazione_corazza"]                       = tempi.rotazione_corazza;
    objTempi["rotazione_ugello1"]                       = tempi.rotazione_ugello1;
    objTempi["rotazione_ugello2"]                       = tempi.rotazione_ugello2;
    objTempi["sostituzione_blocchetto_porta_ugello1"]   = tempi.sostituzione_blocchetto_porta_ugello1;
    objTempi["sostituzione_blocchetto_porta_ugello2"]   = tempi.sostituzione_blocchetto_porta_ugello2;
    objTempi["sostituzione_corazza"]                    = tempi.sostituzione_corazza;
    objTempi["sostituzione_corpo_pistola1"]             = tempi.sostituzione_corpo_pistola1;
    objTempi["sostituzione_corpo_pistola2"]             = tempi.sostituzione_corpo_pistola2;
    objTempi["sostituzione_cuscinetti"]                 = tempi.sostituzione_cuscinetti;
    objTempi["sostituzione_manichette"]                 = tempi.sostituzione_manichette;
    objTempi["sostituzione_puleggia_motore_nastro"]     = tempi.sostituzione_puleggia_motore_nastro;
    objTempi["sostituzione_puleggia_motore_pistola"]    = tempi.sostituzione_puleggia_motore_pistola;
    objTempi["sostituzione_tubo_pescaggio1"]            = tempi.sostituzione_tubo_pescaggio1;
    objTempi["sostituzione_tubo_pescaggio2"]            = tempi.sostituzione_tubo_pescaggio2;
    objTempi["sostituzione_ugello1"]                    = tempi.sostituzione_ugello1;
    objTempi["sostituzione_ugello2"]                    = tempi.sostituzione_ugello2;
    objTempi["sostituzione_ugello_iniettore1"]          = tempi.sostituzione_ugello_iniettore1;
    objTempi["sostituzione_ugello_iniettore2"]          = tempi.sostituzione_ugello_iniettore2;
    objTempi["service"]                                 = tempi.service;

    jTempi.append( objTempi );

    jInfo["sw"]        = "Mistral";
    jInfo["matricola"] = "000000";
    jInfo["release"]   = "4.0";
    jInfo["Parametri"] = jParam;
    jInfo["Tempi"]     = jTempi;

    jsonDoc.setObject(jInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.close();

    QProcess *proc = new QProcess(this);
    proc->start("sync");
    proc->waitForFinished(10000);
    delete proc;
}
void Parametri::save_timer(QString nomefile)
{
    if(nomefile == "")
    {
        nomefile = "/home/root/Timers.json";
    }

    QFile file(nomefile);

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jParam;
    QJsonArray    jTempi;

    QJsonObject   objProg;
    QJsonObject   objTempi;
    //programmi.remove("");

    objTempi["timer_fan"]               = tempi.accensione;
    objTempi["timer_partial"]           = tempi.lavorazione_parziale;
    objTempi["timer_autogun"]           = tempi.lavorazione_totale;
    objTempi["clean_filter"]            = tempi.pulizia_manichette;
    objTempi["turn_hmguard"]            = tempi.rotazione_corazza;
    objTempi["turn_nozzle1"]            = tempi.rotazione_ugello1;
    objTempi["turn_nozzle2"]            = tempi.rotazione_ugello2;
    objTempi["replace_holder1"]         = tempi.sostituzione_blocchetto_porta_ugello1;
    objTempi["replace_holder2"]         = tempi.sostituzione_blocchetto_porta_ugello2;
    objTempi["replace_hmguard"]         = tempi.sostituzione_corazza;
    objTempi["replace_gunbody1"]        = tempi.sostituzione_corpo_pistola1;
    objTempi["replace_gunbody2"]        = tempi.sostituzione_corpo_pistola2;
    objTempi["replace_bearings"]        = tempi.sostituzione_cuscinetti;
    objTempi["replace_filter"]          = tempi.sostituzione_manichette;
    objTempi["replace_conveyorpulley"]  = tempi.sostituzione_puleggia_motore_nastro;
    objTempi["replace_gunpulley"]       = tempi.sostituzione_puleggia_motore_pistola;
    objTempi["replace_sectionpipe1"]    = tempi.sostituzione_tubo_pescaggio1;
    objTempi["replace_sectionpipe2"]    = tempi.sostituzione_tubo_pescaggio2;
    objTempi["replace_nozzle1"]         = tempi.sostituzione_ugello1;
    objTempi["replace_nozzle2"]         = tempi.sostituzione_ugello2;
    objTempi["replace_airnozzle1"]      = tempi.sostituzione_ugello_iniettore1;
    objTempi["replace_airnozzle2"]      = tempi.sostituzione_ugello_iniettore2;

    jTempi.append( objTempi );

    jInfo["Timer"]     = jTempi;

    jsonDoc.setObject(jInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.close();

    QProcess *proc = new QProcess(this);
    proc->start("sync");
    proc->waitForFinished(-1);
    delete proc;
}
void Parametri::open()
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jParam;
    QJsonArray    jTempi;
    QJsonArray    jProgs;

    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc    = QJsonDocument::fromJson(saveData);
        jInfo      = jsonDoc.object();
        jParam     = jInfo["Parametri"].toArray();
        jTempi     = jInfo["Tempi"].toArray();
        qDebug( "file parametri caricato " );
    }
    else
    {
        param.Release_ufg 		= 100;
        param.super_pass       	= 3748;
        param.mid_pass       	= 1247;
        param.low_pass         	= 89;

        param.time_on_lamp      	= 120;    // dopo questo tempo la luminosita si abbassa ( in secondi )
        param.lingua            	= 1;

        param.immagini_importate  = 0;
        param.programma_in_uso 	  = 1;

        param.esclusioni[VENTOLA_FILTRO] = 0;
        param.esclusioni[VIBRATORE]      = 0;
        param.esclusioni[MANICH1]        = 0;
        param.esclusioni[MANICH2]  	     = 0;
        param.esclusioni[MANICH3]  	     = 0;
        param.esclusioni[ 5]             = 0;
        param.esclusioni[ 6]             = 0;
        param.esclusioni[ 7]             = 0;
        param.esclusioni[ 8]             = 0;
        param.esclusioni[ 9]             = 0;
        param.esclusioni[10]             = 0;
        param.esclusioni[11]             = 0;

        strcpy(param.matricola,"00000000");
        param.tipo_macchina    = MISTRAL;
        param.unita_misura		= UM_MM;

        param.velox				= 100;
        param.anticipoy			= 5;

        param.quotamax_y			= 1800;
        param.ofsety				= 15; //10 + valore
        param.ofsetx				= 400;
        param.timer_pausa           = 60;
        param.timer_lavoro          = 20;
        param.timeout_filtro        = 25;
        strcpy(param.IPLocal,"10.10.0.200");
        strcpy(param.IPServer,"10.10.0.100");
        param.UDPPort               = 5000;
        param.as4_0                 = 0;

        tempi.accensione              = 0;
        tempi.lavorazione_parziale    = 0;
        tempi.lavorazione_totale      = 0;
        tempi.pulizia_manichette      = 0;
        //tempi.release                 = 1;
        tempi.rotazione_corazza       = 0;
        tempi.rotazione_ugello1       = 0;
        tempi.rotazione_ugello2       = 0;
        tempi.sostituzione_blocchetto_porta_ugello1 = 0;
        tempi.sostituzione_blocchetto_porta_ugello2 = 0;
        tempi.sostituzione_corazza         = 0;
        tempi.sostituzione_corpo_pistola1  = 0;
        tempi.sostituzione_corpo_pistola2  = 0;
        tempi.sostituzione_cuscinetti      = 0;
        tempi.sostituzione_manichette      = 0;
        tempi.sostituzione_puleggia_motore_nastro   = 0;
        tempi.sostituzione_puleggia_motore_pistola  = 0;
        tempi.sostituzione_tubo_pescaggio1 = 0;
        tempi.sostituzione_tubo_pescaggio2 = 0;
        tempi.sostituzione_ugello1         = 0;
        tempi.sostituzione_ugello2         = 0;
        tempi.sostituzione_ugello_iniettore1 = 0;
        tempi.sostituzione_ugello_iniettore2 = 0;
        tempi.service = 0;

        qDebug( "file parametri non trovato " );
        save();
        return;
    }
    s_Par newprog;
    sTempiLavoro newTempi;

    foreach( QJsonValue prog , jParam )
    {
        QJsonObject obj = prog.toObject();

        newprog.Release_ufg             = obj["Release_ufg"].toInt();
        newprog.Release_prg             = obj["Release_prg"].toInt();
        newprog.time_on_lamp            = obj["time_on_lamp"].toInt();
        newprog.super_pass              = obj["super_pass"].toInt();
        newprog.mid_pass                = obj["mid_pass"].toInt();
        newprog.low_pass                = obj["low_pass"].toInt();
        newprog.tipo_macchina           = obj["tipo_macchina"].toInt();
        newprog.time_on_soffio          = obj["time_on_soffio"].toInt();
        newprog.velox                   = obj["velox"].toInt();
        newprog.anticipoy               = obj["anticipoy"].toInt();
        newprog.opt1                    = obj["opt1"].toInt();
        newprog.opt2                    = obj["opt2"].toInt();
        newprog.quotamax_y              = obj["quotamax_y"].toInt();
        newprog.ofsety                  = obj["ofsety"].toInt() + 5;
        newprog.ofsetx                  = obj["ofsetx"].toInt();
        newprog.timer_pausa             = obj["timer_pausa"].toInt();
        newprog.timer_lavoro            = obj["timer_lavoro"].toInt();
        newprog.timeout_filtro          = obj["timeout_filtro"].toInt();
        strcpy(newprog.matricola,obj["matricola"].toString().toLatin1());

        strcpy(newprog.IPLocal,obj["iplocal"].toString().toLatin1());
        strcpy(newprog.IPServer,obj["ipserver"].toString().toLatin1());
        newprog.UDPPort                 = obj["udpport"].toInt();
        newprog.as4_0                   = obj["as4_0"].toInt();

        if( newprog.timer_lavoro < 3 ) newprog.timer_lavoro = newprog.timer_lavoro * 10;

    }
    foreach( QJsonValue tempi , jTempi )
    {
        QJsonObject  obj = tempi.toObject();
        newTempi.accensione             = obj["accensione"].toInt();
        newTempi.lavorazione_parziale   = obj["lavorazione_parziale"].toInt();
        newTempi.lavorazione_totale     = obj["lavorazione_totale"].toInt();
        newTempi.pulizia_manichette     = obj["pulizia_manichette"].toInt();
        //newTempi.release                = obj["release"].toInt();
        newTempi.rotazione_corazza      = obj["rotazione_corazza"].toInt();
        newTempi.rotazione_ugello1      = obj["rotazione_ugello1"].toInt();
        newTempi.rotazione_ugello2      = obj["rotazione_ugello2"].toInt();
        newTempi.sostituzione_blocchetto_porta_ugello1      = obj["sostituzione_blocchetto_porta_ugello1"].toInt();
        newTempi.sostituzione_blocchetto_porta_ugello2      = obj["sostituzione_blocchetto_porta_ugello2"].toInt();
        newTempi.sostituzione_corazza             = obj["sostituzione_corazza"].toInt();
        newTempi.sostituzione_corpo_pistola1      = obj["sostituzione_corpo_pistola1"].toInt();
        newTempi.sostituzione_corpo_pistola2      = obj["sostituzione_corpo_pistola2"].toInt();
        newTempi.sostituzione_cuscinetti          = obj["sostituzione_cuscinetti"].toInt();
        newTempi.sostituzione_manichette          = obj["sostituzione_manichette"].toInt();
        newTempi.sostituzione_puleggia_motore_nastro      = obj["sostituzione_puleggia_motore_nastro"].toInt();
        newTempi.sostituzione_puleggia_motore_pistola     = obj["sostituzione_puleggia_motore_pistola"].toInt();
        newTempi.sostituzione_tubo_pescaggio1      = obj["sostituzione_tubo_pescaggio1"].toInt();
        newTempi.sostituzione_tubo_pescaggio2      = obj["sostituzione_tubo_pescaggio2"].toInt();
        newTempi.sostituzione_ugello1              = obj["sostituzione_ugello1"].toInt();
        newTempi.sostituzione_ugello2              = obj["sostituzione_ugello2"].toInt();
        newTempi.sostituzione_ugello_iniettore1    = obj["sostituzione_ugello_iniettore1"].toInt();
        newTempi.sostituzione_ugello_iniettore2    = obj["sostituzione_ugello_iniettore2"].toInt();
        newTempi.service                           = obj["service"].toInt();
    }

    param = newprog;
    tempi = newTempi;
}


/*******************************************************************************
 *
 *
 */
Commesse::Commesse(QObject *parent) : QObject(parent)
{
    filename = "/home/root/Jobs.json";
}
bool Commesse::add(s_Commessa comm)
{
    bool trovato = false;

    if( comm.nome == "" ) return false;

    for( s_Commessa c : commesse )
    {
        if( comm.nome == c.nome ) trovato = true;
    }
    if( trovato == true ) return false;

    commesse[comm.nome] = comm;

    //programmi.append( prog );
    return true;
}
void Commesse::setCurrent(QString name)
{
    currentName = name;
}

QString Commesse::current_name()
{
    return currentName;
}

QStringList Commesse::elenco_nomi()
{
    commesse.remove("");

    QStringList list;

    list = commesse.keys();

    return list;
}

s_Commessa Commesse::get_byname(QString name)
{
    return commesse[name];
}

s_Commessa Commesse::current()
{
    return commesse[currentName];
}
void Commesse::replace(s_Commessa comm)
{
    //programmi.replace( index, prog);
    commesse[comm.nome] = comm;
}

void Commesse::open()
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;

    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc    = QJsonDocument::fromJson(saveData);
        jInfo      = jsonDoc.object();
        jProgs     = jInfo["Jobs"].toArray();

        qDebug( "file commesse caricato " );

        commesse.clear();
    }
    else
    {
        qDebug( "file commesse non trovato " );
        s_Commessa comm;

        //comm.nome = "Job 1";
        //comm.programma = "Fiorellino";
        //comm.qta_da_fare = 5;
        //comm.qta_fatti = 1;
        //add(comm);
        save();
    }

    foreach( QJsonValue comm , jProgs )
    {
        QJsonObject obj = comm.toObject();
        s_Commessa newcomm;

        newcomm.nome        = obj["name"].toString();
        newcomm.programma   = obj["program"].toString();
        newcomm.qta_da_fare = obj["to_do"].toInt();
        newcomm.qta_fatti   = obj["done"].toInt();
        newcomm.tempo_singolo = obj["last"].toInt();
        newcomm.tempo_totale  = obj["total"].toInt();

        commesse[newcomm.nome] = newcomm;
    }
}
void Commesse::save()
{
    QFile file(filename);

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;

    commesse.remove("");

    for( s_Commessa comm : commesse.values() )
    {
        QJsonObject objProg;

        objProg["name"]           = comm.nome;
        objProg["program"]        = comm.programma;
        objProg["to_do"]          = comm.qta_da_fare;
        objProg["done"]           = comm.qta_fatti;
        objProg["last"]           = comm.tempo_singolo;
        objProg["total"]          = comm.tempo_totale;
        jProgs.append( objProg );
    }

    jInfo["Jobs"] = jProgs;

    jsonDoc.setObject(jInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.close();

//    QProcess *proc = new QProcess(this);
//    proc->start("sync");
//    proc->waitForFinished(10000);
//    delete proc;
}

/*******************************************************************************
 *
 *
 */
ReportAllarmi::ReportAllarmi(QObject *parent) : QObject(parent)
{
    filename = "/home/root/Alarms.json";
}
bool ReportAllarmi::add(s_Allarme alm)
{
    allarmi.append(alm);

    return true;
}

void ReportAllarmi::clear()
{
    allarmi.clear();
    save( filename );
}


void ReportAllarmi::open()
{
    QFile file( filename );

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;

    if( file.exists() == true )
    {
        file.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = file.readAll();
        file.close();

        jsonDoc    = QJsonDocument::fromJson(saveData);
        jInfo      = jsonDoc.object();
        jProgs     = jInfo["Alarms"].toArray();

        qDebug( "file allarmi caricato " );

        allarmi.clear();
    }
    else
    {
        qDebug( "file commesse non trovato " );
        s_Allarme alm;

        //alm.codice     = 1;
        //alm.code      = "1.0";
        //alm.data_on   = "01/01/2021";
        //alm.data_off  = "01/01/2021";
        //add(alm);
        save("");
    }

    foreach( QJsonValue alm , jProgs )
    {
        QJsonObject obj = alm.toObject();
        s_Allarme newalm;

        //newalm.codice   = obj["code"].toInt();
        newalm.code     = obj["alarm"].toString();;
        newalm.data_on  = obj["date_on"].toString();
        newalm.data_off = obj["date_off"].toString();

        allarmi.append(newalm);
    }
}
void ReportAllarmi::save(QString nomefile)
{
    if(nomefile == "")
    {
        nomefile = filename;
    }
    QFile file(nomefile);

    QJsonDocument jsonDoc;
    QJsonObject   jInfo;
    QJsonArray    jProgs;

    for(int i=0; i < allarmi.count(); i++)
    {
        QJsonObject objProg;
        s_Allarme alm;
        alm = allarmi[i];

        //objProg["code"]                  = alm.codice;
        objProg["alarm"]                 = alm.code;
        objProg["date_on"]               = alm.data_on;
        objProg["date_off"]              = alm.data_off;
        jProgs.append( objProg );
    }

    jInfo["Alarms"]         = jProgs;

    jsonDoc.setObject(jInfo);

    file.open( QFile::WriteOnly | QFile::Text | QFile::Truncate );
    file.write( jsonDoc.toJson() );
    file.close();

    QProcess *proc = new QProcess(this);
    proc->start("sync");
    proc->waitForFinished(10000);
    delete proc;
}

