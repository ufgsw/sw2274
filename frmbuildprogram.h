#ifndef FRMBUILDPROGRAM_H
#define FRMBUILDPROGRAM_H

#include <QDialog>
#include <QTimer>

#include "programmi.h"
#include <formnewprogram.h>
#include "formeditarea.h"
#include "sabbiatrice.h"
#include "formmanuale.h"


class ScribbleArea;


namespace Ui {
class frmbuildprogram;
}

class frmbuildprogram : public QDialog
{
    Q_OBJECT

public:
    int result;
    prog_s prog;
    Programmi *prg;
    QString UM;


    void Init(QString um,int quotamax_y);
    explicit frmbuildprogram(QWidget *parent = 0);
    ~frmbuildprogram();

private slots:

    void on_btnAddArea_clicked();

    void on_btnEditArea_clicked();

    void on_btnDeleteArea_clicked();

    void on_btnEditProgram_2_clicked();

    void on_buttonExit_3_clicked();

    void on_btnSave_clicked();

    void processTimer();

    void processTimerRefresh();

    void on_btnEditProgram_3_clicked();

    void on_buttonExit_8_clicked();

    void on_btnNextArea_clicked();

    void on_btnPriorArea_clicked();

    void on_btnThickness_clicked();

    void on_btnExport_clicked();

    void on_btnWidth_clicked();

private:
    float PixelVetroWidth,PixelVetroHeigth;
    QTimer  *timer;
    QTimer  *timer_refresh;
    bool changed;
    int  max_y;

    FormNewProgram* uiNewProgrammi;
    formEditArea* uiEditArea;
    ScribbleArea *scribbleArea;
    formmanuale* frmManuale;

    void NuovaArea(bool editable);
    void RefreshHeigth();
    void RefreshWidth();
    void AggiornaDimensioni();

    Ui::frmbuildprogram *ui;
};

#endif // FRMBUILDPROGRAM_H
