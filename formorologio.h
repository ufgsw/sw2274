#ifndef FORMOROLOGIO_H
#define FORMOROLOGIO_H

#include <QDialog>
#include <QDate>
#include <QTime>
#include "formtastietan.h"

namespace Ui {
class FormOrologio;
}

class FormOrologio : public QDialog
{
    Q_OBJECT

public:
    explicit FormOrologio(QWidget *parent = 0);
    ~FormOrologio();

private slots:
    void on_buttonOre_clicked();

    void on_buttonMinuti_clicked();

    void update_time(QDate d, QTime t);

    void on_buttonSalva_clicked();

    void on_buttonGiorno_clicked();

    void on_buttonMese_clicked();

    void on_buttonAnno_clicked();

private:
    Ui::FormOrologio *ui;

    FormTastieraN* uiTastieraN;

};

#endif // FORMOROLOGIO_H
