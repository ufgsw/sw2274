#ifndef MODEL_ALARM_H
#define MODEL_ALARM_H

#include "programmi.h"


const int COLS_A= 3;
const int ROWS_A= 1000;

#include <QAbstractTableModel>

class model_alarm : public QAbstractTableModel
{
public:


    QString m_gridData[ROWS_A][COLS_A];  //holds text entered into QTableView


    model_alarm(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role)const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

};

#endif // MODEL_ALARM_H
