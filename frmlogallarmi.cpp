#include "frmlogallarmi.h"
#include "ui_frmlogallarmi.h"
#include "programmi.h"
#include <QProcess>
#include "formmessagebox.h"
#include <qdir.h>

frmLogAllarmi::frmLogAllarmi(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmLogAllarmi)
{
    ui->setupUi(this);
    ui->tbProg->setModel(&myModel);
    ui->tbProg->setColumnWidth(0,80);
    ui->tbProg->setColumnWidth(1,280);
    ui->tbProg->setColumnWidth(2,280);


    for( int r=0;r < ROWS_A;r++ )
    {
        for( int c=0;c < COLS_A;c++ )
        {
            myModel.m_gridData[r][c] =  "";
        }
    }

    alms.open();
    int primo = 0;

    if(alms.allarmi.count() >= MAX_ALLARMI){
        primo = alms.allarmi.count() - MAX_ALLARMI;
    }

    int r = 0;
    for(int i=alms.allarmi.count()-1; i > primo; i--)
    {
        s_Allarme alm;
        alm = alms.allarmi[i];

        myModel.m_gridData[r][0] = alm.code; //QString("%1").arg(alm.codice);
        myModel.m_gridData[r][1] = alm.data_on;
        myModel.m_gridData[r][2] = alm.data_off;
        r++;
    }
}

frmLogAllarmi::~frmLogAllarmi()
{
    delete ui;
}

void frmLogAllarmi::on_buttonExit_3_clicked()
{
    close();
}

void frmLogAllarmi::on_btnExport_clicked()
{

    QString device;
    QDir dir("/dev");
   // dir.setCurrent("/dev");
    QFileInfoList list = dir.entryInfoList(QStringList() << "sd*",QDir::System);
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        device = fileInfo.fileName();
    }

    QString path;
    QString program;
    QStringList arguments;

    path = "/dev/" + device;

    QFile file(path);

    if( device != "" && file.exists())
    {
        QProcess* proc = new QProcess(this);
        program = "mount";
        arguments << path << "/media/storage";
        //mount = "mount /dev/";
        //mount += device;
        //mount += " /media/storage";

        proc->start(program , arguments );
        proc->waitForFinished(-1);

        alms.save("/media/storage/Alarms.json");
        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "EXPORT SUCCESSFUL ! !");
        uiMessageBox->exec();

        program = "umount";
        arguments.clear();
        arguments << path;

        //mount = "umount /dev/";
        //mount += device;
        proc->start( program , arguments );
        proc->waitForFinished(-1);

        delete uiMessageBox;
    }
    else
    {
            FormMessageBox *uiMessageBox = new FormMessageBox(this);
            uiMessageBox->setTitle( "INSERT USB KEY !" );
            uiMessageBox->exec();

            delete uiMessageBox;

    }
}
