/*
 * def.h
 *
 *  Created on: 4-gen-2012
 *      Author: Gegio
 */

#ifndef DEF_H_
#define DEF_H_

//#define RELEASE 100
	
#define BIT_0 0x01
#define BIT_1 0x02
#define BIT_2 0x04
#define BIT_3 0x08
#define BIT_4 0x10
#define BIT_5 0x20
#define BIT_6 0x40
#define BIT_7 0x80

#define BIT_8  0x0100
#define BIT_9  0x0200
#define BIT_10 0x0400
#define BIT_11 0x0800
#define BIT_12 0x1000
#define BIT_13 0x2000
#define BIT_14 0x4000
#define BIT_15 0x8000

#define BIT_16 0x00010000
#define BIT_17 0x00020000
#define BIT_18 0x00040000
#define BIT_19 0x00080000
#define BIT_20 0x00100000
#define BIT_21 0x00200000
#define BIT_22 0x00400000
#define BIT_23 0x00800000

#define BIT_24 0x01000000
#define BIT_25 0x02000000
#define BIT_26 0x04000000
#define BIT_27 0x08000000
#define BIT_28 0x10000000
#define BIT_29 0x20000000
#define BIT_30 0x40000000
#define BIT_31 0x80000000

// Comandi motori
#define CMD_GET_RELEASE			0xff
#define CMD_HOME_MOT1			0x01
#define CMD_HOME_MOT2     	 	0x02
#define CMD_MOVE_QUOTA_MOT1     0x03
#define CMD_MOVE_QUOTA_MOT2     0x04
#define CMD_MOVE_VELO_MOT1      0x05
#define CMD_MOVE_VELO_MOT2      0x06
#define CMD_SET_QUOTA_MOT1		0x07
#define CMD_SET_QUOTA_MOT2		0x08
#define CMD_RESET_ALARM_MOT1	0x09
#define CMD_RESET_ALARM_MOT2	0x0a
#define CMD_DISABLE_ALL_MOT     0x0b

// TASk
#define MAIN_TASK   			1
#define USB_MS_TASK				2
#define USB_CDC_TASK			3
#define UART_TASK				4

#define TICK_USER_TASK			5
#define WIN_TASK				6
#define SABBIATURA_TASK	 		7
#define CMD_MANUALI_TASK        8

// EVENT
#define EVENT_START_SABBIATURA	BIT_0
#define EVENT_START_CMD_MANUALI	BIT_1

#define TASTO1 	  0
#define TASTO2    1
#define TASTO3    2
#define TASTO4    3

#define LINGUA      Par.lingua
#define ITALIANO 	0
#define INGLESE  	1
#define POLACCO  	2
#define CECO  	    3
#define RUSSO	    4

#define ENABLE 			1
#define DISABLE			0



typedef enum {
	Emergenza 						= BIT_0,
	Termico_ventilatore 			= BIT_1,
    Termico_vibratore               = BIT_2,
	NC1								= BIT_3,
	FC_Portello						= BIT_4,
    FC_Vetro						= BIT_5,
	NC2								= BIT_6,
    Pulsante_nastro_indietro        = BIT_7,
    Pulsante_nastro_avanti          = BIT_8,
    Ventilatore_manuale             = BIT_9,
	Type_1							= BIT_10,
	Type_2							= BIT_11,
	Type_3							= BIT_12,
	NC3								= BIT_13,
	NC4								= BIT_14,
	NC5								= BIT_15
}eIngressi;

typedef enum {
    Elettrovalvola_pulizia_filtro	= BIT_0,				// manichetta 1
    Pulizia_nastro					= BIT_1,
	
    Motore_ventilazione				= BIT_8,
    Motore_vibratore				= BIT_9,
	
    Elettrovalvola_pistola_1		= BIT_12,	// invertito pistola 1 con pistola 2  ( 07/01/2013 )
    Elettrovalvola_pistola_2		= BIT_11,
	
	Elettrovalvola_pulizia_filtro_1	= BIT_14,			// manichetta 2
	Elettrovalvola_pulizia_filtro_2	= BIT_15				// manichetta 3
}eUscite;

typedef enum {
    Abilitazione_motore	    = BIT_0,
    Quota_raggiunta		    = BIT_1,
	Quota_home_raggiunta	= BIT_2,
	Stato_motore			= BIT_3
}eStatus;

#define ALLARME_EMERGENZA					(( Input & Emergenza) 				== 0)
#define ALLARME_TERMICO_VENTILATORE         (( Input & Termico_ventilatore) 	== 0)
#define ALLARME_TERMICO_VIBRATORE           (( Input & Termico_vibratore) 	    == 0)
#define ALLARME_PORTELLO					(( Input & FC_Portello) 			== 0)
#define ALLARME_FC_VETRO					(( Input & FC_Vetro) 				== 0)
#define ALLARME_MOT1							( Status_mot1 & Stato_motore)
#define ALLARME_MOT2							( Status_mot2 & Stato_motore)

#define MANUALE_NASTRO_INDIETRO			(( Input & Pulsante_nastro_indietro) 	== 1)
#define MANUALE_NASTRO_AVANTI			(( Input & Pulsante_nastro_avanti) 		== 1)
#define MANUALE_VENTILATORE_ON			(( Input & Ventilatore_manuale) 		== 1)

#define MOT1_QUOTA_RAGGIUNTA				( Status_mot1 & Quota_raggiunta)
#define MOT1_ABILITATO						( Status_mot1 & Abilitazione_motore)
#define MOT1_OK								((Status_mot1 & Stato_motore)	== 0)

#define MOT2_QUOTA_RAGGIUNTA				( Status_mot2 & Quota_raggiunta)
#define MOT2_HOME_OK						( Status_mot2 & Quota_home_raggiunta)
#define MOT2_NOT_HOME 					    ( MOT2_HOME_OK == 0)
#define MOT2_ABILITATO						( Status_mot2 & Abilitazione_motore)
#define MOT2_OK								((Status_mot2 & Stato_motore)	== 0)


#define ALM_EMERGENZA   1
#define ALM_VENTILATORE 2
#define ALM_VIBRATORE   3
#define ALM_PORTELLO    5
#define ALM_VETRO       6
#define ALM_MOT_1       7
#define ALM_MOT_2       8
#define ALM_HOME        20

#define VELMAX_X    1200
#define VELMIN_X     220    // 220
#define VELMAX_Y    2500
#define VELMIN_Y     750    // modifica del 05.10.2004   250 -> 750

#define ZEPHIR   	0x01
//#define STANDARD  	1
#define MISTRAL   	0x00
//#define SIMPLEX_PLUS 3

#define HALTED   0
#define GO       1
#define FERMA    2

#define VENTOLA_FILTRO  0
#define VIBRATORE   		1
#define MANICH1     		2
#define MANICH2     		3
#define MANICH3     		4

#define UM_MM	0
#define UM_IN  1

#define MOT_X	1
#define MOT_Y	2

#define RANGE_QUOTA_FATTO_X	2
#define RANGE_QUOTA_FATTO_Y	5


#define METRICO     0
#define IMPERIALE   1

#define FATTORE_IN_MM   25.4
#define FATTORE_MM_IN   0.0393701
#define FATTORE_BAR_PSI 14.504

#endif /* DEF_H_ */
