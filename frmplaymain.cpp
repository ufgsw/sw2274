#include "frmplaymain.h"
#include "ui_frmplaymain.h"

frmPlayMain::frmPlayMain(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmPlayMain)
{
    ui->setupUi(this);
}

frmPlayMain::~frmPlayMain()
{
    delete ui;
}
void frmPlayMain::Init()
{
    ui->lbNomeProg->setText(prog.nome);
    uint h,m,s;

    h = (uint)(  prog.secondi_impiegati / 3600);
    m = (uint)(( prog.secondi_impiegati - ( h * 3600) ) / 60);
    s = (uint)(( prog.secondi_impiegati - ( h * 3600) - (m * 60)));

    ui->lbTempo->setText(QString("h. %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));

    h = (uint)(   (prog.secondi_impiegati * prog.esecuzioni)  / 3600);
    m = (uint)((  (prog.secondi_impiegati * prog.esecuzioni)  - ( h * 3600) ) / 60);
    s = (uint)((  (prog.secondi_impiegati * prog.esecuzioni)  - ( h * 3600) - (m * 60)));

    ui->lbTempo_2->setText(QString("h. %1:%2:%3/%4").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')).arg(prog.esecuzioni));


}

void frmPlayMain::on_buttonExit_3_clicked()
{
    result = -1;
    close();
}


void frmPlayMain::on_btnStart_clicked()
{
}

void frmPlayMain::on_btnMenuProgEdit_clicked()
{
    result = 2;
    close();
 }

void frmPlayMain::on_btnStart_2_clicked()
{
    float peso;

    peso = ((float)(prog.larghezza_vetro * prog.altezza_vetro * prog.spessore_vetro) * 0.0000025) / ((float)prog.larghezza_vetro / 1000) ;
    if(peso > 75)
    {

        ReportAllarmi alms;
        s_Allarme alm;
        alms.open();
        //alm.codice = 100;
        alm.code = "9.8";

        QString str;
        str = QDate::currentDate().toString("dd.MM.yyyy");
        str += " ";
        str += QTime::currentTime().toString("hh:mm:ss");
        alm.data_on   = str;

        frmAllarmi *frmallarmi = new frmAllarmi(this);
        frmallarmi->SetImageMan(":/png/AlmPeso.png","9.8");
        frmallarmi->exec();

        str = QDate::currentDate().toString("dd.MM.yyyy");
        str += " ";
        str += QTime::currentTime().toString("hh:mm:ss");
        alm.data_off   = str;

        alms.add(alm);
        alms.save("");

        delete frmallarmi;
    }


    result = 1;
    close();

}
