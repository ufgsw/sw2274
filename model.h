#ifndef MODEL_H
#define MODEL_H

// mymodel.h
#include <QAbstractTableModel>
#include "programmi.h"

const int COLS = 5;
const int ROWS = 1000;

class MyModel : public QAbstractTableModel
{
    Q_OBJECT
public:

    MyModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role)const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void setRows( int n) {nrows = n;}

private:

    QString SecondToTime(int sec);

    Programmi* progs;

    int nrows;

    // QAbstractItemModel interface
public:
    bool insertRows(int row, int count, const QModelIndex &parent);
    bool insertProgs( prog_s prog );
};

#endif // MODEL_H
