#include "frmsplash.h"
#include "ui_frmsplash.h"

frmSplash::frmSplash(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmSplash)
{
    ui->setupUi(this);
    timer_btn = new QTimer(this);
    connect(timer_btn,SIGNAL(timeout()), this, SLOT(reset_timer()));

}
void frmSplash::Init(QString modello,QString matricola)
{
    ui->lbModello->setText(modello);
    ui->lbMatricola->setText(matricola);
    ui->imgCentrale->setVisible(true);
    timer_btn->start(4000);

}
void frmSplash::reset_timer()
{
    timer_btn->stop();
    close();
}


frmSplash::~frmSplash()
{
    delete ui;
}

void frmSplash::on_imgCentrale_clicked()
{
    reset_timer();

}
