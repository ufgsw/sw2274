#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QtNetwork>
#include <QtCore>

//#include "textprogressbar.h"

class DownloadManager : public QObject
{
    Q_OBJECT
   public:
       explicit DownloadManager(QObject *parent = nullptr);

       void append(const QUrl &url);
       void append(const QStringList &urls);
       static QString saveFileName(const QUrl &url);

   signals:
       void finished();
       void progress(int p, QString brate);
       void message(QString msg);

   private slots:
       void startNextDownload();
       void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
       void downloadFinished();
       void downloadReadyRead();

   private:
       bool isHttpRedirect() const;
       void reportRedirect();

       QNetworkAccessManager manager;
       QQueue<QUrl> downloadQueue;
       QNetworkReply *currentDownload = nullptr;

       QFile file_output;
       QElapsedTimer downloadTimer;
       //TextProgressBar progressBar;

       int downloadedCount = 0;
       int totalCount = 0;
};

#endif // DOWNLOADMANAGER_H
