#ifndef MODELINFO_H
#define MODELINFO_H

#include <QAbstractTableModel>

const int COLS_I= 2;
const int ROWS_I= 13;


class ModelInfo : public QAbstractTableModel
{
    Q_OBJECT
public:
    QString m_gridData[ROWS_I][COLS_I];  //holds text entered into QTableView


    ModelInfo(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role)const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};

#endif // MODELINFO_H

