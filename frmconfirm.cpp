#include "frmconfirm.h"
#include "ui_frmconfirm.h"

frmConfirm::frmConfirm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmConfirm)
{
    ui->setupUi(this);
    ui->lbHelp->setText("7.1");
}

frmConfirm::~frmConfirm()
{
    delete ui;
}
void frmConfirm::Init(QString msg,bool icona,int tipo)
{
    ui->lbMessage->setText(msg);
    if(tipo == 1){
        ui->imgIcona->setIcon(QIcon(":/png/28+.png"));
    }
    else if(tipo == 2){
        ui->imgIcona->setIcon(QIcon(":/png/33+.png"));
    }
    else{
        ui->imgIcona->setIcon(QIcon(":/png/10-E.png"));
    }

    ui->imgIcona->setVisible(icona);
}
void frmConfirm::InitTitle(QString help,QString file)
{
    ui->lbHelp->setText(help);
    ui->imgIconaTitle->setIcon(QIcon(file));

}
void frmConfirm::on_btnOk_clicked()
{
    result = 1;
    close();
}

void frmConfirm::on_btnCancel_clicked()
{
    result = 0;
    close();
}
