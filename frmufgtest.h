#ifndef FRMUFGTEST_H
#define FRMUFGTEST_H

#include <QDialog>
#include <sabbiatrice.h>
#include <QTimer>

namespace Ui {
class frmUfgTest;
}

class frmUfgTest : public QDialog
{
    Q_OBJECT

public:
    Sabbiatrice *sabbiatrice;
    Programmi* progs;

    void Init();
    explicit frmUfgTest(QWidget *parent = 0);
    ~frmUfgTest();

private slots:

    void processTimer();


    void on_btnAvantiX_pressed();

    void on_btnAvantiX_released();

    void on_btnAvantiY_pressed();

    void on_btnAvantiY_released();

    void on_buttonExit_3_clicked();



    void on_btnOffsetY_clicked();

    void on_btnOffsetX_clicked();

    void on_btnFiltroPausa_clicked();

    void on_btnFiltroLavoro_clicked();

    void on_btnVentilatore_clicked();

    void on_btnVelAsseX_clicked();


    void on_cbModello_currentIndexChanged(int index);

    void on_btnModello_clicked();

    void on_btnIPServer_clicked();

    void on_btnUDPPort_clicked();

    void on_btnQuotaMaxY_clicked();

    void on_btnIPLocal_2_clicked();

private:
    QTimer      *timer;
    bool        pressed;
    int         conta = 0;
    float GetNumber(QString title,float min,float max,bool asinteger);

    Ui::frmUfgTest *ui;
};

#endif // FRMUFGTEST_H
