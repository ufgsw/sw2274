#include "formtastieraip.h"
#include "ui_formtastieraip.h"

FormTastieraIP::FormTastieraIP(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormTastieraIP)
{
    ui->setupUi(this);

    punti = 0;
    primo = true;
    valid = false;
 }

FormTastieraIP::~FormTastieraIP()
{
    delete ui;
}

QString FormTastieraIP::get_val()
{
    return ui->labelVal->text();
}

bool FormTastieraIP::is_valid()
{
   return valid;
}

void FormTastieraIP::on_button1_clicked()
{
    if( ui->labelVal->text().length() < 15 ) ui->labelVal->setText( ui->labelVal->text() + ui->button1->text() );
}

void FormTastieraIP::on_button2_clicked()
{
    if( ui->labelVal->text().length() < 15 ) ui->labelVal->setText( ui->labelVal->text() + ui->button2->text() );
}

void FormTastieraIP::on_button3_clicked()
{
    if( ui->labelVal->text().length() < 15 ) ui->labelVal->setText( ui->labelVal->text() + ui->button3->text() );
}

void FormTastieraIP::on_button4_clicked()
{
    if( ui->labelVal->text().length() < 15 ) ui->labelVal->setText( ui->labelVal->text() + ui->button4->text() );
}

void FormTastieraIP::on_button5_clicked()
{
    if( ui->labelVal->text().length() < 15 ) ui->labelVal->setText( ui->labelVal->text() + ui->button5->text() );
}

void FormTastieraIP::on_button6_clicked()
{
    if( ui->labelVal->text().length() < 15 ) ui->labelVal->setText( ui->labelVal->text() + ui->button6->text() );
}

void FormTastieraIP::on_button7_clicked()
{
    if( ui->labelVal->text().length() < 15 ) ui->labelVal->setText( ui->labelVal->text() + ui->button7->text() );

}

void FormTastieraIP::on_button8_clicked()
{
    if( ui->labelVal->text().length() < 15 ) ui->labelVal->setText( ui->labelVal->text() + ui->button8->text() );

}

void FormTastieraIP::on_button9_clicked()
{
    if( ui->labelVal->text().length() < 15 ) ui->labelVal->setText( ui->labelVal->text() + ui->button9->text() );

}

void FormTastieraIP::on_buttonCanc_clicked()
{
    ui->labelVal->setText("");
    punti = 0;
}

void FormTastieraIP::on_button0_clicked()
{
    if( ui->labelVal->text().length() < 15 ) ui->labelVal->setText( ui->labelVal->text() + ui->button0->text() );
}

void FormTastieraIP::on_buttonPunto_clicked()
{
    if( ++punti < 4 ) ui->labelVal->setText( ui->labelVal->text() + ui->buttonPunto->text() );
}

void FormTastieraIP::on_buttonOK_clicked()
{
    valid = true;
    close();
}

void FormTastieraIP::on_buttonAnnulla_clicked()
{
    valid = false;
    close();
}
