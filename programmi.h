#ifndef PROGRAMMI_H
#define PROGRAMMI_H

#include <QObject>
#include <QList>
#include <QDateTime>
#include <globs.h>

#define MAX_PUNTI 60
//#define MAX_PROGS 30
#define MAX_ESCURSIONE 9

#define MAX_PROG 200
#define MAX_AREE 50
#define MAX_COMMESSE 100
#define MAX_ALLARMI 100


#define ATTIVA 1
#define SELEZIONATA 2
#define CANCELLATA 3

typedef struct
{
    uint8_t  start;
    uint32_t crono;

    uint32_t ingresso_vetro;
    uint32_t cm_x;
    uint32_t cm_y;

    uint16_t cx;
    uint16_t cy;
    uint32_t x[150];
    uint32_t y[150];

}tempoPrevisto_s;

typedef struct {
    uint32_t save_par_fr 			:1;
    //uint32_t save_tempi             :1;
    uint32_t save_prog_in_uso       :1;
    //uint32_t save_commesse          :1;

    uint32_t enable_mass_storage 	:1;
    uint32_t mot_x_done				:1;
    uint32_t mot_y_done				:1;
    uint32_t send_velo_mot1       	:1;
    uint32_t send_velo_mot2       	:1;
    uint32_t seng_move_quota_mot1 	:1;
    uint32_t seng_move_quota_mot2 	:1;
    uint32_t send_quota_mot1		:1;
    uint32_t send_quota_mot2		:1;
    uint32_t send_reset_alarm_mot1	:1;
    uint32_t send_reset_alarm_mot2	:1;

    uint32_t home_mot2			   	:1;
    uint32_t send_home_mot2			:1;

    uint32_t win_start				:1;
    uint32_t stop_esegui			:1;

    uint32_t in_pausa				:1;
    uint32_t in_manuale           	:1;
    uint32_t sabbiatura_completata	:1;
    uint32_t pausa_abilitata 		:1;

    uint32_t esegui_filtro			:1;

    uint32_t disable_mot            :1;
    uint32_t mot_disabilitato		:1;


}flag_s;

typedef struct {
   uint16_t  Release_ufg;      // Se Cambiata la Release ufg aggiorna i dati in memoria ( PAR )
   uint16_t  Release_prg;		// Se cambiata resetto i programmi
   //uint16_t  Release_img;		// Se cambiata ricarico le immagini

   uint16_t  time_on_lamp; 		// dopo questo tempo la luminosita si abbassa
   uint16_t  lingua;

   uint16_t  super_pass;
   uint16_t  mid_pass;
   uint16_t  low_pass;

   uint16_t  tipo_macchina;
   uint16_t  time_on_soffio;

   uint16_t  velox;
   uint16_t  anticipoy;
   uint16_t  opt1;
   uint16_t  opt2;
   uint16_t  quotamax_y;
   int16_t   ofsety;
   int16_t   ofsetx;

   uint16_t  timer_pausa;
   uint16_t  timer_lavoro;
   uint16_t  timeout_filtro;

   char      esclusioni[12];
   uint16_t  unita_misura;

   uint16_t  programma_in_uso;
   uint16_t  immagini_importate;

   char      matricola[12];
   //uint16_t  posizioneImmaginiNand[100];
   char      IPServer[15];
   char      IPLocal[15];
   uint16_t  UDPPort;

   char      as4_0;

} s_Par;


typedef struct  {

    uint16_t    stato;          // 0 = non implementata, 1 = Attiva; 2 = selezionata
    float       y_iniziale;     // quote in mm
    float       y_finale;
    float       x_iniziale;
    float       x_finale;
    float       step;
    float       width;          // larghezza in mm
    float       height;         // altezza in mm

    float _yini;
    float _yfin;
    float _xini;
    float _xfin;
    float _step;

    uint16_t   ripetizioni;
    uint16_t   velocita_pistola;	// da 10% a 100%
    uint16_t   velocita_nastro;		// da 50% a 100%

    // bar
    float pressione_iniziale;
    float pressione_finale;
    // psi
    float _pini;
    float _pfin;

    uint16_t   pis1;
    uint16_t   pis2;

    uint16_t um;	// unita misura
    uint16_t pr; // indica se è stata programmata

    QPoint   p_pixel;   // area del rettangolo in pixel
    QPoint   dim_pixel; // area del rettangolo in pixel

}s_Area;


typedef struct {
   QString  nome;

   uint16_t    naree;
   uint16_t    RetToHome;            // ritorno asse x a home 0 = non rit;  1= ritorno a home
   uint16_t    secondi_impiegati;
   uint16_t    secondi_totali;
   uint16_t    campioni;
   uint16_t    esecuzioni;
   uint16_t    direzione;            // direzione del nastro 0 = avanti, 1 = indietro

   float       larghezza_vetro;                 // larghezza del vetro
   float       altezza_vetro;                   // Altezza del vetro
   float       larghezza_extra;                 // larghezza del vetro
   float       altezza_extra;                   // Altezza del vetro
   float       spessore_vetro;                  // Spessore del vetro
   float       pixel_per_mm;                    // quanti pixel per mm

   s_Area Area[MAX_AREE];
} prog_s;

typedef struct {
   QString      nome;
   QString      programma;
   int          qta_da_fare;
   int          qta_fatti;
   int          tempo_singolo;
   int          tempo_totale;
} s_Commessa;

typedef struct
{
    s_Commessa Commessa[MAX_COMMESSE];
} s_Commesse;

typedef struct {
   //uint16_t  codice;
   QString  code;

   QString  data_on;
   QString  data_off;

} s_Allarme;

typedef struct
{
    s_Allarme Allarme[MAX_ALLARMI];
} s_Allarmi;

typedef struct
{
    //int16_t release;

    // Tutti i tempi in secondi

    int32_t rotazione_ugello1;
    int32_t rotazione_ugello2;
    int32_t sostituzione_ugello1;
    int32_t sostituzione_ugello2;
    int32_t sostituzione_ugello_iniettore1;
    int32_t sostituzione_ugello_iniettore2;
    int32_t sostituzione_blocchetto_porta_ugello1;
    int32_t sostituzione_blocchetto_porta_ugello2;
    int32_t sostituzione_corpo_pistola1;
    int32_t sostituzione_corpo_pistola2;
    int32_t sostituzione_tubo_pescaggio1;
    int32_t sostituzione_tubo_pescaggio2;
    int32_t sostituzione_puleggia_motore_pistola;
    int32_t sostituzione_puleggia_motore_nastro;
    int32_t sostituzione_cuscinetti;
    int32_t rotazione_corazza;
    int32_t sostituzione_corazza;
    int32_t pulizia_manichette;
    int32_t sostituzione_manichette;

    int32_t lavorazione_parziale;
    int32_t lavorazione_totale;
    int32_t accensione;

    int32_t service;

}sTempiLavoro;

typedef struct
{
   uint32_t rotazione_ugello1                            :1;
   uint32_t rotazione_ugello2                            :1;
   uint32_t sostituzione_ugello1                         :1;
   uint32_t sostituzione_ugello2                         :1;
   uint32_t sostituzione_ugello_iniettore1               :1;
   uint32_t sostituzione_ugello_iniettore2               :1;
   uint32_t sostituzione_blocchetto_porta_ugello1        :1;
   uint32_t sostituzione_blocchetto_porta_ugello2        :1;
   uint32_t sostituzione_corpo_pistola1                  :1;
   uint32_t sostituzione_corpo_pistola2                  :1;
   uint32_t sostituzione_tubo_pescaggio1                 :1;
   uint32_t sostituzione_tubo_pescaggio2                 :1;
   uint32_t sostituzione_puleggia_motore_pistola         :1;
   uint32_t sostituzione_puleggia_motore_nastro          :1;
   uint32_t sostituzione_cuscinetti                      :1;
   uint32_t rotazione_corazza                            :1;
   uint32_t sostituzione_corazza                         :1;
   uint32_t pulizia_manichette                           :1;
   uint32_t sostituzione_manichette                      :1;

   uint32_t service :1;
}sFlagTempiLavorazione;

typedef union
{
    sFlagTempiLavorazione type;
    uint32_t               val;
}uFlagTempiLavorazione;


class Programmi : public QObject
{
    Q_OBJECT
public:
    QMap<QString,prog_s> programmi;

    QString modello;
    //QString release;
    short   As4_0;
    short   Tipo;
    QString sn;
    QString Year;
    QString Manufacturer;
    QString Automatic_Guns;
    QString UM;
    QString Volt;
    QString Hz;
    QString A;
    QString kW;
    QString Bar;
    QString Weigh;
    QString DoHoming;

    QString filename;

    explicit Programmi(QObject *parent = nullptr);

    static Programmi* getInstance()
    {
        static Programmi* instance = 0;
        if( instance == 0 )
        {
            instance = new Programmi();
        }
        return instance;
    }

    void save(QString to_remove);
    void export_all_prog();
    void export_prog(QString filename, prog_s prog);
    void import_prog(QString filename);
    void import_restore(QString filename);
    void open();
    bool add( prog_s prog);
    void remove( prog_s prog);
    void replace(prog_s prog);
    void rename(prog_s prog,QString newname);
    bool exist(QString nome);
    void setCurrent(QString name);

    QStringList elenco_nomi();
    prog_s get_byname( QString name);
    prog_s get_byindex( int index);
    prog_s current();

    int count();

private:
    QString current_name;



signals:

public slots:
};

class Config : public QObject
{
    Q_OBJECT
public:

    QString modello;
    //QString release;
    short   As4_0;
    short   Tipo;
    QString sn;
    QString Year;
    QString Manufacturer;
    QString Automatic_Guns;
    QString UM;
    QString Volt;
    QString Hz;
    QString A;
    QString kW;
    QString Bar;
    QString Weigh;
    QString DoHoming;


    explicit Config(QObject *parent = nullptr);

    static Config* getInstance()
    {
        static Config* instance = 0;
        if( instance == 0 )
        {
            instance = new Config();
        }
        return instance;
    }
    void save();
    bool open();

private:
    QString filename;

signals:

public slots:
};



class Parametri : public QObject
{
    Q_OBJECT
public:
    s_Par        param;
    sTempiLavoro tempi;
    QString      filename;

    explicit Parametri(QObject *parent = nullptr);

    static Parametri* getInstance()
    {
        static Parametri* instance = 0;
        if( instance == 0 )
        {
            instance = new Parametri();
        }
        return instance;
    }

    void save();
    void open();
    void save_timer(QString nomefile);

private:


signals:

public slots:
};
class Tempi : public QObject
{
    Q_OBJECT
public:
    //s_Par        param;
    sTempiLavoro tempi;
    QString      filename;

    explicit Tempi(QObject *parent = nullptr);

    static Tempi* getInstance()
    {
        static Tempi* instance = 0;
        if( instance == 0 )
        {
            instance = new Tempi();
        }
        return instance;
    }

    void save();
    void save_timer(QString nomefile);
    bool open();

private:


signals:

public slots:
};

class Commesse : public QObject
{
    Q_OBJECT
public:
    QMap<QString,s_Commessa> commesse;

    explicit Commesse(QObject *parent = nullptr);

    static Commesse* getInstance()
    {
        static Commesse* instance = 0;
        if( instance == 0 )
        {
            instance = new Commesse();
        }
        return instance;
    }
    void save();
    void open();
    void setCurrent(QString name);

    QString current_name();
    QStringList elenco_nomi();
    s_Commessa get_byname(QString name);
    s_Commessa current();
    void replace(s_Commessa comm);

private:
    QString currentName;
    QString filename;
    bool add(s_Commessa comm);

signals:

public slots:
};

class ReportAllarmi : public QObject
{
    Q_OBJECT
public:
    QList<s_Allarme> allarmi;

    explicit ReportAllarmi(QObject *parent = nullptr);

    static ReportAllarmi* getInstance()
    {
        static ReportAllarmi* instance = 0;
        if( instance == 0 )
        {
            instance = new ReportAllarmi();
        }
        return instance;
    }
    void save(QString file);
    void open();
    bool add(s_Allarme comm);

    void clear();

    s_Allarme get_byname(QString code);

private:
    QString current_name;
    QString filename;

signals:

public slots:
};


#endif // PROGRAMMI_H
