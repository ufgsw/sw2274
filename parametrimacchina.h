#ifndef PARAMETRIMACCHINA_H
#define PARAMETRIMACCHINA_H

#include <QObject>

class ParametriMacchina : public QObject
{
    Q_OBJECT
public:
    explicit ParametriMacchina(QObject *parent = nullptr);

    static ParametriMacchina* getInstance()
    {
        static ParametriMacchina* instance = 0;
        if( instance == 0 )
        {
            instance = new ParametriMacchina();
        }
        return instance;
    }

    
    QString modello;
    //QString release;
    short   As4_0;
    short   Tipo;
    QString sn;
    QString Year;
    QString Manufacturer;
    QString Automatic_Guns;
    QString UM;
    QString Volt;
    QString Hz;
    QString A;
    QString kW;
    QString Bar;
    QString Weigh;
    QString DoHoming;

    QString filename;
    
    void load();
    void save();
    
signals:

public slots:
    
    
    
};

#endif // PARAMETRIMACCHINA_H
