#ifndef FRMEDITPROG_1_H
#define FRMEDITPROG_1_H

#include <QDialog>
#include <QTimer>
#include "programmi.h"

namespace Ui {
class frmEditProg_1;
}

class frmEditProg_1 : public QDialog
{
    Q_OBJECT

public:
    int result;
    int modalita;
    void Init();
    prog_s prog;
    explicit frmEditProg_1(QWidget *parent = 0);
    ~frmEditProg_1();

private slots:
    void on_btnNext_clicked();

    void on_btnPrior_clicked();

    void processTimer();

    void on_buttonExit_3_clicked();

    void on_buttonExit_clicked();

private:
    int fase;

    QTimer *timer;
    Ui::frmEditProg_1 *ui;
};

#endif // FRMEDITPROG_1_H
