#include "frmmainsetup.h"
#include "ui_frmmainsetup.h"


frmMainSetup::frmMainSetup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmMainSetup)
{
    ui->setupUi(this);
    frmedittimer = new frmEditTimer();

}

frmMainSetup::~frmMainSetup()
{
    delete ui;
}

void frmMainSetup::Init(int alarm,QString n_p)
{

    n_pistole = n_p;
    frmedittimer->n_pistole = n_pistole;
        //Ugelli
    if(sabbiatrice->warningTempiLavorazione.type.rotazione_ugello1 == 1
            || sabbiatrice->warningTempiLavorazione.type.rotazione_ugello2 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello1 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello2 == 1){

        ui->imgWarning_1->setVisible(true);
    }
    else{
        ui->imgWarning_1->setVisible(false);
    }

        //Ugelli iniettore
    if(sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello_iniettore1 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello_iniettore2 == 1){

        ui->imgWarning_2->setVisible(true);
    }
    else{
        ui->imgWarning_2->setVisible(false);
    }

        //Blocchetto porta ugello
    if(sabbiatrice->warningTempiLavorazione.type.sostituzione_blocchetto_porta_ugello1 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_blocchetto_porta_ugello2 == 1){

        ui->imgWarning_3->setVisible(true);
    }
    else{
        ui->imgWarning_3->setVisible(false);
    }

        //Corpo pistola
    if(sabbiatrice->warningTempiLavorazione.type.sostituzione_corpo_pistola1 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_corpo_pistola2 == 1){

        ui->imgWarning_4->setVisible(true);
    }
    else{
        ui->imgWarning_4->setVisible(false);
    }

        //Corazza
    if(sabbiatrice->warningTempiLavorazione.type.rotazione_corazza == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_corazza == 1){

        ui->imgWarning_5->setVisible(true);
    }
    else{
        ui->imgWarning_5->setVisible(false);
    }

        //Tubo di pescaggio
    if(sabbiatrice->warningTempiLavorazione.type.sostituzione_tubo_pescaggio1 == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_tubo_pescaggio2 == 1){

        ui->imgWarning_6->setVisible(true);
    }
    else{
        ui->imgWarning_6->setVisible(false);
    }

        //Puleggia pistola
    if(sabbiatrice->warningTempiLavorazione.type.sostituzione_puleggia_motore_pistola == 1){

        ui->imgWarning_7->setVisible(true);
    }
    else{
        ui->imgWarning_7->setVisible(false);
    }
        //Puleggia nastro
    if(sabbiatrice->warningTempiLavorazione.type.sostituzione_puleggia_motore_nastro == 1){

        ui->imgWarning_8->setVisible(true);
    }
    else{
        ui->imgWarning_8->setVisible(false);
    }

        //Cuscinetti
    if(sabbiatrice->warningTempiLavorazione.type.sostituzione_cuscinetti == 1){
        ui->imgWarning_9->setVisible(true);
    }
    else{
        ui->imgWarning_9->setVisible(false);
    }
        //Manichette
    if(sabbiatrice->warningTempiLavorazione.type.pulizia_manichette == 1
            || sabbiatrice->warningTempiLavorazione.type.sostituzione_manichette == 1){

        ui->imgWarning_10->setVisible(true);
    }
    else{
        ui->imgWarning_10->setVisible(false);
    }

    sabbiatrice->Verifica_TempiLavoro();

}

void frmMainSetup::on_buttonExit_3_clicked()
{
    result = -1;
    close();
}


void frmMainSetup::on_btn1_clicked()
{
    int sec;
    static int page = 0;

    Maintenance* manutenzioni = Maintenance::GetInstance();

    switch(page)
    {
        case 0:
            sec = 54000 - sabbiatrice->tempiLavoro.rotazione_ugello1;
            frmedittimer->Init(81,sabbiatrice->warningTempiLavorazione.type.rotazione_ugello1,sec,54000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.1", 54000 - sec );
                else          manutenzioni->reset( "8.1", sabbiatrice->tempiLavoro.rotazione_ugello1 );
                sabbiatrice->tempiLavoro.rotazione_ugello1 = 0;
                sabbiatrice->warningTempiLavorazione.type.rotazione_ugello1 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1)
            {
                page++;
                on_btn1_clicked();
            }
            else if (frmedittimer->result == 2)
            {
                if(n_pistole == "1"){
                    page = 1;
                }
                else{
                    page = 3;
                }
                on_btn1_clicked();
             }
            else{
                page = 0;
            }

        break;
        case 1:
            sec = 216000 - sabbiatrice->tempiLavoro.sostituzione_ugello1;
            frmedittimer->Init(82,sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello1,sec,216000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.2", 216000 - sec );
                else          manutenzioni->reset( "8.2", sabbiatrice->tempiLavoro.sostituzione_ugello1 );
                sabbiatrice->tempiLavoro.rotazione_ugello1 = 0;
                sabbiatrice->tempiLavoro.sostituzione_ugello1 = 0;
                sabbiatrice->warningTempiLavorazione.type.rotazione_ugello1 = 0;
                sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello1 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                if(n_pistole == "1"){
                    page=0;
                    on_btn1_clicked();
                }
                else{
                    page++;
                    on_btn1_clicked();
                }
            }
            else if (frmedittimer->result == 2){
                page--;
                on_btn1_clicked();
            }
            else{
                page=0;
            }

        break;
        case 2:


            sec = 54000 - sabbiatrice->tempiLavoro.rotazione_ugello2;
            frmedittimer->Init(811,sabbiatrice->warningTempiLavorazione.type.rotazione_ugello2,sec,54000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.1.1", 54000 - sec );
                else          manutenzioni->reset( "8.1.1", sabbiatrice->tempiLavoro.rotazione_ugello2 );
                sabbiatrice->tempiLavoro.rotazione_ugello2 = 0;
                sabbiatrice->warningTempiLavorazione.type.rotazione_ugello2 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                page++;
                on_btn1_clicked();
            }
            else if (frmedittimer->result == 2){
                page--;
                on_btn1_clicked();
            }
            else{
                page = 0;
            }

        break;
        case 3:
            sec = 216000 - sabbiatrice->tempiLavoro.sostituzione_ugello2;
            frmedittimer->Init(821,sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello2,sec,216000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.2.1", 216000 - sec );
                else          manutenzioni->reset( "8.2.1", sabbiatrice->tempiLavoro.sostituzione_ugello2 );
                sabbiatrice->tempiLavoro.rotazione_ugello2 = 0;
                sabbiatrice->tempiLavoro.sostituzione_ugello2 = 0;
                sabbiatrice->warningTempiLavorazione.type.rotazione_ugello2 = 0;
                sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello2 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                page = 0;
                on_btn1_clicked();
            }
            else if (frmedittimer->result == 2){
                page--;
                on_btn1_clicked();
            }
            else{
                page = 0;
            }

        break;
    }


    page = 0;
    Init(1,n_pistole);

    
}

void frmMainSetup::on_btn2_clicked()
{
    int sec;
    static int page = 0;

    Maintenance* manutenzioni = Maintenance::GetInstance();

    switch(page){
        case 0:
            sec = 1800000 - sabbiatrice->tempiLavoro.sostituzione_ugello_iniettore1;
            frmedittimer->Init(83,sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello_iniettore1,sec,1800000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.3", 1800000 - sec );
                else          manutenzioni->reset( "8.3", sabbiatrice->tempiLavoro.sostituzione_ugello_iniettore1 );
                sabbiatrice->tempiLavoro.sostituzione_ugello_iniettore1 = 0;
                sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello_iniettore1 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                if(n_pistole != "1"){
                    page++;
                }
                on_btn2_clicked();
            }
            else if (frmedittimer->result == 2){
                if(n_pistole != "1"){
                    page = 1;
                }
                on_btn2_clicked();
            }
            else{
                page = 0;
            }

        break;
        case 1:
            sec = 1800000 - sabbiatrice->tempiLavoro.sostituzione_ugello_iniettore2;
            frmedittimer->Init(831,sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello_iniettore2,sec,1800000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.3.1", 1800000 - sec );
                else          manutenzioni->reset( "8.3.1", sabbiatrice->tempiLavoro.sostituzione_ugello_iniettore2 );
                sabbiatrice->tempiLavoro.sostituzione_ugello_iniettore2 = 0;
                sabbiatrice->warningTempiLavorazione.type.sostituzione_ugello_iniettore2 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                page = 0;
                on_btn2_clicked();
            }
            else if (frmedittimer->result == 2){
                page--;
                on_btn2_clicked();
            }
            else{
                page = 0;
            }

        break;
    }

    page = 0;
    Init(1,n_pistole);


}

void frmMainSetup::on_btn3_clicked()
{

    int sec;
    static int page = 0;

    Maintenance* manutenzioni = Maintenance::GetInstance();

    switch(page){
        case 0:
            sec = 1440000 - sabbiatrice->tempiLavoro.sostituzione_blocchetto_porta_ugello1;
            frmedittimer->Init(84,sabbiatrice->warningTempiLavorazione.type.sostituzione_blocchetto_porta_ugello1,sec,1440000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.4", 1440000 - sec );
                else          manutenzioni->reset( "8.4", sabbiatrice->tempiLavoro.sostituzione_blocchetto_porta_ugello1 );
                sabbiatrice->tempiLavoro.sostituzione_blocchetto_porta_ugello1 = 0;
                sabbiatrice->warningTempiLavorazione.type.sostituzione_blocchetto_porta_ugello1 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                if(n_pistole != "1"){
                    page++;
                }
                on_btn3_clicked();
            }
            else if (frmedittimer->result == 2){
                if(n_pistole != "1"){
                    page = 1;
                }
                on_btn3_clicked();
            }
            else{
                page = 0;
            }

        break;
        case 1:
            sec = 1440000 - sabbiatrice->tempiLavoro.sostituzione_blocchetto_porta_ugello2;
            frmedittimer->Init(841,sabbiatrice->warningTempiLavorazione.type.sostituzione_blocchetto_porta_ugello2,sec,1440000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.4.1", 1440000 - sec );
                else          manutenzioni->reset( "8.4.1", sabbiatrice->tempiLavoro.sostituzione_blocchetto_porta_ugello2 );
                sabbiatrice->tempiLavoro.sostituzione_blocchetto_porta_ugello2 = 0;
                sabbiatrice->warningTempiLavorazione.type.sostituzione_blocchetto_porta_ugello2 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                page = 0;
                on_btn3_clicked();
            }
            else if (frmedittimer->result == 2){
                page--;
                on_btn3_clicked();
            }
            else{
                page = 0;
            }

        break;
    }

    page = 0;
    Init(1,n_pistole);




}
void frmMainSetup::on_btn4_clicked()
{
    int sec;
    static int page = 0;

    Maintenance* manutenzioni = Maintenance::GetInstance();

    switch(page){
        case 0:
            sec = 1800000 - sabbiatrice->tempiLavoro.sostituzione_corpo_pistola1;
            frmedittimer->Init(85,sabbiatrice->warningTempiLavorazione.type.sostituzione_corpo_pistola1,sec,1800000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.5", 1800000 - sec );
                else          manutenzioni->reset( "8.5", sabbiatrice->tempiLavoro.sostituzione_corpo_pistola1 );
                sabbiatrice->warningTempiLavorazione.type.sostituzione_corpo_pistola1 = 0;
                sabbiatrice->tempiLavoro.sostituzione_corpo_pistola1 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                if(n_pistole != "1"){
                    page++;
                }
                on_btn4_clicked();
            }
            else if (frmedittimer->result == 2){
                if(n_pistole != "1"){
                    page = 1;
                }
                on_btn4_clicked();
            }
            else{
                page = 0;
            }

        break;
        case 1:
            sec = 1800000 - sabbiatrice->tempiLavoro.sostituzione_corpo_pistola2;
            frmedittimer->Init(851,sabbiatrice->warningTempiLavorazione.type.sostituzione_corpo_pistola2,sec,1800000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.5.1", 1800000 - sec );
                else          manutenzioni->reset( "8.5.1", sabbiatrice->tempiLavoro.sostituzione_corpo_pistola2 );
                sabbiatrice->tempiLavoro.sostituzione_corpo_pistola2 = 0;
                sabbiatrice->warningTempiLavorazione.type.sostituzione_corpo_pistola2 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                page = 0;
                on_btn4_clicked();
            }
            else if (frmedittimer->result == 2){
                page--;
                on_btn4_clicked();
            }
            else{
                page = 0;
            }

        break;
    }

    page = 0;
    Init(1,n_pistole);



}



void frmMainSetup::on_btn5_clicked()
{

    int sec;
    static int page = 0;

    Maintenance* manutenzioni = Maintenance::GetInstance();

    switch(page){
        case 0:
            sec = 1350000 - sabbiatrice->tempiLavoro.rotazione_corazza;
            frmedittimer->Init(86,sabbiatrice->warningTempiLavorazione.type.rotazione_corazza,sec,1350000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.10", 1350000 - sec );
                else          manutenzioni->reset( "8.10", sabbiatrice->tempiLavoro.rotazione_corazza );
                sabbiatrice->tempiLavoro.rotazione_corazza = 0;
                sabbiatrice->warningTempiLavorazione.type.rotazione_corazza = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                page++;
                on_btn5_clicked();
            }
            else if (frmedittimer->result == 2){
                page = 1;
                on_btn5_clicked();
            }
            else{
                page = 0;
            }

        break;
        case 1:
            sec = 5400000 - sabbiatrice->tempiLavoro.sostituzione_corazza;
            frmedittimer->Init(110,sabbiatrice->warningTempiLavorazione.type.sostituzione_corazza,sec,5400000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                 if( sec < 0 ) manutenzioni->reset( "8.10.1", 5400000 - sec );
                 else          manutenzioni->reset( "8.10.1", sabbiatrice->tempiLavoro.sostituzione_corazza );
                 sabbiatrice->tempiLavoro.rotazione_corazza    = 0;
                 sabbiatrice->tempiLavoro.sostituzione_corazza = 0;
                 sabbiatrice->warningTempiLavorazione.type.rotazione_corazza = 0;
                 sabbiatrice->warningTempiLavorazione.type.sostituzione_corazza = 0;
                 sabbiatrice->SalvaTempi();
                 sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                page = 0;
                on_btn5_clicked();
            }
            else if (frmedittimer->result == 2){
                page--;
                on_btn5_clicked();
            }
            else{
                page = 0;
            }

        break;
    }

    page = 0;
    Init(1,n_pistole);
}

void frmMainSetup::on_btn6_clicked()
{

    int sec;
    static int page = 0;

    Maintenance* manutenzioni = Maintenance::GetInstance();

    switch(page){
        case 0:
            sec = 1260000 - sabbiatrice->tempiLavoro.sostituzione_tubo_pescaggio1;
            frmedittimer->Init(87,sabbiatrice->warningTempiLavorazione.type.sostituzione_tubo_pescaggio1,sec,1260000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.6", 1260000 - sec );
                else          manutenzioni->reset( "8.6", sabbiatrice->tempiLavoro.sostituzione_tubo_pescaggio1 );
                sabbiatrice->warningTempiLavorazione.type.sostituzione_tubo_pescaggio1 = 0;
                sabbiatrice->tempiLavoro.sostituzione_tubo_pescaggio1 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                if(n_pistole != "1"){
                    page++;
                }
                on_btn6_clicked();
            }
            else if (frmedittimer->result == 2){
                if(n_pistole != "1"){
                    page = 1;
                }
                on_btn6_clicked();
            }
            else{
                page = 0;
            }

        break;
        case 1:
            sec = 1260000 - sabbiatrice->tempiLavoro.sostituzione_tubo_pescaggio2;
            frmedittimer->Init(871,sabbiatrice->warningTempiLavorazione.type.sostituzione_tubo_pescaggio2,sec,1260000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.6.1", 1260000 - sec );
                else          manutenzioni->reset( "8.6.1", sabbiatrice->tempiLavoro.sostituzione_tubo_pescaggio2 );
                sabbiatrice->warningTempiLavorazione.type.sostituzione_tubo_pescaggio2 = 0;
                sabbiatrice->tempiLavoro.sostituzione_tubo_pescaggio2 = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                page = 0;
                on_btn6_clicked();
            }
            else if (frmedittimer->result == 2){
                page--;
                on_btn6_clicked();
            }
            else{
                page = 0;
            }

        break;
    }

    page = 0;
    Init(1,n_pistole);


}

void frmMainSetup::on_btn7_clicked()
{
    Maintenance* manutenzioni = Maintenance::GetInstance();

    int sec = 2520000 - sabbiatrice->tempiLavoro.sostituzione_puleggia_motore_pistola;

    frmedittimer->Init(88,sabbiatrice->warningTempiLavorazione.type.sostituzione_puleggia_motore_pistola,sec,2520000);
    frmedittimer->exec();
    if(frmedittimer->okReset)
    {
        if( sec < 0 ) manutenzioni->reset( "8.7", 2520000 - sec );
        else          manutenzioni->reset( "8.7", sabbiatrice->tempiLavoro.sostituzione_puleggia_motore_pistola );
        sabbiatrice->tempiLavoro.sostituzione_puleggia_motore_pistola = 0;
        sabbiatrice->warningTempiLavorazione.type.sostituzione_puleggia_motore_pistola = 0;
        sabbiatrice->SalvaTempi();
        sabbiatrice->updateiot_maintenance();
    }

    Init(1,n_pistole);



}

void frmMainSetup::on_btn8_clicked()
{

    Maintenance* manutenzioni = Maintenance::GetInstance();

    int sec = 2520000 - sabbiatrice->tempiLavoro.sostituzione_puleggia_motore_nastro;

    frmedittimer->Init(89,sabbiatrice->warningTempiLavorazione.type.sostituzione_puleggia_motore_nastro,sec,2520000);
    frmedittimer->exec();
    if(frmedittimer->okReset)
    {
        if( sec < 0 ) manutenzioni->reset( "8.8", 2520000 - sec );
        else          manutenzioni->reset( "8.8", sabbiatrice->tempiLavoro.sostituzione_puleggia_motore_nastro );
        sabbiatrice->tempiLavoro.sostituzione_puleggia_motore_nastro = 0;
        sabbiatrice->warningTempiLavorazione.type.sostituzione_puleggia_motore_nastro = 0;
        sabbiatrice->SalvaTempi();
        sabbiatrice->updateiot_maintenance();
    }

    Init(1,n_pistole);



}

void frmMainSetup::on_btn9_clicked()
{
    Maintenance* manutenzioni = Maintenance::GetInstance();
    int sec = 1800000 - sabbiatrice->tempiLavoro.sostituzione_cuscinetti;

    frmedittimer->Init(90,sabbiatrice->warningTempiLavorazione.type.sostituzione_cuscinetti,sec,1800000);
    frmedittimer->exec();
    if(frmedittimer->okReset)
    {
        if( sec < 0 ) manutenzioni->reset( "8.9", 1800000 - sec );
        else          manutenzioni->reset( "8.9", sabbiatrice->tempiLavoro.sostituzione_cuscinetti );
        sabbiatrice->tempiLavoro.sostituzione_cuscinetti = 0;
        sabbiatrice->warningTempiLavorazione.type.sostituzione_cuscinetti = 0;
        sabbiatrice->SalvaTempi();
        sabbiatrice->updateiot_maintenance();
    }

    Init(1,n_pistole);
}

void frmMainSetup::on_btn10_clicked()
{
    Maintenance* manutenzioni = Maintenance::GetInstance();
    int sec;
    static int page = 0;

    switch(page){
        case 0:
            sec = 1080000 - sabbiatrice->tempiLavoro.pulizia_manichette;
            frmedittimer->Init(100,sabbiatrice->warningTempiLavorazione.type.pulizia_manichette,sec,1080000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.12", 1080000 - sec );
                else          manutenzioni->reset( "8.12", sabbiatrice->tempiLavoro.pulizia_manichette );
                sabbiatrice->tempiLavoro.pulizia_manichette = 0;
                sabbiatrice->warningTempiLavorazione.type.pulizia_manichette = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                page++;
                on_btn10_clicked();
            }
            else if (frmedittimer->result == 2){
                page = 1;
                on_btn10_clicked();
            }
            else{
                page = 0;
            }

        break;
        case 1:
            sec = 12600000 - sabbiatrice->tempiLavoro.sostituzione_manichette;
            frmedittimer->Init(120,sabbiatrice->warningTempiLavorazione.type.sostituzione_manichette,sec,12600000);
            frmedittimer->exec();
            if(frmedittimer->okReset)
            {
                if( sec < 0 ) manutenzioni->reset( "8.12.1", 12600000 - sec );
                else          manutenzioni->reset( "8.12.1", sabbiatrice->tempiLavoro.sostituzione_manichette );
                sabbiatrice->warningTempiLavorazione.type.sostituzione_manichette = 0;
                sabbiatrice->tempiLavoro.sostituzione_manichette = 0;
                sabbiatrice->SalvaTempi();
                sabbiatrice->updateiot_maintenance();
            }
            if(frmedittimer->result == 1){
                page = 0;
                on_btn10_clicked();
            }
            else if (frmedittimer->result == 2){
                page--;
                on_btn10_clicked();
            }
            else{
                page = 0;
            }

        break;
    }

    page = 0;
    Init(1,n_pistole);

}
