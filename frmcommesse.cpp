#include "frmcommesse.h"
#include "ui_frmcommesse.h"
#include "programmi.h"

frmCommesse::frmCommesse(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmCommesse)
{
    ui->setupUi(this);

    ui->tbProg->setModel(&myModel);
    ui->tbProg->setColumnWidth(0,190);
    ui->tbProg->setColumnWidth(1,200);
    ui->tbProg->setColumnWidth(2,70);
    ui->tbProg->setColumnWidth(3,70);
    ui->tbProg->setColumnWidth(4,100);
    ui->tbProg->setColumnWidth(5,100);
}

frmCommesse::~frmCommesse()
{
    delete ui;
}
void frmCommesse::Init()
{

    for( int r=0;r < ROWS_C;r++ )
    {
        for( int c=0;c < COLS_C;c++ )
        {
            myModel.m_gridData[r][c] =  "";
        }
    }

    Commesse *comms = Commesse::getInstance();
    comms->open();


    int n_prog=0;
    int idx = 0;
    int last_idx=0;
    QString last_nome;

    int oo;
    int mm;
    int ss;

    for( s_Commessa comm : comms->commesse.values() )
    {
        myModel.m_gridData[n_prog][0] =  comm.nome;
        myModel.m_gridData[n_prog][1] =  comm.programma;
        myModel.m_gridData[n_prog][2] =  QString( "%1").arg(comm.qta_da_fare);
        myModel.m_gridData[n_prog][3] =  QString( "%1").arg(comm.qta_fatti);

        oo = comm.tempo_singolo / 3600;
        mm = (comm.tempo_singolo - ( oo * 3600)) / 60;
        ss =  comm.tempo_singolo - ( oo * 3600) - (mm * 60);
        myModel.m_gridData[n_prog][4] =  QString( "%1:%2:%3").arg(oo,2,10,QChar('0')).arg(mm,2,10,QChar('0')).arg(ss,2,10,QChar('0'));

        oo = comm.tempo_totale / 3600;
        mm = (comm.tempo_totale - ( oo * 3600)) / 60;
        ss =  comm.tempo_totale - ( oo * 3600) - (mm * 60);
        myModel.m_gridData[n_prog][5] =  QString( "%1:%2:%3").arg(oo,2,10,QChar('0')).arg(mm,2,10,QChar('0')).arg(ss,2,10,QChar('0'));

        if(comm.nome == last_nome){
            idx = n_prog;
        }
        n_prog++;
    }

    if( myModel.rowCount() > 0 )
    {
        ui->tbProg->selectRow(last_idx);
        ui->lbProgrammaSel->setText(myModel.m_gridData[last_idx][0]);

        SelectedComm = myModel.m_gridData[last_idx][0];
        SelectedProg = myModel.m_gridData[last_idx][1];
    }
    else
    {
        ui->tbProg->selectRow(-1);
        ui->lbProgrammaSel->setText("");
    }


}
void frmCommesse::Message(QString msg)
{
        ui->lbProgrammaSel->setText(msg);
}

void frmCommesse::on_buttonExit_clicked()
{
    //SelectedProg =
    result = 1;
    close();
}

void frmCommesse::on_buttonExit_3_clicked()
{
    result = 0;
    close();
}

void frmCommesse::on_tbProg_clicked(const QModelIndex &index)
{
    if(myModel.m_gridData[index.row()][0] != "")
    {
        SelectedComm = myModel.m_gridData[index.row()][0];
        SelectedProg = myModel.m_gridData[index.row()][1];

        ui->lbProgrammaSel->setText(myModel.m_gridData[index.row()][0]);
    }
}

void frmCommesse::on_tbProg_entered(const QModelIndex &index)
{
    if(myModel.m_gridData[index.row()][0] != "")
    {
        SelectedComm = myModel.m_gridData[index.row()][0];
        SelectedProg = myModel.m_gridData[index.row()][1];

        ui->lbProgrammaSel->setText(myModel.m_gridData[index.row()][0]);
    }
}
