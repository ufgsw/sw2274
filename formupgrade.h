#ifndef FORMUPGRADE_H
#define FORMUPGRADE_H

#include <QDialog>
#include "downloadmanager.h"
//#include "application.h"
#include "formtastieraa.h"

namespace Ui {
class FormUpgrade;
}

class FormUpgrade : public QDialog
{
    Q_OBJECT

public:
    explicit FormUpgrade(QWidget *parent = 0);
    ~FormUpgrade();

private:
    Ui::FormUpgrade *ui;

    FormTastieraA* uiTastieraA;

    DownloadManager *manager;

    bool download_release;
    bool download_firmware;
    bool download_error;

    QString Md5_gen(QString const &s);

    QString cloud_path;
    QString cloud_file;

    QString application;
    QString md5sum;
    QString release;
    QString my_release;
    QString note;

    void save_settings();

private slots:
    void on_finish_download();
    void on_progress(int p, QString brate);
    void on_message(QString msg);

    void on_buttonVerifyDownload_clicked();
    void on_buttonExit_clicked();
    void on_buttonCloudPath_clicked();

    // QWidget interface
    void on_buttonOk_clicked();

    void on_buttonNo_clicked();

    void on_buttonExit_3_clicked();

protected:
    void showEvent(QShowEvent *event);
};

#endif // FORMUPGRADE_H
