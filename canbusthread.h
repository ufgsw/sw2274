#ifndef CANBUSTHREAD_H
#define CANBUSTHREAD_H

#include <linux/version.h>
#include <linux/types.h>
#include <linux/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#include <net/if.h>
#include <sys/ioctl.h>

#include <unistd.h>
#include <stdint.h>

#include <def.h>

#include <QThread>
#include <QObject>

#define RX_ID1   0x180  	// Ricevo dall'azionamento gli ingressi
#define RX_ID2   0x280    	// Ricevo dall'azionamento info varie
#define RX_ID3   0x181
#define RX_ID4   0x182
#define RX_ID5   0x183
#define RX_ID6   0x184
#define RX_ID7   0x185
#define RX_ID8   0x186
#define RX_ID9   0x187
#define RX_ID10  0x188

#define TX_ID1   0x100
#define TX_ID2   0x200
#define TX_ID3   0x202
#define TX_ID4   0x203
#define TX_ID5   0x204




class CanBusThread : public QThread
{
    Q_OBJECT

public:
    /*
    CanBusThread(Sabbiatrice *parent = 0)
    {
        sab = parent;
    }
    */

//    ushort  Output;
//    ushort  Input;
//    uchar   Pressione_set;
//    uchar   Comandi_mot;
//    int     Quota_mot1;
//    int     Quota_mot2;
//    int     Quota_mot1_set;
//    int     Quota_mot2_set;
//    short   Velo_mot1_set;
//    short   Velo_mot2_set;
//    ushort  Status_mot1;
//    ushort  Status_mot2;
//    short   Risposta_azionamento;
//    short	Release_azionamento;
//    ushort  Timer_rx_azionamento;
//    ushort  TipoMacchina;

    void init();
    void write_frame(can_frame tx);

public slots:


protected:
    void run() override;

private:
    QString nome;
    struct sockaddr_can addr;
    struct can_frame frame, rframe;
    struct ifreq ifr;
    struct can_filter rfilter[3];



    int socket_d;

    bool mInit;
    bool mRun;
    //void analizzaCanBus(int id, unsigned char len, unsigned char b0,unsigned char b1,unsigned char b2,unsigned char b3,unsigned char b4,unsigned char b5,unsigned char b6,unsigned char b7);

signals:
    void frame_receive( can_frame rx );
    void data_receive( int id, unsigned char len, unsigned char b0,unsigned char b1,unsigned char b2,unsigned char b3,unsigned char b4,unsigned char b5,unsigned char b6,unsigned char b7);

};

#endif // CANBUSTHREAD_H
