#ifndef FRMIMPORT_H
#define FRMIMPORT_H

#include <QDialog>

namespace Ui {
class frmImport;
}

class frmImport : public QDialog
{
    Q_OBJECT

public:
    explicit frmImport(QWidget *parent = 0);
    ~frmImport();

private slots:
    void on_buttonExit_3_clicked();

    void on_btnImage_2_clicked();

private:
    QString device;

    Ui::frmImport *ui;
};

#endif // FRMIMPORT_H
