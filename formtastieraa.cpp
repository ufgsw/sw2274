#include "formtastieraa.h"
#include "ui_formtastieraa.h"

FormTastieraA::FormTastieraA(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormTastieraA)
{
    ui->setupUi(this);
    tastiera_attuale = type_maiuscolo;
    maxlen = 30;
}

FormTastieraA::~FormTastieraA()
{
    delete ui;
}
void FormTastieraA::set_text(QString str)
{
   ui->label->setText(str);
}
void FormTastieraA::set_title(QString title)
{
    //ui->labelTitle->setText( title );
}

QString FormTastieraA::get_text()
{
    return ui->label->text();
}

void FormTastieraA::update_keys()
{
    if( tastiera_attuale == type_maiuscolo )
    {
        ui->buttonQ->setText( "Q" );
        ui->buttonW->setText( "W" );
        ui->buttonE->setText( "E" );
        ui->buttonR->setText( "R" );
        ui->buttonT->setText( "T" );
        ui->buttonY->setText( "Y" );
        ui->buttonU->setText( "U" );
        ui->buttonI->setText( "I" );
        ui->buttonO->setText( "O" );
        ui->buttonP->setText( "P" );

        ui->buttonA->setText( "A" );
        ui->buttonS->setText( "S" );
        ui->buttonD->setText( "D" );
        ui->buttonF->setText( "F" );
        ui->buttonG->setText( "G" );
        ui->buttonH->setText( "H" );
        ui->buttonJ->setText( "J" );
        ui->buttonK->setText( "K" );
        ui->buttonL->setText( "L" );

        ui->buttonZ->setText( "Z" );
        ui->buttonX->setText( "X" );
        ui->buttonC->setText( "C" );
        ui->buttonV->setText( "V" );
        ui->buttonB->setText( "B" );
        ui->buttonN->setText( "N" );
        ui->buttonM->setText( "M" );
    }
    else if( tastiera_attuale == type_minuscolo )
    {
        ui->buttonQ->setText( "q" );
        ui->buttonW->setText( "w" );
        ui->buttonE->setText( "e" );
        ui->buttonR->setText( "r" );
        ui->buttonT->setText( "t" );
        ui->buttonY->setText( "y" );
        ui->buttonU->setText( "u" );
        ui->buttonI->setText( "i" );
        ui->buttonO->setText( "o" );
        ui->buttonP->setText( "p" );

        ui->buttonA->setText( "a" );
        ui->buttonS->setText( "s" );
        ui->buttonD->setText( "d" );
        ui->buttonF->setText( "f" );
        ui->buttonG->setText( "g" );
        ui->buttonH->setText( "h" );
        ui->buttonJ->setText( "j" );
        ui->buttonK->setText( "k" );
        ui->buttonL->setText( "l" );

        ui->buttonZ->setText( "z" );
        ui->buttonX->setText( "x" );
        ui->buttonC->setText( "c" );
        ui->buttonV->setText( "v" );
        ui->buttonB->setText( "b" );
        ui->buttonN->setText( "n" );
        ui->buttonM->setText( "m" );
    }
    else if( tastiera_attuale == type_symbol1 )
    {
        ui->buttonQ->setText( "1" );
        ui->buttonW->setText( "2" );
        ui->buttonE->setText( "3" );
        ui->buttonR->setText( "4" );
        ui->buttonT->setText( "5" );
        ui->buttonY->setText( "6" );
        ui->buttonU->setText( "7" );
        ui->buttonI->setText( "8" );
        ui->buttonO->setText( "9" );
        ui->buttonP->setText( "0" );

        ui->buttonA->setText( "-" );
        ui->buttonS->setText( "/" );
        ui->buttonD->setText( ":" );
        ui->buttonF->setText( ";" );
        ui->buttonG->setText( "(" );
        ui->buttonH->setText( ")" );
        ui->buttonJ->setText( "?" );
        ui->buttonK->setText( "*" );
        ui->buttonL->setText( "@" );

        ui->buttonZ->setText( "." );
        ui->buttonX->setText( "," );
        ui->buttonC->setText( "*" );
        ui->buttonV->setText( "è" );
        ui->buttonB->setText( "ò" );
        ui->buttonN->setText( "à" );
        ui->buttonM->setText( "ù" );
    }
}

void FormTastieraA::on_buttonFN_clicked()
{
    if(      tastiera_attuale == type_maiuscolo ) tastiera_attuale = type_minuscolo;
    else if( tastiera_attuale == type_minuscolo ) tastiera_attuale = type_maiuscolo;
    else                                          tastiera_attuale = tastiera_old;

    tastiera_old = tastiera_attuale;

    this->update_keys();

}

void FormTastieraA::on_button123_clicked()
{
    if(tastiera_attuale != type_symbol1){
        tastiera_attuale = type_symbol1;
        tastiera_old = tastiera_attuale;
        ui->button123->setText("ABC");
    }
    else{
        tastiera_attuale = type_maiuscolo;
        ui->button123->setText("123");
    }

    tastiera_old = tastiera_attuale;

    this->update_keys();

}
bool FormTastieraA::is_valid()
{
   return valid;
}

void FormTastieraA::on_buttonOK_clicked()
{
    valid  = true;
    close();
}

void FormTastieraA::on_buttonAnnulla_clicked()
{
    valid  = false;
    ui->label->setText("");
    close();
}

void FormTastieraA::on_buttonQ_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonQ->text() );
}

void FormTastieraA::on_buttonW_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonW->text() );
}

void FormTastieraA::on_buttonE_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonE->text() );
}

void FormTastieraA::on_buttonR_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonR->text() );
}

void FormTastieraA::on_buttonT_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonT->text() );
}

void FormTastieraA::on_buttonY_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonY->text() );
}

void FormTastieraA::on_buttonU_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonU->text() );
}

void FormTastieraA::on_buttonI_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonI->text() );
}

void FormTastieraA::on_buttonO_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonO->text() );
}

void FormTastieraA::on_buttonP_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonP->text() );
}

void FormTastieraA::on_buttonA_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonA->text() );
}

void FormTastieraA::on_buttonS_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonS->text() );
}

void FormTastieraA::on_buttonD_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonD->text() );
}

void FormTastieraA::on_buttonF_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonF->text() );
}

void FormTastieraA::on_buttonG_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonG->text() );
}

void FormTastieraA::on_buttonH_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonH->text() );
}

void FormTastieraA::on_buttonJ_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonJ->text() );
}

void FormTastieraA::on_buttonK_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonK->text() );
}

void FormTastieraA::on_buttonL_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonL->text() );
}

void FormTastieraA::on_buttonZ_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonZ->text() );
}

void FormTastieraA::on_buttonX_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonX->text() );
}

void FormTastieraA::on_buttonC_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonC->text() );
}

void FormTastieraA::on_buttonV_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonV->text() );
}

void FormTastieraA::on_buttonB_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonB->text() );
}

void FormTastieraA::on_buttonN_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonN->text() );
}

void FormTastieraA::on_buttonM_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + ui->buttonM->text() );
}

void FormTastieraA::on_buttonSpace_clicked()
{
    if( ui->label->text().length() < maxlen ) ui->label->setText( ui->label->text() + " "  );
}

void FormTastieraA::on_buttonCAN_clicked()
{
    QString next = ui->label->text();

    if( next.size() > 1 )
        next.remove( next.size() -1, 1 );
    else
        next = "";

    ui->label->setText( next );
}
