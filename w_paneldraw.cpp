#include "w_paneldraw.h"

ScribbleArea::ScribbleArea(QWidget *parent)
    : QWidget(parent)
{
    setAttribute(Qt:: WA_StaticContents);
    AreaSelected = -1;
}

bool ScribbleArea::openImage(const QString &fileName)
{
    QImage loadedImage;
    if (!loadedImage.load(fileName))
        return false;

    QSize newSize = loadedImage.size().expandedTo(size());
    resizeImage(&loadedImage, newSize);
    image = loadedImage;
    modified = false;
    update();
    return true;
}
bool ScribbleArea::saveImage(const QString &fileName, const char *fileFormat)
{
    QImage visibleImage = image;
    resizeImage(&visibleImage, size());

    if (visibleImage.save(fileName, fileFormat)) {
        modified = false;
        return true;
    }
    return false;
}
void ScribbleArea::setPenColor(const QColor &newColor)
{
    myPenColor = newColor;
}

void ScribbleArea::setPenWidth(int newWidth)
{
    myPenWidth = newWidth;
}
void ScribbleArea::clearImage()
{
    image.fill(qRgb(0, 255, 255));
    modified = true;
    update();
}
void ScribbleArea::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        if(AreaSelected >= 0){
            MoveArea(AreaSelected,QPoint(0,0));
        }
        AreaSelected = FindAreaToClick(event->pos());
        if(AreaSelected >= 0){
            qDebug(QString( "Area %1").arg( AreaSelected ).toLatin1());
            SelectArea(AreaSelected);
            lastPoint = event->pos();
            startPoint = event->pos();
                // attivare per movimenti con il mouse
            //scribbling = true;

        }
    }
}

void ScribbleArea::mouseMoveEvent(QMouseEvent *event)
{
    if ((event->buttons() & Qt::LeftButton) && scribbling && AreaSelected >= 0){
        int16_t delta_x,delta_y;
        int16_t step=10;
        delta_x = event->pos().x() - startPoint.x();
        delta_y = event->pos().y() - startPoint.y();
        if(delta_x > step || delta_y > step || delta_x < -step || delta_y < -step){
            int16_t delta_x_arr,delta_y_arr;
            delta_x_arr = (int16_t)(delta_x / 5) * -5;
            delta_y_arr = (int16_t)(delta_y / 5) * -5;
            offsetPoint.setX(delta_x_arr);
            offsetPoint.setY(delta_y_arr);
            MoveArea(AreaSelected,offsetPoint);
            startPoint = event->pos();
        }
    }
}

void ScribbleArea::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && scribbling) {
        //drawLineTo(event->pos());
        scribbling = false;
    }
    if(AreaSelected >= 0){
        SelectArea(AreaSelected);
    }

}
void ScribbleArea::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, image, dirtyRect);
}
void ScribbleArea::resizeEvent(QResizeEvent *event)
{
    if (width() > image.width() || height() > image.height()) {
        int newWidth = qMax(width() + 128, image.width());
        int newHeight = qMax(height() + 128, image.height());
        resizeImage(&image, QSize(newWidth, newHeight));
        update();
    }
    QWidget::resizeEvent(event);
}
void ScribbleArea::drawLineTo(const QPoint &endPoint)
{
    QPainter painter(&image);
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                        Qt::RoundJoin));
    painter.drawLine(lastPoint, endPoint);
    modified = true;

    int rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
    lastPoint = endPoint;
}
void ScribbleArea::resizeImage(QImage *image, const QSize &newSize)
{
    if (image->size() == newSize)
        return;

    QImage newImage(newSize, QImage::Format_RGB32);
    newImage.fill(qRgb(0, 255, 255));
    QPainter painter(&newImage);
    painter.drawImage(QPoint(0, 0), *image);
    *image = newImage;
    DrawVetro();

}

void ScribbleArea::AddArea(s_Area  *area,u_int16_t x1,u_int16_t y1,u_int16_t w,u_int16_t h )
{

    ushort xp,yp,wp,hp;



    xp = (u_int16_t)((float)x1 * Prog.pixel_per_mm);
    yp = (u_int16_t)((float)y1 * Prog.pixel_per_mm);
    wp = (u_int16_t)((float)w  * Prog.pixel_per_mm);
    hp = (u_int16_t)((float)h  * Prog.pixel_per_mm);



    if(Prog.naree >= MAX_AREE -1){
        return;
    }
    int n_area = Prog.naree;

    area->x_iniziale = x1;
    area->y_iniziale = y1;
    area->x_finale   = x1 + w;
    area->y_finale   = y1 + h;
    area->width      = w;
    area->height     = h;

    area->p_pixel.setX(xp);
    area->p_pixel.setY(yp);
    area->dim_pixel.setX(wp);
    area->dim_pixel.setY(hp);
    area->stato      = ATTIVA;





    DrawAreaVetro(n_area,xp,yp,wp,hp,false);
    /*
    background = QBrush(QColor(255, 0, 0));
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                                Qt::RoundJoin));
    painter.fillRect(xp,yp,wp,hp,background);
    painter.setFont(QFont("Ubuntu", 24));
    painter.drawText(QRect(xp,yp,wp,hp), Qt::AlignCenter,QString("%1").arg(n_area+1));
    int rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
    lastPoint = endPoint;
    */

    modified = true;



    NormalizzaArea(n_area);



/*
    qDebug(QString( "Area %1").arg( NArea ).toLatin1());
    qDebug((QString( "Area Xi : %1").arg(Prog.Area[NArea].x_iniziale)).toLatin1());
    qDebug((QString( "Area Yi : %1").arg(Prog.Area[NArea].y_iniziale)).toLatin1());
    qDebug((QString( "Area Xf : %1").arg(Prog.Area[NArea].x_finale)).toLatin1());
    qDebug((QString( "Area Yf : %1").arg(Prog.Area[NArea].y_finale)).toLatin1());
*/
    DrawVetro();

}
//--------------------------------------------------------------------

void ScribbleArea::DrawVetro()
{
    QPainter painter(&image);
    QPoint endPoint;
    QRect  rect;
    short xm,ym;
    short wp,hp;
    short xi,yi;
    short xf,yf;
    QBrush background;


    hp = Prog.altezza_vetro * Prog.pixel_per_mm;
    wp = Prog.larghezza_vetro * Prog.pixel_per_mm;


    xm = this->width() - 0 - wp;
    ym = this->height() - 0 - hp;





    lastPoint.setX(xm);
    lastPoint.setY(ym);
    endPoint.setX(xm+wp);
    endPoint.setY(ym+hp);

    /*
    setPenWidth(3);
    setPenColor(QColor(255, 0, 0));
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,Qt::RoundJoin));
    //rect.setRect(xm+2,ym+2,wp-5,hp-5);
    rect.setRect(xm,ym,wp,hp);



    //painter.fillRect(rect,QColor(255, 255, 255));
    painter.drawRect(rect);
    */

    background = QBrush(QColor(0, 0, 255));
    //painter.fillRect(0,0,xm,hp,background);

    for(int i=0;i < MAX_AREE;i++){
        if(Prog.Area[i].stato == ATTIVA){
            xi = Prog.Area[i].p_pixel.x();
            yi = Prog.Area[i].p_pixel.y();
            xf = Prog.Area[i].p_pixel.x() + Prog.Area[i].dim_pixel.x();
            yf = Prog.Area[i].p_pixel.y() + Prog.Area[i ].dim_pixel.y();

            DrawAreaVetro(i,xi,yi,xf-xi,yf-yi,false);
       }

    }

    xi = Prog.Area[AreaSelected].p_pixel.x();
    yi = Prog.Area[AreaSelected].p_pixel.y();
    xf = Prog.Area[AreaSelected].p_pixel.x() + Prog.Area[AreaSelected].dim_pixel.x();
    yf = Prog.Area[AreaSelected].p_pixel.y() + Prog.Area[AreaSelected].dim_pixel.y();

    DrawAreaVetro(AreaSelected,xi,yi,xf-xi,yf-yi,false);

    rect.setRect(0,0,this->width(),ym);
    painter.fillRect(rect,QColor(1, 80, 135));
    rect.setRect(0,0,xm,this->height());
    painter.fillRect(rect,QColor(1, 80, 135));

    int rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
    lastPoint = endPoint;

    setPenColor(QColor(0, 0, 255));

}

//--------------------------------------------------------------------

void ScribbleArea::DrawAreaVetro(short n_area,ushort xp,ushort yp,ushort wp,ushort hp,bool clear)
{
    QPainter painter(&image);
    QBrush background;
    QPoint endPoint;
    short xm,ym;
    short xd,yd;

/*
    xp /= 10;
    yp /= 10;
    wp /= 10;
    hp /= 10;
*/


    if(wp < 10)
        wp = 10;
    if(hp < 10)
        hp = 10;


    yd = this->height();
    xd = this->width();

    xm = xd - xp - wp;
    ym = yd - hp - yp;



    if(ym < 0){
        ym = 0;
    }
    if(xm < 0){
        xm = 0;
    }


    if(clear){
            background = QBrush(QColor(0, 255, 255));
    }
    else{
        if(Prog.Area[n_area].pis1 > 0 || Prog.Area[n_area].pis2 > 0 ){
            background = QBrush(QColor(0, 255, 0));
        }
        else{
            background = QBrush(QColor(255, 0, 0));
        }
    }


    lastPoint.setX(xm);
    lastPoint.setY(ym);
    endPoint.setX(xm+wp);
    endPoint.setY(ym+hp);



    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,
                                Qt::RoundJoin));
    painter.fillRect(xm,ym,wp,hp,background);
    painter.setFont(QFont("Ubuntu", 24));
    if(clear == false){
        painter.drawText(QRect(xm,ym,wp,hp), Qt::AlignCenter,QString("%1").arg(n_area+1));
    }
    //QRect rect;
    //rect.setRect(xm,ym,wp,hp);
    //setPenWidth(3);
    //painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,Qt::RoundJoin));
    //painter.drawRect(rect);


    int rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
    lastPoint = endPoint;

    //DrawVetro();
}
//--------------------------------------------------------------------

void ScribbleArea::DeleteArea()
{
    CleanArea(AreaSelected);
    int lastarea=0;
    if(AreaSelected >= 0){
        for(int i=AreaSelected;i < MAX_AREE - 1;i++){
            Prog.Area[i] = Prog.Area[i+1];
            lastarea = i+1;
        }
    }
    if(Prog.naree > 0) Prog.naree--;

    for(int i=lastarea;i < MAX_AREE - 1;i++){
        Prog.Area[i].stato = 0;
    }
    DrawVetro();

}
//--------------------------------------------------------------------
int16_t ScribbleArea::FindAreaToClick(const QPoint &point)
{

    int16_t area = -1;

    short xp,yp,wp,hp;
    short xm,ym;
    short xc,yc;
    short offset_w = 0;
    short offset_h = 0;



    xc = point.x();
    yc = point.y();

    for(int i=0;i < MAX_AREE;i++){

        xp = Prog.Area[i].p_pixel.x();
        yp = Prog.Area[i].p_pixel.y();
        wp = Prog.Area[i].dim_pixel.x();
        hp = Prog.Area[i].dim_pixel.y();

        if(wp < 10)
            offset_w = 5;
        if(hp < 10)
            offset_h = 5;


        xm = this->width() - xp - wp - offset_w;
        ym = this->height() - hp - yp - offset_h;

        if(area == -1 && Prog.Area[i].stato == ATTIVA &&  xc > xm && point.x() < (xm + wp + offset_w)
                && yc > ym && point.y() < (ym + hp + offset_h)){
            area = i;
            break;
        }

    }

    return area;
}
//--------------------------------------------------------------------
void ScribbleArea::NormalizzaArea(int area)
{

    Prog.Area[area].p_pixel.setX(Prog.Area[area].x_iniziale * Prog.pixel_per_mm);
    Prog.Area[area].p_pixel.setY(Prog.Area[area].y_iniziale * Prog.pixel_per_mm);
    Prog.Area[area].width              = Prog.Area[area].x_finale - Prog.Area[area].x_iniziale;
    Prog.Area[area].height             = Prog.Area[area].y_finale - Prog.Area[area].y_iniziale;
    Prog.Area[area].dim_pixel.setX(Prog.Area[area].width  * Prog.pixel_per_mm);
    Prog.Area[area].dim_pixel.setY(Prog.Area[area].height * Prog.pixel_per_mm);

}

//--------------------------------------------------------------------
void ScribbleArea::SelectArea(ushort area)
{
    if(area < MAX_AREE)
    {
        QPainter painter(&image);
        QRect    rect;

        short xp,yp,wp,hp;

        short xm,ym;


        xp = Prog.Area[area].p_pixel.x();
        yp = Prog.Area[area].p_pixel.y();
        wp = Prog.Area[area].dim_pixel.x();
        hp = Prog.Area[area].dim_pixel.y();

        xm = this->width() - xp - wp;
        ym = this->height() - hp - yp;

        if(ym < 0) ym = 0;
        if(xm < 0) xm = 0;

        rect.setRect(xm+3,ym+3,wp-6,hp-6);
        setPenColor(QColor(255, 255, 0));
        setPenWidth(6);
        painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,Qt::RoundJoin));
        painter.drawRect(rect);
        modified = true;

        painter.setFont(QFont("Ubuntu", 24));
        painter.drawText(QRect(xm,ym,wp,hp), Qt::AlignCenter,QString("%1").arg(area+1));


        int rad = (myPenWidth / 2) + 2;
        update(rect.normalized().adjusted(-rad, -rad, +rad, +rad));
        setPenWidth(1);
        setPenColor(QColor(0, 0, 255));

        AreaSelected = area;


    }

}
//--------------------------------------------------------------------
void ScribbleArea::MoveArea(ushort area,const QPoint &offsetPoint)
{
    QString str;

    short xi,yi,xf,yf;
    short offset_x,offset_y;

    xi = Prog.Area[area].p_pixel.x();
    yi = Prog.Area[area].p_pixel.y();
    xf = Prog.Area[area].p_pixel.x() + Prog.Area[area].dim_pixel.x();
    yf = Prog.Area[area].p_pixel.y() + Prog.Area[area].dim_pixel.y();

    offset_x = (int16_t) offsetPoint.x();
    offset_y = (int16_t) offsetPoint.y();

    DrawAreaVetro(area,xi,yi,xf-xi,yf-yi,true);
/*
    QPoint endPoint;
    QPainter painter(&image);
    QBrush background;

    lastPoint.setX(xi);
    lastPoint.setY(yi);
    endPoint.setX(xf);
    endPoint.setY(yf);

    background = QBrush(QColor(255, 255, 255));
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,Qt::RoundJoin));
    painter.fillRect(xi,yi,xf-xi,yf-yi,background);
    painter.setFont(QFont("Ubuntu", 24));
    painter.drawText(QRect(xi,yi,xf-xi,yf-yi), Qt::AlignCenter,QString("%1").arg(area+1));

    int16_t rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
*/

    modified = true;

    xi = xi + offset_x;
    yi = yi + offset_y;
    xf = xf + offset_x;
    yf = yf + offset_y;


    if(xi < 0){
        xi = 0;
        xf = Prog.Area[area].dim_pixel.x();
    }
    else if(xf > this->width()){
        xf = this->width();
        xi = xf - Prog.Area[area].dim_pixel.x();
    }
    if(yi < 0){
        yi = 0;
        yf = Prog.Area[area].dim_pixel.y();
    }
    else if(yf > this->height()){
        yf = this->height();
        yi = yf - Prog.Area[area].dim_pixel.y();
    }

    Prog.Area[area].p_pixel.setX(xi);
    Prog.Area[area].p_pixel.setY(yi);

    if(offset_x != 0){
        Prog.Area[area].x_iniziale = ((xi / Prog.pixel_per_mm) / 10) * 10;
        Prog.Area[area].x_finale = ((xf / Prog.pixel_per_mm) / 10) * 10;
    }
    if(offset_y != 0){
        Prog.Area[area].y_iniziale = ((yi / Prog.pixel_per_mm) / 10) * 10;
        Prog.Area[area].y_finale = ((yf / Prog.pixel_per_mm) / 10) * 10;
    }
    for(int i=0;i < MAX_AREE;i++){
        if(Prog.Area[i].stato == ATTIVA){
            xi = Prog.Area[i].p_pixel.x();
            yi = Prog.Area[i].p_pixel.y();
            xf = Prog.Area[i].p_pixel.x() + Prog.Area[i].dim_pixel.x();
            yf = Prog.Area[i].p_pixel.y() + Prog.Area[i ].dim_pixel.y();

            DrawAreaVetro(i,xi,yi,xf-xi,yf-yi,false);
            /*
            background = QBrush(QColor(255, 0, 0));
            painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,Qt::RoundJoin));
            painter.fillRect(xi,yi,xf-xi,yf-yi,background);
            painter.setFont(QFont("Ubuntu", 24));
            painter.drawText(QRect(xi,yi,xf-xi,yf-yi), Qt::AlignCenter,QString("%1").arg(i+1));
            lastPoint.setX(xi);
            lastPoint.setY(yi);
            endPoint.setX(xf);
            endPoint.setY(yf);
            update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
            */
       }

    }
    modified = true;
    /*

    lastPoint.setX(xi);
    lastPoint.setY(yi);
    endPoint.setX(xf);
    endPoint.setY(yf);

    update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
*/

    DrawVetro();


}
//--------------------------------------------------------------------
void ScribbleArea::CleanArea(ushort area)
{
    QString str;

    if( area >= MAX_AREE)
        return;

    int16_t xi,yi,xf,yf;

    int wp,hp;

    wp = Prog.Area[area].dim_pixel.x();
    hp = Prog.Area[area].dim_pixel.y();

    if(wp < 10)
        wp = 10;
    if(hp < 10)
        hp = 10;

    xi = Prog.Area[area].p_pixel.x();
    yi = Prog.Area[area].p_pixel.y();
    xf = Prog.Area[area].p_pixel.x() + wp;
    yf = Prog.Area[area].p_pixel.y() + hp;



    DrawAreaVetro(area,xi,yi,xf-xi,yf-yi,true);

/*
    QPoint endPoint;
    QPainter painter(&image);
    QBrush background;
    lastPoint.setX(xi);
    lastPoint.setY(yi);
    endPoint.setX(xf);
    endPoint.setY(yf);

    background = QBrush(QColor(255, 255, 255));
    painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap,Qt::RoundJoin));
    painter.fillRect(xi,yi,xf-xi,yf-yi,background);
    int16_t rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
*/
    modified = true;

    DrawVetro();

}
//--------------------------------------------------------------------
void ScribbleArea::CleanAll()
{
    QPainter painter(&image);
    QBrush background;
    QPoint endPoint;
    short xm,ym;

    xm = this->width();
    ym = this->height();


    background = QBrush(QColor(0, 255, 255));

    lastPoint.setX(0);
    lastPoint.setY(0);
    endPoint.setX(xm);
    endPoint.setY(ym);

    painter.fillRect(0,0,xm,ym,background);

    int rad = (myPenWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
    lastPoint = endPoint;
}


