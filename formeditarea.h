#ifndef FORMEDITAREA_H
#define FORMEDITAREA_H

#include <QDialog>
#include "programmi.h"
#include "formtastietan.h"

namespace Ui {
class formEditArea;
}

class formEditArea : public QDialog
{
    Q_OBJECT

public:
    s_Area  *pArea;
    short Tipo;
    void Init(s_Area  *Area,float pixel_coef,int num,int maxh,QString n_pistole,QString um);
    explicit formEditArea(QWidget *parent = 0);
    ~formEditArea();

private slots:
    void on_buttonExit_clicked();


    void on_btnXIni_clicked();

    void on_btnXFin_clicked();

    void on_btnYFin_clicked();

    void on_btnYIni_clicked();

    void on_btnBarIni_clicked();

    void on_btnBarFin_clicked();

    void on_btnRipetizione_clicked();

    void on_btnVelPiostola_clicked();

    void on_btnPistola1_clicked();

    void on_btnPistola2_clicked();

    void on_btnPistola3_clicked();

    void on_btnVelPistola_clicked();

    void on_btnStepNastro_clicked();

private:
    Ui::formEditArea *ui;
    FormTastieraN*   uiTastieraN;
    int                 max_height;
    QString         UM;
    float           fattore_unita;
    float           fattore_unita_p;

    float       pixel_per_mm;
    void Normalizza();              // partendo dalle coordinate in mm normalizza le altre dimensioni della struttura
    void AggiornaValori();
    float GetNumber(QString title,float old,float min,float max,bool asinteger);
};

#endif // FORMEDITAREA_H
