#ifndef FRMLOGMANUTENZIONE_H
#define FRMLOGMANUTENZIONE_H

#include <QDialog>
#include "maintenance.h"
#include "model_maintenance.h"
#include "programmi.h"

namespace Ui {
class frmLogManutenzione;
}

class frmLogManutenzione : public QDialog
{
    Q_OBJECT

public:
    explicit frmLogManutenzione(QWidget *parent = 0);
    ~frmLogManutenzione();

private slots:
    void on_buttonExit_3_clicked();

    void on_btnExport_clicked();

private:
    Ui::frmLogManutenzione *ui;

    Parametri   param;
    model_maintenance myModel;

    QString SecToOraString(int secondi);
};

#endif // FRMLOGMANUTENZIONE_H
