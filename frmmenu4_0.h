#ifndef FRMMENU4_0_H
#define FRMMENU4_0_H

#include <QDialog>

namespace Ui {
class frmMenu4_0;
}

class frmMenu4_0 : public QDialog
{
    Q_OBJECT

public:
    explicit frmMenu4_0(QWidget *parent = 0);
    ~frmMenu4_0();

private slots:
    void on_btnSetup40_clicked();

    void on_buttonExit_clicked();

    void on_btnSetupTempi_clicked();

    void on_btnMenuProgEdit_clicked();

    void on_btnImport_clicked();

private:

    Ui::frmMenu4_0 *ui;
};

#endif // FRMMENU4_0_H
