#include "savemsg.h"
#include "ui_savemsg.h"

SaveMsg::SaveMsg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SaveMsg)
{
    ui->setupUi(this);
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &SaveMsg::processTimer);
    timer->start(5000);

}
void SaveMsg::processTimer()
{
    timer->stop();
    close();
}
void SaveMsg::Init(QString nome)
{
    ui->lbNomeProgramma->setText(nome);
}



SaveMsg::~SaveMsg()
{
    delete ui;
}

