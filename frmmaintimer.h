#ifndef FRMMAINTIMER_H
#define FRMMAINTIMER_H

#include <QDialog>
#include <QTimer>
#include <sabbiatrice.h>
#include "formmessagebox.h"


namespace Ui {
class frmMainTimer;
}

class frmMainTimer : public QDialog
{
    Q_OBJECT

public:
    Sabbiatrice *sabbiatrice;

    bool okReset;
    void Init(uint  lavorazione_totale,uint  lavorazione_parziale, uint  accensione);
    explicit frmMainTimer(QWidget *parent = 0);

    ~frmMainTimer();

private slots:
    void processTimer();
    void on_buttonExit_clicked();
    void on_btnReset_clicked();

    void on_btnExport_clicked();

private:
    Ui::frmMainTimer *ui;
    FormMessageBox* uiMessageBox;

    QTimer  *timer;

    Tempi   tempi;

};

#endif // FRMMAINTIMER_H
