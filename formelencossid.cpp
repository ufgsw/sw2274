#include "formelencossid.h"
#include "ui_formelencossid.h"

#include <QProcess>
#include <QDebug>

FormElencoSSID::FormElencoSSID(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormElencoSSID)
{
    ui->setupUi(this);

    list.clear();

    QProcess* proc = new QProcess(this);

    QStringList arguments;

    arguments << "dev" << "wlan0" << "scan" ;//<< "|" << "grep" << "SSID";
//    iw dev wlan0 scan | grep SSID
    proc->start( "iw" , arguments );

    proc->waitForFinished( 15000 );

    QByteArray arr = proc->readAll();

    QStringList lista = QString(arr ).split( '\n');

    foreach (QString str, lista)
    {
        if( str.contains( "SSID"))
        {
            if( list.contains( str) == false )
                list.append( str );
        }
    }

    foreach (QString str, list )
    {
        QStringList ssid = str.split( ": " );
        if( ssid.count() == 2 )
        {
            ui->listSSID->addItem( ssid.at(1)) ;
        }
    }
}

FormElencoSSID::~FormElencoSSID()
{
    delete ui;
}

void FormElencoSSID::on_pushButton_2_clicked()
{
    valid = false;
    close();
}

void FormElencoSSID::on_pushButton_clicked()
{
    valid = true;
    close();
}

void FormElencoSSID::on_listSSID_currentTextChanged(const QString &currentText)
{
    SSID = currentText;
}
