#include "canbus.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <QTimer>
#include <QDebug>

#define SIOCSCANBAUDRATE	0x89F0

CanBus::CanBus(QObject *parent) : QObject(parent)
{
    int ret;
    int iMode = 1; // no blocking
    mInit = true;

    /* open socket */
    if ((socket_d = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
    {
        //perror("socket");
        qDebug( "Errore 1 creazione socket ");
        mInit = false;
    }
    else
    {
        strcpy(ifr.ifr_name, "can0");
        ioctl(socket_d, SIOCGIFINDEX, &ifr);
        ioctl(socket_d, FIONBIO, &iMode);

        addr.can_family  = PF_CAN;
        addr.can_ifindex = ifr.ifr_ifindex;
/*
        ifr.ifr_ifru.ifru_ivalue = 1000000/8;
        ret = ioctl(socket_d, SIOCSCANBAUDRATE, &ifr);
        if (ret)
        {
            qDebug( "Errore 2 creazione socket ");
            mInit = false;
        }        
        else
*/
        {
            if (bind(socket_d, (struct sockaddr *)&addr, sizeof(addr)) < 0)
            {
                //perror("bind");
                qDebug( "Errore bind creazione socket ");
                mInit = false;
            }
        }
    }

    QTimer* timer = new QTimer(this);

    connect( timer, &QTimer::timeout, this, &CanBus::read_frame );

    timer->setInterval(1);
    timer->start();

    qDebug("Init canbus OK");
}

void CanBus::write_frame( can_frame tx)
{
    int nbytes;

    if( mInit == false ) return;

//    if (parse_canframe( tx , &frame))
//    {
//        fprintf(stderr, "\nWrong CAN-frame format!\n\n");
//        fprintf(stderr, "Try: <can_id>#{R|data}\n");
//        fprintf(stderr, "can_id can have 3 (SFF) or 8 (EFF) hex chars\n");
//        fprintf(stderr, "data has 0 to 8 hex-values that can (optionally)");
//        fprintf(stderr, " be seperated by '.'\n\n");
//        fprintf(stderr, "e.g. 5A1#11.2233.44556677.88 / 123#DEADBEEF / ");
//        fprintf(stderr, "5AA# /\n     1F334455#1122334455667788 / 123#R ");
//        fprintf(stderr, "for remote transmission request.\n\n");
//        return;
//    }
    if ((nbytes = write(socket_d, &tx, sizeof(tx))) != sizeof(tx))
    {
        qDebug("Write error");
        return;
    }
}

void CanBus::read_frame()
{
    //QString rx;

    int lenrx = read(socket_d , &rframe, sizeof(rframe));

    if( lenrx != -1 )
    {
        //rx = QString("Ricevuto : %1: %2%3%4%5%6%7%8%9%10").arg(rframe.can_id).arg(rframe.data[0]).arg(rframe.data[1]).arg(rframe.data[2]).arg(rframe.data[3]).arg(rframe.data[4]).arg(rframe.data[5]).arg(rframe.data[6]).arg(rframe.data[7]).arg(lenrx);

        //qDebug(rx.toLatin1());

        emit frame_receive( rframe );
    }
}
