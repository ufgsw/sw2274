#ifndef FRMPLAYTIMER_H
#define FRMPLAYTIMER_H

#include <QDialog>

namespace Ui {
class frmPlayTimer;
}

class frmPlayTimer : public QDialog
{
    Q_OBJECT

public:
    int result;
    void Init(int cronometro);
    explicit frmPlayTimer(QWidget *parent = 0);
    ~frmPlayTimer();

private slots:
    void on_buttonExit_3_clicked();

    void on_btnStart_clicked();

private:
    bool        avvio;

    Ui::frmPlayTimer *ui;
};

#endif // FRMPLAYTIMER_H
