#include "frmmaintimer.h"
#include "ui_frmmaintimer.h"
#include "frmconfirm.h"

#include <QDir>
#include <QFile>

frmMainTimer::frmMainTimer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmMainTimer)
{
    ui->setupUi(this);
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &frmMainTimer::processTimer);
    timer->start(500);

    okReset = false;

}

frmMainTimer::~frmMainTimer()
{
    delete ui;
}
void frmMainTimer::Init(uint  lavorazione_totale,uint  lavorazione_parziale, uint  accensione)

{
    uint h,m,s;

    h = (uint)(  lavorazione_totale / 3600);
    m = (uint)(( lavorazione_totale - ( h * 3600) ) / 60);
    s = (uint)(( lavorazione_totale - ( h * 3600) - (m * 60)));

    ui->lbTempoTotale->setText(QString("h %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));

    h = (uint)(  lavorazione_parziale / 3600);
    m = (uint)(( lavorazione_parziale - ( h * 3600) ) / 60);
    s = (uint)(( lavorazione_parziale - ( h * 3600) - (m * 60)));

    ui->lbTempoParziale->setText(QString("h %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));

    h = (uint)(  accensione / 3600);
    m = (uint)(( accensione - ( h * 3600) ) / 60);
    s = (uint)(( accensione - ( h * 3600) - (m * 60)));

    ui->lbTempoVentilatore->setText(QString("h %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));


}
void frmMainTimer::processTimer()
{
    uint h,m,s;

    h = (uint)(  sabbiatrice->tempiLavoro.lavorazione_totale / 3600);
    m = (uint)(( sabbiatrice->tempiLavoro.lavorazione_totale - ( h * 3600) ) / 60);
    s = (uint)(( sabbiatrice->tempiLavoro.lavorazione_totale - ( h * 3600) - (m * 60)));

    ui->lbTempoTotale->setText(QString("h %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));

    h = (uint)(  sabbiatrice->tempiLavoro.lavorazione_parziale / 3600);
    m = (uint)(( sabbiatrice->tempiLavoro.lavorazione_parziale - ( h * 3600) ) / 60);
    s = (uint)(( sabbiatrice->tempiLavoro.lavorazione_parziale - ( h * 3600) - (m * 60)));

    ui->lbTempoParziale->setText(QString("h %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));

    h = (uint)(  sabbiatrice->tempiLavoro.accensione / 3600);
    m = (uint)(( sabbiatrice->tempiLavoro.accensione - ( h * 3600) ) / 60);
    s = (uint)(( sabbiatrice->tempiLavoro.accensione - ( h * 3600) - (m * 60)));

    ui->lbTempoVentilatore->setText(QString("h %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));

}
void frmMainTimer::on_buttonExit_clicked()
{
    close();
}

void frmMainTimer::on_btnReset_clicked()
{
    frmConfirm *frmconfirm = new frmConfirm();
    frmconfirm->Init("h 00:00:00 ?",true,1);
    frmconfirm->InitTitle("7.1",":/png/26+.png");
    frmconfirm->exec();

    if(frmconfirm->result == 1)
    {
        sabbiatrice->tempiLavoro.lavorazione_parziale = 0;
        ui->lbTempoParziale->setText("h 00:00:00");

        sabbiatrice->SalvaTempi();
    }

}

void frmMainTimer::on_btnExport_clicked()
{
    tempi.open();

    QString path;
    QString program;
    QStringList arguments;

    QString device;
    QDir dir("/dev");
   // dir.setCurrent("/dev");
    QFileInfoList list = dir.entryInfoList(QStringList() << "sd*",QDir::System);
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        device = fileInfo.fileName();
    }

    path = "/dev/" + device;

    QFile file(path);

    if(device != "" && file.exists())
    {
        QProcess* proc = new QProcess(this);
        program = "mount";
        arguments << path << "/media/storage";
        //mount = "mount /dev/";
        //mount += device;
        //mount += " /media/storage";

        proc->start( program, arguments );
        proc->waitForFinished(-1);

        tempi.save_timer("/media/storage/Timers.json");
        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "EXPORT SUCCESSFUL ! !");
        uiMessageBox->exec();

        program = "umount";
        arguments.clear();
        arguments << path;
        //mount = "umount /dev/";
        //mount += device;
        proc->start(program, arguments);
        proc->waitForFinished(-1);

        delete uiMessageBox;
    }
    else
    {
        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "INSERT USB KEY !");
        uiMessageBox->exec();

        delete uiMessageBox;
    }
}
