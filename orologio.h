#ifndef OROLOGIO_H
#define OROLOGIO_H

#include <QWidget>
#include <QTime>
#include <QDate>
#include <QFile>

class Orologio : public QWidget
{
    Q_OBJECT
public:
    explicit Orologio(QWidget *parent = nullptr);

    void salva();
    void set_time( QTime time);
    void set_date( QDate date);
    QString get_date_time_string();


protected:
    void paintEvent(QPaintEvent *event) override;

private:

    QDate new_date;
    QTime new_time;

signals:

    void update_datetime( QDate date, QTime time);

public slots:

    void update();
};

#endif // OROLOGIO_H
