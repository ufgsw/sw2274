#ifndef SABBIATRICE_H
#define SABBIATRICE_H

#include <QObject>
#include <globs.h>
#include <def.h>
#include <programmi.h>
#include "canbusthread.h"
#include "iot.h"

#include "maintenance.h"

#include <QThread>


#define MAX_PROG 200


class Sabbiatrice: public QThread
{
protected:
    void run() override;

public slots:
    void processCanBus( int id, unsigned char len, unsigned char b0,unsigned char b1,unsigned char b2,unsigned char b3,unsigned char b4,unsigned char b5,unsigned char b6,unsigned char b7 );

    void on_timer_1s();
    void on_timer_100ms();

    void reset_all_timers();

private:

    CanBusThread    *canbus;
    bool             mRun;
    QDateTime       *dteStart;

    Iot     *iot;

    int	currpgm;
    int   veloy;
    int   velox;
    int   inizioy;
    int   finey;
    short velmx;
    short velmy;
    int   xareamissing;
    int   step;
    int   lastmanic;
    int   timeoff_filtro;

    uint32_t Timer_send;
    uint32_t Timer_secondi;
    uint32_t Timer_minuti;
    uint32_t Timer_pausa;
    uint32_t Timer_lavoro;
    uint32_t Timer_funzionamento;

    int16_t Num_prog_exe;   // numero del programma da eseguire
    int16_t Num_prog_ed;	// numero del programma da editare
    int16_t Num_area_ed;	// numero dell'area da editare

    uchar Tx_buff[8];

    char        Quota_fatto_mot1_set;
    char        Quota_fatto_mot2_set;

    bool       use_topic;
    void FLEXCAN_Tx_message(int device, int mb_tx_av_altezza , int id_cmd_pezza , int flexcan_standard, int len, uchar *Tx_buff);
    void iot_dara_receive(QByteArray rx);

public:
    prog_s     Prog;
    prog_s     Prog_new;
    Parametri *parametri;
    Tempi     *ftempi;
    //Commesse  *commesse;

    flag_s  flag;
    //s_Par   Par;

    sTempiLavoro tempiLavoro;
    uFlagTempiLavorazione warningTempiLavorazione;
    uFlagTempiLavorazione visTempiLavorazione;

    QString Modello;
    QString serial_number;

    short   Tipo;

    uint32_t Timer_rx_azionamento;

    bool  mot1_in_allarme;
    bool  mot_pistola_in_allarme;
    bool  vis_allarme;
    char  manuale;
    char  win_start;

    uint32_t  cronometro;

    uint16_t  Output;
    uint16_t  Input;
    uint8_t   Pressione_set;
    uint8_t   Comandi_mot;
    int32_t   Quota_mot1;
    int32_t   Quota_mot2;
    int32_t   Quota_mot1_set;
    int32_t   Quota_mot2_set;
    int16_t   Velo_mot1_set;
    int16_t   Velo_mot2_set;
    uint16_t  Status_mot_nastro;
    uint16_t  Status_mot_pistola;
    int16_t   Risposta_azionamento;
    int16_t	  Release_azionamento;

    int      Allarmi;
    int      Allarmi_old;

    int      Warning;
    int      Warning_old;

    uchar    flexe;
    uchar    partito;

    void updateiot();
    void updateiot_alarm();
    void updateiot_param();
    void updateiot_status();
    void updateiot_maintenance();

    void Set_Pressione( float bar);
    void Home_Pistola(void);
    void Set_Mot_Quota(int mot, int32_t mm);
    void Set_Mot_Zero( int mot);
    void Move_Mot_Pos( int nmot, int32_t quota , char quota_ala, int16_t vel);
    void Move_Mot_Vel( int nmot, int16_t vel);
    void Stop_Mot_Vel( int nmot, int en_ds );
    void Move_AsseY_To_Zero(void);

    int  Verifica_Allarmi(void);
    int  Verifica_TempiLavoro(void);
    void SalvaTempi(void);
    void AzzeraCronometro();

    void Init_Hardware(void);
    void Init_Var(void);
    void Main_task(u_int32_t initial_data);
    void Esegui_Sabbiatura(u_int32_t initial_data);
    void Esegui_Comandi_Manuali();

    void Read_Par_FR(void);
    //void Init_Tempi_FR(void);
    //void Read_Tempi_FR(void);
    //void Save_Tempi_FR(void);

    //void Init_Prog_FR(void);
    //void Read_Prog_FR(void);
    void Save_Prog_FR(int nProg, prog_s* prog);
    void Read_Prog_FR(int nProg, prog_s* prog);

    void GetRealase(void);

    void SetInManuale(bool man);
    void SetManuale(u_int16_t cmd);

    void Ventola(bool on);
    void Vibratore(bool on);
    void Manichetta(int num,bool on);
    void Pistola(int num,bool on);

    void PISTOLA1_ON()	{	(Output |= Elettrovalvola_pistola_1); }
    void PISTOLA2_ON()	{	(Output |= Elettrovalvola_pistola_2); }
    void PULNAST_ON() 	{	(Output |= Pulizia_nastro); }
    void VIBRAT_ON()	{	(Output |= Motore_vibratore); }
    void VENTOLA_ON()	{	(Output |= Motore_ventilazione); }
    void MANICH1_ON()	{	(Output |= Elettrovalvola_pulizia_filtro_1); }
    void MANICH2_ON()	{	(Output |= Elettrovalvola_pulizia_filtro_2); }
    void MANICH3_ON()	{	(Output |= Elettrovalvola_pulizia_filtro); }

    void PISTOLA1_OFF()	{   (Output &= ~Elettrovalvola_pistola_1); }
    void PISTOLA2_OFF()	{   (Output &= ~Elettrovalvola_pistola_2); }
    void PULNAST_OFF()	{	(Output &= ~Pulizia_nastro); }
    void VIBRAT_OFF()	{	(Output &= ~Motore_vibratore); }
    void VENTOLA_OFF()	{	(Output &= ~Motore_ventilazione); }
    void MANICH1_OFF()	{	(Output &= ~Elettrovalvola_pulizia_filtro_1); }
    void MANICH2_OFF()	{	(Output &= ~Elettrovalvola_pulizia_filtro_2); }
    void MANICH3_OFF()	{	(Output &= ~Elettrovalvola_pulizia_filtro); }

    Sabbiatrice();
 };




#endif // SABBIATRICE_H
