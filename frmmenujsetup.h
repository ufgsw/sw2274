#ifndef FRMMENUJSETUP_H
#define FRMMENUJSETUP_H

#include <QDialog>

namespace Ui {
class frmMenujSetup;
}

class frmMenujSetup : public QDialog
{
    Q_OBJECT

public:
    explicit frmMenujSetup(QWidget *parent = 0);
    ~frmMenujSetup();

private:
    Ui::frmMenujSetup *ui;
};

#endif // FRMMENUJSETUP_H
