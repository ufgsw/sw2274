#ifndef FRMINFO_H
#define FRMINFO_H

#include <QDialog>
#include "modelinfo.h"
#include "programmi.h"
#include "formupgrade.h"

namespace Ui {
class frmInfo;
}

class frmInfo : public QDialog
{
    Q_OBJECT

public:
    bool okEdit;
    //Programmi *progs;
    Config  *config;
    void Init(bool edit);
    explicit frmInfo(QWidget *parent = 0);

    ~frmInfo();

    bool reset_timers;
    bool reset_service;

private slots:

    void on_buttonExit_3_clicked();

    void on_buttonExit_5_clicked();

    void on_buttonExit_8_clicked();

    void on_tbProg_clicked(const QModelIndex &index);

    void on_buttonUpgrade_clicked();

private:

    ModelInfo myModelInfo;
    //MyModel myModel;
    Ui::frmInfo *ui;

    FormUpgrade* uiUpgrade;
};

#endif // FRMINFO_H
