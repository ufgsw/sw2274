#include "formmenu.h"
#include "ui_formmenu.h"

formMenu::formMenu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::formMenu)
{
    ui->setupUi(this);
}

formMenu::~formMenu()
{
    delete ui;
}

void formMenu::on_btnMenuSetup_clicked()
{
    frmSetup = new frmMainSetup();
    frmSetup->show();


}
