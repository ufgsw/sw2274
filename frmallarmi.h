#ifndef FRMALLARMI_H
#define FRMALLARMI_H

#include <QDialog>
#include <QTimer>
#include <sabbiatrice.h>



namespace Ui {
class frmAllarmi;
}

class frmAllarmi : public QDialog
{
    Q_OBJECT

public:
    Sabbiatrice *sabbiatrice;
    void SetImage(void);
    void SetImageMan(QString file,QString page);
    void SetManuale(int manuale);
    explicit frmAllarmi(QWidget *parent = 0);
    ~frmAllarmi();

private slots:
    void ToggleImage(void);
    void on_buttonExit_3_clicked();

    void on_imgAllarme_clicked();

private:

    void viewImage();

    QTimer  *timer_flash;
    bool    toggle;
    bool    okHome;
    bool    no_man;
    Ui::frmAllarmi *ui;

    int   allarme;
};

#endif // FRMALLARMI_H
