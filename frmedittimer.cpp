#include "frmedittimer.h"
#include "ui_frmedittimer.h"
#include <frmconfirm.h>

frmEditTimer::frmEditTimer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmEditTimer)
{
    ui->setupUi(this);

    timer_btn = new QTimer(this);
    connect(timer_btn,SIGNAL(timeout()), this, SLOT(reset_timer()));
}

frmEditTimer::~frmEditTimer()
{
    delete ui;
}
void frmEditTimer::Init(int code,int alarm,int tempo,uint limite)
{
    uint h,m,s;

    okReset = false;
    Limite = limite;

    if(tempo > 0 )
    {
        h = (uint)(  tempo / 3600);
        m = (uint)(( tempo - ( h * 3600) ) / 60);
        s = (uint)(( tempo - ( h * 3600) - (m * 60)));

        ui->lbTimer->setText(QString("h %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));
        ui->lbTimer->setStyleSheet( "color : white");
    }
    else
    {
        tempo = tempo * -1;

        h = (uint)(  tempo / 3600);
        m = (uint)(( tempo - ( h * 3600) ) / 60);
        s = (uint)(( tempo - ( h * 3600) - (m * 60)));

        ui->lbTimer->setText(QString("h %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));
        ui->lbTimer->setStyleSheet( "color : red");
    }

    h = (uint)(  limite / 3600);
    m = (uint)(( limite - ( h * 3600) ) / 60);
    s = (uint)(( limite - ( h * 3600) - (m * 60)));

    ui->lbTimer_2->setText(QString("h %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));

    ui->lbNum_3->setVisible(false);
    ui->panNum_3->setVisible(false);

    if(alarm == 1){
        ui->imgWarning->setVisible(true);
    }
    else{
        ui->imgWarning->setVisible(false);
    }
    switch(code){
        case 86:
        case 110:
        case 88:
        case 89:
        case 90:
        case 100:
        case 120:
            ui->lbNum->setVisible(false);
            ui->panNum->setVisible(false);
        break;
        default:
            ui->lbNum->setVisible(true);
            ui->panNum->setVisible(true);
        break;
    }

    switch(code){
        case 81:
        case 82:
        case 83:
        case 84:
        case 85:
        case 86:
        case 87:
        case 88:
        case 89:
        case 90:
        case 100:
        case 110:
        case 120:
            ui->lbNum->setText("1");
        break;
        case 811:
        case 821:
        case 831:
        case 841:
        case 851:
        case 861:
        case 871:
        case 881:
        case 891:
        case 901:
        case 1001:
            ui->lbNum->setText("2");
            if(n_pistole == "3"){
                ui->lbNum_3->setVisible(true);
                ui->panNum_3->setVisible(true);
            }
            else{
                ui->lbNum_3->setVisible(false);
                ui->panNum_3->setVisible(false);

            }

        break;
    }

    switch(code){
        case 81: ui->lbHelp->setText("8.1"); break;
        case 82: ui->lbHelp->setText("8.2"); break;
        case 83: ui->lbHelp->setText("8.3"); break;
        case 84: ui->lbHelp->setText("8.4"); break;
        case 85: ui->lbHelp->setText("8.5"); break;
        case 86: ui->lbHelp->setText("8.10"); break;
        case 87: ui->lbHelp->setText("8.6"); break;
        case 88: ui->lbHelp->setText("8.7"); break;
        case 89: ui->lbHelp->setText("8.8"); break;
        case 90: ui->lbHelp->setText("8.9"); break;
        case 100: ui->lbHelp->setText("8.12"); break;
        case 110: ui->lbHelp->setText("8.10.1"); break;
        case 120: ui->lbHelp->setText("8.12.1"); break;
        break;
        case 811: ui->lbHelp->setText("8.1.1"); break;
        case 821: ui->lbHelp->setText("8.2.1"); break;
        case 831: ui->lbHelp->setText("8.3.1"); break;
        case 841: ui->lbHelp->setText("8.4.1"); break;
        case 851: ui->lbHelp->setText("8.5.1"); break;
        case 861: ui->lbHelp->setText("8.10.1"); break;
        case 871: ui->lbHelp->setText("8.6.1"); break;
        case 881: ui->lbHelp->setText("8.7.1"); break;
        case 891: ui->lbHelp->setText("8.8.1"); break;
        case 901: ui->lbHelp->setText("8.9.1"); break;
        case 1001:ui->lbHelp->setText("8.11.1"); break;
        break;
    }


    switch(code){
        case 81:
        case 811:
             ui->imgCentrale->setIcon(QIcon(":/png/87+.png"));
        break;
        case 82:
        case 821:
             ui->imgCentrale->setIcon(QIcon(":/png/89+.png"));
        break;
        case 83:
        case 831:
             ui->imgCentrale->setIcon(QIcon(":/png/90+.png"));
        break;
        case 84:
        case 841:
             ui->imgCentrale->setIcon(QIcon(":/png/91+.png"));
        break;
        case 85:
        case 851:
             ui->imgCentrale->setIcon(QIcon(":/png/92+.png"));
        break;
        case 86:
        case 861:
             ui->imgCentrale->setIcon(QIcon(":/png/97+.png"));
        break;
        case 87:
        case 871:
             ui->imgCentrale->setIcon(QIcon(":/png/93+.png"));
        break;
        case 88:
        case 881:
             ui->imgCentrale->setIcon(QIcon(":/png/94+.png"));
        break;
        case 89:
        case 891:
             ui->imgCentrale->setIcon(QIcon(":/png/95+.png"));
        break;
        case 90:
        case 901:
             ui->imgCentrale->setIcon(QIcon(":/png/96+.png"));
        break;
        case 100:
        case 1001:
             ui->imgCentrale->setIcon(QIcon(":/png/99+.png"));
        break;
        case 110:
             ui->imgCentrale->setIcon(QIcon(":/png/98+.png"));
        break;
        case 120:
             ui->imgCentrale->setIcon(QIcon(":/png/100+.png"));
        break;
    }

}

void frmEditTimer::reset_timer()
{
    timer_btn->stop();
    uint h,m,s;
    h = (uint)(  Limite / 3600);
    m = (uint)(( Limite - ( h * 3600) ) / 60);
    s = (uint)(( Limite - ( h * 3600) - (m * 60)));

    ui->lbTimer->setText(QString("h %1:%2:%3").arg(h).arg(m,2,10,QChar('0')).arg(s,2,10,QChar('0')));
    ui->imgWarning->setVisible(false);
    okReset = true;
 }

void frmEditTimer::on_buttonExit_3_clicked()
{
    result = 0;
    close();
}

void frmEditTimer::on_btnReset_pressed()
{
    //timer_btn->start(3000);
}

void frmEditTimer::on_btnReset_released()
{
    timer_btn->stop();
}

void frmEditTimer::on_btnNext_clicked()
{
    result = 1;
    close();

}


void frmEditTimer::on_btnPrior_clicked()
{
    result = 2;
    close();

}

void frmEditTimer::on_btnReset_clicked()
{
    frmConfirm *frmconfirm = new frmConfirm();
    frmconfirm->Init("h 00:00:00 ?",true,2);
    frmconfirm->exec();
    if(frmconfirm->result == 1){
        reset_timer();
    }

}
