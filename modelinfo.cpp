#include "modelinfo.h"
#include <QFont>
#include <QBrush>
ModelInfo::ModelInfo(QObject *parent)
    : QAbstractTableModel(parent)
{
}

int ModelInfo::rowCount(const QModelIndex & /*parent*/) const
{
   return ROWS_I;
}

int ModelInfo::columnCount(const QModelIndex & /*parent*/) const
{
    return COLS_I;
}
QVariant ModelInfo::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return QString("");
        case 1:
            return QString("");
        }
    }
    return QVariant();
}
QVariant ModelInfo::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();
    // generate a log message when this method gets called
    //qDebug(QString("row %1, col%2, role %3").arg(row).arg(col).arg(role));

    switch (role) {
    case Qt::DisplayRole:
        //if (row == 0 && col == 1) return QString("Programma 1");
        //if (row == 1 && col == 1) return QString("Programma 2");
        if(m_gridData[row][col] != NULL)
            return m_gridData[row][col];
        //return QString("Row%1, Column%2").arg(row + 1).arg(col +1);
    case Qt::FontRole:

        if (row == -1) { //change font only for cell(0,0)
            QFont boldFont;
            boldFont.setBold(true);
            return boldFont;
        }
        break;
    case Qt::BackgroundRole:
        //if (row == 0)  //change background
        //    return QBrush(Qt::red);
        break;
    case Qt::TextAlignmentRole:
       // if (row == 1 && col == 1) //change text alignment only for cell(1,1)
       //     return int(Qt::AlignRight | Qt::AlignVCenter);
        break;
    case Qt::CheckStateRole:
        //if (row == 1 && col == 0) //add a checkbox to cell(1,0)
        //    return Qt::Checked;
        break;
    }
    return QVariant();
}
