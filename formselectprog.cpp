#include "formselectprog.h"
#include "ui_formselectprog.h"
#include "formnewprogram.h"
#include "formtastieraa.h"
#include "frmconfirm.h"
#include "frmnewname.h"
#include <QMessageBox>
#include <qprocess.h>
#include "formmessagebox.h"
#include <qdir.h>

#include "programmi.h"

formselectprog::formselectprog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::formselectprog)
{
    ui->setupUi(this);

    ui->tbProg->setColumnWidth(0,250);
    ui->tbProg->setColumnWidth(1,80);

/*
    myModel.m_gridData[0][0] = "1";
    myModel.m_gridData[0][1] = "Programma 1";
    myModel.m_gridData[0][2] = "5";
    myModel.m_gridData[0][3] = "4";
    myModel.m_gridData[0][4] = "00:01:00";
    myModel.m_gridData[0][5] = "00:04:00";

    myModel.m_gridData[1][0] = "2";
    myModel.m_gridData[1][1] = "Programma 2";
    myModel.m_gridData[1][2] = "2";
    myModel.m_gridData[1][3] = "2";
    myModel.m_gridData[1][4] = "00:01:00";
    myModel.m_gridData[1][5] = "00:02:00";

*/

    last_idx = 0;
}

formselectprog::~formselectprog()
{
    delete ui;
}

QString formselectprog::SecondToTime(int sec)
{
    int seconds = sec % 60;
    int minutes = (sec / 60) % 60;
    int hours = (sec / 60 / 60);

    QString timeString = QString("%1:%2:%3")
        .arg(hours, 2, 10, QChar('0'))
        .arg(minutes, 2, 10, QChar('0'))
        .arg(seconds, 2, 10, QChar('0'));

    return timeString;
}
void formselectprog::Init(bool edit,QString um)
{
    UM = um;
//    for( int r=0;r < ROWS;r++ )
//    {
//        for( int c=0;c < COLS;c++ )
//        {
//            myModel.m_gridData[r][c] =  "";
//        }
//    }

//    n_prog=0;
//    int idx = 0;
//    QString ore;
//    for( prog_s prog : progs->programmi.values() )
//    {
//        //myModel.m_gridData[px][0] =  QString( "%1").arg(px);
//        myModel.m_gridData[n_prog][0] =  prog.nome;
//        myModel.m_gridData[n_prog][1] =  QString( "%1").arg(prog.naree);
//        myModel.m_gridData[n_prog][2] =  QString( "%1").arg(prog.esecuzioni);
//        //ore = QString( "%1:%1").arg((prog.secondi_impiegati / 3600),(prog.secondi_impiegati / 60);
//        myModel.m_gridData[n_prog][3] =  SecondToTime(prog.secondi_impiegati);
//        myModel.m_gridData[n_prog][4] =  SecondToTime(prog.secondi_totali);

//        if(prog.nome == last_nome)
//        {
//            idx = n_prog;
//        }
//        n_prog++;
//    }

    ui->tbProg->setModel( nullptr);     // Modo idiota per forzare il rinfresco della tabella
    ui->tbProg->setModel(&myModel);     // ma almeno funziona


    ui->tbProg->setColumnWidth(0,220);
    ui->tbProg->setColumnWidth(1,100);
    ui->tbProg->setColumnWidth(2,100);
    ui->tbProg->setColumnWidth(3,120);
    ui->tbProg->setColumnWidth(4,120);


    if( myModel.rowCount() > 0 )
    {
        ui->tbProg->selectRow(last_idx);
        last_nome = ui->tbProg->model()->index( last_idx, 0).data().toString();
        ui->lbProgrammaSel->setText( last_nome );
    }
    else
    {
        ui->tbProg->selectRow(-1);
        ui->lbProgrammaSel->setText("");
    }

    if(!edit)
    {
        ui->btnCopia->setVisible(false);
        ui->btnRemove->setVisible(false);
        ui->btnNext->setVisible(false);
        ui->btnAggiungi->setVisible(false);
        ui->btnExport->setVisible(false);
        ui->label->setText("1.0");
        ui->btnImage->setIcon(QIcon(":/png/02+.png"));
    }
    else
    {
        ui->btnAggiungi->setVisible(true);
        ui->btnCopia->setVisible(true);
        ui->btnRemove->setVisible(true);
        ui->btnNext->setVisible(true);
        ui->btnExport->setVisible(true);
        ui->label->setText("2.0");
        ui->btnImage->setIcon(QIcon(":/png/04+.png"));
    }


}

void formselectprog::on_buttonExit_clicked()
{
    QString nome;

    QModelIndexList indexList = ui->tbProg->selectionModel()->selectedIndexes();
    int row=0;
    foreach (QModelIndex index, indexList)
    {
        row = index.row();
        break;
    }

    nome = ui->lbProgrammaSel->text();
    if(nome != "")
    {
        progs->setCurrent(nome);
        result = 1; //ui->listProgrammi->selectedIndexes();
        //last_nome =  myModel.m_gridData[row][0];
        last_idx  =  row;
        last_nome = ui->tbProg->model()->index( last_idx, 0).data().toString();

        close();
    }
}

void formselectprog::on_buttonExit_3_clicked()
{
    result = -1;
    close();
}

void formselectprog::on_btnNext_clicked()
{
    prog_s newprog;

    QString nome;
    nome = ui->lbProgrammaSel->text();

    if(nome != "")
    {
        progs->setCurrent(nome);
        newprog = progs->current();

        frmNewName *frmnewname = new frmNewName(this);
        frmnewname->Init(newprog.nome);
        frmnewname->exec();

        if( frmnewname->result == 1 && frmnewname->nome != "")
        {
            nome = frmnewname->nome;
            progs->rename(newprog,nome);
            //progs->save("");

            Init(true,progs->UM);
        }
        delete frmnewname;
    }

    /*
    QItemSelectionModel *selectionModel = ui->tbProg->selectionModel();
    int row = -1;
    if ( selectionModel->hasSelection() )
        row = selectionModel->selection().first().indexes().first().row();
    int rowcount = ui->tbProg->model()->rowCount();
    row = (row + 1 ) % rowcount;
    QModelIndex newIndex = ui->tbProg->model()->index(row, 0);
    selectionModel->select( newIndex, QItemSelectionModel::ClearAndSelect );
    ui->lbProgrammaSel->setText(myModel.m_gridData[row][1]);
    */

}

void formselectprog::on_btnPrior_clicked()
{
    QItemSelectionModel *selectionModel = ui->tbProg->selectionModel();
    int row = -1;
    if ( selectionModel->hasSelection() )
    {
        row = selectionModel->selection().first().indexes().first().row();
    }
    row = (row - 1 );
    if(row < 0)
    {
        row = 0;
    }

    QModelIndex newIndex = ui->tbProg->model()->index(row, 0);
    selectionModel->select( newIndex, QItemSelectionModel::ClearAndSelect );


    last_idx = row;
    last_nome = ui->tbProg->model()->index( last_idx, 0).data().toString();

    //ui->lbProgrammaSel->setText(myModel.m_gridData[row][0]);
    ui->lbProgrammaSel->setText( last_nome );

}

void formselectprog::on_btnAggiungi_clicked()
{
    prog_s newprog;
    QString nome;

    Parametri* par = Parametri::getInstance();

    n_prog = progs->count();

    frmNewName *frmnewname = new frmNewName(this);
    frmnewname->Init(QString( "P%1").arg(n_prog+1,4,10,QChar('0')));

    if( frmnewname->result == 0 )
    {
        delete frmnewname;
        return;
    }

    frmnewname->exec();

    if( frmnewname->result == 1)
    {
        nome = frmnewname->nome;

        FormNewProgram *frmnewprog = new FormNewProgram(this);

        frmnewprog->Init( UM, par->param.quotamax_y );

        frmnewprog->set_program_name(nome);
        frmnewprog->exec();
        if(frmnewprog->result == 1)
        {
            if(progs->exist(nome) == false)
            {
                newprog = frmnewprog->prog;
                newprog.nome = nome;
                progs->add(newprog);
                progs->save("");

                //myModel.insertRow(0,QModelIndex());

                Init(true,progs->UM);
            }
            else
            {
                FormMessageBox *uiMessageBox = new FormMessageBox(this);
                uiMessageBox->setTitle( "PROGRAM NAME ALREADY EXISTING !");
                uiMessageBox->exec();

                delete uiMessageBox;

            }
        }
        delete frmnewprog;
    }
    delete frmnewname;

}
//
void formselectprog::on_btnRemove_clicked()
{
    frmConfirm *frmconfirm = new frmConfirm();
    frmconfirm->Init("?",true,0);
    frmconfirm->InitTitle("2.8",":/png/04+.png");
    frmconfirm->exec();
    if(frmconfirm->result == 1)
    {
        QItemSelectionModel *selectionModel = ui->tbProg->selectionModel();
        int row = -1;
        if ( selectionModel->hasSelection() )
        {
            row = selectionModel->selection().first().indexes().first().row();
        }
        if(row >=0)
        {
            //progs->remove(progs->get_byname( myModel.m_gridData[row][0]) );
            progs->remove( progs->get_byname(last_nome ) );
            Init(true,progs->UM);
        }
    }
}

void formselectprog::on_btnCerca_clicked()
{
    QStringList list;
    QString     cerca;
    int idx = -1;

    FormTastieraA *uiTastieraA = new FormTastieraA(this);

    //uiTastieraA->set_title( tr("Name") );
    uiTastieraA->set_text(ui->btnCerca->text());
    uiTastieraA->exec();

    cerca = uiTastieraA->get_text();

    delete uiTastieraA;


    list = progs->elenco_nomi();

    for (int i = 0; i < list.size(); ++i)
    {
        QString sel = ui->tbProg->model()->data(ui->tbProg->model()->index( i  ,0)).toString();

        if( sel.indexOf( cerca ,0,Qt::CaseInsensitive) == 0 )
        {
            idx = i;
            break;
        }
//        if(myModel.m_gridData[i][0].indexOf(cerca,0,Qt::CaseInsensitive) == 0)
//        {
//            idx = i;
//            break;
//        }
    }
    if(idx >= 0)
    {
        ui->tbProg->selectRow(idx);
        last_idx  = idx;
        last_nome = ui->tbProg->model()->index( idx, 0).data().toString();
        ui->lbProgrammaSel->setText( last_nome );
        //ui->lbProgrammaSel->setText(myModel.m_gridData[idx][0]);
     }

}

void formselectprog::on_tbProg_clicked(const QModelIndex &index)
{
    last_idx = index.row();

    last_nome = ui->tbProg->model()->index( last_idx, 0).data().toString();

    ui->lbProgrammaSel->setText( last_nome );

//    if(myModel.m_gridData[index.row()][0] != "")
//    {
//        ui->lbProgrammaSel->setText(myModel.m_gridData[index.row()][0]);
//    }

}

void formselectprog::on_btnCopia_clicked()
{
    prog_s newprog;

    QString nome;
    nome = ui->lbProgrammaSel->text();
    if(nome != "")
    {
        progs->setCurrent(nome);
        newprog = progs->current();
        FormTastieraA *uiTastieraA = new FormTastieraA(this);
        uiTastieraA->exec();

        newprog.nome = uiTastieraA->get_text();
        newprog.secondi_totali = 0;
        newprog.esecuzioni = 0;
        delete uiTastieraA;
        if(!progs->exist(newprog.nome))
        {
            progs->add(newprog);
            progs->save("");
            Init(true,progs->UM);
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setText("Program name already existing !");
            msgBox.exec();
        }
    }
}

void formselectprog::on_btnExport_clicked()
{

    Programmi prgs;
    prgs.open();

    QString device;

    QDir dir("/dev");
    QFileInfoList list = dir.entryInfoList(QStringList() << "sd*",QDir::System);

    for (int i = 0; i < list.size(); ++i)
    {
        QFileInfo fileInfo = list.at(i);
        device = fileInfo.fileName();
    }

    QString     path;
    QString     program;
    QStringList arguments;

    path = "/dev/" + device;

    QFile file( path);
    if(device != "" && file.exists())
    {
        QProcess* proc = new QProcess(this);
        program = "mount";
        arguments << path << "/media/storage";

        //mount = "mount /dev/";
        //mount += device;
        //mount += " /media/storage";

        proc->start(program,arguments);
        proc->waitForFinished(-1);

        prgs.filename = "/media/storage/backup.json";
        prgs.export_all_prog();
        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "EXPORT SUCCESSFUL !");
        uiMessageBox->exec();

        program = "umount";
        arguments.clear();
        arguments << path;

        //mount = "umount /dev/";
        //mount += device;

        proc->start( program, arguments );
        proc->waitForFinished(-1);

        delete uiMessageBox;
    }
    else
    {
        QProcess* proc = new QProcess(this);

        program = "umount";
        arguments << path;

        proc->start(program,arguments);
        proc->waitForFinished(-1);

        FormMessageBox *uiMessageBox = new FormMessageBox(this);
        uiMessageBox->setTitle( "INSERT USB KEY !");
        uiMessageBox->exec();

        delete uiMessageBox;
    }
   // progs->import_prog("BVCV1.json");
}

void formselectprog::on_tbProg_activated(const QModelIndex &index)
{
    //
//    qDebug( "Selezionato nuovo programma nella lista");

//    last_idx  = index.row();
//    last_nome = ui->tbProg->model()->index( last_idx, 0).data().toString();

}

void formselectprog::on_tbProg_entered(const QModelIndex &index)
{
    last_idx = index.row();

    last_nome = ui->tbProg->model()->index( last_idx, 0).data().toString();

    ui->lbProgrammaSel->setText( last_nome );
}
