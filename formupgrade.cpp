#include "formupgrade.h"
#include "ui_formupgrade.h"
#include "release.h"
#include <QFile>
#include <QProcess>

FormUpgrade::FormUpgrade(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormUpgrade)
{
    ui->setupUi(this);

    ui->buttonOk->setVisible(false);
    ui->buttonNo->setVisible(false);
}

FormUpgrade::~FormUpgrade()
{
    delete ui;
}

QString FormUpgrade::Md5_gen(const QString &s)
{
    QString pakchunk_Md5;
    QCryptographicHash crypto(QCryptographicHash::Md5);
    QFile pakchunk(s);

    if (pakchunk.open(QIODevice::ReadOnly))
    {
        while(!pakchunk.atEnd())
        {
            crypto.addData(pakchunk.read(8192));
        }
    }
    else
    {
        qDebug() << "Can't open file.";
        pakchunk_Md5 = "nofile";
        return pakchunk_Md5;
    }

    pakchunk_Md5 = crypto.result().toHex();
    return pakchunk_Md5;
}

void FormUpgrade::save_settings()
{
    QSettings settings( "/home/root/Cloud.ini" , QSettings::IniFormat );
    settings.setValue( "path", cloud_path );
    settings.setValue( "file", cloud_file );
    settings.sync();
}

void FormUpgrade::on_finish_download()
{

    QString path_app = "/home/root/" + cloud_file;

    if( download_release == true )
    {
        // Ho scaricato il file di testo contenete la release del nuovo firmware
        QFile filerelease( "/home/root/Release.json" );

        if( filerelease.exists() == false )
        {
            ui->txtOutput->append( "Download error" );
            return;
        }


        filerelease.open(QFile::ReadOnly | QFile::Text );
        QByteArray saveData = filerelease.readAll();
        filerelease.close();

        QJsonDocument jsonDoc;
        QJsonObject   obj;

        jsonDoc = QJsonDocument::fromJson( saveData );
        obj     = jsonDoc.object();

        application = obj["application"].toString();
        release     = obj["release"].toString();
        md5sum      = obj["md5"].toString();

        cloud_file  = application;

        if( release != release_fw )
        {
            ui->buttonOk->setVisible(true);
            ui->buttonNo->setVisible(true);

            QString a = tr("Software Upgrade Available!\r\n");
            QString b = tr("INSTALL ?");

            QString m = QString("%1 (Rel. %2), %3").arg(a).arg(release).arg(b);

            ui->txtOutput->setText( m );
        }
        else
        {
            ui->txtOutput->setText( tr("Software Updated at last version"));
            ui->buttonNo->setVisible(true);
        }
        //
        QFile::remove( "/home/root/Release.json");
        download_release = false;
    }
    else if( download_error == false )
    {
        QFile fileapp( path_app );
        if( fileapp.exists() )
        {
            ui->txtOutput->setText("");;
            ui->txtOutput->append( "Download finish !");
            ui->txtOutput->append( "Verify file !");

            QString md5 = Md5_gen( path_app  );

            ui->txtOutput->append( "MD5 : " + md5 );
            if( md5 == md5sum )
            {
                ui->txtOutput->append( "L'applicazione si riavviera a breve");
                QFile::copy( path_app, "/home/root/newapplication");
                QFile::remove( path_app );


                QProcess* sync = new QProcess();
                sync->start("sync");
                sync->waitForFinished();

                QProcess* reboot = new QProcess();
                reboot->start("reboot");

            }
            else
            {
                ui->txtOutput->append( "Error file !");
            }
        }
    }
    else
    {
        // Download error
        ui->buttonNo->setVisible(true);
    }
}

void FormUpgrade::on_progress(int p, QString brate)
{
    ui->progressBar->setValue( p );
    //ui->txtOutput->append( brate );
    ui->labelBrate->setText( brate );
}

void FormUpgrade::on_message(QString msg)
{
    ui->txtOutput->append( msg );
    download_error = true;
}

void FormUpgrade::on_buttonVerifyDownload_clicked()
{
    download_error = false;
    QStringList urls;
    //urls.append( "http://download.inspire.net.nz/data/release.txt");
    //urls.append( "http://download.inspire.net.nz/data/50MB.zip");
    //urls.append( "http://demo.ufg.it/release.txt");

    QString fileapp = cloud_path + cloud_file;
    urls.append( fileapp );

    manager->append( urls );
}

void FormUpgrade::on_buttonExit_clicked()
{
    close();
}


void FormUpgrade::on_buttonCloudPath_clicked()
{
    uiTastieraA = new FormTastieraA(this);
    uiTastieraA->set_title( tr("Cloud path ") + cloud_path );
    uiTastieraA->exec();

    if( uiTastieraA->is_valid() )
    {
        cloud_path = uiTastieraA->get_text();

        save_settings();
    }

}

void FormUpgrade::showEvent(QShowEvent *event)
{
//    if( QFile::exists( "/home/root/Cloud.ini" ) == false )
//    {
//        cloud_path = "http://cloud.fratellipezza.com/update/";
//        cloud_file = "app";
//        save_settings();
//    }
//    else
//    {
//        QSettings settings( "/home/root/Cloud.ini" , QSettings::IniFormat );
//        cloud_path = settings.value( "path").toString();
//        cloud_file = settings.value( "file").toString();
//    }

    cloud_path = "http://update.fratellipezza.com/update/";
    //cloud_path = "http://demo.ufg.it/sw2273/";
    cloud_file = "sw2274.bin";

    my_release = release_fw;
    manager    = new DownloadManager(this);

    QObject::connect(manager, &DownloadManager::finished, this, &FormUpgrade::on_finish_download );
    QObject::connect(manager, &DownloadManager::progress, this, &FormUpgrade::on_progress );
    QObject::connect(manager, &DownloadManager::message,  this, &FormUpgrade::on_message  );

    download_error    = false;
    download_firmware = false;
    download_release  = true;

    //ui->buttonVerifyDownload->setEnabled( false );

    ui->txtOutput->append( tr("Connecting to Cloud....."));

    QStringList urls;
    QString filerelease = cloud_path + "Release.json";
    //QString filerelease = cloud_path + cloud_file;
    urls.append( filerelease);
    manager->append( urls );


}

void FormUpgrade::on_buttonOk_clicked()
{
    download_error = false;
    QStringList urls;
    //urls.append( "http://download.inspire.net.nz/data/release.txt");
    //urls.append( "http://download.inspire.net.nz/data/50MB.zip");
    //urls.append( "http://demo.ufg.it/release.txt");
    QString fileapp = cloud_path + cloud_file;
    urls.append( fileapp );

    manager->append( urls );
}

void FormUpgrade::on_buttonNo_clicked()
{
    close();
}

void FormUpgrade::on_buttonExit_3_clicked()
{
    close();
}
