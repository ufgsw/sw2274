#include "frmmenu4_0.h"
#include "ui_frmmenu4_0.h"
#include "formsetupethernet.h"
#include "frmlogmanutenzione.h"
#include "frmlogallarmi.h"
#include "frmimport.h"
#include "programmi.h"
#include "def.h"

frmMenu4_0::frmMenu4_0(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmMenu4_0)
{
    ui->setupUi(this);

    Programmi* progs = Programmi::getInstance();

    if( progs->Tipo == ZEPHIR )
        ui->btnSetupTempi->setVisible( false );
}

frmMenu4_0::~frmMenu4_0()
{
    delete ui;
}

void frmMenu4_0::on_btnSetup40_clicked()
{
    FormSetupEthernet *frmSetupEthernet = new FormSetupEthernet();
    frmSetupEthernet->exec();
    delete frmSetupEthernet;

}

void frmMenu4_0::on_buttonExit_clicked()
{
    close();
}

void frmMenu4_0::on_btnSetupTempi_clicked()
{

    frmLogManutenzione *frmManutenzioni = new frmLogManutenzione();
    frmManutenzioni->exec();
    delete frmManutenzioni;


}

void frmMenu4_0::on_btnMenuProgEdit_clicked()
{
    frmLogAllarmi *frmAlarm = new frmLogAllarmi();
    frmAlarm->exec();
    delete frmAlarm;


}

void frmMenu4_0::on_btnImport_clicked()
{
    frmImport *frm = new frmImport();
    frm->exec();
    delete frm;

}
