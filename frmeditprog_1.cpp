#include "frmeditprog_1.h"
#include "ui_frmeditprog_1.h"

frmEditProg_1::frmEditProg_1(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::frmEditProg_1)
{
    ui->setupUi(this);

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &frmEditProg_1::processTimer);
    timer->start(500);
    modalita = 0;

}

frmEditProg_1::~frmEditProg_1()
{
    delete ui;
}
void frmEditProg_1::Init()
{
    ui->lbNomeProgramma->setText(prog.nome);
    modalita = prog.direzione;

    if(modalita > 0)
    {
        ui->imgCentrale->setIcon(QIcon(":/png/50-A.png"));
        ui->btnNext->setVisible(true);
        ui->btnPrior->setVisible(false);
    }
    else
    {
        ui->imgCentrale->setIcon(QIcon(":/png/47++.png"));
        ui->btnNext->setVisible(false);
        ui->btnPrior->setVisible(true);
    }

    timer->start(500);

}
void frmEditProg_1::processTimer()
{
    if(fase < 3)
        fase++;
    else
    {
        fase = 1;
        timer->stop();
        return;
    }
    if(modalita > 0)
    {
        switch(fase)
        {
            case 1: ui->imgCentrale->setIcon(QIcon(":/png/50-A.png")); break;
            case 2: ui->imgCentrale->setIcon(QIcon(":/png/50-B.png")); break;
            case 3: ui->imgCentrale->setIcon(QIcon(":/png/50-C.png")); break;
            case 4: ui->imgCentrale->setIcon(QIcon(":/png/50-D.png")); break;
        }
        ui->lbHelp->setText("2.4");
        ui->btnNext->setVisible(true);
        ui->btnPrior->setVisible(false);
    }
    else{
        switch(fase){
            case 1: ui->imgCentrale->setIcon(QIcon(":/png/47++.png")); break;
            case 2: ui->imgCentrale->setIcon(QIcon(":/png/48++.png")); break;
            case 3: ui->imgCentrale->setIcon(QIcon(":/png/49++.png")); break;
            case 4: ui->imgCentrale->setIcon(QIcon(":/png/50++.png")); break;
        }
        ui->lbHelp->setText("2.3");
        ui->btnNext->setVisible(false);
        ui->btnPrior->setVisible(true);
    }
}

void frmEditProg_1::on_btnNext_clicked()
{
    modalita = 0;
    fase = 1;
    timer->start(500);
    ui->lbHelp->setText("2.3");
}

void frmEditProg_1::on_btnPrior_clicked()
{
    modalita = 1;
    fase = 1;
    timer->start(500);
    ui->lbHelp->setText("2.3");

}

void frmEditProg_1::on_buttonExit_3_clicked()
{
    result = -1;
    close();
}

void frmEditProg_1::on_buttonExit_clicked()
{
    result = 1;
    prog.direzione = modalita;
    prog.RetToHome = modalita;
    close();
}
