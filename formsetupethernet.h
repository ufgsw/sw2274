#ifndef FORMSETUPETHERNET_H
#define FORMSETUPETHERNET_H

#include <QDialog>
#include <QFile>
#include <QProcess>

#include "formtastietan.h"
#include "formtastieraip.h"
#include "formtastieraa.h"
#include "formelencossid.h"
#include "formmessagebox.h"

namespace Ui {
class FormSetupEthernet;
}

class FormSetupEthernet : public QDialog
{
    Q_OBJECT

public:
    explicit FormSetupEthernet(QWidget *parent = 0);
    ~FormSetupEthernet();

private slots:
    void ready_read_ping();

    void on_checkBox_toggled(bool checked);

    void on_buttonIfconfig_clicked();

    void on_buttonPing_clicked();

    void on_buttonExit_clicked();

    void on_buttonSave_clicked();

    void on_buttonIP_clicked();

    void on_buttonNM_clicked();

    void on_buttonNT_clicked();

    void on_buttonGW_clicked();

    void on_checkDHCP_toggled(bool checked);

    void on_buttonIpPing_clicked();

    void on_buttonRestart_clicked();

    void on_buttonIfConfig_clicked();

    void on_buttonPassword_clicked();

    void on_buttonSSID_clicked();


    void on_checkWIFI_clicked();

    void on_checkCABLE_clicked();

private:
    Ui::FormSetupEthernet *ui;

    FormTastieraN* uiTastieraN;
    FormTastieraIP* uiTastieraIP;
    FormTastieraA* uiTastieraA;
    FormElencoSSID* uiElencoSSID;
    FormMessageBox* uiMessageBox;

    QProcess *m_process;

    bool cambio_ssid;

    QString ip;
    QString netmask;

};

#endif // FORMSETUPETHERNET_H
