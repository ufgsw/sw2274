#include "model_maintenance.h"
#include <QFont>
#include <QBrush>

model_maintenance::model_maintenance(QObject *parent)
    : QAbstractTableModel(parent)
{
    mData = Maintenance::GetInstance();
}


int model_maintenance::rowCount(const QModelIndex &parent) const
{
    return mData->manutenzioni.count();
}

int model_maintenance::columnCount(const QModelIndex &parent) const
{
    return COLS_C;
}

QVariant model_maintenance::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return QString("ALERT");
        case 1:
            return QString("DONE");
        case 2:
            return QString("LIFESPAN");
        case 3:
            return QString("RESET");
        }
    }
    return QVariant();
}

QVariant model_maintenance::data(const QModelIndex &index, int role) const
{
    int nrows = mData->manutenzioni.count();

    int row = nrows - index.row() - 1;
    int col = index.column();

    maintenance_s manutenzione = mData->manutenzioni.at(row);

    // generate a log message when this method gets called
    //qDebug(QString("row %1, col%2, role %3").arg(row).arg(col).arg(role));

    switch (role) {
    case Qt::DisplayRole:

        if(      col == 0 ) return manutenzione.alert;
        else if( col == 1 ) return manutenzione.done;
        else if( col == 2 )
        {
            if( manutenzione.lifespan == 0) return "";
            else if( manutenzione.lifespan > 0 )
            {
                int oo = manutenzione.lifespan / 60;
                int mm = (manutenzione.lifespan - ( oo * 60));
                return QString( "%1:%2").arg(oo,2,10,QChar('0')).arg(mm,2,10,QChar('0'));
            }
        }
        else if( col == 3 ) return manutenzione.reset;


        //if (row == 0 && col == 1) return QString("Programma 1");
        //if (row == 1 && col == 1) return QString("Programma 2");
//        if(m_gridData[row][col] != NULL)
//            return m_gridData[row][col];
        //return QString("Row%1, Column%2").arg(row + 1).arg(col +1);
    case Qt::FontRole:
        if (row == -1)
        {
            QFont boldFont;
            boldFont.setBold(true);
            return boldFont;
        }
        break;

    case Qt::ForegroundRole:
        if( col == 2 && manutenzione.lifespan < 0 ) return QBrush(Qt::red);
        break;
    case Qt::BackgroundRole:
//        if (m_gridData[row][2] != "" && m_gridData[row][2].toInt() <=  m_gridData[row][3].toInt())  //change background
//            return QBrush(Qt::green);
//        else if (m_gridData[row][2] != "" && m_gridData[row][3].toInt() >  0)  //change background
//            return QBrush(Qt::yellow);
        break;
    case Qt::TextAlignmentRole:
       // if (row == 1 && col == 1) //change text alignment only for cell(1,1)
        return int(Qt::AlignHCenter | Qt::AlignVCenter);
        break;
    case Qt::CheckStateRole:
        //if (row == 1 && col == 0) //add a checkbox to cell(1,0)
        //    return Qt::Checked;
        break;
    }
    return QVariant();
}

void model_maintenance::loadData()
{
//    Maintenance* mm = Maintenance::GetInstance();

//    int row = 0;
//    foreach (maintenance_s manutenzione , mm->manutenzioni)
//    {
//        m_gridData[row][0] = manutenzione.alert;
//        m_gridData[row][1] = manutenzione.done;

//        int oo = manutenzione.lifespan / 60;
//        int mm = (manutenzione.lifespan - ( oo * 60));
//        m_gridData[row][2] = QString( "%1:%2").arg(oo,2,10,QChar('0')).arg(mm,2,10,QChar('0'));
//        m_gridData[row][3] = manutenzione.reset;

//        ++row;
//    }

}
