#include "formmanuale.h"
#include "ui_formmanuale.h"
#include "formtastietan.h"

formmanuale::formmanuale(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::formmanuale)
{
    ui->setupUi(this);
    pressed[0] = false;
    pressed[1] = false;
    pressed[2] = false;
    pressed[3] = false;
    pressed[4] = false;
    pressed[5] = false;
    pressed[6] = false;
    pressed[7] = false;
}

formmanuale::~formmanuale()
{
    delete ui;
}

void formmanuale::Init(QString n_pistole,QString modello)
{
    if(sabbiatrice->Tipo == ZEPHIR || n_pistole == "1"){
        ui->btn4->setVisible(false);
    }

    if(modello == "Mistral 120 ev+" || modello == "Zephir 120+"){
        ui->btn7->setVisible(false);
    }


}

void formmanuale::on_buttonExit_clicked()
{
    close();
}

void formmanuale::on_btnPistolaDw_clicked()
{
    sabbiatrice->Home_Pistola();
}

void formmanuale::on_btnPistolaUp_clicked()
{
    int32_t quota = GetNumber("Quota",0,10000,true);
    //ui->btnPistolaUp->setText( QString("%1").arg(quota));
    sabbiatrice->Move_Mot_Pos(MOT_Y,quota,200,1000);

}

void formmanuale::on_btnNastroInd_clicked()
{
    sabbiatrice->Move_Mot_Pos(MOT_X,0,200,1000);

}

void formmanuale::on_btnNastroAv_clicked()
{
//    int32_t quota = GetNumber("Quota",0,10000,true);
//    ui->btnPistolaUp->setText( QString("%1").arg(quota));
//    sabbiatrice->Move_Mot_Pos(MOT_X,quota,200,1000);

}


float formmanuale::GetNumber(QString title,u_int16_t min,u_int16_t max,bool asinteger)
{
    float num;

    FormTastieraN *uiTastieraN = new FormTastieraN(this);

    uiTastieraN->set_title( tr(title.toLatin1()) );
    uiTastieraN->set_max( max );
    uiTastieraN->set_min( min );
    uiTastieraN->set_default( 1 );
    uiTastieraN->set_integer( asinteger );

    uiTastieraN->exec();

    num = uiTastieraN->get_val();

    delete uiTastieraN;

    return num;

}


void formmanuale::on_btnNastroAv_pressed()
{



}

void formmanuale::on_btnNastroAv_released()
{


}

void formmanuale::on_btnNastroInd_pressed()
{
    if(!pressed[0]){
//        int32_t quota = GetNumber("Quota",0,10000,true);
//        ui->btnPistolaUp->setText( QString("%1").arg(quota));
        pressed[0] = true;
        int32_t quota;
        quota = sabbiatrice->Quota_mot2 - 1000;
        if(quota < 0 ) quota = 0;
        sabbiatrice->Move_Mot_Pos(MOT_X,quota,200,1000);
    }

}

void formmanuale::on_btnNastroInd_released()
{
    if(pressed[0]){
        sabbiatrice->Stop_Mot_Vel(MOT_X,0);
        pressed[0] = false;
    }

}



void formmanuale::on_btn1_clicked()
{
    //ui->btn1->setDown(true);
}

void formmanuale::on_buttonExit_3_clicked()
{
    close();
}

void formmanuale::on_btn1_pressed()
{
    ui->btn1->setIcon(QIcon(":/png/61-A.png"));
    if(!pressed[1]){
        pressed[1] = true;
        sabbiatrice->Ventola(true);
        ui->lbHelp->setText("6.1");
    }

}

void formmanuale::on_btn1_released()
{
    ui->btn1->setIcon(QIcon(":/png/61-B.png"));
    if(pressed[1]){
        pressed[1] = false;
        sabbiatrice->Ventola(false);
        ui->lbHelp->setText("6.0");
    }
}

void formmanuale::on_btn2_pressed()
{
    ui->btn2->setIcon(QIcon(":/png/62-A.png"));
    if(!pressed[2]){
        pressed[2] = true;
        sabbiatrice->Vibratore(true);
        ui->lbHelp->setText("6.2");
    }

}
void formmanuale::on_btn2_released()
{
    ui->btn2->setIcon(QIcon(":/png/62-B.png"));
    if(pressed[2]){
        pressed[2] = false;
        sabbiatrice->Vibratore(false);
        ui->lbHelp->setText("6.0");
    }

}
void formmanuale::on_btn3_pressed()
{
    ui->btn3->setIcon(QIcon(":/png/63-A.png"));
    if(!pressed[3]){
        pressed[3] = true;
        sabbiatrice->Pistola(1,true);
        sabbiatrice->Set_Pressione(6);
        ui->lbHelp->setText("6.3");
//        QThread::msleep(500);
    }

}
void formmanuale::on_btn3_released()
{
    ui->btn3->setIcon(QIcon(":/png/63-B.png"));
    if(pressed[3]){
        pressed[3] = false;
        sabbiatrice->Set_Pressione(0);
//        QThread::msleep(500);
        sabbiatrice->Pistola(1,false);
        ui->lbHelp->setText("6.0");
    }

}
void formmanuale::on_btn4_pressed()
{
    ui->btn4->setIcon(QIcon(":/png/64-A.png"));
    if(!pressed[4]){
        pressed[4] = true;
        sabbiatrice->Pistola(2,true);
        sabbiatrice->Set_Pressione(6);
        ui->lbHelp->setText("6.4");
    }

}
void formmanuale::on_btn4_released()
{
    ui->btn4->setIcon(QIcon(":/png/64-B.png"));
    if(pressed[4]){
        pressed[4] = false;
        sabbiatrice->Set_Pressione(0);
        sabbiatrice->Pistola(2,false);
        ui->lbHelp->setText("6.0");
    }

}
void formmanuale::on_btn5_pressed()
{
    ui->btn5->setIcon(QIcon(":/png/65-A.png"));
    if(!pressed[5]){
        pressed[5] = true;
        sabbiatrice->Manichetta(1,true);
        ui->lbHelp->setText("6.5");
    }

}
void formmanuale::on_btn5_released()
{
    ui->btn5->setIcon(QIcon(":/png/65-B.png"));
    if(pressed[5]){
        sabbiatrice->Manichetta(1,false);
        pressed[5] = false;
        ui->lbHelp->setText("6.0");
    }

}
void formmanuale::on_btn6_pressed()
{
    ui->btn6->setIcon(QIcon(":/png/66-A.png"));
    if(!pressed[6]){
        pressed[6] = true;
        sabbiatrice->Manichetta(2,true);
        ui->lbHelp->setText("6.6");
    }

}
void formmanuale::on_btn6_released()
{
    ui->btn6->setIcon(QIcon(":/png/66-B.png"));
    if(pressed[6]){
        sabbiatrice->Manichetta(2,false);
        pressed[6] = false;
        ui->lbHelp->setText("6.0");
    }

}
void formmanuale::on_btn7_pressed()
{
    ui->btn7->setIcon(QIcon(":/png/67-A.png"));
    if(!pressed[7]){
        pressed[7] = true;
        sabbiatrice->Manichetta(3,true);
        ui->lbHelp->setText("6.7");
    }

}
void formmanuale::on_btn7_released()
{
    ui->btn7->setIcon(QIcon(":/png/67-B.png"));
    if(pressed[7]){
        sabbiatrice->Manichetta(3,false);
        pressed[7] = false;
        ui->lbHelp->setText("6.0");
    }

}

